function change_main_block_height() {
    // height between content's bottom & main-block's bottom
    var temp_height = 200;

    if (($(window).width() <= 991) && ($(window).width() > 650))
        temp_height += 50;
    else
        if ($(window).width() <= 650) temp_height += 100;

    var main_block_height = $('.main-block').height();
    var content_height = $('.main-block .content').height();

    main_block_height = content_height + temp_height;

    $('.main-block').css('height', main_block_height);

    change_count_on_cart_icon();
}

function change_count_on_cart_icon() {
    var cart_items_count = $('.content .product').length;
    if (cart_items_count > 0) {
        $('#cart-items-count').show();
        $('#cart-items-count').html(cart_items_count);
    }
    else {
        $('#cart-items-count').hide();
    }
}

$('body').on('click', '.scratbox-header', function(){
    $(this).parent().find('.scratbox-options').toggle();
    change_main_block_height();
});

$(window).resize(function(){
    change_main_block_height();
});

$(document).ready(function(){
    change_main_block_height();
});

function clearCartPage() {
    if (confirm('Вы действительно хотите очистить корзину?')) {
        $.ajax({
            url: '/cart/clear-cart-page',
            type: 'GET',
            success: function (res) {
                if (!res) alert('Ошибка очистки корзины!');
                else {
                    $('.main-block').html(res);
                    change_main_block_height();
                }
            },
            error: function () {
                alert('Ошибка очистки корзины!');
            }
        });
    }
}

function deleteProductCartPage(elem) {
    var id = elem.getAttribute('data-id'),
        scratbox = elem.getAttribute('data-scratbox');
    if (confirm('Вы действительно хотите удалить этот товар из корзины?')) {
        $.ajax({
            url: '/cart/del-product-cart-page',
            data: {id: id, scratbox: scratbox},
            type: 'GET',
            success: function (res) {
                if (!res) alert('Ошибка удаления товара!');
                else {
                    $('.main-block').html(res);
                    change_main_block_height();
                }
            },
            error: function () {
                alert('Ошибка удаления товара!');
            }
        });
    }
}

function decreaseProductWeightCartPage(elem) {
    var id = elem.getAttribute('data-id'),
        scratbox = elem.getAttribute('data-scratbox');
    $.ajax({
        url: '/cart/decrease-product-weight-cart-page',
        data: {id: id, scratbox: scratbox},
        type: 'GET',
        success: function (res) {
            if (!res) alert('Ошибка уменьшения массы товара!');
            else {
                $('.main-block').html(res);
                change_main_block_height();
            }
        },
        error: function () {
            alert('Ошибка уменьшения массы товара!');
        }
    });
}

function increaseProductWeightCartPage(elem) {
    var id = elem.getAttribute('data-id'),
        scratbox = elem.getAttribute('data-scratbox');
    $.ajax({
        url: '/cart/increase-product-weight-cart-page',
        data: {id: id, scratbox: scratbox},
        type: 'GET',
        success: function (res) {
            if (!res) alert('Ошибка увеличения массы товара!');
            else {
                $('.main-block').html(res);
                change_main_block_height();
            }
        },
        error: function () {
            alert('Ошибка увеличения массы товара!');
        }
    });
}
