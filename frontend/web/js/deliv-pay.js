// https://maps-creator.com/ - конструктор карт (для границ объектов)

// карта
var myMap;

function showShopListFromCity(myMap, cityId) {
    for (var i = 0; i < shopList[cityId].shops.length; i++) {
        myPlacemark =  new ymaps.Placemark(
            shopList[cityId].shops[i].coordinates,
            {
                balloonWorkTime: shopList[cityId].shops[i].work_time,
                balloonHeader: shopList[cityId].shops[i].name,
                balloonContent: shopList[cityId].shops[i].city_name + ", " + shopList[cityId].shops[i].address,
                balloonTelephone: shopList[cityId].shops[i].telephone
            },
            {
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                // Своё изображение иконки метки.
                iconImageHref: '/img/icons/map-point.svg',
                // Размеры метки.
                iconImageSize: [30, 30],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                // iconImageOffset: [-5, -38]

                balloonShadow: false,
                balloonLayout: MyBalloonLayout,
                balloonContentLayout: MyBalloonContentLayout,
                balloonPanelMaxMapArea: 0,
                // Не скрываем иконку при открытом балуне.
                hideIconOnBalloonOpen: false,
                // И дополнительно смещаем балун, для открытия над иконкой.
                balloonOffset: [-110, -220]
            }
        );
        myMap.geoObjects.add(myPlacemark);
        //myPlacemark.balloon.open();
    }
}

ymaps.ready(function () {
    // Создание экземпляра карты и его привязка к созданному контейнеру.
    myMap = new ymaps.Map('map', {
        center: [53.932, 27.573856],
        zoom: 10,
        behaviors: ['default', 'scrollZoom'],
        controls:[]
    }, {
        searchControlProvider: 'yandex#search'
    });
    // Создание макета балуна на основе Twitter Bootstrap.
    MyBalloonLayout = ymaps.templateLayoutFactory.createClass(
        '<div class="my-popover">' +
        '<div class="popover-inner">' +
        '$[[options.contentLayout observeSize minWidth=235 maxWidth=235 maxHeight=350]]' +
        '</div>' +
        '</div>', {
            /**
             * Строит экземпляр макета на основе шаблона и добавляет его в родительский HTML-элемент.
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/layout.templateBased.Base.xml#build
             * @function
             * @name build
             */
            build: function () {
                this.constructor.superclass.build.call(this);

                this._$element = $('.my-popover', this.getParentElement());

                this.applyElementOffset();

                this._$element.find('.popover-close')
                    .on('click', $.proxy(this.onCloseClick, this));
            },

            /**
             * Удаляет содержимое макета из DOM.
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/layout.templateBased.Base.xml#clear
             * @function
             * @name clear
             */
            clear: function () {
                this._$element.find('.popover-close')
                    .off('click');

                this.constructor.superclass.clear.call(this);
            },

            /**
             * Метод будет вызван системой шаблонов АПИ при изменении размеров вложенного макета.
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
             * @function
             * @name onSublayoutSizeChange
             */
            onSublayoutSizeChange: function () {
                MyBalloonLayout.superclass.onSublayoutSizeChange.apply(this, arguments);

                if(!this._isElement(this._$element)) {
                    return;
                }

                this.applyElementOffset();

                this.events.fire('shapechange');
            },

            /**
             * Сдвигаем балун, чтобы "хвостик" указывал на точку привязки.
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
             * @function
             * @name applyElementOffset
             */
            applyElementOffset: function () {
                this._$element.css({
                    left: -(this._$element[0].offsetWidth / 2),
                    top: -(this._$element[0].offsetHeight + this._$element.find('.popover-arrow')[0].offsetHeight)
                });
            },

            /**
             * Закрывает балун при клике на крестик, кидая событие "userclose" на макете.
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
             * @function
             * @name onCloseClick
             */
            onCloseClick: function (e) {
                e.preventDefault();

                this.events.fire('userclose');
            },

            /**
             * Используется для автопозиционирования (balloonAutoPan).
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/ILayout.xml#getClientBounds
             * @function
             * @name getClientBounds
             * @returns {Number[][]} Координаты левого верхнего и правого нижнего углов шаблона относительно точки привязки.
             */
            getShape: function () {
                if(!this._isElement(this._$element)) {
                    return MyBalloonLayout.superclass.getShape.call(this);
                }

                var position = this._$element.position();

                return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
                    [position.left, position.top], [
                        position.left + this._$element[0].offsetWidth,
                        position.top + this._$element[0].offsetHeight + this._$element.find('.popover-arrow')[0].offsetHeight
                    ]
                ]));
            },

            /**
             * Проверяем наличие элемента (в ИЕ и Опере его еще может не быть).
             * @function
             * @private
             * @name _isElement
             * @param {jQuery} [element] Элемент.
             * @returns {Boolean} Флаг наличия.
             */
            _isElement: function (element) {
                return element && element[0] && element.find('.popover-arrow')[0];
            }
        });

        // Создание вложенного макета содержимого балуна.
    MyBalloonContentLayout = ymaps.templateLayoutFactory.createClass(

        '<div class="popover-head">' +
        '<div class="popover-content">' +
        '<div class="popover-cancel">' +
        '<a class="popover-close" href="#"><img src="/img/icons/cancel.svg" ></a>' +
        '</div>' +
        '<div class="popover-worktime">' +
        '$[properties.balloonWorkTime]' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="popover-body">' +
        '<div class="popover-content">' +
        '<div class="shop-name">' +
        '$[properties.balloonHeader]' +
        '</div>' +
        '</div>' +
        '<div class="popover-content">$[properties.balloonContent]</div>'+
        '<div class="popover-content">$[properties.balloonTelephone]</div>'+
        '</div>'+
        '<div class="popover-arrow"></div>'
    );
    select_courier();
});

function select_courier() {
    if (myMap) {
        myMap.geoObjects.removeAll();
    }
    myMap.geoObjects.add(new ymaps.GeoObject({
            geometry: {
                type: "Polygon",
                coordinates: [[[53.97127371235324,27.588769435882558],[53.97005923376096,27.598811626434333],[53.943939342394835,27.67056608200074],[53.937862592743556,27.673999309539795],[53.88069766806727,27.697688579559326],[53.87411924838299,27.693152607079217],[53.84022383786632,27.662253559227654],[53.835350374057,27.651610553856568],[53.833725759492054,27.640624225731557],[53.832913428466064,27.57127302944251],[53.833116512706546,27.56200331508704],[53.84225427961388,27.46861952602454],[53.84672090325684,27.443556964989387],[53.85321695573985,27.43257063686438],[53.895619392165585,27.409911335106553],[53.90393250619916,27.406478107567498],[53.92440406469372,27.416777790184693],[53.94954892140868,27.443256545927223],[53.96048335228199,27.45853440847607],[53.96412752242379,27.464542556669425],[53.96716408639065,27.48050706472606],[53.97127371235324,27.588769435882558]]],
                hideIconOnBalloonOpen: false
            },
            properties: {
                balloonContent:decodeURIComponent("%D0%A2%D0%BE%D1%87%D0%BA%D0%B0"),
                iconCaption:decodeURIComponent("%D0%9C%D0%BD%D0%BE%D0%B3%D0%BE%D1%83%D0%B3%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA1"),
                hintCaption:decodeURIComponent("%D0%9C%D0%BD%D0%BE%D0%B3%D0%BE%D1%83%D0%B3%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA1"),
            }
        }, {
            fillColor: "#1e98ff",
            strokeColor: "#1e98ff",
            fillOpacity: 0.35,
            strokeOpacity: 0.8,
            strokeWidth: 3
        })
    );
    myMap.setBounds(myMap.geoObjects.getBounds());
}

function select_post() {
    if (myMap) {
        myMap.geoObjects.removeAll();
    }
    myMap.geoObjects.add(new ymaps.GeoObject({
            geometry: {
                type: "Polygon",
                coordinates: [[[51.58497737840719,23.653219518974527],[51.7898359051551,23.504904089287017],[52.088611501714695,23.702657995537024],[52.27770119774604,23.08742362053701],[52.65345996375403,23.658712683037017],[52.78687911915488,23.94435721428701],[53.15827408625283,23.878439245537017],[53.95631945522035,23.52687674553703],[53.94335851327662,24.16408377678701],[53.95631945522035,24.471700964287013],[54.0727859160681,24.823263464287027],[54.27902951402199,25.416525183037024],[54.18892506452061,25.680197058037027],[54.35610468648937,25.614279089287006],[54.815516070516054,25.834005651787006],[54.99277302624112,26.009786901787024],[55.13149699171252,26.33937674553702],[55.30735709934163,26.800802526787006],[55.39499466016951,26.47121268303701],[55.68157715801615,26.625021276787013],[55.879703691022875,27.152365026787017],[55.80552478226911,27.635763464287017],[56.15047873295331,28.075216589287013],[56.1013886342716,28.580587683037027],[55.978387938876786,28.734396276787006],[55.96606620749552,29.437521276787027],[55.73120365757612,29.459493933037017],[55.879703691022875,30.030782995537027],[55.80552478226911,30.470236120537024],[55.54477766647389,30.887716589287027],[55.28228200509073,30.77785330803703],[55.06850050728678,31.041525183037006],[54.802824851550525,30.77785330803703],[54.62472701173601,31.19533377678701],[54.47144565915178,31.129415808037017],[54.15024835511225,31.480978308037027],[54.0727859160681,31.854513464287027],[53.839525231605485,31.810568151787024],[53.70936891626708,32.42580252678701],[53.53955894194526,32.57961112053702],[53.3559181877044,32.82131033928703],[53.07895934013758,32.20607596428702],[53.15827408625283,31.744650183037027],[53.15827408625283,31.371115026787027],[52.97297749616517,31.30519705803701],[52.720220808694705,31.590841589287027],[52.43913542962802,31.56886893303701],[52.115673800548315,31.78859549553701],[52.020883531215816,31.107443151787027],[51.84430754199438,30.71193533928701],[51.53019099005635,30.536154089287017],[51.24147311508237,30.514181433037027],[51.543893806609795,30.052755651787017],[51.43415512762154,29.74513846428701],[51.39293465293083,29.34963065178702],[51.68069398785783,29.12990408928702],[51.44788698420124,28.734396276787006],[51.62602364366841,28.38283377678702],[51.639697444034425,28.075216589287013],[51.55759247652775,27.78957205803702],[51.61234569960257,27.372091589287013],[51.7898359051551,27.19631033928702],[51.83069583764315,26.668966589287017],[51.9123040378494,26.009786901787024],[51.93947370022035,25.416525183037024],[51.92589093538238,24.845236120537017],[51.88511784266581,24.317892370537013],[51.72506484634324,24.323385534599552],[51.62602364366841,24.081686315849566],[51.58839932523248,23.966329870537052],[51.58497737840719,23.653219518974527]]],
                hideIconOnBalloonOpen: false
            },
            properties: {
                balloonContent:decodeURIComponent("%D0%A2%D0%BE%D1%87%D0%BA%D0%B0"),
                iconCaption:decodeURIComponent("%D0%9C%D0%BD%D0%BE%D0%B3%D0%BE%D1%83%D0%B3%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA1"),
                hintCaption:decodeURIComponent("%D0%9C%D0%BD%D0%BE%D0%B3%D0%BE%D1%83%D0%B3%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA1"),
            }
        }, {
            fillColor: "#1e98ff",
            strokeColor: "#1e98ff",
            fillOpacity: 0.35,
            strokeOpacity: 0.8,
            strokeWidth: 3
        })
    );
    myMap.setBounds(myMap.geoObjects.getBounds());
}

function select_pickup() {
    if (myMap) {
        myMap.geoObjects.removeAll();
    }
    // идём по массиву городов и добавляем их на карту
    shopList.forEach(function(item, i) {
        showShopListFromCity(myMap, i);
    });
    // Спозиционируем карту так, чтобы на ней были видны все объекты.
    myMap.setBounds(myMap.geoObjects.getBounds());
}


$("input[type=radio]").change(function(){
    switch($(this).val()) {
        case 'courier':
            select_courier();
            break;
        case 'post':
            select_post();
            break;
        case 'pickup':
        default:
            select_pickup();
            break;
    }
});