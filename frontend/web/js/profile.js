﻿$('#ur-person').on('click', function () {
	$('.ur-person-inputs').css('display', 'block');
	change_main_block_height();	
	$('.client-type-button span').text('Юридическое лицо');
	$('#user-client_type').val(1);
});

$('#fiz-person').on('click', function () {
	$('.ur-person-inputs').css('display', 'none');
	change_main_block_height();
	$('.client-type-button span').text('Физическое лицо');
    $('#user-client_type').val(0);
});

$('.scratbox-header').on('click', function () {
    $(this).parent().find('.scratbox-options').toggle();
    change_main_block_height();
});

$('.order-header').on('click', function () {
    $(this).parent().find('.order-inf').toggle();
    change_main_block_height();
});

function change_main_block_height() {
	// height between content's bottom & main-block's bottom
	var temp_height = 150;

    if (($(window).width() <= 991) && ($(window).width() > 650))
        temp_height += 50;
    else
    if ($(window).width() <= 650) temp_height += 100;
	
	var main_block_height = $('.main-block').height();
	var content_height = $('.main-block .content').height();

	main_block_height = content_height + temp_height;
	
	$('.main-block').css('height', main_block_height);
}

$(window).resize(function(){
	change_main_block_height();
});

$(document).ready(function(){
	change_main_block_height();
	if ($('#user-client_type').val() === '1') {
        $('#ur-person').click();
	}
	else {
        $('#fiz-person').click();
	}

	$('#edit_private_inf').click(function () {
        $('.information').css('display', 'none');
        $('#profile-form').css('display', 'block');
        if ($('#user-birthday-val').val() != '') {
            var date = new Date($('#user-birthday-val').val());
            $('#birth_day').val(date.getDate());
            var month = date.getMonth() + 1;
            if (month < 10) month = '0' + month;
            $('#birth_month').val(month);
            $('#birth_year').val(date.getFullYear());
        }
    });
});

function translate_date(date) {
    if (!date) {
        date = new Date();
    }
    else {
        date = new Date(date);
    }

    var months = ['января' , 'февраля' , 'марта' , 'апреля' , 'мая' , 'июня' , 'июля' , 'августа' , 'сентября' , 'октября' , 'ноября' , 'декабря' ];

    return date.getDate() + ' ' + months[date.getMonth()] + ' ' + date.getFullYear() + ' г.';
}
