$(document).ready(function () {
    $('.shop').on('click', function() {
        $('.shop').each(function() {
            $(this).removeClass('selected');
            $(this).addClass('not-selected');
        });
        $(this).removeClass('not-selected');
        $(this).addClass('selected');

        var shop_balloon = myMap.geoObjects.get(parseInt($(this).attr('id')) - 1);
        shop_balloon.balloon.open();
    });

    $('.shop-choose').on('click', function () {
        if ($('.selected span').html()) {
            $('#order-address').val($('.selected span').html());
            $('.mfp-close').click();
        }
        else {
            alert('Вы не выбрали магазин!');
        }
    });

});

// date comparison
Date.prototype.sameDay = function(d) {
    return this.getFullYear() === d.getFullYear()
        && this.getDate() === d.getDate()
        && this.getMonth() === d.getMonth();
};

$('#order-delivery_date').change(function() {
    disable_datetime($(this).val());
    $('#delivery-date').text(translate_date($(this).val()));
});

// delivery time is from 11 to 22. if the order is being checked out before 9 or after 20, then the closest time to deliver
// in select shows 11, else - current time + 4 hours
function disable_datetime(date) {
    var cur_date = new Date();
    var sel_date = new Date(date);

    $('#delivery-time').removeClass('disabled');
    $('li.ui-timepicker').removeClass('disabled');
    $('#delivery-time').html('Время<img src="/img/icons/arrow-down.svg">');
    $('#delivery_time').val('');        // hidden input for form

    if (sel_date.sameDay(cur_date)) {
        // if client can make an order with a delivery for today
        if (cur_date.getHours() < 21) {
            $('#delivery-time').removeClass('disabled');
        }
        // for each list's with time item
        for (var i = 11; i < 22; i++) {
            if (i < (parseInt(cur_date.getHours()) + 4)) {
                var temp_str = 'li.ui-timepicker[data-id=' + i + ']';
                var elem = $(temp_str);
                elem.addClass('disabled');
            }
        }
    }

    // disable_delivery_time(date, '2020-01-07', 11, 22);
}

function disable_delivery_time(choosen_date, disable_date, from, to) {
    if (choosen_date === disable_date) {
        for (i = from; i < to; i++) {
            temp_str = 'li.ui-timepicker[data-id=' + i + ']';
            elem = $(temp_str);
            elem.addClass('disabled');
        }
    }
}

function translate_date(date) {
    if (!date) {
        date = new Date();
    }
    else {
        date = new Date(date);
    }

    var months = ['января' , 'февраля' , 'марта' , 'апреля' , 'мая' , 'июня' , 'июля' , 'августа' , 'сентября' , 'октября' , 'ноября' , 'декабря' ];
    var days_of_week = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];

    return days_of_week[date.getDay()] + ', ' + date.getDate() + ' ' + months[date.getMonth()];
}

$('#delivery-time').on('click', function() {
    $('.ui-timepicker-wrapper').show();
});


$('li.ui-timepicker').click(function() {
    $('#delivery-time').text($(this).text());
    $('.ui-timepicker-wrapper').hide();

    $('#delivery_time').val($(this).text());        // hidden input for form
});

$('li.shop').click(function() {
    $('.ui-shopspicker-wrapper').hide();

    $('#order-address').val($(this).text());
});


$('.delivery-type-button').click(function() {
	$('.delivery-type-button').each(function() {
		$(this).removeClass("delivery-type-button-selected");
		$(this).addClass("delivery-type-button-not-selected");
	});
	$(this).removeClass("delivery-type-button-not-selected");
	$(this).addClass("delivery-type-button-selected");

    $('#delivery_type').val($(this).attr('data-id'));

    if ($(this).attr('data-id') == 2) {
        $(".delivery-time-buttons").addClass("disabled");

        $('#delivery-date').html('Дата<img src="/img/icons/arrow-down.svg">');
        $('#delivery-time').html('Время<img src="/img/icons/arrow-down.svg">');

        $('#order-delivery_date').val('');
        $('#delivery_time').val('');

        $('.order_index').show();
    }
    else {
        $(".delivery-time-buttons").removeClass("disabled");
        $('.order_index').hide();
    }

    if ($(this).attr('data-id') == 3) {
        $('#shops-popup-modal-click').click();
        $("#order-address").addClass("disabled");
    }
    else {
        $("#order-address").removeClass("disabled");
        $("#order-address").val($("#log_in_user_address").val());
    }

    var delivery_type = $(this).attr('data-id');
    $.ajax({
        url: '/cart/change-delivery-type',
        data: {delivery_type: delivery_type},
        type: 'GET',
        success: function (res) {
            if (!res) alert('Ошибка изменения типа доставки!');
            else if (res === 'MIN_SUM_FOR_PROMO_ERROR') alert('Не возможно изменить способ доставки, т.к. вы использовали промокод. Чтобы изменить способ доставки, сначала отмените действие промокода.');
            else {
                $('#cart-checkout').html(res);
                change_main_block_height();
            }
        },
        error: function () {
            alert('Ошибка изменения типа доставки!');
        }
    });

});

$('.payment-type-button').click(function() {
    $(".payment-type-button").each(function() {
        $(this).removeClass("payment-type-button-selected");
        $(this).addClass("payment-type-button-not-selected");
    });
    $(this).removeClass("payment-type-button-not-selected");
    $(this).addClass("payment-type-button-selected");

    $('#id_pay_type').val($(this).attr('data-id'));     // hidden input for form
});

function change_main_block_height() {
    // height between content's bottom & main-block's bottom
    var temp_height = 200;

    if (($(window).width() <= 991) && ($(window).width() > 650))
        temp_height += 50;
    else
        if ($(window).width() <= 650) temp_height += 100;

    var main_block_height = $('.main-block').height();
    var content_height = $('.main-block .content').height();
    var content_cart_height = $('.main-block .content-cart').height();
    main_block_height = ((content_height > content_cart_height) ? content_height : content_cart_height) + temp_height;

    if ($(window).width() <= 1200) {
        main_block_height = content_height + content_cart_height + temp_height;
    }

    $('.main-block').each(function () {
        $(this).css('height', main_block_height);
    });
}

$(window).resize(function(){
    change_main_block_height();
});

$(document).ready(function() {
    change_main_block_height();
    $(".payment-type-buttons div:last-child").click();
    $(".delivery-type .delivery-type-buttons div:first-child").click();

    $(document).bind( "mouseup touchend", function(e){
        hide_div(e, $('.ui-timepicker-wrapper'));
        hide_div(e, $('.ui-shopspicker-wrapper'));
    });
    //$('#shops-popup-modal').addClass("mfp-hide");
});

function hide_div(e, div) {
    if (!div.is(e.target)
        && div.has(e.target).length === 0) {
        div.hide();
    }
}

function check_date_and_time() {
    var date = $('#order-delivery_date').val();
    var time = $('#delivery_time').val();
    if (date === '' || time === '') return false;

    return true;
}

function check_form() {
    if (($('#delivery_type').val() != 2) && !check_date_and_time()) {
        alert('Вы выбрали дату или время заказа, в рамках которых невозможно его осуществить. Пожалуйста, выберите другие дату и время.');
    }
    else
        if (($('#delivery_type').val() == 2) && $('#order-index').val() == '') {
            alert('Введите почтовый индекс.');
        }
        else {
            if ($('#id_pay_type').val() == '') {
                alert('Выберите тип оплаты.');
            }
            else {
                // $('#order-delivery_date').val(new Date($('#order-delivery_date').val()).getTime() / 1000);      //convert delivery date to int
                if ($('#delivery_type').val() == 2) {
                    $('#order-address').val($('#order-index').val() + ', ' + $('#order-address').val());
                }
                $('#checkout-main-form').submit();
            }
        }
}

//----------------------------------------------------------------------------------------------------------------

function clearCheckout() {
    if (confirm('Вы действительно хотите очистить корзину?')) {
        $.ajax({
            url: '/cart/clear-checkout',
            type: 'GET',
            success: function (res) {
                if (!res) alert('Ошибка очистки корзины!');
                else {
                    $('#cart-checkout').html(res);
                    $('#cart-header').load('/cart #cart-header');
                    change_main_block_height();
                }
            },
            error: function () {
                alert('Ошибка очистки корзины!');
            }
        });
    }
}

function deleteProductCheckout(elem) {
    var id = elem.getAttribute('data-id'),
        scratbox = elem.getAttribute('data-scratbox'),
        delivery_type = $('.delivery-type-button-selected') ? $('.delivery-type-button-selected').attr('data-id') : "1";
    if (confirm('Вы точно хотите удалить этот товар?')) {
        $.ajax({
            url: '/cart/del-product-checkout',
            data: {id: id, scratbox: scratbox, delivery_type: delivery_type},
            type: 'GET',
            success: function (res) {
                if (!res) alert('Ошибка удаления товара!');
                else if (res === 'MIN_SUM_FOR_PROMO_ERROR') alert('Не возможно удалить товар, т.к. вы использовали промокод. Чтобы удалить товар, сначала отмените действие промокода.');
                else {
                    $('#cart-checkout').html(res);
                    $('#cart-header').load('/cart #cart-header');
                    change_main_block_height();
                }
            },
            error: function () {
                alert('Ошибка удаления товара!');
            }
        });
    }
}

function decreaseProductWeightCheckout(elem) {
    var id = elem.getAttribute('data-id'),
        scratbox = elem.getAttribute('data-scratbox'),
        delivery_type = $('.delivery-type-button-selected') ? $('.delivery-type-button-selected').attr('data-id') : "1";
    $.ajax({
        url: '/cart/decrease-product-weight-checkout',
        data: {id: id, scratbox: scratbox, delivery_type: delivery_type},
        type: 'GET',
        success: function (res) {
            if (!res) alert('Ошибка уменьшения массы товара!');
            else if (res === 'MIN_SUM_FOR_PROMO_ERROR') alert('Не возможно уменьшить количество товара, т.к. вы использовали промокод. Чтобы уменьшить количество товара, сначала отмените действие промокода.');
            else {
                $('#cart-checkout').html(res);
                change_main_block_height();
            }
        },
        error: function () {
            alert('Ошибка уменьшения массы товара!');
        }
    });
}

function increaseProductWeightCheckout(elem) {
    var id = elem.getAttribute('data-id'),
        scratbox = elem.getAttribute('data-scratbox'),
        delivery_type = $('.delivery-type-button-selected') ? $('.delivery-type-button-selected').attr('data-id') : "1";
    $.ajax({
        url: '/cart/increase-product-weight-checkout',
        data: {id: id, scratbox: scratbox, delivery_type: delivery_type},
        type: 'GET',
        success: function (res) {
            if (!res) alert('Ошибка увеличения массы товара!');
            else {
                $('#cart-checkout').html(res);
                change_main_block_height();
            }
        },
        error: function () {
            alert('Ошибка увеличения массы товара!');
        }
    });
}

function decreaseBonusPointsCheckout() {
    $.ajax({
        url: '/cart/decrease-bonus-points-checkout',
        type: 'POST',
        success: function (res) {
            if (!res) alert('Ошибка уменьшения бонусных баллов!');
            else {
                $('#cart-checkout').html(res);
                change_main_block_height();
            }
        },
        error: function () {
            alert('Ошибка уменьшения бонусных баллов!');
        }
    });
}

function increaseBonusPointsCheckout() {
    $.ajax({
        url: '/cart/increase-bonus-points-checkout',
        type: 'POST',
        success: function (res) {
            if (!res) alert('Ошибка увеличения бонусных баллов!');
            else {
                $('#cart-checkout').html(res);
                change_main_block_height();
            }
        },
        error: function () {
            alert('Ошибка увеличения бонусных баллов!');
        }
    });
}

function checkBonusPoints() {
    var points = $('#points-count').val();
    $.ajax({
        url: '/cart/change-bonus-points-checkout',
        type: 'GET',
        data: { points: points },
        success: function (res) {
            if (!res) alert('Ошибка изменения бонусных баллов!');
            else {
                $('#cart-checkout').html(res);
                change_main_block_height();
            }
        },
        error: function () {
            alert('Ошибка изменения бонусных баллов!');
        }
    });

    if (points > 0) {
        $(".payment-type-button").each(function() {
            if (($(this).attr('data-id') == 1) || ($(this).attr('data-id') == 2)) {
                $(this).removeClass("payment-type-button-selected");
                $(this).addClass("payment-type-button-not-selected");
                $(this).addClass("disabled");
                $('#id_pay_type').val('');
            }
        });
    }
    else {
        $(".payment-type-button").each(function() {
            if (($(this).attr('data-id') == 1) || ($(this).attr('data-id') == 2)) {
                $(this).removeClass("disabled");
            }
        });
    }
}

$(document).ready(function() {
    $('body').on('keyup', '.points-count', function(){
        if (/[^0-9\.]/g.test(this.value)) {
            // Filter non-digits from input value.
            this.value = this.value.replace(/[^0-9\.]/g, '');
        }
    });
    $('body').on('change', '.points-count', function(){
        if (!$.isNumeric(this.value)) {
            if (this.value !== '') alert('Неверный формат числа!');
            this.value = '0.00';
        }

        checkBonusPoints();
    });
});

function applyPromo() {
    var promo_name = $('#promo-input').val();
    $.ajax({
        url: '/cart/apply-promo',
        data: { promo_name: promo_name },
        type: 'GET',
        success: function (res) {
            if (res === 'MIN_SUM_FOR_PROMO_ERROR') alert('Минимальная сумма заказа для применения промокода (без скрат-боксов) - ' + getMinSumForPromo());
            else if (res === 'WRONG_PROMO_ERROR') alert('Промокод уже не действителен либо введён неверно');
            else {
                $('#cart-checkout').html(res);
                change_main_block_height();
            }
        },
        error: function () {
            alert('Не удалось примененить промокод (возможно, он введён неверно или уже не действителен).');
        }
    });
}

function rollbackPromo() {
    if (confirm('Вы точно хотите отменить промокод?')) {
        $.ajax({
            url: '/cart/rollback-promo',
            type: 'POST',
            success: function (res) {
                if (!res) alert('Не удалось отменить промокод.');
                else {
                    $('#cart-checkout').html(res);
                    change_main_block_height();
                }
            },
            error: function () {
                alert('Не удалось отменить промокод.');
            }
        });
    }
}

function getMinSumForPromo() {
    var min_sum = 0;
    $.ajax({
        async: false,
        url: '/cart/get-min-sum-for-promo',
        type: 'POST',
        success: function (res) {
            min_sum = res;
        },
        error: function () {
            alert('Ошибка!');
        }
    });

    return min_sum;
}
