$(document).ready(function(){
    $('.main-block .content .inf:first').css('display', 'block');


    $('.main-block .content .prev').on('click', function () {
        var shop =  $('.main-block .content .inf').filter(function () {
            return $(this).css('display') === 'block'
        });
        if (shop.prev('.inf').length) {
            shop.css('display', 'none');
            shop.prev('.inf').css('display', 'block');

            var shop_balloon = myMap.geoObjects.get(parseInt(shop.attr('id')) - 1);
            shop_balloon.balloon.open();
        }
        else {
            shop.css('display', 'none');
            shop =  $('.main-block .content .inf:last');
            shop.css('display', 'block');

            var shop_last_balloon = myMap.geoObjects.get(parseInt(shop.attr('id')));
            shop_last_balloon.balloon.open();
        }
    });

    $('.main-block .content .next').on('click', function () {
        var shop =  $('.main-block .content .inf').filter(function () {
            return $(this).css('display') === 'block'
        });
        if (shop.next('.inf').length) {
            shop.css('display', 'none');
            shop.next('.inf').css('display', 'block');

            var shop_balloon = myMap.geoObjects.get(parseInt(shop.attr('id')) + 1);
            shop_balloon.balloon.open();
        }
        else {
            shop.css('display', 'none');
            shop =  $('.main-block .content .inf:first');
            shop.css('display', 'block');

            var shop_first_balloon = myMap.geoObjects.get(parseInt(shop.attr('id')));
            shop_first_balloon.balloon.open();
        }
    });
});

function showShopListFromCity(myMap, cityId) {
    for (var i = 0; i < shopList[cityId].shops.length; i++) {
        myPlacemark =  new ymaps.Placemark(
            shopList[cityId].shops[i].coordinates,
            {
                balloonWorkTime: shopList[cityId].shops[i].work_time,
                balloonHeader: shopList[cityId].shops[i].name,
                balloonContent: shopList[cityId].shops[i].city_name + ", " + shopList[cityId].shops[i].address,
                balloonTelephone: shopList[cityId].shops[i].telephone
            },
            {
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                // Своё изображение иконки метки.
                iconImageHref: '/img/icons/map-point.svg',
                // Размеры метки.
                iconImageSize: [30, 30],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                // iconImageOffset: [-5, -38]

                balloonShadow: false,
                balloonLayout: MyBalloonLayout,
                balloonContentLayout: MyBalloonContentLayout,
                balloonPanelMaxMapArea: 0,
                // Не скрываем иконку при открытом балуне.
                hideIconOnBalloonOpen: false,
                // И дополнительно смещаем балун, для открытия над иконкой.
                balloonOffset: [-110, -220]
            }
        );
        myMap.geoObjects.add(myPlacemark);
        // myPlacemark.balloon.open();
    }
}

ymaps.ready(function () {
    // Создание экземпляра карты и его привязка к созданному контейнеру.
    myMap = new ymaps.Map('map', {
        center: [53.932, 27.573856],
        zoom: 12,
        behaviors: ['default', 'scrollZoom'],
        controls:[]
    }, {
        searchControlProvider: 'yandex#search'
    });

    // Создание макета балуна на основе Twitter Bootstrap.
    MyBalloonLayout = ymaps.templateLayoutFactory.createClass(
        '<div class="my-popover">' +
        '<div class="popover-inner">' +
        '$[[options.contentLayout observeSize minWidth=235 maxWidth=235 maxHeight=350]]' +
        '</div>' +
        '</div>', {
            /**
             * Строит экземпляр макета на основе шаблона и добавляет его в родительский HTML-элемент.
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/layout.templateBased.Base.xml#build
             * @function
             * @name build
             */
            build: function () {
                this.constructor.superclass.build.call(this);

                this._$element = $('.my-popover', this.getParentElement());

                this.applyElementOffset();

                this._$element.find('.popover-close')
                    .on('click', $.proxy(this.onCloseClick, this));
            },

            /**
             * Удаляет содержимое макета из DOM.
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/layout.templateBased.Base.xml#clear
             * @function
             * @name clear
             */
            clear: function () {
                this._$element.find('.popover-close')
                    .off('click');

                this.constructor.superclass.clear.call(this);
            },

            /**
             * Метод будет вызван системой шаблонов АПИ при изменении размеров вложенного макета.
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
             * @function
             * @name onSublayoutSizeChange
             */
            onSublayoutSizeChange: function () {
                MyBalloonLayout.superclass.onSublayoutSizeChange.apply(this, arguments);

                if(!this._isElement(this._$element)) {
                    return;
                }

                this.applyElementOffset();

                this.events.fire('shapechange');
            },

            /**
             * Сдвигаем балун, чтобы "хвостик" указывал на точку привязки.
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
             * @function
             * @name applyElementOffset
             */
            applyElementOffset: function () {
                this._$element.css({
                    left: -(this._$element[0].offsetWidth / 2),
                    top: -(this._$element[0].offsetHeight + this._$element.find('.popover-arrow')[0].offsetHeight)
                });
            },

            /**
             * Закрывает балун при клике на крестик, кидая событие "userclose" на макете.
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/IBalloonLayout.xml#event-userclose
             * @function
             * @name onCloseClick
             */
            onCloseClick: function (e) {
                e.preventDefault();

                this.events.fire('userclose');
            },

            /**
             * Используется для автопозиционирования (balloonAutoPan).
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/ILayout.xml#getClientBounds
             * @function
             * @name getClientBounds
             * @returns {Number[][]} Координаты левого верхнего и правого нижнего углов шаблона относительно точки привязки.
             */
            getShape: function () {
                if(!this._isElement(this._$element)) {
                    return MyBalloonLayout.superclass.getShape.call(this);
                }

                var position = this._$element.position();

                return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
                    [position.left, position.top], [
                        position.left + this._$element[0].offsetWidth,
                        position.top + this._$element[0].offsetHeight + this._$element.find('.popover-arrow')[0].offsetHeight
                    ]
                ]));
            },

            /**
             * Проверяем наличие элемента (в ИЕ и Опере его еще может не быть).
             * @function
             * @private
             * @name _isElement
             * @param {jQuery} [element] Элемент.
             * @returns {Boolean} Флаг наличия.
             */
            _isElement: function (element) {
                return element && element[0] && element.find('.popover-arrow')[0];
            }
        });

    // Создание вложенного макета содержимого балуна.
    MyBalloonContentLayout = ymaps.templateLayoutFactory.createClass(

        '<div class="popover-head">' +
            '<div class="popover-content">' +
                '<div class="popover-cancel">' +
                    '<a class="popover-close" href="#"><img src="/img/icons/cancel.svg" ></a>' +
                '</div>' +
                '<div class="popover-worktime">' +
                    '$[properties.balloonWorkTime]' +
                '</div>' +
            '</div>' +
        '</div>' +
        '<div class="popover-body">' +
            '<div class="popover-content">' +
                '<div class="shop-name">' +
                    '$[properties.balloonHeader]' +
                '</div>' +
            '</div>' +
            '<div class="popover-content">$[properties.balloonContent]</div>'+
            '<div class="popover-content">$[properties.balloonTelephone]</div>'+
        '</div>'+
        '<div class="popover-arrow"></div>'
    );

    // идём по массиву городов и добавляем их на карту
    shopList.forEach(function(item, i) {
        showShopListFromCity(myMap, i);
    });

    // Спозиционируем карту так, чтобы на ней были видны все объекты.
    myMap.setBounds(myMap.geoObjects.getBounds());

    var shop_balloon = myMap.geoObjects.get(0);
    shop_balloon.balloon.open();
});
