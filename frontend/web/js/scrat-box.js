$(document).ready(function(){
	var children = get_number_of_non_blur_children() + 1;
	$(".wrapper-content .item:nth-child(n+" + children +")").css("opacity", "0.1");
	$(".wrapper-content .item:nth-child(n+" + children +")").css("filter", "alpha(opacity=30)");
	$(".wrapper-content .item:nth-child(n+" + children +")").css("pointer-events", "none");
});

$(window).resize(function(){
	if ($('.show-more').css("display") != "none") {
		$(".wrapper-content .item").css("opacity", "1");
		$(".wrapper-content .item").css("filter", "alpha(opacity=100)");
		$(".wrapper-content .item").css("pointer-events", "auto");

		var children = get_number_of_non_blur_children() + 1;
		$(".wrapper-content .item:nth-child(n+" + children +")").css("opacity", "0.1");
		$(".wrapper-content .item:nth-child(n+" + children +")").css("filter", "alpha(opacity=30)");
		$(".wrapper-content .item:nth-child(n+" + children +")").css("pointer-events", "none");
	}
});

$('.show-more').click(function() {
	$(".wrapper-content").css( "max-height", "100%");
	$(".wrapper-content .item").css("opacity", "1");
	$(".wrapper-content .item").css("filter", "alpha(opacity=100)");
	$(".wrapper-content .item").css("pointer-events", "auto");
	$(this).hide();
});


$('.my-pagination li a').on('click', function (e) {
    e.preventDefault();
    $('.my-pagination li').removeClass('active');
    $(this).parent().addClass('active');
    var page = $(this).data('page') + 1;

    $.ajax({
        url: '/scrat-box/scrat-box-popular-products',
        data: {page: page},
        type: 'GET',
        success: function (res) {
            if (!res) alert('Ошибка загрузки товаров!');
            else {
                $('#proposed_products').html(res);
                location.href = "#proposed_products";
            }
        },
        error: function () {
            alert('Ошибка загрузки товаров!');
        }
    });
});


$('body').on('click', '.show-product-popup', function(e){
    e.preventDefault();

    var slug = $(this).data('slug');

    $.ajax({
        url: '/scrat-box/scrat-box-product-popup',
        data: {slug: slug},
        type: 'GET',
        success: function (res) {
            if (!res) alert('Ошибка загрузки товара!');
            else {
                $('#product-popup-modal-click').html(res);
                $('#product-popup-modal-click').click();

            }
        },
        error: function () {
            alert('Ошибка загрузки товара!');
        }
    });
});

$('body').on('click', '.add-to-scrat-box', function(e){
    var id = $(this).data('id');
    $.ajax({
        url: '/scrat-box/add-to-scrat-box',
        data: { id: id },
        type: 'GET',
        success: function (res) {
            switch (res) {
                case 'MAX_COUNT_ERROR':
                    alert('Вы уже набрали максимальное количество товаров для скрат-бокса.');
                    break;
                case 'SIMILAR_PRODUCT_ERROR':
                    alert('В скрат-бокс можно добавлять только по одному товару каждого вида. Этот товар вы уже добавили.');
                    break;
                default:
                    $('#scrat-box').html(res);
            }
            if (!res) alert('Ошибка добавления товара!');
        },
        error: function () {
            alert('Ошибка добавления товара!');
        }
    });
});

function get_number_of_non_blur_children() {
	children = 1;
	if ($(window).width() > 991) {
		children = 3;
	}
	else 
		if (($(window).width() <= 991) && ($(window).width() >= 768)) {
			children = 2;
		}
		else {
			children = 1;
		}
	return children;
}
