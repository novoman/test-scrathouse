$(".content-img .mini-img").click(function(){
    selectImage(this);
});

function selectImage(img) {
    var temp_src = $(img).children("img").attr('src');
    $(".content-img .main-img .thumbnail").attr("style", "background : url('" + temp_src + "') 100% 50% no-repeat; background-size: contain; height: 333px; width: 500px;" );
    $(".content-img .main-img .thumbnail .popup").attr("style", "background-image : url('" + temp_src + "'); height: 333px; width: 500px;" );

    $(".content-img .mini-img .img-frame").css("visibility", "hidden");
    $(img).children(".img-frame").css("visibility", "visible");
}

$(document).keydown(function(e) {
    switch(e.which) {
        // left
        case 37:
            var prev;
            $(".mini-imgs .mini-img .img-frame").each(function() {
                if ($(this).css("visibility") == "visible") {
                    prev = $(this).parent().prev(".mini-img");
                }
            });
            if (!prev.length) {
                prev = $(".mini-imgs .mini-img:last-child");
            }
            selectImage(prev);
            break;
        // right
        case 39:
            var next;
            $(".mini-imgs .mini-img .img-frame").each(function() {
                if ($(this).css("visibility") == "visible") {
                    next = $(this).parent().next(".mini-img");
                }
            });
            if (!next.length) {
                next = $(".mini-imgs .mini-img:first-child");
            }
            selectImage(next);
            break;
        default: return;
    }
    // Prevent the default action (scroll / move caret)
    e.preventDefault();
});
