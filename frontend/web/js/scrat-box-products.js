// $(function() {
//     $('.wrapper-content .filter ul .parent span').click(function(){
//         $(this).parent().toggleClass('active');
//     });
// });

$('body').on('click', '.add-to-scrat-box', function(e){
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
        url: '/scrat-box/add-to-scrat-box',
        data: {id: id},
        type: 'GET',
        success: function (res) {
            switch (res) {
                case 'MAX_COUNT_ERROR':
                    alert('Вы уже набрали максимальное количество товаров для скрат-бокса.');
                    break;
                case 'SIMILAR_PRODUCT_ERROR':
                    alert('В скрат-бокс можно добавлять только по одному товару каждого вида. Этот товар вы уже добавили.');
                    break;
                case 'WRONG_COUNT_FOR_ONE_CATEGORY':
                    alert('В скрат-бокс можно добавлять только по 2 товара из категорий Сухофрукты/Сладости и Орехи.');
                    break;
                default:
                    showToast('Товар добавлен в скрат-бокс', '25px', 3);
                    $('#scrat-box').html(res);
            }
            if (!res) alert('Ошибка добавления товара!');
        },
        error: function () {
            alert('Ошибка добавления товара!');
        }
    });
});

$('body').on('click', '.add-to-cart-button a', function(e){
    e.preventDefault();
    var count = ($("#product-count").text() !== '' ? parseInt($("#product-count").text()) : 1),
        name = ($("#box-name").val() !== '' ? $("#box-name").val() : "Бокс");
    $.ajax({
        url: '/cart/add',
        data: {scratbox: true, count: count, name: name},
        type: 'GET',
        success: function (res) {
            if (!res) alert('Ошибка 1 добавления товара!');
            else {
                showCart(res);
                $.ajax({
                    url: '/scrat-box/refresh-scrat-box-client',
                    type: 'GET',
                    success: function (res) {
                        if (!res) alert('Ошибка 2 добавления товара!');
                        else $('#scrat-box').html(res);
                    },
                    error: function () {
                        alert('Ошибка 2 добавления товара!');
                    }
                });
            }
        },
        error: function () {
            alert('Ошибка 1 добавления товара!');
        }
    });
});

$('body').on('click', '.show-product-popup', function(e){
    e.preventDefault();
    var slug = $(this).data('slug');

    $.ajax({
        url: '/scrat-box/scrat-box-product-popup',
        data: {slug: slug},
        type: 'GET',
        success: function (res) {
            if (!res) alert('Ошибка загрузки товара!');
            else {
                $('#product-popup-modal-click').html(res);
                $('#product-popup-modal-click').click();

            }
        },
        error: function () {
            alert('Ошибка загрузки товара!');
        }
    });
});

function clearScratBox() {
    if (confirm('Вы действительно хотите очистить?')) {
        $.ajax({
            url: '/scrat-box/clear-scrat-box',
            type: 'GET',
            success: function (res) {
                if (!res) alert('Ошибка очистки!');
                else $('#scrat-box').html(res);

            },
            error: function () {
                alert('Ошибка очистки!');
            }
        });
    }
}

function deleteProductFromScratBox(elem) {
    if (confirm('Вы действительно хотите убрать этот товар из скрат-бокса?')) {
        var id = elem.getAttribute('data-id');
        $.ajax({
            url: '/scrat-box/delete-product-from-scrat-box',
            data: {id: id},
            type: 'GET',
            success: function (res) {
                if (!res) alert('Ошибка удаления товара!');
                else $('#scrat-box').html(res);
            },
            error: function () {
                alert('Ошибка удаления товара!');
            }
        });
    }
}

$('body').on('click', '.increase-product-count', function() {
    var temp_count = parseInt($(this).closest('.change').children(".line-block").children(".product-count").text());
    $(this).closest('.change').children(".line-block").children(".product-count").text(temp_count + 1);
    var temp_money = $(this).closest('.add-box-to-cart-inner').children(".price").text();
    temp_money = parseFloat(parseFloat(parseFloat(temp_money.replace(/[^0-9.]/g, '')).toFixed(2) / temp_count).toFixed(2) * (temp_count + 1)).toFixed(2);
    $(this).closest('.add-box-to-cart-inner').children(".price").text(temp_money + " BYN");
});

$('body').on('click', '.decrease-product-count', function() {
    var temp_count = parseInt($(this).closest('.change').children(".line-block").children(".product-count").text());
    if (temp_count > 1) {
        $(this).closest('.change').children(".line-block").children(".product-count").text(temp_count - 1);

        var temp_money =  $(this).closest('.add-box-to-cart-inner').children(".price").text();
        temp_money = parseFloat(parseFloat(parseFloat(temp_money.replace(/[^0-9.]/g, '')).toFixed(2) / temp_count).toFixed(2) * (temp_count - 1)).toFixed(2);
        $(this).closest('.add-box-to-cart-inner').children(".price").text(temp_money + " BYN");
    }
});

$('body').on('dblclick', '.scrat-box .add-box-to-cart .box-name span', function(){
    $(".scrat-box .add-box-to-cart .box-name span").hide();
    $("#box-name").show();
    $('.scrat-box .add-box-to-cart .box-name img').hide();
});

$('body').on('click', '.scrat-box .add-box-to-cart .box-name img', function() {
    $(".scrat-box .add-box-to-cart .box-name span").hide();
    $("#box-name").show();
    $('.scrat-box .add-box-to-cart .box-name img').hide();
});

$('body').on('keyup', '#box-name', function(e) {
    if (e.keyCode === 13) {
        save_box_name();
    }
});

function save_box_name() {
    if ($("#box-name").val()) {
        $(".scrat-box .add-box-to-cart .box-name span").text($("#box-name").val());
        $("#box-name").hide()
        $(".scrat-box .add-box-to-cart .box-name span").show();
        $('.scrat-box .add-box-to-cart .box-name img').show();
    }
    else {
        alert("Заполните название Бокса!");
    }
}
