$(function() {
	$('.my-modal').magnificPopup({
		closeMarkup: '<img src="/img/icons/cancel.svg" title="Закрыть" class="mfp-close"/>',
        closeOnBgClick: true
	});

    $('.nav .menu .items .name').click(function(){
    	$(this).parent().find('ul').toggleClass('active');
    });

	$('#change-signup').click(function () {
        if ($('#change-signup a')[0].innerText === 'Вход в аккаунт') {
            $('#signup-form').css('display', 'none');
            $('#login-form').css('display', 'block');
            $('#change-signup a')[0].innerText = 'Создать аккаунт';
        }
        else {
            $('#login-form').css('display', 'none');
            $('#signup-form').css('display', 'block');
            $('#change-signup a')[0].innerText = 'Вход в аккаунт';
        }

    });

	$('.navbar-header').addClass('col-lg-3 col-lg-push-0 col-md-3 col-md-push-0 col-sm-6 col-sm-push-6 col-xs-12');

	$('#search_form img').on('click', function () {
	    if ($(window).width() > 650) {
            $('#search_form').submit();
        }
        else {
            expandSearchInput();
        }
    });

    $('#searchform-param').on('click', function () {
        if ($(window).width() <= 650) {
            expandSearchInput();
        }
    });

	function expandSearchInput() {
        var search_div = $('.search');
        search_div.css('position', 'absolute');
        search_div.css('width', '80%');
        search_div.css('left', '15%');
        search_div.css('top', '60px');
        search_div.css('z-index', '999999');

        $('.my-help-block').css('visibility', 'hidden');
        $('.search-close').css('display', 'block');
    }

    $('.search-close').on('click', function () {
        if ($(window).width() <= 650) {
            var search_div = $('.search');
            search_div.css('width', '10%');
            search_div.css('left', '70%');
            search_div.css('top', 'initial');
            search_div.css('position', 'relative');

            $('.my-help-block').css('visibility', 'visible');
            $('.search-close').css('display', 'none');

            $('#searchform-param').val('');
        }
    });

    $('body').on('click', '.add-to-cart a', function(e){
       e.preventDefault();
       var id = $(this).data('id'),
           weight = ($("#product-weight").text() !== '' ? parseInt($("#product-weight").text()) : 0);
       $.ajax({
           url: '/cart/add',
           data: { id: id, weight: weight },
           type: 'GET',
           success: function (res) {
               if (!res) alert('Ошибка добавления товара! Возможно, вы выбрали товар, которого нет в наличии.');
               else showCart(res, 'add');
           },
           error: function () {
               alert('Ошибка добавления товара!');
           }
       });
    });


    $(window).resize(function(){
        var blok_size = get_blok_size();
        var height = blok_size *  $("#cart-popup-modal  .product").length;
        $("#cart-popup-modal .content-cart").css("height", height);
    });

    function get_blok_size() {
        if ($(window).width() > 991) {
            blok_size = 170;
        }
        else {
            blok_size = 250;
        }
        return blok_size;
    }

    $('body').on('click', '.scratbox-header-popup', function(){
        $(this).parent().find('.scratbox-options').toggle();

        var blok_size = get_blok_size();
        var temp = blok_size *  5;
        if ($(this).parent().find('.scratbox-options').css('display') === 'none') {
            temp = -temp;
        }
        var height = parseInt($("#cart-popup-modal .content-cart").height()) + temp;
        $("#cart-popup-modal .content-cart").height(height);
    });

});

function showCart(cart, action) {
    $('#cart-popup-modal-click').html(cart);
    if (action === 'add') {
        if ($('#cart-popup-modal .product').length % 3 === 0) {
            $('#cart-popup-modal-click').click();
        }
        else {
            showToast('Товар добавлен в корзину', '25px', 3);
        }
    }
    else {
        $('#cart-popup-modal-click').click();
    }

    var height = ($(window).width() > 500 ? 120 : 250) * ($('#cart-popup-modal .product').length) + $('#cart-popup-modal .content-cart').height();

    $('#cart-popup-modal .content-cart').css("height", height);
    var cart_items_count = $('#cart-popup-modal .product').length;
    if (cart_items_count > 0) {
        $('#cart-items-count').show();
        $('#cart-items-count').html(cart_items_count);
        $('#cart-popup-modal .empty-cart').hide();
    }
    else {
        $('#cart-items-count').hide();
        $('#cart-popup-modal .overall').hide();
    }

}

function clearCart() {
    if (confirm('Вы действительно хотите очистить корзину?')) {
        $.ajax({
            url: '/cart/clear',
            type: 'GET',
            success: function (res) {
                if (!res) alert('Ошибка очистки корзины!');
                else showCart(res);

            },
            error: function () {
                alert('Ошибка очистки корзины!');
            }
        });
    }
}

function deleteProductPopup(elem) {
    var id = elem.getAttribute('data-id'),
        scratbox = elem.getAttribute('data-scratbox');
    if (confirm('Вы действительно хотите удалить этот товар из корзины?')) {
        $.ajax({
            url: '/cart/del-product',
            data: {id: id, scratbox: scratbox},
            type: 'GET',
            success: function (res) {
                if (!res) alert('Ошибка удаления товара!');
                else showCart(res);
            },
            error: function () {
                alert('Ошибка удаления товара!');
            }
        });
    }
}

function decreaseProductWeightPopup(elem) {
    var id = elem.getAttribute('data-id'),
        scratbox = elem.getAttribute('data-scratbox');
    $.ajax({
        url: '/cart/decrease-product-weight',
        data: {id: id, scratbox: scratbox},
        type: 'GET',
        success: function (res) {
            if (!res) alert('Ошибка уменьшения массы товара!');
            else showCart(res);
        },
        error: function () {
            alert('Ошибка уменьшения массы товара!');
        }
    });
}

function increaseProductWeightPopup(elem) {
    var id = elem.getAttribute('data-id'),
        scratbox = elem.getAttribute('data-scratbox');
    $.ajax({
        url: '/cart/increase-product-weight',
        data: {id: id, scratbox: scratbox},
        type: 'GET',
        success: function (res) {
            if (!res) alert('Ошибка увеличения массы товара!');
            else showCart(res);
        },
        error: function () {
            alert('Ошибка увеличения массы товара!');
        }
    });
}

function searchByName(size,element) {
    if (size >= 3) {
        setTimeout(function () {
            $.ajax({
                method: 'POST',
                dataType: 'text',
                url: '/search-by-name',
                data: {data: '{"data" : "' + $.trim(element.val()) + '"}'},

                success: function (response) {
                    response = JSON.parse(response);

                    if (!response){
                        $('.autocomplete').css('display', 'none');
                        $('.autocomplete').empty();
                        return false;
                    }

                    var html = '';

                    for(var i = 0; i < response.length; i++) {
                        html += '<a href="/product/'+ response[i].slug +'">' +
                                    '<span>'+ response[i].name + '&nbsp;&nbsp;<span class="country">' + response[i].country + '</span></span>' +
                                '</a>';
                    }

                    $('.autocomplete').css('display', 'block');
                    $('.autocomplete').empty();
                    $('.autocomplete').append(html);
                },
                error: function(e) {
                    alert(e);
                }
            });
        }, 200);
    }
    else {
        $('.autocomplete').css('display', 'none');
        $('.autocomplete').empty();
    }

}

$('#searchform-param').on('input', function (e) {
    var inputTextSize = $.trim($(this).val()).length;
    var input = $(this);

    searchByName(inputTextSize, input);
});

$('body').on('click', function (e) {
    var target = $( e.target );
    if (!target.is( '.autocomplete' ) ) {
        $('.autocomplete').css('display', 'none');
    }
});

$('body').on('click', '.pushy-submenu > a', function(e){
    if (e.offsetX > this.offsetWidth - 30) {
        e.preventDefault();
    }
});

$('body').on('click', '.my-pagination li a', function(e){
    location.href = "#elements";
});

// $(function() {
//     var nav = $("#site_nav");
//     var container = $("#container");
//
//     if ($(window).width() <= 768) {
//         nav.addClass("fixed-nav");
//         container.css("position", "fixed");
//     } else {
//         nav.removeClass("fixed-nav");
//         container.css("position", "absolute");
//     }
// });
