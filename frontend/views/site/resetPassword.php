<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\assets\AppAsset;

$this->title = 'Смена пароля — Скрэтхаус.бай';
$this->params['breadcrumbs'][] = $this->title;
AppAsset::register($this);
?>
<div class="site-request-password-reset container site-about"">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Пожалуйста, введите Ваш новый пароль:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

	            <?= $form->field($model, 'password')->passwordInput(array('placeholder' => 'Пароль'))->label(false) ?>
	            <?= $form->field($model, 'password_confirm')->passwordInput(array('placeholder' => 'Повторите пароль'))->label(false) ?>


                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
