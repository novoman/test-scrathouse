<?php

/* @var $this yii\web\View */

use backend\models\ContactInfo;
use frontend\assets\AppAsset;
use yii\web\View;

$this->title = 'Контакты — Скрэтхаус.бай';
$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag([
	'name' => 'description',
	'content' => 'Информация о торговых магазинах scrathouse.by, а также контакты для связи.'
]);

AppAsset::register($this);
\frontend\assets\ContactAsset::register($this);

// Вытягиваем список магазинов из базы и записываем в JS переменную shopList
$shops = "";
foreach($shopList as $key => $value) {
	$shops .= "
          {
            'coordinates': [".$shopList[$key]['coordinante_X'].", ".$shopList[$key]['coordinante_Y']."],
            'city_name': '".$shopList[$key]['city']."',
            'address': '".$shopList[$key]['address']."',
            'name': '".$shopList[$key]['name']."',
            'work_time': '".$shopList[$key]['work_time']."',
            'telephone': '".$shopList[$key]['telephone']."'
          },
	";
}
$this->registerJs("
    // Список городов и магазинов в них
    var shopList = [
      {      
        'shops': [
            ".$shops."
        ]
      }
    ];
", View::POS_HEAD);


$contact_info = ContactInfo::getContactInfoArray();
?>

<div class="main-block">
    <div class="content">
        <div class="shops-inf col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <?php foreach($shopList as $key => $value): ?>
	            <?php
                    $model = \backend\models\Shops::find()->where(['=', 'id', $shopList[$key]['id']])->one();
                    $main_image = $model->getImage();
	            ?>
                <div id="<?= $key ?>" class="inf">
                    <span>
                        <b><?= $shopList[$key]['name'] ?></b><br>
	                    <?= \yii\helpers\Html::img(($main_image->id ? '' : '/0').$main_image->getUrl('x150'), ['alt' => $shopList[$key]['name'], 'class' => 'shop-img'])?><br><br>
                        <b>Телефон</b> <?= $shopList[$key]['telephone'] ?><br>
                        Velcom / Viber / Telegram / WhatsApp<br><br>
                        <b>Режим работы</b> <?= $shopList[$key]['work_time'] ?><br><br>
                        <b>Адрес</b> <?= $shopList[$key]['address'] ?><br><br>
                        <span class="small-text">*Самовывоз по предварительной договоренности</span>
                    </span>
                </div>
            <?php endforeach; ?>
            <div class="arrows">
                <div class="prev"><img src="/img/icons/arrow-left.svg"></div>
                <div class="next"><img src="/img/icons/arrow-right.svg"></div>
            </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div id="map"></div>
        </div>
        <div class="socials col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ul>
                <li><a href="<?=$contact_info['footer']['fb'] ?>" target="_blank">
                        <img src="/img/icons/socials/alt/fb.svg" alt="" style="width: 14px;">
                    </a></li>
                <li><a href="<?=$contact_info['footer']['tw'] ?>"  target="_blank">
                        <img src="/img/icons/socials/alt/twitter.svg" alt="" style="width: 31px;">
                    </a></li>
                <li><a href="<?=$contact_info['footer']['vk'] ?>" target="_blank">
                        <img src="/img/icons/socials/alt/vk.svg" alt="" style="width: 35px;">
                    </a></li>
                <li><a href="<?=$contact_info['footer']['in'] ?>" target="_blank">
                        <img src="/img/icons/socials/alt/instagram.svg" alt="" style="width: 28px;">
                    </a></li>
            </ul>
        </div>
    </div>
</div>
