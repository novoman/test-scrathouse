<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use backend\models\TextByAdmin;

$this->title = 'Оптовая торговля — Скрэтхаус.бай';
$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag([
	'name' => 'description',
	'content' => 'Информация для оптовых заказчиков.'
]);

AppAsset::register($this);

?>
<div class="site-about container text-center">
    <?= TextByAdmin::findOne(['page' => 'wholesale']) ? TextByAdmin::findOne(['page' => 'wholesale'])->text : '' ?>
</div>
