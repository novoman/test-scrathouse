<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use backend\models\TextByAdmin;

$this->title = 'Договор публичной оферты - Скрэтхаус.бай';
$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag([
'name' => 'description',
'content' => 'Договор публичной оферты.'
]);

AppAsset::register($this);

?>
<div class="site-about container">
	<?= TextByAdmin::findOne(['page' => 'contact-offer']) ? TextByAdmin::findOne(['page' => 'contact-offer'])->text : '' ?>
</div>
