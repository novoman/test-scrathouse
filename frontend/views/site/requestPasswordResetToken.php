<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\assets\AppAsset;

$this->title = 'Запрос на смену пароля — Скрэтхаус.бай';
$this->params['breadcrumbs'][] = $this->title;

AppAsset::register($this);

?>
<div class="site-request-password-reset container site-about">
    <h2><?= Html::encode($this->title) ?></h2>

    <p>Пожалуйста, введите Ваш email. Ссылка для изменения пароля будет отправлена на него.</p>

    <div class="row">
        <div class="col-lg-5 col-md-5 col-sm-5">
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

	        <?= $form->field($model, 'email')->textInput(array('placeholder' => 'Email',))->label(false); ?>

                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
