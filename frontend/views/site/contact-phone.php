<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\ContactPhoneForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use frontend\assets\AppAsset;

$this->title = 'Обратная связь — Скрэтхаус.бай';
$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag([
	'name' => 'description',
	'content' => 'Закажите у scrathouse.by обратный звонок и мы вам перезвоним в кратчайшие сроки!'
]);

AppAsset::register($this);

?>
<div class="site-about container">
    <h5>
        Закажите обратный звонок, и наши сотрудники оперативно перезвонят Вам.
    </h5>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'contact-tel-form']); ?>

                <?= $form->field($model, 'name')->textInput(['autofocus' => true])->label('Имя') ?>

                <?= $form->field($model, 'phone')->label('Телефон') ?>

                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ])->label('Подтверждающий код') ?>

                <div class="form-group">
                    <?= Html::submitButton('Заказать звонок', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
