<?php

/* @var $this \yii\web\View */
/* @var $content string */

use common\models\Category;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\bootstrap\Nav;
use common\widgets\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\SignupWidget;
use common\widgets\LoginWidget;
use yii\web\View;
use backend\models\ContactInfo;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-137051473-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-137051473-1');
    </script>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(53109172, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/53109172" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => \yii\helpers\Url::to(['/img/favicon/128.png'])]); ?>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<?= $this->render('main/flashes') ?>
    <?php
        // если пользователь только зарегистрировался, то вывести popup с выбором роли (юр. или физ. лицо)
         foreach(Yii::$app->session->getAllFlashes() as $type => $message) {
             if ($message === 'Вы успешно зарегистрировались.')
	            $this->registerJs("setTimeout(function() { $('#after-reg-popup-click').click(); }, 2000);", View::POS_LOAD);
         }
    ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<header>
    <?php
    NavBar::begin([
        'brandLabel' => '<img src="/img/Logo.svg" alt="Scrathouse">',
        'brandUrl' => '/',
        'brandOptions' => [
            'class' => false,
        ],
        'options' => [
            'class' => 'nav fixed-nav',
            'id' => 'site_nav',
        ],
        'renderInnerContainer' => true,
        'innerContainerOptions' => [
            'class' => 'container',
        ],

    ]);
    ?>

    <div class="menu col-lg-6 col-md-6">
        <div class="text-center">
            <?php
                // array for navbar
                $items = array();
                // get all root categories
                $root_cats = \common\models\Category::find()->where(['depth' => 0])->all();

                foreach ($root_cats as $root_cat) {
                    // for each root category search it's children
                    $cats = \common\models\Category::getTree($root_cat->id, true);

                    // for root in navbar as string
                    $main_temp_item = '';
                    // for it's children as categories
                    $temp_items = array();

                    foreach ($cats as $cat) {
                        if ($cat['depth'] == 0) {
                            if (count($cats) > 1) {
	                            $main_temp_item = '<div class="li-name col-lg-3 col-md-3">
                                                       <div class="name">'.$cat['name'].'</div>';
                            }
                            else {
	                            $main_temp_item = '<div class="li-name col-lg-3 col-md-3">
                                                       <div class="name"><a href="/products/'.($cat['slug'] ? $cat['slug'] : \common\models\Category::getStartCategory()).'">'.$cat['name'].'</a></div>';
	                            $temp_items[] = array();
                            }
                        }
                        else {
                            $temp_items[] = array(
                                'label' => $cat['name'],
                                'url' => array(
                                    '/products/products', 'slug' => $cat['slug'])
                            );
                        }
                    }
                    $items[] = array(
                        'label' => $main_temp_item,
                        'items' => $temp_items,
                    );
                }

                echo \yii\widgets\Menu::widget([
                    'items' => [
                        [
                            'label' => 'Товары 
                                        <div class="drop">
                                            <div class="items text-left">',

                            'url' => ['/products'],
                            'items' => $items,
                        ],
                        ['label' => 'Cкрат бокс', 'url' => ['/scrat-box/scrat-box']],
                        ['label' => 'Доставка', 'url' => ['/deliv-pay/deliv-pay']],
                        ['label' => 'Контакты', 'url' => ['/site/contact']],
//                        ['label' => 'Статьи', 'url' => ['/articles/articles']],
                        ['label' => 'Сотрудничество', 'url' => ['/site/wholesale']],
                    ],
                    'encodeLabels' => false,
                ]);
            ?>
        </div>
    </div>
    <div class="search col-lg-2 col-lg-pull-0 col-md-2 col-md-pull-0 col-sm-4 col-sm-pull-4 col-xs-pull-4 col-xs-3">
        <div class="wrapper-form text-left">
            <?php
                ActiveForm::begin(
                    [
                        'id' => 'search_form',
                        'action' => ['products/search'],
                        'method' => 'get',
                        'options'=> [
                            'class' => false,
                        ],
                    ]
                );
                echo Html::button(
                    '<img src="/img/icons/search.svg" alt="">',
                    []
                );
                echo Html::input(
                        'type: text',
                        'q',
                        '',
                        ['id' => 'searchform-param']
                );
            ?>
                <div class="search-close">X</div>
                <div class="autocomplete"></div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <div class="my-help-block col-lg-2 col-lg-pull-0 col-md-2 col-md-pull-0 col-sm-3 col-sm-pull-4 col-xs-pull-4 col-xs-3">
        <div class="text-left">
            <div class="help">
                <a class="help-button">Помощь</a>
                <div class="wrapper-drop">
                    <div class="help-down">
                        <a href="<?= \yii\helpers\Url::to(['/callback/callback'])?>">Возврат</a>
                        <a href="<?= \yii\helpers\Url::to(['/deliv-pay/deliv-pay'])?>">Доставка и оплата</a>
                        <a href="<?= \yii\helpers\Url::to(['/site/contact-phone'])?>">Обратный звонок</a>
                    </div>
                </div>
            </div>
            <div id="cart-header" class="cart">
                <?php echo $this->render('/cart/cart-header');?>
            </div>
            <?php if (Yii::$app->user->isGuest): ?>
                <div class="login-buttons"><a href="#login-modal" class="my-modal">Войти</a></div>
            <?php else: ?>
                <div class="login-buttons">
                    <a href="#" class="help-button">Профиль</a>
                    <div class="wrapper-drop">
                        <div class="help-down">
                            <a href="<?= \yii\helpers\Url::to(['/profile/profile', 'page' => 1])?>">Мои заказы</a>
                            <a href="<?= \yii\helpers\Url::to(['/profile/profile', 'page' => 2])?>">Мои данные</a>
                            <a href="<?= \yii\helpers\Url::to(['/profile/profile', 'page' => 3])?>">Программа лояльности</a>
                            <?= Html::a("Выход", ['site/logout'], [
                                'data' => [
                                    'method' => 'post'
                                ],
                            ]);?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <?php NavBar::end(); ?>
</header>

<?=$content ?>

<?php $contact_info = ContactInfo::getContactInfoArray();?>

<div class="footer">
    <div class="container">
        <div class="row">
            <div class="content">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 copyright item">
                    <div class="title"><?=$contact_info['footer']['name'] ?></div>
                    <div class="info">
	                    <?=$contact_info['footer']['domain'] ?>
                    </div>
                    <div class="info">
	                    <?=$contact_info['footer']['reg_number'] ?>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 contacts item">
                    <div class="title">Контакты</div>
                    <div class="info">
                        Email: <a href="mailto:<?=$contact_info['footer']['email'] ?>"><?=$contact_info['footer']['email'] ?></a>
                    </div>
                    <div class="info">
                        <a href="tel:<?=$contact_info['footer']['tel'] ?>"><?=$contact_info['footer']['tel'] ?></a>
                    </div>

                    <div class="info">
                        <a href="<?=$contact_info['footer']['skype'] ?>"><?=$contact_info['footer']['skype'] ?></a>
                    </div>
                </div>

                <div class="row separator"></div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 menu item">
                    <ul>
                        <li><a href="<?= \yii\helpers\Url::to(['/deliv-pay/deliv-pay'])?>">Доставка и оплата</a></li>
                        <li><a href="<?= \yii\helpers\Url::to(['/shops/shops'])?>">Магазины</a></li>
                        <li><a href="<?= \yii\helpers\Url::to(['/site/wholesale'])?>">Сотрудничество</a></li>
                        <li><a href="<?= \yii\helpers\Url::to(['/scrat-box/scrat-box'])?>">Cкрат боксы</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 copyright item">
                    <div class="info">
                        <a href="<?= \yii\helpers\Url::to(['/site/contact-offer'])?>">Договор публичной оферты</a>
                    </div>
                    <div class="title">Социальные сети</div>
                    <div class="socials">
                        <ul>
                            <li><a href="<?=$contact_info['footer']['fb'] ?>" target="_blank">
                                    <img src="/img/icons/socials/fb.svg" alt="" style="width: 10px;">
                                </a></li>
                            <li><a href="<?=$contact_info['footer']['tw'] ?>"  target="_blank">
                                    <img src="/img/icons/socials/twitter.svg" alt="" style="width: 21px;">
                                </a></li>
                            <li><a href="<?=$contact_info['footer']['vk'] ?>" target="_blank">
                                    <img src="/img/icons/socials/vk.svg" alt="" style="width: 25px;">
                                </a></li>
                            <li><a href="<?=$contact_info['footer']['in'] ?>" target="_blank">
                                    <img src="/img/icons/socials/instagram.svg" alt="" style="width: 18px;">
                                </a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 webpay">
                    <img alt="Платежные системы" class="pay-types" src="/img/icons/log_sys.png">
                </div>

                <div class="row separator"></div>

                <div class="col-md-12 item seo-text">
	                <?=$contact_info['footer']['seo'] ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Mobile menu section -->

<nav class="pushy pushy-left">
    <div class="pushy-content">
        <ul>
            <li class="pushy-link"><a href="<?= \yii\helpers\Url::to(['/scrat-box/scrat-box'])?>"">Cкрат бокс</a></li>
            <!-- Submenu -->
        <?php
            // get all root categories
            $root_cats = Category::find()->where(['depth' => 0])->all();

            $selected_cat_parent_root_id = Category::find()->where(['slug' =>  Category::getCorrectSlug(Yii::$app->request->get('slug'))])->one()->getParentId();

            foreach ($root_cats as $root_cat) {
                // for each root category search it's children
                $cats = Category::getTree($root_cat->id, true);

                foreach ($cats as $cat) {
                    if ($cat['depth'] == 0) {
	                    if (count($cats) > 1) {
		                    echo '<li class="pushy-submenu '.(($selected_cat_parent_root_id  == $cat['id'] && Yii::$app->request->get('slug')) ? "pushy-submenu-open" : "").'">
                                    <a class="'.(Yii::$app->request->get('slug') == $cat['slug'] ? "selected" : "").'" href="'.\yii\helpers\Url::to(['/products/products', 'slug' => $cat['slug']]).'">'.$cat['name'].'</a>
                                    <ul>';
	                    }
	                    else {
		                    echo '<li class="pushy-link">
                                    <a class="'.(Yii::$app->request->get('slug') == $cat['slug'] ? "selected" : "").'" href="'.\yii\helpers\Url::to(['/products/products', 'slug' => $cat['slug']]).'">'.$cat['name'].'</a>
                                    <ul>';
	                    }
                    }
                    else {
                        echo '<li class="pushy-link">
                                <a class="'.(Yii::$app->request->get('slug') == $cat['slug'] ? "selected" : "").'" href="'.\yii\helpers\Url::to(['/products/products', 'slug' => $cat['slug']]).'">'.$cat['name'].'</a>
                              </li>';
                    }
                }
                echo '</ul>
                   </li>';
            }
        ?>
            <hr>
            <li class="pushy-link"><a href="<?= \yii\helpers\Url::to(['/deliv-pay/deliv-pay'])?>">Доставка</a></li>
            <li class="pushy-link"><a href="<?= \yii\helpers\Url::to(['/site/contact'])?>">Контакты</a></li>
<!--            <li class="pushy-link"><a href="--><?//= \yii\helpers\Url::to(['/articles/articles'])?><!--">Статьи</a></li>-->
            <li class="pushy-link"><a href="<?= \yii\helpers\Url::to(['/site/wholesale'])?>">Сотрудничество</a></li>
        </ul>
    </div>
</nav>

<div class="site-overlay"></div>

<div id="container">
    <button class="menu-btn"><span></span><span></span><span></span></button>
</div>

<div id="phones-modal" class="mfp-hide">
    <div class="info">
        <div class="title text-center">Телефоны</div>
        <p><a href="tel:<?=$contact_info['popup']['tel1'] ?>"><?=$contact_info['popup']['tel1'] ?></a></p>
        <p><a href="tel:<?=$contact_info['popup']['tel2'] ?>"><?=$contact_info['popup']['tel2'] ?></a></p>
        <p><a href="tel:<?=$contact_info['popup']['tel3'] ?>"><?=$contact_info['popup']['tel3'] ?></a></p>
    </div>
</div>

<div id="login-modal" class="mfp-hide">
    <?= SignupWidget::widget(); ?>
    <?= LoginWidget::widget(); ?>
    <div id="change-signup" class="create text-center"><a href="#">Создать аккаунт</a></div>
</div>

<div class="cart-popup"><a href="#cart-popup-modal" class="my-modal"><span id="cart-popup-modal-click"></span></a></div>
<div class="after-reg-popup"><a href="#client-modal" class="my-modal"><span id="after-reg-popup-click"></span></a></div>

<div id="client-modal" class="mfp-hide">
    <div class="client-popup-img">
        <img src="/img/client-popup.jpg">
    </div>
    <div class="client-popup-text">
        <div class="client-head">
            Добро пожаловать в Scrat House!
        </div>
        <div class="client-text">
            Чтобы обслуживание ваших заказов происходило как можно скорее, рекомендуем вам сейчас заполнить профиль покупателя — так вы получите скидку от Scrat House и будете всегда в курсе всех акцией и специальных предложений нашего вкусного магазина! Внимание: если вы пропустите этот шаг, вам все равно надо будет внести всю необходимую информацию при оформлении вашего заказа.<br><br>
            Выберите, профиль юридического или физического лица вы хотите заполнить:
        </div>
        <div class="client-buttons">
            <div class="client-button">
                <a href="<?= \yii\helpers\Url::to(['/profile/profile', 'page' => 2, 'client_type' => 1])?>" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    Юридическое Лицо
                    <div class="arrow-img">
                        <img src="/img/icons/right-arrow.svg">
                    </div>
                </a>
            </div>
            <div class="client-button">
                <a href="<?= \yii\helpers\Url::to(['/profile/profile', 'page' => 2, 'client_type' => 0])?>" class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    Физическое Лицо
                    <div class="arrow-img">
                        <img src="/img/icons/right-arrow.svg">
                    </div>
                </a>

            </div>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
