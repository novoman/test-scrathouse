<?php use kartik\growl\Growl; ?>
<?php foreach(Yii::$app->session->getAllFlashes() as $type => $message): ?>
    <?php
		switch ($type) {
			case 'success':
				$flash_type = Growl::TYPE_SUCCESS;
				break;
			case 'warning':
				$flash_type = Growl::TYPE_WARNING;
				break;
			case 'error':
				$flash_type = Growl::TYPE_DANGER;
				break;
			case 'growl':
				$flash_type = Growl::TYPE_GROWL;
				break;
			case 'minimalist':
				$flash_type = Growl::TYPE_MINIMALIST;
				break;
			case 'pastel':
				$flash_type = Growl::TYPE_PASTEL;
				break;
			default:
			case 'info':
				$flash_type = Growl::TYPE_INFO;
				break;
		}

		echo Growl::widget([
			'type' => $flash_type,
			'title' => '',
			'body' => $message,
			'showSeparator' => false,
			'delay' => 0,
			'pluginOptions' => [
				'showProgressbar' => false,
				'placement' => [
					'from' => 'top',
					'align' => 'center',
				],
				'delay' => 10000,
			]
		]);
    ?>
<?php endforeach; ?>

