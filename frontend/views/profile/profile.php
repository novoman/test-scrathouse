<?php

/* @var $this yii\web\View */

use common\models\PaymentsType;
use frontend\assets\AppAsset;
use frontend\models\Profile;
use yii\web\View;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\TextByAdmin;
use common\models\Order;
use backend\models\AdminNumbers;
use common\models\User;

$this->title = 'Личный кабинет — Скрэтхаус.бай';

AppAsset::register($this);
\frontend\assets\ProfileAsset::register($this);

?>

<?php if (!Yii::$app->user->isGuest): ?>
<div class="main-block">
    <div class="container">
        <div class="row">
            <div class="content">
                <div class="user-panel">
                    <div class="user-name col-lg-8 col-md-8 col-sm-8 col-xs-12 text-left">
                        Здравствуйте, <?= $user['last_name'].' '.$user['first_name']?>
                    </div>
                </div>
                <div class="profile-nav">
                    <ul>
                        <li><a <?= (($page == 1) ? 'class="selected"' : '')?> href="<?= \yii\helpers\Url::to(['/profile/profile', 'page' => 1])?>">Мои заказы</a></li>
                        <li><a <?= (($page == 2) ? 'class="selected"' : '')?> href="<?= \yii\helpers\Url::to(['/profile/profile', 'page' => 2])?>">Мои данные</a></li>
                        <li><a <?= (($page == 3) ? 'class="selected"' : '')?> href="<?= \yii\helpers\Url::to(['/profile/profile', 'page' => 3])?>">Программа лояльности</a></li>
                    </ul>
                </div>
                <div class="inform">
	                <?php if ($page == 1): ?>
                        <?php if (!empty($orders)): ?>
                            <div class="orders">
                                <?php foreach ($orders as $order): ?>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left order">
                                    <div class="order-header">
                                        <span>Заказ №BY<?= $order['id'] ?> от <?= date('d.m.Y', $order['created_at']) ?> <?= $order['status'] ? 'доставлен' : 'обрабатывается' ?> <?= date('d.m.Y', $order['delivery_date']) ?>, <?= $order['delivery_time'] ?></span>
                                    </div>
                                    <div class="order-inf">
                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 text-left main-inf">
                                            <span>
                                                Продавец - "Скрэт Хаус"<br>
                                                Стоимость доставки - <?= $order['delivery_cost'] ?  $order['delivery_cost'] : 0 ?> р.<br>
                                                Способ оплаты - <?= PaymentsType::findOne($order['id_pay_type']) ? PaymentsType::findOne($order['id_pay_type'])-> name : '' ?><br>
                                                Адрес доставки - <?= $order['address'] ?>
                                            </span>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-left main-inf">
                                            <span>
                                                Получатель - <?= $order['name'] ?><br>
                                                <?= $order['telephone'] ?><br>
                                                <?= $order['email'] ?>
                                            </span>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left overall-sum">
                                            <span>К оплате <?= $order['sum'] ?>р. <?= ($order['id_promo'] ? '(использован промокод <i>'.\backend\models\Promo::findOne($order['id_promo'])->name.'</i> на скидку '.\backend\models\Promo::findOne($order['id_promo'])->discount.(\backend\models\Promo::findOne($order['id_promo'])->discount_type ? 'р.)' : '%)' ) : '')?></span>
                                        </div>

                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
                                            <div class="products">
                                                <?php
                                                    $products = \common\models\OrderItems::find()
                                                                    ->select('*')
                                                                    ->where(['=', 'id_order', $order['id']])
                                                                    ->asArray()
                                                                    ->all();
                                                ?>
                                                <?php foreach ($products as $product): ?>

                                                        <?php if ($product['is_scratbox'] == 1): ?>

                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product scratbox">
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 scratbox-header">
                                                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-left">
                                                                        <img class="product-img"  src="/img/scrat-box/open-box.png" alt="">
                                                                    </div>
                                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-left">
                                                                        <div class="product-name"><?= $product['name'] ?></div>
                                                                    </div>
                                                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-right">
                                                                        <div class="product-weight"><?= $product['weight'] * \frontend\models\Scratboxes::NUMBER_OF_PRODUCTS_IN_SCRATBOX * Yii::$app->params['min_scrat_box_weight'] ?> rp.</div>
                                                                    </div>
                                                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-right">
                                                                        <div class="price"><?= $product['sum'] ?>р.</div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 scratbox-options">

                                                                    <?php
                                                                        $options = \frontend\models\ScratboxesProducts::find()
                                                                                        ->select(['scratboxes_products.*', 'products.name as name'])
                                                                                        ->leftJoin('products','products.id = scratboxes_products.id_product')
                                                                                        ->where(['=','scratboxes_products.id_box', $product['id_scratbox']])
                                                                                        ->asArray()
                                                                                        ->all();

                                                                        $option_count = 1;
                                                                    ?>

                                                                    <?php foreach ($options as $option): ?>
                                                                        <div class="option">
                                                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-left">
                                                                                <div class="cell">Отсек <?= $option_count ?></div>
                                                                            </div>
                                                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-left">
                                                                                <div class="product-name"><?= $option['name'] ?></div>
                                                                            </div>
                                                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-right">
                                                                                <div class="product-weight"><?= $product['weight'] * $option['weight'] ?> rp.</div>
                                                                            </div>
                                                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-right">
                                                                                <div class="price"><?= $product['weight'] * \frontend\models\Scratboxes::getScratboxPrice() / \frontend\models\Scratboxes::NUMBER_OF_PRODUCTS_IN_SCRATBOX ?>р.</div>
                                                                            </div>
                                                                        </div>
                                                                        <?php $option_count++; ?>
                                                                    <?php endforeach; ?>

                                                                </div>
                                                            </div>

                                                        <?php else: ?>
                                                        <?php
                                                            $model = \common\models\Products::findOne($product['id_product']);
                                                            $main_image = $model->getImage();
                                                        ?>
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product">
                                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-left">
                                                                    <?= \yii\helpers\Html::img(($main_image->id ? '' : '/0').$main_image->getUrl('x40'), ['alt' => $product['name'], 'class' => 'product-img'])?>
                                                                </div>
                                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-left">
                                                                    <div class="product-name"><?= $product['name'] ?></div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-right">
                                                                    <div class="product-weight"><?= $product['weight'] ?> <?= \common\models\Products::findOne($product['id_product'])->special ? '' : 'rp.'?></div>
                                                                </div>
                                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-right">
                                                                    <div class="price"><?= $product['sum'] ?>p.</div>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
		                <?php else: ?>
                            <div class="empty-orders">
                                У вас пока нет заказов. Давайте прогуляемся по магазину!
                                <div class="button"><a href="<?= \yii\helpers\Url::to(['/products/orehi'])?>">Выбрать и купить</a></div>
                            </div>
		                <?php endif; ?>
                    <?php endif; ?>
                    <?php if ($page == 2): ?>
                        <div class="inputs">
	                        <?php $form = ActiveForm::begin(['id' => 'profile-form',
	                                                         'action' => ['/profile/save', 'id' => $user['id']]
	                        ]); ?>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">
                                    <div class="client-type">
                                        <a class="client-type-button"><span><?= ($user['client_type'] ? (($user['client_type'] == 0) ? 'Физическое лицо' : 'Юридическое лицо') : (($client_type == 0) ? 'Физическое лицо' : 'Юридическое лицо')) ?></span><img src="/img/icons/arrow-down.svg"></a>
                                        <div class="wrapper-drop">
                                            <ul class="client-type-down">
                                                <li id="fiz-person">Физическое лицо</li>
                                                <li id="ur-person">Юридическое лицо</li>
	                                            <?= $form->field($model_user, 'client_type')->hiddenInput(array('value' => ($user['client_type'] ? $user['client_type'] : $client_type)))->label(false); ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 first">
                                    <?= $form->field($model_user, 'name')->textInput(array('placeholder' => 'Имя', 'required' => true, 'class' => 'form-input', 'value' => $user['last_name'].' '.$user['first_name']))->label(false); ?>
                                    <?= $form->field($model_user, 'last_name')->hiddenInput(array('value' => $user['last_name']))->label(false); ?>
                                    <?= $form->field($model_user, 'first_name')->hiddenInput(array('value' => $user['first_name']))->label(false); ?>
                                </div>
                                <div class="birthday col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                    <?php
                                        echo Html::dropDownList( 'birth_day', $user['birthday'] ? date('d', $user['birthday']) : date('d'), $model_profile->getDaysList(), ['id' => 'birth_day', 'class' => 'user_birthday_select']);
                                        echo Html::dropDownList( 'birth_month', $user['birthday'] ? date('m', $user['birthday']) : date('m'), $model_profile->getMonthsList(), ['id' => 'birth_month', 'class' => 'user_birthday_select']);
                                        echo Html::dropDownList( 'birth_year', $user['birthday'] ? date('Y', $user['birthday']) : date('Y'), $model_profile->getYearsList(), ['id' => 'birth_year', 'class' => 'user_birthday_select']);
                                    ?>
                                    <input type="hidden" id="user-birthday-val" value="<?= $user['birthday'] ? date("Y-m-d", $user['birthday']) : '';?>">
                                </div>
                                <div class="row"><span></span></div>

                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                    <?= $form->field($model_user, 'email')->textInput(array('placeholder' => 'Email', 'required' => true, 'class' => 'form-input', 'value' => $user['email']))->label(false); ?>
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                    <?= $form->field($model_user, 'telephone')->textInput(array('placeholder' => 'Телефон', 'required' => true, 'class' => 'form-input', 'value' => $user['telephone']))->label(false); ?>
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                    <?= $form->field($model_user, 'address')->textInput(array('placeholder' => 'Адрес (ул., д., под., кв.)', 'required' => true, 'class' => 'form-input', 'value' => $user['address']))->label(false) ?>
                                </div>
                                <div class="ur-person-inputs">
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                        <?= $form->field($model_profile, 'jur_name')->textInput(array('placeholder' => 'Юридическое название', 'class' => 'form-input', 'value' => $user['jur_name']))->label(false) ?>
                                    </div>
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                        <?= $form->field($model_profile, 'jur_address')->textInput(array('placeholder' => 'Юридический адрес', 'class' => 'form-input', 'value' => $user['jur_address']))->label(false) ?>
                                    </div>
                                    <div class=" col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                        <?= $form->field($model_profile, 'bank')->textInput(array('placeholder' => 'Банк', 'class' => 'form-input', 'value' => $user['bank']))->label(false) ?>
                                    </div>
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                        <?= $form->field($model_profile, 'checking_account')->textInput(array('placeholder' => 'Расчётный счёт', 'class' => 'form-input', 'value' => $user['checking_account']))->label(false) ?>
                                    </div>
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                        <?= $form->field($model_profile, 'bank_code')->textInput(array('placeholder' => 'Код банка', 'class' => 'form-input', 'value' => $user['bank_code']))->label(false) ?>
                                    </div>
                                    <div class=" col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                        <?= $form->field($model_profile, 'unp')->textInput(array('placeholder' => 'УНП (девять цифр)', 'class' => 'form-input', 'value' => $user['unp']))->label(false) ?>
                                    </div>
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                        <?= $form->field($model_profile, 'director')->textInput(array('placeholder' => 'Директор (Сотрудник), ФИО', 'class' => 'form-input', 'value' => $user['director']))->label(false) ?>
                                    </div>
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                        <?= $form->field($model_profile, 'based')->textInput(array('placeholder' => 'На основании', 'class' => 'form-input', 'value' => $user['based']))->label(false) ?>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right save">
                                    <?= Html::submitButton('Сохранить', ['class'=>'save']) ?>
                                </div>
	                        <?php ActiveForm::end(); ?>
                            <div class="information">
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
		                            Имя:<br><?= $user['first_name'] != '' ? '<span class="data">'.$user['last_name'].' '.$user['first_name'].'</span>' : '<span style="color: #FF0000">не задано</span>'; ?>
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                    Дата рождения:<br><span class="data"><?= $user['birthday'] != '' ? '<span class="data">'.date("d.m.Y", $user['birthday']).'</span>' : '<span style="color: #FF0000">не задано</span>'; ?></span>
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                    Email:<br><span class="data"><?= $user['email'] != '' ? '<span class="data">'.$user['email'].'</span>' : '<span style="color: #FF0000">не задано</span>'; ?></span>
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                    Телефон:<br><span class="data"><?= $user['telephone'] != '' ? '<span class="data">'.$user['telephone'].'</span>' : '<span style="color: #FF0000">не задано</span>'; ?></span>
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                                    Адрес:<br><span class="data"><?= $user['address'] != '' ? '<span class="data">'.$user['address'].'</span>' : '<span style="color: #FF0000">не задано</span>'; ?></span>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right save">
                                    <button id="edit_private_inf" class="save">Редактировать</button>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($page == 3): ?>
                        <div class="loyalty">
                            <span>
                                <?= TextByAdmin::findOne(['page' => 'profile']) ? TextByAdmin::findOne(['page' => 'profile'])->text : '' ?>
                            </span>
                            <div class="discount">
                                <div class="discount-label">
                                    <b>Вы сделали заказы на <?= $user['overall_sum']?> рублей - ваш кэшбэк <?= User::getClientsDiscountPercent($user['id']) ?> %</b>
                                </div>
                                <table class="percent-table">
                                    <tr>
	                                    <?php foreach ($discount_percents_labels as $percents): ?>
                                            <td><span><?= $percents['percent'] ?>%</span></td>
	                                    <?php endforeach; ?>
                                    </tr>
                                </table>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: <?= (($user['overall_sum'] / $discount_sums_labels[count($discount_sums_labels)-2]['sum'] * 83) < 100 ? ($user['overall_sum'] / $discount_sums_labels[count($discount_sums_labels)-2]['sum'] * 83) : 100) ?>%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="200"></div>
                                    <table class="progress-table">
                                        <tr>
	                                        <?php for ($i = 0; $i < Profile::CELLS_COUNT; $i++): ?>
                                                <td></td>
                                            <?php endfor; ?>
                                        </tr>
                                    </table>
                                </div>
                                <table class="points-table">
                                    <tr>
                                        <?php foreach ($discount_sums_labels as $sum): ?>
                                            <td><span><?= $sum['sum'] ?> руб</span></td>
                                        <?php endforeach; ?>
                                    </tr>
                                </table>
                            </div>
                            <div class="discount-label">
                                На Вашем счету <b><?= $user['users_points']?></b> скрэтублей<br>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tbody>
                                        <?php foreach ($orders as $order): ?>
                                            <?php if ($order['status'] == Order::$status_array['STATUS_DELIVERED'] && $order['bonus_points_to_get'] > 0): ?>
                                                <tr>
                                                    <td class="text-left"><b>+<?= $order['bonus_points_to_get'] ?> скрэтублей</b></td>
                                                    <td class="text-center">Начислены за заказ</td>
                                                    <td class="text-right"><?= date('d.m.Y', $order['created_at']) ?></td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php else: ?>
    <div class="main-block">
        <div class="container">
            <div class="row">
                <div class="content text-center" style="padding-top: 100px;">
                    <h3>Для просмотра данной страницы авторизируйтесь</h3>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
