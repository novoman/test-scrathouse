<?php

/* @var $this yii\web\View */

use frontend\assets\AppAsset;
use yii\widgets\LinkPager;
use backend\models\TextByAdmin;

$this->title = 'Скрат-бокс — Скрэтхаус.бай';

$this->registerMetaTag([
	'name' => 'description',
	'content' => 'Только у scrathouse.by вы можеет собрать свой уникальный скрат-бокс — коробку с фиксированной ценой и несколькими продуктами.'
]);

AppAsset::register($this);
\frontend\assets\ScratBoxAsset::register($this);
?>

<div class="main-block">
    <div class="container">
        <div class="row">
            <div class="content">
                <div class="text"><?= TextByAdmin::findOne(['page' => 'scrat-box', 'type' => 1]) ? TextByAdmin::findOne(['page' => 'scrat-box', 'type' => 1])->text : '' ?></div>
                <div class="button"><a href="<?= \yii\helpers\Url::to(['/scrat-box-products'])?>">Собрать Бокс</a></div>
                <div class="promo"><?= TextByAdmin::findOne(['page' => 'scrat-box', 'type' => 2]) ? TextByAdmin::findOne(['page' => 'scrat-box', 'type' => 2])->text : '' ?></div>
            </div>
        </div>
    </div>
</div>
<div class="description">
    <div class="container">
        <div class="row">
            <div class="content-img">
                <div class="desc-img">
                    <img src="/img/scrat-box/open-box.png" alt="">
                </div>
            </div>
            <div class="content-description">
                <div class="box">
                    <div class="line-block">
                        <div class="box-description">
	                        <?= TextByAdmin::findOne(['page' => 'scrat-box', 'type' => 3]) ? TextByAdmin::findOne(['page' => 'scrat-box', 'type' => 3])->text : '' ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="collect-box"><a href="<?= \yii\helpers\Url::to(['/scrat-box-products'])?>">Собрать Бокс</a></div>
<div class="blocks-label">
    <span>Ваши любимые снеки и орехи</span>
</div>
<div class="blocks">
    <div class="container">
        <div class="row">
            <div class="wrapper-content">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id="proposed_products">
	                    <?php echo $this->render('scrat-box-popular-products', [
	                            'products' => $products,
                                'pages' => $pages,
                        ]);?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="show-more">
    <a>Посмотреть все</a>
</div>

<div class="product-popup"><a href="#product-popup-modal" class="my-modal"><span id="product-popup-modal-click"></span></a></div>
