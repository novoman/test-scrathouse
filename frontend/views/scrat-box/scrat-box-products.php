<?php

/* @var $this yii\web\View */

use frontend\assets\AppAsset;
use yii\widgets\Breadcrumbs;
use common\models\Category;
use yii\widgets\LinkPager;

$this->title = 'Собрать скрат-бокс — Скрэтхаус.бай';
AppAsset::register($this);
\frontend\assets\ScratBoxProductsAsset::register($this);

$breadcrumbs = Category::getTreeOfParents(Yii::$app->request->get('slug') ? Yii::$app->request->get('slug') : '');

foreach ($breadcrumbs as $slug => $name) {
	$this->params['breadcrumbs'][] = ['label' => $name, 'url' => ['/scrat-box/scrat-box-products', 'slug' => $slug]];
}

?>

<div id="scrat-box" class="scrat-box">
    <?php echo $this->render('scrat-box-client', compact('session')); ?>
</div>

<div class="main-block">
    <div class="container">
        <div class="row">
            <div class="breadcrumbs">
		        <?= Breadcrumbs::widget([
			        'homeLink' => ['label' => 'Товары', 'url' => '/scrat-box-products'],
			        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
			        'options' => [
				        'class' => false,
			        ],
		        ]) ?>
            </div>
            <div class="sort">
                <a href="#">Сортировать по
		            <?php if (Yii::$app->request->get('sort_type')): ?>
                        <b><?= (Yii::$app->request->get('sort_type') == 'price' ? 'цене' : 'популярности') ?></b>
		            <?php endif; ?>
                </a>
                <div class="wrapper-drop">
	                <?php if (Yii::$app->request->get('slug')): ?>
                        <div>
                            <a class="method" href="<?= \yii\helpers\Url::to(['/products/products', 'slug' => Yii::$app->request->get('slug') ? Yii::$app->request->get('slug') : Category::getStartCategory(), 'sort_type' => 'price'])?>">Цене</a>
                            <a class="method" href="<?= \yii\helpers\Url::to(['/products/products', 'slug' => Yii::$app->request->get('slug') ? Yii::$app->request->get('slug') : Category::getStartCategory(), 'sort_type' => 'hits'])?>">Популярности</a>
                        </div>
	                <?php else: ?>
                        <div>
                            <a class="method" href="<?= \yii\helpers\Url::to(['/products', 'sort_type' => 'price'])?>">Цене</a>
                            <a class="method" href="<?= \yii\helpers\Url::to(['/products', 'sort_type' => 'hits'])?>">Популярности</a>
                        </div>
	                <?php endif; ?>
                </div>
            </div>

            <div class="row"></div>

            <div class="wrapper-content">

                <div class="np-left wrapper-filter col-lg-2 col-md-3 col-sm-3">
                    <div class="filter">
                    <?php
			            // array for navbar
			            $items = array();
			            // get all root categories
			            $root_cats = Category::find()->where(['depth' => 0])->all();
			            // get selected categiry parent's id (for make it active)
			            $selected_cat_parent_root_id = Category::find()->where(['slug' =>  Category::getCorrectSlug(Yii::$app->request->get('slug'))])->one()->getParentId();

			            foreach ($root_cats as $root_cat) {
				            // for each root category search it's children
				            $cats = Category::getTree($root_cat->id, true);
				            //echo '<pre>';
				            //print_r($cats);

				            // for root in navbar as string
				            $main_temp_item = '';
				            // for it's children as categories
				            $temp_items = array();

				            foreach ($cats as $cat) {
					            if ($cat['depth'] == 0) {
						            $main_temp_item = '<li class="parent '.(($selected_cat_parent_root_id  == $cat['id'] && Yii::$app->request->get('slug')) ? "active" : "").'" >
                                                           <span class="'.(Yii::$app->request->get('slug') == $cat['slug'] ? 'selected' : '').'"><a href="/scrat-box-products/'.($cat['slug'] ? $cat['slug'] : Category::getStartCategory()).'">'.$cat['name'].'</a></span>';
					            }
					            else {
						            $temp_items[] = array(
							            'label' => $cat['name'],
							            'options' => [
								            'class' => (Yii::$app->request->get('slug') == $cat['slug'] ? 'selected' : ''),
							            ],
							            'url' => array(
								            '/scrat-box/scrat-box-products', 'slug' => $cat['slug'] ? $cat['slug'] : Category::getStartCategory()));
					            }
				            }
				            $items[] = array(
					            'label' => $main_temp_item,
					            'items' => $temp_items,
				            );
			            }


			            echo \yii\widgets\Menu::widget([
				            'items' => $items,
				            'encodeLabels' => false,
				            'hideEmptyItems' => false,
			            ]);
                    ?>
                    </div>
                </div>

                <div id="elements" class="np-right col-lg-10 col-md-9 col-sm-9 col-xs-12">
	                <?php if (!empty($products)): ?>
		                <?php \yii\widgets\Pjax::begin(); ?>
		                <?php foreach ($products as $product): ?>
			                <?php
                                $model = \common\models\Products::find()->where(['=', 'slug', $product['slug']])->one();
                                $main_image = $model->getImage();
			                ?>
                            <div class="item col-lg-4 col-md-6 col-sm-6 col-sx-12">
                                <div class="content">
	                                <?php if ($product['type']): ?>
                                        <div class="percent">
	                                        <?php if ($product['discount_price'] && $product['type'] == 2): ?>
                                                -<?= round(($product['price'] - $product['discount_price']) / $product['price'] * 100) ?>%
			                                <?php else: ?>
                                                Новинка
			                                <?php endif; ?>
                                        </div>
	                                <?php endif; ?>
					                <?= \yii\helpers\Html::img(($main_image->id ? '' : '/0').$main_image->getUrl('x1200'), ['alt' => $product['name'], 'class' => 'product-img'])?>
                                    <div class="name"><?=$product['name'] ?></div>
	                                <?php if ($product['country'] != ''): ?>
                                        <div class="country"><?= $product['country'] ?></div>
	                                <?php endif; ?>
                                    <div class="presence <?= ($product['presence'] ? 'in-stock': 'out-stock') ?>"><?= ($product['presence'] ? 'в наличии': 'нет в наличии') ?></div>
                                    <div class="fade">
                                        <div class="buttons">
                                            <div><a class="add-to-scrat-box" href="<?= \yii\helpers\Url::to(['/scrat-box/add-to-scrat-box', 'id' => $product['id']])?>" data-id="<?= $product['id'] ?>">Добавить в бокс</a></div>
                                            <div><a href="<?= \yii\helpers\Url::to(['/scrat-box/scrat-box-product-popup', 'slug' => $product['slug']])?>" data-slug="<?=$product['slug'] ?>" class="show-product-popup">Просмотреть</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
		                <?php endforeach;?>
                        <div class="row"></div>
                        <div class="my-pagination">
			                <?= LinkPager::widget([
				                'pagination' => $pages,
				                'maxButtonCount' => 5,
				                'registerLinkTags' => true,
				                'options' => [
					                'class' => false,
				                ],
				                'prevPageLabel'=>false,
				                'nextPageLabel'=>'<img src="/img/icons/arrow.svg">',
			                ]); ?>
                        </div>
		                <?php \yii\widgets\Pjax::end(); ?>
	                <?php else: ?>
                        <div class="text-center">
                            <h3>Товаров для Скрат-бокса в данной категории нет.</h3>
                        </div>
	                <?php endif; ?>
                    <div class="row"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="product-popup"><a href="#product-popup-modal" class="my-modal"><span id="product-popup-modal-click"></span></a></div>
