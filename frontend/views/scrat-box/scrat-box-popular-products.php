<?php if ($products) : ?>
	<?php \yii\widgets\Pjax::begin(); ?>
    <?php foreach ($products as $product): ?>
		<?php
            $model = \common\models\Products::find()->where(['=', 'slug', $product['slug']])->one();
            $main_image = $model->getImage();
		?>
        <div class="item col-lg-4 col-md-4 col-sm-6 col-sx-12">
            <div class="content">
	            <?php if ($product['type']): ?>
                    <div class="percent">
	                    <?php if ($product['discount_price'] && $product['type'] == 2): ?>
                            -<?= round(($product['price'] - $product['discount_price']) / $product['price'] * 100) ?>%
			            <?php else: ?>
                            Новинка
			            <?php endif; ?>
                    </div>
	            <?php endif; ?>
                <?= \yii\helpers\Html::img(($main_image->id ? '' : '/0').$main_image->getUrl('x1200'), ['alt' => $product['name'], 'class' => 'product-img'])?>
                <div class="name"><?=$product['name'] ?></div>
                <div class="fade">
                    <div class="buttons">
                        <div class="add-to-cart"><a href="<?= \yii\helpers\Url::to(['/cart/add', 'id' => $product['id']])?>" data-id="<?= $product['id'] ?>">Купить</a></div>
                        <div><a href="<?= \yii\helpers\Url::to(['/scrat-box/scrat-box-product-popup', 'slug' => $product['slug']])?>" data-id="<?=$product['id'] ?>" data-slug="<?=$product['slug'] ?>" class="show-product-popup">Просмотреть</a></div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach;?>
    <div class="row"></div>

    <div class="my-pagination">
		<?= \yii\widgets\LinkPager::widget([
			'pagination' => $pages,
			'maxButtonCount' => 5,
			'registerLinkTags' => true,
			'options' => [
				'class' => false,
			],
			'prevPageLabel'=> false,
			'nextPageLabel'=>'<img src="/img/icons/arrow.svg">',
		]); ?>
    </div>
	<?php \yii\widgets\Pjax::end(); ?>
<?php endif; ?>
