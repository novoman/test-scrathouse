<?php if ($product) : ?>
	<?php
        $model = \common\models\Products::find()->where(['=', 'slug', $product['slug']])->one();
        $main_image = $model->getImage();
	?>
    <div id="product-popup-modal" class="mfp-hide">
        <div class="content-img">
            <div class="img-frame">
                <div class="main-img">
	                <?php if ($product['type']): ?>
                        <div class="percent">
	                        <?php if ($product['discount_price'] && $product['type'] == 2): ?>
				                -<?= round(($product['price'] - $product['discount_price']) / $product['price'] * 100) ?>%
			                <?php else: ?>
                                Новинка
			                <?php endif; ?>
                        </div>
	                <?php endif; ?>
	                <?= \yii\helpers\Html::img(($main_image->id ? '' : '/0').$main_image->getUrl('x350'), ['alt' => $product['name'], 'class' => 'product-img'])?>
                </div>
            </div>
        </div>
        <div class="content-description">
            <div class="product">
                <div class="line-block">
                    <div class="product-name"><?=$product['name'] ?></div>
	                <?php if ($product['country']): ?>
                        <div class="country"><b>Страна производства:</b> <?=$product['country'] ?></div>
	                <?php endif; ?>
                    <?php if ($user['client_type'] == 1): ?>
                        <div class="articule"><?=$product['articule'] ?></div>
	                <?php endif; ?>
                    <div class="popup-description">
	                    <?=$product['description'] ?>
                    </div>
                </div>
                <div class="add-box">
                    <a class="add-to-scrat-box" href="<?= \yii\helpers\Url::to(['/scrat-box/add-to-scrat-box', 'id' => $product['id']])?>" data-id="<?= $product['id'] ?>">Добавить в бокс</a>
                </div>
            </div>
        </div>
    </div>
<?php else: ?>
    <?php throw new \yii\web\NotFoundHttpException('Страница не найдена',404); ?>
<?php endif; ?>
