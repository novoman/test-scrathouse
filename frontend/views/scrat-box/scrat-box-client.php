<?php
    use frontend\models\Scratboxes;
?>

<?php if (empty($session['current-scrat-box']['products'])): ?>

    <div class="scrat-box-header">
        <span>Создай свой бокс</span>
    </div>
    <div class="scrat-box-label">
        <span>Выбери <b><?=Scratboxes::NUMBER_OF_PRODUCTS_IN_SCRATBOX?></b> снека или ореха из ассортимента ниже, чтобы добавить в свой бокс</span>
    </div>
    <div class="scrat-box-images">
	    <?php for ($i = 0; $i < Scratboxes::NUMBER_OF_PRODUCTS_IN_SCRATBOX; $i++): ?>
            <div class="mini-img">
                <img class="product-img" src="/img/scrat-box/box.svg">
            </div>
        <?php endfor; ?>
    </div>

<?php elseif (count($session['current-scrat-box']['products']) < Scratboxes::NUMBER_OF_PRODUCTS_IN_SCRATBOX): ?>

    <div class="scrat-box-header">
        <span>Создай свой бокс</span>
    </div>
    <div class="scrat-box-label">
        <span>Выбери <b><?= Scratboxes::NUMBER_OF_PRODUCTS_IN_SCRATBOX - count($session['current-scrat-box']['products']) ?></b> снеков или орехов из ассортимента ниже, чтобы добавить в свой бокс</span>
    </div>
    <div class="scrat-box-images">
        <?php foreach ($session['current-scrat-box']['products'] as $id => $product):?>
	        <?php
                $model = \common\models\Products::findOne($product['id']);
                $main_image = $model->getImage();
	        ?>
            <div class="mini-img">
	            <?= \yii\helpers\Html::img(($main_image->id ? '' : '/0').$main_image->getUrl('x100'), ['class' => 'product-img'])?>
                <img data-id="<?= $id ?>" class="cancel-img" src="/img/icons/cancel.svg" onclick="deleteProductFromScratBox(this);">
            </div>
        <?php endforeach; ?>

		<?php for ($i = count($session['current-scrat-box']['products']); $i < Scratboxes::NUMBER_OF_PRODUCTS_IN_SCRATBOX; $i++): ?>
            <div class="mini-img">
                <img class="product-img" src="/img/scrat-box/box.svg">
            </div>
		<?php endfor; ?>
    </div>
    <div class="clear-scrat-box">
        <a href="#" onclick="clearScratBox();">Очистить бокс</a>
    </div>

<?php else: ?>

    <div class="scrat-box-header">
        <span>Бокс создан</span>
        <div class="hint"><i>добавьте бокс в корзину</i></div>
    </div>
    <div class="add-box-to-cart">
        <div class="add-box-to-cart-inner">
            <div class="change">
                <div class="line-block">
                    <img class="decrease-product-count" src="/img/icons/arrow-left.svg">
                </div>
                <div class="line-block">
                    <div id="product-count" class="product-count">1</div>
                </div>
                <div class="line-block">
                    <img class="increase-product-count" src="/img/icons/arrow-right.svg">
                </div>
            </div>
            <div class="box-name">
                <span>Бокс</span>
                <input id="box-name" name="box-name" type="text" value="Бокс" maxlength="10">
                <img src="/img/icons/pencil-beige.svg">
            </div>
            <div class="price"><?= Scratboxes::getScratboxPrice() ?> BYN</div>
            <div class="add-to-cart-button"><a href="#">Добавить бокс в корзину</a></div>
        </div>
    </div>
	<?php foreach ($session['current-scrat-box']['products'] as $id => $product):?>
		<?php
            $model = \common\models\Products::findOne($product['id']);
            $main_image = $model->getImage();
		?>
        <div class="mini-img">
			<?= \yii\helpers\Html::img(($main_image->id ? '' : '/0').$main_image->getUrl('x200'), ['class' => 'product-img'])?>
            <img data-id="<?= $id ?>" class="cancel-img" src="/img/icons/cancel.svg" onclick="deleteProductFromScratBox(this);">
        </div>
	<?php endforeach; ?>
    <div class="clear-scrat-box">
        <a href="#" onclick="clearScratBox();">Очистить бокс</a>
    </div>
<?php endif; ?>
