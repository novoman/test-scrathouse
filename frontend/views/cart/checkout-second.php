<?php

/* @var $this yii\web\View */

use frontend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Products;

$this->title = 'Оформление заказа — Скрэтхаус.бай';
AppAsset::register($this);
\frontend\assets\CheckoutSecondAsset::register($this);

?>
<div class="main-block">
    <div class="container">
        <div class="content">
            <div class="checkout-header">
                <div><span class="checkout-header-label">Оформление заказа</span></div>
                <div><span class="checkout-header-step">Шаг 2</span></div>
            </div>
			<?

                $form = ActiveForm::begin(['id' => 'checkout-main-form',
                                                 'action' => ['/cart/checkout-second'],
                ]);
			?>


            <div class="inputs">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 label-input">
                    <div><span>Юридическое название</span></div>
	                <?= $form->field($order, 'jur_name')->textInput(array('value' => $user['jur_name']))->label(false); ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 label-input">
                    <div><span>Юридический адрес(улица, дом, офис)</span></div>
	                <?= $form->field($order, 'jur_address')->textInput(array('value' => $user['jur_address']))->label(false); ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 label-input">
                    <div><span>Банк</span></div>
	                <?= $form->field($order, 'bank')->textInput(array('value' => $user['bank']))->label(false); ?>
                </div>
                <div class=" col-lg-6 col-md-6 col-sm-12 col-xs-12 label-input">
                    <div><span>Расчётный счёт</span></div>
	                <?= $form->field($order, 'checking_account')->textInput(array('value' => $user['checking_account']))->label(false); ?>
                </div>
                <div class=" col-lg-6 col-md-6 col-sm-12 col-xs-12 label-input">
                    <div><span>Код банка</span></div>
	                <?= $form->field($order, 'bank_code')->textInput(array('value' => $user['bank_code']))->label(false); ?>
                </div>
                <div class=" col-lg-6 col-md-6 col-sm-12 col-xs-12 label-input">
                    <div><span>УНП (девять цифр)</span></div>
	                <?= $form->field($order, 'unp')->textInput(array('value' => $user['unp']))->label(false); ?>
                </div>
                <div class=" col-lg-6 col-md-6 col-sm-12 col-xs-12 label-input">
                    <div><span>Директор (Сотрудник), ФИО</span></div>
	                <?= $form->field($order, 'director')->textInput(array('value' => $user['director']))->label(false); ?>
                </div>
                <div class=" col-lg-6 col-md-6 col-sm-12 col-xs-12 label-input">
                    <div><span>На основании</span></div>
	                <?= $form->field($order, 'based')->textInput(array('value' => $user['based']))->label(false); ?>
                </div>
            </div>


			<?php ActiveForm::end();?>
        </div>

        <div id="cart-checkout">
			<?php echo $this->render('cart-checkout', compact('session'));?>
        </div>

		<?php if (!empty($session['cart'])): ?>
            <div class="purchase">
                <a id="zakaz" href="#" onclick="$('#checkout-main-form').submit();" >Оформить заказ</a>
            </div>
		<?php endif; ?>
    </div>
</div>
