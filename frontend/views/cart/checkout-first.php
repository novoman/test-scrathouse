<?php

/* @var $this yii\web\View */

use frontend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Products;
use yii\web\View;

$this->title = 'Оформление заказа — Скрэтхаус.бай';
AppAsset::register($this);
\frontend\assets\CheckoutFirstAsset::register($this);
setlocale(LC_TIME, 'ru_RU.UTF-8');

// get shops
$shopList = \backend\models\Shops::find()->asArray()->all();

// Вытягиваем список магазинов из базы и записываем в JS переменную shopList
$shops = "";
foreach($shopList as $key => $value) {
	$shops .= "
          {
            'coordinates': [".$shopList[$key]['coordinante_X'].", ".$shopList[$key]['coordinante_Y']."],
            'city_name': '".$shopList[$key]['city']."',
            'address': '".$shopList[$key]['address']."',
            'name': '".$shopList[$key]['name']."',
            'work_time': '".$shopList[$key]['work_time']."',
            'telephone': '".$shopList[$key]['telephone']."'
          },
	";
}
$this->registerJs("
    // Список городов и магазинов в них
    var shopList = [
      {      
        'shops': [
            ".$shops."
        ]
      }
    ];
", View::POS_HEAD);
$this->registerJs("
    $('#shops-popup-modal').addClass('mfp-hide');
", View::POS_LOAD);

?>
<div class="main-block">
    <div class="container">
        <div class="content">
            <div class="checkout-header">
                <div><span class="checkout-header-label">Оформление заказа</span></div>
                <div><span class="checkout-header-step">Шаг 1</span></div>
            </div>
            <?php
                $action = ($user['client_type'] == 1) ? '/cart/checkout-second' : '/cart/checkout-first';
                $form = ActiveForm::begin(['id' => 'checkout-main-form',
                                                 'action' => [$action],
                ]);
            ?>

                <div class="inputs col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 label-input">
                        <div><span><br>Имя</span></div>
                        <?= $form->field($order, 'name')->textInput(array('value' => $user ? ($user['last_name'].' '.$user['first_name']) : $user['name']))->label(false); ?>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 label-input">
                        <div><span>Адрес (город, улица, дом, квартира, подъезд, этаж)</span></div>
	                    <?= $form->field($order, 'address')->textInput(array('value' => $user['address'], 'placeholder' => 'Адрес (город,улица,дом,квартира,подъезд)'))->label(false); ?>
	                    <input type="hidden" id="log_in_user_address" value="<?= $user['address'] ?>">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 label-input">
                        <div><span>Телефон</span></div>
	                    <?= $form->field($order, 'telephone')->textInput(array('value' => $user['telephone'], 'placeholder' => '+375291234567'))->label(false); ?>
                    </div>
                    <div class=" col-lg-6 col-md-6 col-sm-12 col-xs-12 label-input">
                        <div><span>Электронная почта</span></div>
	                    <?= $form->field($order, 'email')->textInput(array('value' => $user['email'], 'placeholder' => 'example@example.com'))->label(false); ?>
                    </div>
                </div>

                <div class="delivery col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="delivery-type col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <span>Способы доставки</span>
                        <div class="delivery-type-buttons col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php foreach (\common\models\Order::getDeliveryTypeArray() as $key => $value): ?>
                                <div data-id="<?= $key ?>" class="delivery-type-button delivery-type-button-not-selected">
                                    <a class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><?= $value['name'] ?></a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <?= $form->field($order, 'delivery_type')->hiddenInput(['id' => 'delivery_type', 'value' => '1'])->label(false); ?>
                    </div>

                    <div class="delivery-time col-lg-6 col-md-12 col-sm-12 col-xs-12">
                        <span>Время доставки</span>
                        <div class="delivery-time-buttons col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="delivery-time-button delivery-time-button-not-selected col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <?php
                                    // current date
                                    $current_date = strftime('%A, %d %B');
                                    $encoding = 'UTF-8';
                                ?>
                                <?php
                                    echo $form->field($order, 'delivery_date')->widget(\kartik\date\DatePicker::classname(), [
	                                    'type' => \kartik\date\DatePicker::TYPE_BUTTON,
                                        'name' => 'delivery_date',
	                                    'value' => date('Y-m-d'),
	                                    'buttonOptions' => [
                                            'class' => 'col-lg-12 col-md-12 col-sm-12 col-xs-12',
                                            'label' => '<a id="delivery-date" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">'. 'Дата<img src="/img/icons/arrow-down.svg"></a>',
                                        ],
                                        'pluginOptions' => [
                                            'autoclose'=> true,
                                            'format' => 'yyyy-mm-dd',
                                            'todayHighlight' => true,
                                            'startDate' => "0d"
                                        ],
                                    ])->label(false);
                                ?>
                            </div>
                            <div class="delivery-time-button delivery-time-button-not-selected col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <a id="delivery-time" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">Время<img src="/img/icons/arrow-down.svg"></a>

                                <div class="ui-timepicker-wrapper col-lg-12 col-md-12 col-sm-12 col-xs-12" tabindex="-1">
                                    <ul class="ui-timepicker-list">
                                        <?php for ($i = 11; $i < 22; $i++): ?>
                                            <li data-id="<?= $i ?>"  class="ui-timepicker"><?php echo $i.':00 - '.($i + 1).':00'; ?></li>
                                        <?php endfor; ?>
                                    </ul>
                                </div>
	                            <?= $form->field($order, 'delivery_time')->hiddenInput(['id' => 'delivery_time'])->label(false); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="order_index inputs col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 label-input">
                        <div><span>Почтовый индекс</span></div>
                        <input type="text" id="order-index" class="form-control" name="Order[index]" aria-required="true" aria-invalid="false">
                    </div>
                </div>

                <div class="payment-type col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <span>Способ оплаты</span>
                    <div class="payment-type-buttons col-lg-12 col-md-12 col-sm-12 col-xs-12">
	                    <?php foreach ($pay_types as $pay_type): ?>
                            <div data-id="<?= $pay_type['id'] ?>" class="payment-type-button payment-type-button-not-selected">
                                <a class="col-lg-3 col-md-6 col-sm-12 col-xs-12"><?= $pay_type['name'] ?></a>
                            </div>
                        <?php endforeach; ?>
                    </div>
	                <?= $form->field($order, 'id_pay_type')->hiddenInput(['id' => 'id_pay_type', 'value' => '1'])->label(false); ?>
                </div>


                <div class="comment col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div><span>Комментарий</span></div>
                    <div class="textarea-div">
	                    <?= $form->field($order, 'comment')->textarea(['class' => false, 'rows' => 5,])->label(false); ?>
                    </div>
                </div>
            <?php ActiveForm::end();?>
        </div>

        <div id="cart-checkout">
	        <?php echo $this->render('cart-checkout', compact('session'));?>
        </div>
	    <?php if (!empty($session['cart'])): ?>
            <div class="purchase">
                <a id="zakaz" href="#" onclick="check_form();"><?php echo ($user['client_type'] == 1) ? 'Далее' : 'Оформить заказ'; ?></a>
            </div>
        <?php endif; ?>
    </div>
</div>

<div class="shops-popup"><a href="#shops-popup-modal" class="my-modal"><span id="shops-popup-modal-click"></span></a></div>
<div id="shops-popup-modal" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 content-shops">
        <div class="shops">
            <div class="line-block">
                <div class="shops-label">Пункты самовывоза<br><h6>(выберите магазин из списка ниже)</h6></div>
				<?php foreach ($shopList as $key => $value): ?>
                    <div id="<?= $value['id'] ?>" class="shop not-selected">
                        <span> <?= $value['city'] ?>, <?= $value['address'] ?></span><br>
						<?= $value['work_time'] ?>
                    </div>
				<?php endforeach; ?>
            </div>
            <div class="shop-choose">
                <a href="#">Выбрать этот пункт</a>
            </div>
        </div>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div id="map"></div>
    </div>
</div>
