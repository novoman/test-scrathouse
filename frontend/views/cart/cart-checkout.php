<?php
    use common\models\Products;
    use frontend\models\Scratboxes;
    use common\models\User;
    use common\models\Order;
    use backend\models\AdminNumbers;
    use frontend\models\Cart;
    use yii\web\View;

    $user = User::findOne(Yii::$app->user->id);
?>
<div class="content-cart">
    <?php if(!empty($session['cart'])): ?>

	    <?php foreach ($session['cart'] as $id => $product): ?>
		    <?php if ($id == 'scrat-box'): ?>
			    <?php foreach ($product as $key => $value): ?>
                    <div class="product">
                        <div class="first-block">
                            <div class="line-block">
                                <img data-id="<?= $key ?>" data-scratbox="1" class="remove-product-img" src="/img/icons/cancel.svg" onclick="deleteProductCheckout(this);">
                            </div>
                            <div class="line-block">
                                <div class="img-frame"></div>
                                <img class="product-img" src="/img/scrat-box/open-box.png">
                            </div>
                            <div class="line-block">
                                <div class="product-name"><?=$value['name'] ?></div>
                            </div>
                        </div>
                        <div class="second-block">
                            <div class="change">
                                <div class="line-block">
                                    <img data-id="<?= $key ?>" data-scratbox="1" class="decrease-product-weight" src="/img/icons/arrow-left.svg" onclick="decreaseProductWeightCheckout(this);">
                                </div>
                                <div class="line-block">
                                    <div class="product-weight"><?=$value['weight'] ?></div>
                                </div>
                                <div class="line-block">
                                    <img data-id="<?= $key ?>" data-scratbox="1" class="increase-product-weight" src="/img/icons/arrow-right.svg" onclick="increaseProductWeightCheckout(this);">
                                </div>
                            </div>
                            <div class="line-block">
                                <div class="price price-special"><?=$value['weight'] * Scratboxes::getScratboxPrice() ?>р.</div>
                            </div>
                        </div>
                    </div>
			    <?php endforeach; ?>
		    <?php else: ?>
			    <?php
			    $model = \common\models\Products::find()->where(['=', 'slug', $product['slug']])->one();
			    $main_image = $model->getImage();
			    ?>
                <div class="product">
                    <div class="first-block">
                        <div class="line-block">
                            <img data-id="<?= $id ?>" class="remove-product-img" src="/img/icons/cancel.svg" onclick="deleteProductCheckout(this);">
                        </div>
                        <div class="line-block">
                            <div class="img-frame"></div>
						    <?= \yii\helpers\Html::img(($main_image->id ? '' : '/0').$main_image->getUrl('x40'), ['alt' => $product['name'], 'class' => 'product-img'])?>
                        </div>
                        <div class="line-block">
                            <div class="product-name">
                                <a href="<?= \yii\helpers\Url::to(['/product/'.$product['slug']])?>">
								    <?=$product['name'] ?>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="second-block">
                        <div class="change">
                            <div class="line-block">
                                <img data-id="<?= $id ?>"  class="decrease-product-weight" src="/img/icons/arrow-left.svg" onclick="decreaseProductWeightCheckout(this);">
                            </div>
                            <div class="line-block">
                                <div class="product-weight"><?=$product['weight'] ?>  <?= Products::findOne($id)->special ? '' : 'rp.'?></div>
                            </div>
                            <div class="line-block">
                                <img data-id="<?= $id ?>"  class="increase-product-weight" src="/img/icons/arrow-right.svg" onclick="increaseProductWeightCheckout(this);">
                            </div>
                        </div>
                        <div class="line-block">
	                        <?php if (!empty($product['price_before'])): ?>
                                <div class="price <?= Products::findOne($id)->special ? ' price-special ' : '' ?>"><s><?=$product['price_before'] * ($product['weight'] / Products::getMinProductWeight($id)) ?>р.</s><br><?=$product['price'] * ($product['weight'] / Products::getMinProductWeight($id)) ?>р.</div>
	                        <?php else: ?>
                                <div class="price <?= Products::findOne($id)->special ? ' price-special ' : '' ?>"><?=$product['price'] * ($product['weight'] / Products::getMinProductWeight($id)) ?>р.</div>
	                        <?php endif; ?>
                        </div>
                    </div>
                </div>
		    <?php endif; ?>
	    <?php endforeach;?>

        <div class="delivery-cost">
            <span class="delivery-cost-label">Стоимость доставки</span>
            <span class="delivery-cost-sum"><?= $session['cart.delivery-cost'] ?>р.</span>
        </div>

        <?php
            $promo = \backend\models\Promo::findOne($session['promo']);

            $overall_sum =
                ($promo ?
                    ($promo->discount_type ?
                        (
                            ($session['cart.sum'] > $promo->discount) ?
                                ($session['cart.sum'] - $promo->discount)
                                : 1
                        )
                        : ($session['cart.sum'] - round(($session['cart.sum'] - Cart::findScratBoxesSum()) / 100 * $promo->discount, 2))
                    )
                    : $session['cart.sum']
                )  + $session['cart.delivery-cost'] - round($session['cart.points'] / Order::getOnePointCost(), 2);
        ?>

	    <?php if ($user && !$session['promo.used']): ?>
            <?php if ($user->users_points): ?>
                <div class="users-points">
                    У вас накоплено <i><?= $user->users_points ?></i> скрэтублей <i>(курс: <?= Order::getOnePointCost() ?> скрэтубль = 1 BYN)</i>. Вы можете оплатить ими до
                    <i><?= AdminNumbers::findOne(['name' => 'max-percent-for-users-points']) ? AdminNumbers::findOne(['name' => 'max-percent-for-users-points'])->value : 1; ?>%</i>
                    от суммы заказа.
                    <br><br>
                    Использовать бонусы:
                </div>
                <div class="change-points">
                    <div class="line-block">
                        <img class="decrease-points-count" src="/img/icons/arrow-left.svg" onclick="decreaseBonusPointsCheckout();">
                    </div>
                    <div class="line-block">
                        <input id="points-count" title="points" class="points-count" value="<?= $session['cart.points'] ?>">
                    </div>
                    <div class="line-block">
                        <img class="increase-points-count" src="/img/icons/arrow-right.svg" onclick="increaseBonusPointsCheckout();">
                    </div>
                </div>
            <?php endif; ?>
        <?php endif; ?>

        <div class="overall">
            <span class="overall-label">Итого:</span>
            <span id="overall-sum" class="overall-sum"><?= $overall_sum ?>р.</span>
        </div>

        <div class="promo-buttons">
            <?php if (!$session['promo.used']): ?>
                <div class="promo-input">
                    <input id="promo-input" type="text" placeholder="Промокод">
                </div>
                <div class="promo">
                    <a href="#" onclick="applyPromo();">Применить</a>
                </div>
            <?php elseif ($session['promo.used'] && $promo): ?>
                <div class="promo-used">
                    Скидка <b><?= $promo->discount.($promo->discount_type ? 'р.' : '% ('.($session['cart.sum'] - $overall_sum).'р.)') ?></b> по промокоду <i><?= $promo->name ?></i>
                    <br>(не распространяется на скрат-боксы)
                    <div>
                        <a href="#" onclick="rollbackPromo();">Отменить промокод</a>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="clear-cart" onclick="clearCheckout();">
            <a href="#" >Очистить корзину</a>
        </div>
    <?php else: ?>
        <div class="empty-cart">
            <span>Ваша корзина пуста</span>
        </div>
    <?php endif; ?>
</div>
