<?php

/* @var $this yii\web\View */

use frontend\assets\AppAsset;
use common\models\Products;
use frontend\models\Scratboxes;

$this->title = 'Корзина — Скрэтхаус.бай';
AppAsset::register($this);
\frontend\assets\CartAsset::register($this);
?>

<div class="main-block">
    <div class="container">
        <div class="row">
	        <?php if(!empty($session['cart'])): ?>
            <div class="content">
                <div class="cart-header">Корзина</div>

	            <?php foreach ($session['cart'] as $id => $product): ?>
		            <?php if ($id == 'scrat-box'): ?>
			            <?php foreach ($product as $key => $value): ?>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product scratbox">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 scratbox-header">
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left imgs">
                                        <img data-id="<?= $key ?>" data-scratbox="1" class="remove-product-img" src="/img/icons/cancel.svg" onclick="deleteProductCartPage(this);">
                                        <div class="img-frame"></div>
                                        <img class="product-img"  src="/img/scrat-box/open-box.png" alt="">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
                                        <div class="product-name"><?= $value['name'] ?></div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">
                                        <img data-id="<?= $key ?>" data-scratbox="1" class="decrease-product-weight arrow" src="/img/icons/arrow-left.svg" onclick="decreaseProductWeightCartPage(this);">
                                        <span class="product-weight"><?= $value['weight']?></span>
                                        <img data-id="<?= $key ?>" data-scratbox="1" class="increase-product-weight arrow" src="/img/icons/arrow-right.svg" onclick="increaseProductWeightCartPage(this);">
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">
                                        <div class="price"><?= $value['weight'] * Scratboxes::getScratboxPrice()?>р.</div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 scratbox-options">
                                    <?php
                                    $options = \frontend\models\ScratboxesProducts::find()
                                                                                  ->select(['scratboxes_products.*', 'products.name as name'])
                                                                                  ->leftJoin('products','products.id = scratboxes_products.id_product')
                                                                                  ->where(['=','scratboxes_products.id_box', $key])
                                                                                  ->asArray()
                                                                                  ->all();
                                    $option_count = 1;
                                    ?>

                                    <?php foreach ($options as $option): ?>
                                        <div class="option">
                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left">
                                                <div class="cell">Отсек <?= $option_count ?></div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
                                                <div class="product-name"><?= $option['name'] ?></div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">
                                                <div class="product-weight"><?= $value['weight'] * $option['weight'] ?> гр.</div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">
                                                <div class="price"><?= $value['weight'] * Scratboxes::getScratboxPrice() / Scratboxes::NUMBER_OF_PRODUCTS_IN_SCRATBOX ?>р.</div>
                                            </div>
                                        </div>
                                        <?php $option_count++; ?>
                                    <?php endforeach; ?>

                                </div>
                            </div>
			            <?php endforeach; ?>
		            <?php else: ?>
			            <?php
                            $model = \common\models\Products::find()->where(['=', 'slug', $product['slug']])->one();
                            $main_image = $model->getImage();
			            ?>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-left imgs">
                                <img data-id="<?= $id ?>"  class="remove-product-img" src="/img/icons/cancel.svg" onclick="deleteProductCartPage(this);">
                                <div class="img-frame"></div>
	                            <?= \yii\helpers\Html::img(($main_image->id ? '' : '/0').$main_image->getUrl('x40'), ['alt' => $product['name'], 'class' => 'product-img'])?>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
                                <div class="product-name">
                                    <a href="<?= \yii\helpers\Url::to(['/product/'.$product['slug']])?>"><?=$product['name'] ?></a>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right ">
                                <img data-id="<?= $id ?>" class="decrease-product-weight arrow" src="/img/icons/arrow-left.svg" onclick="decreaseProductWeightCartPage(this);">
                                <span class="product-weight"><?=$product['weight'] ?><?= Products::findOne($id)->special ? '' : 'гр.'?></span>
                                <img data-id="<?= $id ?>" class="increase-product-weight arrow" src="/img/icons/arrow-right.svg" onclick="increaseProductWeightCartPage(this);">
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-right">
	                            <?php if (!empty($product['price_before'])): ?>
                                    <div class="price <?= Products::findOne($id)->special ? ' price-special ' : '' ?>"><s><?=$product['price_before'] * ($product['weight'] / Products::getMinProductWeight($id)) ?>р.</s><br><?=$product['price'] * ($product['weight'] / Products::getMinProductWeight($id)) ?>р.</div>
	                            <?php else: ?>
                                    <div class="price <?= Products::findOne($id)->special ? ' price-special ' : '' ?>"><?=$product['price'] * ($product['weight'] / Products::getMinProductWeight($id)) ?>р.</div>
	                            <?php endif; ?>
                            </div>
                        </div>
		            <?php endif; ?>
	            <?php endforeach;?>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 overall">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 overall-label">
                        <span>Итого:</span>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 overall-sum">
                        <span><?=$session['cart.sum'] ?>р.</span>
                    </div>
                    <div class="clear-cart text-right">
                        <a href="#" onclick="clearCartPage();">Очистить корзину</a>
                    </div>
                </div>
            </div>
            <div class="buttons">
                <div class="continue">
                    <a href="<?= \yii\helpers\Url::to(['/products/'.\common\models\Category::getStartCategory()])?>">Продолжить покупки</a>
                </div>
                <div class="purchase">
                    <a href="<?= \yii\helpers\Url::to(['/cart/checkout-first'])?>" >Оформить заказ</a>
                </div>
            </div>
	        <?php else: ?>
                <div class="content">
                    <div class="empty-cart">
                        <span>Ваша корзина пуста :(</span>
                        <div class="go-on">
                            <a href="<?= \yii\helpers\Url::to(['/products/'.\common\models\Category::getStartCategory()])?>">Продолжить покупки</a>
                        </div>
                    </div>
                </div>
	        <?php endif; ?>
        </div>
    </div>
</div>
