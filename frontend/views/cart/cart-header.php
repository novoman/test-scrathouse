<a href="<?= \yii\helpers\Url::to(['/cart/cart'])?>"><img src="/img/icons/s-cart.svg" alt=""></a>
<div id="cart-items-count" class="items-count" style="display: <?= empty($_SESSION['cart']) ? 'none' : 'block' ?>;">
	<?= empty($_SESSION['cart']) ? '0' : count($_SESSION['cart']) ?>
</div>
