<?php

/* @var $this yii\web\View */
/* @var $order \common\models\Order */

use backend\models\Promo;
use frontend\assets\AppAsset;
use common\models\Products;
use frontend\models\Scratboxes;

$this->title = 'Переадресация на платежный сервис';
AppAsset::register($this);
\frontend\assets\CartAsset::register($this);

/** @var \common\models\OrderItems $items */
$items = $order->getOrderItems()->all();

try {
    $wsb_storeid = \Yii::$app->params['webPayStoreIdErip'];
    $wsb_order_num = $order->unique_token;
    $wsb_test = \Yii::$app->params['webPayTestMode'];
    $wsb_currency_id = 'BYN';
//    $wsb_seed = \Yii::$app->security->generateRandomString(10);
    $wsb_seed = random_int(1000000000, 9999999999);
    $wsb_total = null;
    $secretKey = \Yii::$app->params['webPaySecretKeyErip'];

} catch (Exception $e) {
    \Yii::error($e->getMessage());
    return false;
}
?>

<div class="text-center" style="margin: 200px 0;">
    <h2>Идет перенаправление на сервис оплаты...</h2>
</div>

<form action="<?= Yii::$app->params['webPayRequestURL'] ?>" method="post" id="webpay-form">
    <input type="hidden" name="*scart">
    <input type="hidden" name="wsb_version" value="2">
    <input type="hidden" name="wsb_storeid" value="<?= $wsb_storeid ?>" >
    <input type="hidden" name="wsb_order_num" value="<?= $wsb_order_num ?>" >
    <input type="hidden" name="wsb_test" value="<?= $wsb_test ?>" >
    <input type="hidden" name="wsb_currency_id" value="<?= $wsb_currency_id ?>" >
    <input type="hidden" name="wsb_seed" value="<?= $wsb_seed ?>">
    <input type="hidden" name="wsb_customer_name" value="Майоров Евгений Борисович">

<?php
	$overall_sum = 0;

	/** @var \common\models\OrderItems $item */
	foreach ($items as $item): ?>

		<?php if ((int) $item->is_scratbox === 1): ?>
            <input type="hidden" name="wsb_invoice_item_name[]" value="<?= str_replace('"', '', $item->name) ?>">
            <input type="hidden" name="wsb_invoice_item_quantity[]" value="1">
            <input type="hidden" name="wsb_invoice_item_price[]" value="<?= $item->sum ?>">
			<?php $overall_sum += $item->sum; ?>
		<?php elseif ((int) $item->special === 0): ?>
            <input type="hidden" name="wsb_invoice_item_name[]" value="<?= str_replace('"', '', $item->name) . ' ' . $item->weight . 'гр.' ?>">
            <input type="hidden" name="wsb_invoice_item_quantity[]" value="1">
            <input type="hidden" name="wsb_invoice_item_price[]" value="<?= $item->sum ?>">
			<?php $overall_sum += $item->sum; ?>
		<?php elseif ((int) $item->special === 1): ?>
            <input type="hidden" name="wsb_invoice_item_name[]" value="<?= str_replace('"', '', $item->name) ?>">
            <input type="hidden" name="wsb_invoice_item_quantity[]" value="<?= $item->weight ?>">
            <input type="hidden" name="wsb_invoice_item_price[]" value="<?= $item->sum ?>">
			<?php $overall_sum += $item->weight * $item->sum; ?>
		<?php endif; ?>

	<?php endforeach; ?>

	<?php if ((int) $order->delivery_cost): ?>
        <input type="hidden" name="wsb_shipping_name" value="Стоимость доставки" >
        <input type="hidden" name="wsb_shipping_price" value="<?= $order->delivery_cost ?>" >
	<?php endif; ?>

	<?php if ((int) $order->id_promo): ?>
		<?php
            $promo = Promo::findOne($order->id_promo);
            $promo_sum = $overall_sum + $order->delivery_cost - $order->sum;
		?>
        <input type='hidden' name='wsb_discount_name' value='Промокод'>
        <input type='hidden' name='wsb_discount_promo_code' value='<?= $promo->name ?>'>
        <input type='hidden' name='wsb_discount_price' value='<?= $promo_sum ?>'>
	<?php endif; ?>

    <input type="hidden" name="wsb_total" value="<?= $order->sum ?>" >
    <input type="hidden" name="wsb_signature" value="<?= sha1($wsb_seed . $wsb_storeid. $wsb_order_num . $wsb_test. $wsb_currency_id . $order->sum . $secretKey) ?>" >
    <input type='hidden' name='wsb_return_url' value='https://scrathouse.by/compete-order'>
</form>

<?php $this->registerJs("
    $(document).ready(function () {
        $('#webpay-form').submit();
    });
");?>
