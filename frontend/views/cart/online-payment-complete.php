<?php

/* @var $this yii\web\View */

use frontend\assets\AppAsset;
use common\models\Products;
use frontend\models\Scratboxes;

$this->title = 'Оплата прошла успешно';
AppAsset::register($this);
\frontend\assets\CartAsset::register($this);

?>

<div class="text-center" style="margin: 200px 0;">
    <h2>Оплата прошла успешно, ваш заказ готовится.</h2>
</div>