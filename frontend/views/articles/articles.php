<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\Articles */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
use frontend\assets\AppAsset;
use common\models\Articles;

AppAsset::register($this);
\frontend\assets\ArticlesAsset::register($this);

$count = count($model);

$this->title = 'Статьи';
?>

<div class="wrapper-white">
    <div class="wrapper-head">
        <div class="container text-center">
            <div class="title">СТАТЬИ</div>
        </div>
    </div>

    <?php if ($count): ?>
    <div class="content">
        <div class="container">

            <?php $altCount = $count > Articles::ARTICLES_PER_PAGE ? Articles::ARTICLES_PER_PAGE : $count ?>

            <div class="items-wrapper">
                <?php for($i = 0; $i < $altCount; $i++): ?>
                <div class="item">
                    <div class="image">
                        <?php
                            $main_image = $model[$i]->getImage();
                        ?>
                        <?= \yii\helpers\Html::img(($main_image->id ? '' : '/0').$main_image->getUrl('x150'), ['alt' => $model[$i]->title])?><br><br>
                    </div>

                    <div class="content">
                        <a href="<?= \yii\helpers\Url::to(['/articles/articles-item', 'slug' => $model[$i]->slug])?>">
                            <h4><?= $model[$i]->title ?></h4>
                            <p><?= $model[$i]->preview ?></p>
                        </a>
                    </div>
                </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>

        <?php if ($count > 5): ?>
            <div class="content">
                <div class="container">
                    <div class="items-wrapper" style="margin-top: 0">
                        <?php for($i = 5; $i < $count; $i++): ?>
                            <div class="item">
                                <div class="image">
                                    <?php
                                        $main_image = $model[$i]->getImage();
                                    ?>
                                    <?= \yii\helpers\Html::img(($main_image->id ? '' : '/0').$main_image->getUrl('x150'), ['alt' => $model[$i]->title])?><br><br>
                                </div>

                                <div class="content">
                                    <a href="<?= \yii\helpers\Url::to(['/articles/articles-item', 'slug' => $model[$i]->slug])?>">
                                        <h4><?= $model[$i]->title ?></h4>
                                        <p><?= $model[$i]->preview ?></p>
                                    </a>
                                </div>
                            </div>
                        <?php endfor; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <?php else: ?>
        <p class="text-center">Статей нет</p>
    <?php endif; ?>

    <div class="container" style="margin-top: 60px;">
        <div class="my-pagination">
            <?= LinkPager::widget([
                'pagination' => $pages,
                'maxButtonCount' => 5,
                'registerLinkTags' => true,
                'options' => [
                    'class' => false,
                ],
                'prevPageLabel'=>false,
                'nextPageLabel'=>'<img src="/img/icons/arrow.svg">',
            ]); ?>
        </div>
    </div>
</div>
