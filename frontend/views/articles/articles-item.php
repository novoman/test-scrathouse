<?php

/* @var $item \common\models\Articles */
/* @var $last \common\models\Articles */

use yii\widgets\LinkPager;
use frontend\assets\AppAsset;

AppAsset::register($this);
\frontend\assets\ArticlesAsset::register($this);

$this->title = $item->title;

?>

<div class="wrapper-white">
    <div class="item-content">
        <div class="preview-image">
            <?php
                $main_image = $item->getImage();
            ?>
            <?= \yii\helpers\Html::img(($main_image->id ? '' : '/0').$main_image->getUrl('x150'), ['alt' => $item->title])?><br><br>
        </div>
        <h1><?= $item->title ?></h1>
        <div class="content">
            <?= $item->text ?>
        </div>
    </div>

    <?php if ($last): ?>
    <div class="content" style="padding-bottom: 130px;">
        <div class="container">
            <div class="other-title">Другие статьи</div>

            <div class="items-wrapper">
                <?php foreach ($last as $el): ?>
                    <div class="item">
                        <div class="image">
                            <?php
                                $main_image = $el->getImage();
                            ?>
                            <?= \yii\helpers\Html::img(($main_image->id ? '' : '/0').$main_image->getUrl('x150'), ['alt' => $el->title])?><br><br>
                        </div>

                        <div class="content">
                            <a href="<?= \yii\helpers\Url::to(['/articles/articles-item', 'slug' => $el->slug])?>">
                                <h4><?= $el->title ?></h4>
                                <p><?= $el->preview ?></p>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>
