<?php

/* @var $this yii\web\View */

use frontend\assets\AppAsset;
use yii\web\View;

$this->title = 'Магазины — Скрэтхаус.бай';

AppAsset::register($this);
\frontend\assets\ShopsAsset::register($this);

// Вытягиваем список магазинов из базы и записываем в JS переменную shopList
$shops = "";
foreach($shopList as $key => $value) {
	$shops .= "
          {
            'coordinates': [".$shopList[$key]['coordinante_X'].", ".$shopList[$key]['coordinante_Y']."],
            'city_name': '".$shopList[$key]['city']."',
            'address': '".$shopList[$key]['address']."',
            'name': '".$shopList[$key]['name']."',
            'work_time': '".$shopList[$key]['work_time']."',
            'telephone': '".$shopList[$key]['telephone']."'
          },
	";
}
$this->registerJs("
    // Список городов и магазинов в них
    var shopList = [
      {      
        'shops': [
            ".$shops."
        ]
      }
    ];
", View::POS_HEAD);

?>
<div class="main-block">
    <div id="map"></div>
</div>
