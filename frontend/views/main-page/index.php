<?php

/* @var $this yii\web\View */

use frontend\assets\AppAsset;
use backend\models\TextByAdmin;

$this->title = 'Скрэтхаус.бай — Вкусно. Здорово. Легко! Орешки, сухофрукты, специи и сладости в Минске со скидкой!';
AppAsset::register($this);
\frontend\assets\MainPageAsset::register($this);
$this->registerMetaTag([
	'name' => 'yandex-verification',
	'content' => '38400029fc30723c'
]);
$this->registerMetaTag([
	'name' => 'description',
	'content' => 'Купить орехи в Минске, а также специи и приправы? Заказать с доставкой орехи и сухофрукты, выбрать семена — все это на сайте scrathouse.by! Заказывайте товары и получайте скидочный промокод в размере 5% на всю покупку.'
]);
?>
<div class="main-block">
    <div class="container">
        <div class="row">
            <div class="content">
                <div class="text"><?= TextByAdmin::findOne(['page' => 'main-page', 'type' => 1]) ? TextByAdmin::findOne(['page' => 'main-page', 'type' => 1])->text : '' ?></div>
                <div class="button"><a href="<?= \yii\helpers\Url::to(['/products/orehi'])?>">Выбрать и купить</a></div>
                <div class="promo"><?= TextByAdmin::findOne(['page' => 'main-page', 'type' => 2]) ? TextByAdmin::findOne(['page' => 'main-page', 'type' => 2])->text : '' ?></div>
            </div>
        </div>
    </div>
</div>

<div class="blocks">
    <div class="container">
        <div class="row">
            <div class="items">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="item">
                        <a href="<?= \yii\helpers\Url::to(['/products/specii'])?>">
                            <img src="/img/main-page/1.jpg" alt="">
                            <div class="fade"></div>
                            <div class="button"><span>СПЕЦИИ</span></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="item">
                        <a href="<?= \yii\helpers\Url::to(['/products/orehi'])?>">
                            <img src="/img/main-page/2.jpg" alt="">
                            <div class="fade"></div>
                            <div class="button"><span>ОРЕХИ</span></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="item">
                        <a href="<?= \yii\helpers\Url::to(['/products/sladosti'])?>">
                            <img src="/img/main-page/3.jpg" alt="">
                            <div class="fade"></div>
                            <div class="button"><span>СЛАДОСТИ</span></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="item">
                        <a href="<?= \yii\helpers\Url::to(['/products/suhofrukty'])?>">
                            <img src="/img/main-page/4.jpg" alt="">
                            <div class="fade"></div>
                            <div class="button"><span>СУХОФРУКТЫ</span></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="item">
                        <a href="<?= \yii\helpers\Url::to(['/products/instrumenty'])?>">
                            <img src="/img/main-page/5.jpg" alt="">
                            <div class="fade"></div>
                            <div class="button"><span>ИНСТРУМЕНТЫ</span></div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="item">
                        <a href="<?= \yii\helpers\Url::to(['/scrat-box/scrat-box'])?>">
                            <img src="/img/main-page/6.jpg" alt="">
                            <div class="fade"></div>
                            <div class="button"><span>СКРАТ БОКС</span></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
