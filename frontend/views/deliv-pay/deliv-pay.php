<?php

/* @var $this yii\web\View */

use frontend\assets\AppAsset;
use yii\web\View;
use backend\models\TextByAdmin;

$this->title = 'Доставка и оплата — Скрэтхаус.бай';

$this->registerMetaTag([
	'name' => 'description',
	'content' => 'Информация о способах и сроках доставки продукции scrathouse.by'
]);

AppAsset::register($this);
\frontend\assets\DelivPayAsset::register($this);

// Вытягиваем список магазинов из базы и записываем в JS переменную shopList
$shops = "";
foreach($shopList as $key => $value) {
	$shops .= "
          {
            'coordinates': [".$shopList[$key]['coordinante_X'].", ".$shopList[$key]['coordinante_Y']."],
            'city_name': '".$shopList[$key]['city']."',
            'address': '".$shopList[$key]['address']."',
            'name': '".$shopList[$key]['name']."',
            'work_time': '".$shopList[$key]['work_time']."',
            'telephone': '".$shopList[$key]['telephone']."'
          },
	";
}
$this->registerJs("
    // Список городов и магазинов в них
    var shopList = [
      {      
        'shops': [
            ".$shops."
        ]
      }
    ];
", View::POS_HEAD);

$contact_info = \backend\models\ContactInfo::getContactInfoArray();

?>

<div class="main-block">
    <div class="content">
        <div id="map"></div>
        <div class="delivery">
            <div class="type">
                <input type="radio" name="deliv-type" id="courier" value="courier" checked>
                <label for="courier"><span></span>Доставка курьером</label>
                <div class="deliv-desc">
                    <span>
                        <?= TextByAdmin::findOne(['page' => 'delivery-pay', 'type' => 1]) ? TextByAdmin::findOne(['page' => 'delivery-pay', 'type' => 1])->text : '' ?>
                    </span>
                </div>
            </div>
            <div class="type">
                <input type="radio" name="deliv-type" id="post" value="post">
                <label for="post"><span></span>Почтой</label>
                <div class="deliv-desc">
                    <span>
                        <?= TextByAdmin::findOne(['page' => 'delivery-pay', 'type' => 2]) ? TextByAdmin::findOne(['page' => 'delivery-pay', 'type' => 2])->text : '' ?>
                    </span>
                </div>
            </div>
            <div class="type">
                <input type="radio" name="deliv-type" id="pickup" value="pickup">
                <label for="pickup"><span></span>Самовывоз</label>
                <div class="deliv-desc">
                    <span>
                        <?= TextByAdmin::findOne(['page' => 'delivery-pay', 'type' => 3]) ? TextByAdmin::findOne(['page' => 'delivery-pay', 'type' => 3])->text : '' ?>
                    </span>
                </div>
            </div>
        </div>
        <div class="payment">
            <hr/>
            <?= TextByAdmin::findOne(['page' => 'delivery-pay', 'type' => 5]) ? TextByAdmin::findOne(['page' => 'delivery-pay', 'type' => 5])->text : '' ?>
            <div class="check">
                <hr/>
                <p>Образец документа, подтверждающего оплату товара:</p>
                <img alt="Образец документа, подтверждающего оплату товара" src="/img/icons/check.png">
            </div>
            <div class="order-steps">
                <hr/>
	            <?= TextByAdmin::findOne(['page' => 'delivery-pay', 'type' => 4]) ? TextByAdmin::findOne(['page' => 'delivery-pay', 'type' => 4])->text : '' ?>
            </div>
            <div class="order-steps">
                <hr/>
                <div class="info">
                    <a href="<?= \yii\helpers\Url::to(['/site/contact-offer'])?>"><h4>Ссылка на договор публичной оферты</h4></a>
                </div>
            </div>
        </div>
    </div>
</div>
