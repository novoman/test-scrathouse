<?php

/* @var $this yii\web\View */

use frontend\assets\AppAsset;
use yii\web\View;
use backend\models\TextByAdmin;

$this->title = 'Возврат — Скрэтхаус.бай';

$this->registerMetaTag([
	'name' => 'description',
	'content' => 'Правовая информация для покупателей — ваши права и обязанности.'
]);

AppAsset::register($this);
\frontend\assets\CallbackAsset::register($this);
?>

<div class="main-block">
    <div class="content">
        <div class="callback-head">
            Возврат
        </div>
        <div class="callback">
	        <?= TextByAdmin::findOne(['page' => 'callback']) ? TextByAdmin::findOne(['page' => 'callback'])->text : '' ?>
        </div>
        <div class="callback-buttons">
            <div class="callback-button">
                <a class="col-lg-6 col-md-6 col-sm-12 col-xs-12" href="<?= \yii\helpers\Url::to(['/site/contact-email'])?>"><img src="/img/icons/mail2.svg">Остались вопросы? Напишите нам</a>
            </div>
            <div class="callback-button">
                <a href="#phones-modal" class="my-modal col-lg-6 col-md-6 col-sm-12 col-xs-12" onclick="openPhonesModal()"><img src="/img/icons/phone2.svg">Остались вопросы? Позвоните нам</a>
            </div>
        </div>
    </div>
</div>
