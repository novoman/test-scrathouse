<?php

/* @var $this yii\web\View */

use frontend\assets\AppAsset;
use common\models\Category;
use yii\widgets\Breadcrumbs;
use backend\models\ContactInfo;

$this->title = $product['name'] ? $product['name'] : 'Товары  — Скрэтхаус.бай';
AppAsset::register($this);
\frontend\assets\ProductCardAsset::register($this);

$breadcrumbs = Category::getTreeOfParents($product['slug_cat'] ? $product['slug_cat'] : Category::getStartCategory());

foreach ($breadcrumbs as $slug => $name) {
	$this->params['breadcrumbs'][] = ['label' => $name, 'url' => ['/products/products', 'slug' => $slug]];
}

$images = $model->getImages();

$contact_info = ContactInfo::getContactInfoArray();

$this->registerJsFile('/libs/jquery/dist/jquery.min.js',  ['position' => yii\web\View::POS_HEAD]);
$this->registerJsFile('/libs/image-magnifier/image-magnifier.js',  ['position' => yii\web\View::POS_HEAD]);

$this->registerJs("
    $(\".increase-product-weight\").click(function(){
        var temp_count = parseInt($(this).closest('.inside-change').children(\".line-block\").children(\".product-weight\").text());
        $(this).closest('.inside-change').children(\".line-block\").children(\".product-weight\").text((temp_count + ".\common\models\Products::getMinProductWeight($product['id']).") + \" ".(\common\models\Products::findOne($product['id'])->special ? '' : 'rp.')." \");
    
    });
    
    $(\".decrease-product-weight\").click(function(){
        var temp_count = parseInt($(this).closest('.inside-change').children(\".line-block\").children(\".product-weight\").text());
        if (temp_count > ".\common\models\Products::getMinProductWeight($product['id']).") {
            $(this).closest('.inside-change').children(\".line-block\").children(\".product-weight\").text((temp_count - ".\common\models\Products::getMinProductWeight($product['id']).") + \" ".(\common\models\Products::findOne($product['id'])->special ? '' : 'rp.')." \");
        }
    });
    
    $(document).ready(function(){
        $('#share-viber').attr('href', \"viber://chat?number=".$contact_info['product']['viber']."\");
    
        $('#share-whatsapp').attr('href', \"whatsapp://send?phone=".$contact_info['product']['whatsapp']."&text=\" +
            encodeURIComponent(\"Здравствуйте. Я хочу купить этот товар. \" + window.location.href));
    
        $('#share-skype').attr('href', \"skype:".$contact_info['product']['skype']."?chat\");
    
        $('#share-telegram').attr('href', \"tg://resolve?domain=".$contact_info['product']['tg']."\");
    });
", \yii\web\View::POS_READY);

?>
<div class="main-block">
    <div class="container">
        <div class="breadcrumbs">
		    <?= Breadcrumbs::widget([
			    'homeLink' => ['label' => 'Главная', 'url' => '/'],
			    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
			    'options' => [
				    'class' => false,
			    ],
		    ]) ?>
        </div>
        <div class="content-img">
            <div class="imgs">
                <div class="mini-imgs">
                    <?php foreach ($images as $image): ?>
                        <div class="mini-img">
                            <div class="img-frame"></div>
                            <?= \yii\helpers\Html::img(($image->id ? '' : '/0').$image->getUrl('x1200'), ['alt' => $product['name'], 'class' => 'product-img'])?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="main-img">
	                <?php if ($product['type']): ?>
                        <div class="percent">
	                        <?php if ($product['discount_price'] && $product['type'] == 2): ?>
				                -<?= round(($product['price'] - $product['discount_price']) / $product['price'] * 100) ?>%
			                <?php else: ?>
                                Новинка
			                <?php endif; ?>
                        </div>
	                <?php endif; ?>
                    <div class="product-img">
                    <script type="text/javascript">
                        imageZoom(
                            { blur: false, inner: true, tint: false },
                            { src: '<?= ($images[0]->id ? '' : '/0').$images[0]->getUrl('x1200') ?>', height: 667, width: 1000 },
                            { src: '<?= ($images[0]->id ? '' : '/0').$images[0]->getUrl('x500') ?>', height: 333, width: 500 }
                        );
                    </script>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-cart">
            <div id="product-card" class="product">
                <div class="product-inf">
                    <div class="product-name"><?=$product['name'] ?> </div>
                    <div class="price">
	                    <?php if ($product['discount_price'] && $product['type'] == 2): ?>
                            <div class="price-before"><s><?=$product['price'] ?>р.</s></div>
		                    <?=$product['discount_price'] ?>р. за
	                    <?php else: ?>
		                    <?=$product['price'] ?>р. за
	                    <?php endif; ?>
	                    <?php if (\common\models\Products::getMinProductWeight($product['id'])): ?>
		                    <?= \common\models\Products::getMinProductWeight($product['id']).' '.(common\models\Products::findOne($product['id'])->special ? 'шт.' : 'гр.') ?> <br>
	                    <?endif; ?>
	                    <?php if (!\common\models\Products::findOne($product['id'])->special && $product['price_per_kg']): ?>
                            (<?=$product['price_per_kg'] ?>р. за кг)
	                    <?php endif; ?>
                    </div>
                    <div class="presence <?= ($product['presence'] ? 'in-stock': 'out-stock') ?>"><?= ($product['presence'] ? 'В наличии': 'Нет в наличии') ?></div>
                    <?php if ($product['country']): ?>
                        <div class="country"><b>Страна производства:</b> <?=$product['country'] ?></div>
                    <?php endif; ?>
                    <?php if ($user['client_type'] == 1): ?>
                        <div class="articule">АРТИКУЛ <?=$product['articule'] ?></div>
                    <?php endif; ?>
                    <div class="description"><?=$product['description'] ?></div>
                </div>
                <div class="second-block">
                    <div class="change">
                        <div class="inside-change">
                            <div class="line-block">
                                <img class="decrease-product-weight" src="/img/icons/arrow-left.svg">
                            </div>
                            <div class="line-block">
                                <div id="product-weight" class="product-weight"><?= $product['weight'] ?> <?= $product['special'] ? '' : 'rp.'?></div>
                            </div>
                            <div class="line-block">
                                <img class="increase-product-weight" src="/img/icons/arrow-right.svg">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="add-to-cart">
                    <a data-id="<?= $product['id'] ?>" href="#">Добавить в корзину</a>
                </div>

                <div class="socials">
                    <div class="contact">Связаться по мессенджерам:</div>
                    <div class="line-block">
                        <a id="share-viber">
                            <img width="40" src="/img/icons/messengers/viber.svg" />
                        </a>
                    </div>
                    <div class="line-block">
                        <a id="share-whatsapp">
                            <img width="40" src="/img/icons/messengers/whatsapp.svg" />
                        </a>
                    </div>
                    <div class="line-block">
                        <a id="share-skype">
                            <img width="40" src="/img/icons/messengers/skype.svg" />
                        </a>
                    </div>
                    <div class="line-block">
                        <a id="share-telegram">
                            <img width="40" src="/img/icons/messengers/telegram.svg" />
                        </a>
                    </div>
<!--                    <div class="line-block">-->
<!--                        <a href="--><?//=$contact_info['footer']['in'] ?><!--" target="_blank">-->
<!--<!--                            <img src="/img/icons/socials/alt/instagram.svg" alt="" style="width: 18px;">-->
<!--                        </a>-->
<!--                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>