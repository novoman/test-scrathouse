<?php

/* @var $this yii\web\View */

use frontend\assets\AppAsset;
use yii\widgets\Breadcrumbs;
use common\models\Category;
use yii\widgets\LinkPager;

$this->title = 'Поиск по сайту — Скрэтхаус.бай';
AppAsset::register($this);
\frontend\assets\ProductsAsset::register($this);

?>
<div class="main-block">
    <div class="container">
        <div class="row" style="margin: 0;">
            <div class="wrapper-content">
                <div id="elements" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php if (!empty($products)): ?>
	                    <?php \yii\widgets\Pjax::begin(); ?>
                        <h3 class="title text-center" style="margin-bottom: 50px;">Поиск по запросу "<?= $q ?>"</h3>
                        <?php foreach ($products as $product): ?>
		                    <?php
                                $model = \common\models\Products::find()->where(['=', 'slug', $product['slug']])->one();
                                $main_image = $model->getImage();
		                    ?>
                            <div class="item col-lg-4 col-md-6 col-sm-6 col-sx-12">
                                <div class="content">
				                    <?php if ($product['type']): ?>
                                        <div class="percent">
	                                        <?php if ($product['discount_price'] && $product['type'] == 2): ?>
                                                -<?= round(($product['price'] - $product['discount_price']) / $product['price'] * 100) ?>%
						                    <?php else: ?>
                                                Новинка
						                    <?php endif; ?>
                                        </div>
				                    <?php endif; ?>
				                    <?= \yii\helpers\Html::img(($main_image->id ? '' : '/0').$main_image->getUrl('x1200'), ['alt' => $product['name'], 'class' => 'product-img'])?>
                                    <div class="name"><?=$product['name'] ?></div>
                                    <div class="price">
	                                    <?php if ($product['discount_price'] && $product['type'] == 2): ?>
                                            <div class="price-before"><s><?=$product['price'] ?>р.</s></div>
						                    <?=$product['discount_price'] ?>р. за
					                    <?php else: ?>
						                    <?=$product['price'] ?>р. за
					                    <?php endif; ?>
					                    <?php if ($product['weight']): ?>
						                    <?= $product['weight'].' '.($product['special'] ? 'шт.' : 'гр.') ?> <br>
					                    <?endif; ?>
					                    <?php if (!$product['special'] && $product['price_per_kg']): ?>
                                            (<?=$product['price_per_kg'] ?>р. за кг)
					                    <?php endif; ?>
                                    </div>
				                    <?php if ($product['country'] != ''): ?>
                                        <div class="country"><?= $product['country'] ?></div>
				                    <?php endif; ?>
                                    <div class="presence <?= ($product['presence'] ? 'in-stock': 'out-stock') ?>"><?= ($product['presence'] ? 'в наличии': 'нет в наличии') ?></div>
                                    <div class="fade">
                                        <div class="buttons">
                                            <div class="add-to-cart"><a href="<?= \yii\helpers\Url::to(['/cart/add', 'id' => $product['id']])?>" data-id="<?= $product['id'] ?>"><img src="/img/icons/s-cart.svg" alt=""></a></div>
                                            <div class="show-product"><a href="<?= \yii\helpers\Url::to(['/products/product-card', 'slug' => $product['slug']])?>"><img src="/img/icons/search.svg" alt=""></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                        <div class="row"></div>
                        <div class="my-pagination">
                            <?= LinkPager::widget([
                                'pagination' => $pages,
                                'maxButtonCount' => 5,
                                'registerLinkTags' => true,
                                'options' => [
                                        'class' => false,
                                ],
                                'prevPageLabel'=>false,
                                'nextPageLabel'=>'<img src="/img/icons/arrow.svg">',
                            ]); ?>
                        </div>
	                    <?php \yii\widgets\Pjax::end(); ?>
                    <?php else: ?>
                        <div class="text-center">
                            <h3>По запросу "<?= $q ?>" ничего не найдено</h3>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
