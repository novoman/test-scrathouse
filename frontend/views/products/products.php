<?php

/* @var $this yii\web\View */

use frontend\assets\AppAsset;
use yii\widgets\Breadcrumbs;
use common\models\Category;
use yii\widgets\LinkPager;

$this->title = Yii::$app->request->get('slug')? Category::getCategoryNameBySlug(Yii::$app->request->get('slug')) : 'Товары — Скрэтхаус.бай';
AppAsset::register($this);
\frontend\assets\ProductsAsset::register($this);

$meta_tag_description_content = '';

switch (Yii::$app->request->get('slug')) {
    case 'suhofrukty':
        $meta_tag_description_content = 'Cухофрукты — продукт для приготовления компота из сухофруктов. Купить их в Минске очень легко в интернет-магазине scrathouse.by!';
        break;
	case 'orehi':
		$meta_tag_description_content = 'Польза от орехов очевидна, но только если покупать их в Минске от проверенных поставщиков. В противном случае вред от орехов может быть значителен!';
		break;
	case 'sladosti':
		$meta_tag_description_content = 'Сладости в Минске — их множество, но именно scrathouse.by отбирает для вас лучшие продукты — от желатинок и до сладостей без сахара!';
		break;
	case 'specii':
		$meta_tag_description_content = 'Где в Минске купить специи? Смеси специй, специи с солью и многое другое — всегда на scrathouse.by!';
		break;
	case 'semena':
		$meta_tag_description_content = 'Купить семена в Минске можно во многих интернет-магазинах, но только scrathouse.by доставляет семена почтой по самым демократичным ценам!';
		break;
	case 'urbechi-i-pasty':
		$meta_tag_description_content = 'Если встал вопрос купить урбеч, то откройте соответствующий каталог на scrathouse.by и сделайте свой заказ в нашем интернет-магазине!';
		break;
	case 'muka':
		$meta_tag_description_content = 'На scrathouse.by можно заказать доставку муки — но не простой, а муки арахисовой, миндальной и даже муки кешью!';
		break;
    default:
	    $meta_tag_description_content = 'Каталог всех товаров интернет-магазина Скрэтхаус.бай. Выбери продукт себе по вкусу!';
}

$this->registerMetaTag([
	'name' => 'description',
	'content' => $meta_tag_description_content
]);

$breadcrumbs = Category::getTreeOfParents(Yii::$app->request->get('slug') ? Yii::$app->request->get('slug') : '');

foreach ($breadcrumbs as $slug => $name) {
	$this->params['breadcrumbs'][] = ['label' => $name, 'url' => ['/products/products', 'slug' => $slug]];
}

?>
<div class="main-block">
    <div class="container">
        <div class="row" style="margin: 0;">
            <div class="breadcrumbs">
                <?= Breadcrumbs::widget([
                    'homeLink' => ['label' => 'Товары', 'url' => '/products'],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'options' => [
                        'class' => false,
                    ],
                ]) ?>
            </div>
            <div class="sort">
                <a href="#">Сортировать по
	                <?php if (Yii::$app->request->get('sort_type')): ?>
                        <b><?= (Yii::$app->request->get('sort_type') == 'price' ? 'цене' : 'популярности') ?></b>
	                <?php endif; ?>
                </a>
                <div class="wrapper-drop">
                    <?php if (Yii::$app->request->get('slug')): ?>
                        <div>
                            <a class="method" href="<?= \yii\helpers\Url::to(['/products/products', 'slug' => Yii::$app->request->get('slug') ? Yii::$app->request->get('slug') : Category::getStartCategory(), 'sort_type' => 'price'])?>">Цене</a>
                            <a class="method" href="<?= \yii\helpers\Url::to(['/products/products', 'slug' => Yii::$app->request->get('slug') ? Yii::$app->request->get('slug') : Category::getStartCategory(), 'sort_type' => 'hits'])?>">Популярности</a>
                        </div>
                    <?php else: ?>
                        <div>
                            <a class="method" href="<?= \yii\helpers\Url::to(['/products', 'sort_type' => 'price'])?>">Цене</a>
                            <a class="method" href="<?= \yii\helpers\Url::to(['/products', 'sort_type' => 'hits'])?>">Популярности</a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <div class="row"></div>

            <div class="wrapper-content">

                <div class="np-left wrapper-filter col-lg-2 col-md-3 col-sm-3">
                    <div class="filter">
			            <?php
			            // array for navbar
			            $items = array();
			            // get all root categories
			            $root_cats = Category::find()->where(['depth' => 0])->all();
                        // get selected category parent's id (for make it active)
			            $selected_cat_parent_root_id = Category::find()->where(['slug' =>  Category::getCorrectSlug(Yii::$app->request->get('slug'))])->one()->getParentId();

			            foreach ($root_cats as $root_cat) {
				            // for each root category search it's children
				            $cats = Category::getTree($root_cat->id, true);

				            // for root in navbar as string
				            $main_temp_item = '';
				            // for it's children as categories
				            $temp_items = array();

				            foreach ($cats as $cat) {
					            if ($cat['depth'] == 0) {
                                    $main_temp_item = '<li class="parent '.(($selected_cat_parent_root_id  == $cat['id'] && Yii::$app->request->get('slug')) ? "active" : "").'" >
                                                           <span class="'.(Yii::$app->request->get('slug') == $cat['slug'] ? 'selected' : '').'"><a href="/products/'.($cat['slug'] ? $cat['slug'] : Category::getStartCategory()).'">'.$cat['name'].'</a></span>';
					            }
					            else {
						            $temp_items[] = array(
							            'label' => $cat['name'],
							            'options' => [
								            'class' => (Yii::$app->request->get('slug') == $cat['slug'] ? 'selected' : ''),
							            ],
							            'url' => array(
								            '/products/products', 'slug' => $cat['slug'] ? $cat['slug'] : Category::getStartCategory()
                                        )
                                    );
					            }
				            }
				            $items[] = array(
					            'label' => $main_temp_item,
					            'items' => $temp_items,
				            );
			            }

			            echo \yii\widgets\Menu::widget([
				            'items' => $items,
				            'encodeLabels' => false,
				            'hideEmptyItems' => false,
			            ]);
			            ?>
                    </div>
                </div>
                <div id="elements" class="np-right col-lg-10 col-md-9 col-sm-9 col-xs-12">
                    <?php if (!empty($products)): ?>
	                    <?php \yii\widgets\Pjax::begin(); ?>
                        <?php foreach ($products as $product): ?>
                            <?php
		                        $model = \common\models\Products::find()->where(['=', 'slug', $product['slug']])->one();
		                        $main_image = $model->getImage();
                            ?>
                            <div class="item col-lg-4 col-md-6 col-sm-6 col-sx-12">
                                <div class="content">
	                                <?php if ($product['type']): ?>
                                        <div class="percent">
	                                        <?php if ($product['discount_price'] && $product['type'] == 2): ?>
                                            -<?= round(($product['price'] - $product['discount_price']) / $product['price'] * 100) ?>%
                                        <?php else: ?>
                                            Новинка
		                                <?php endif; ?>
                                        </div>
	                                <?php endif; ?>
	                                <?= \yii\helpers\Html::img(($main_image->id ? '' : '/0').$main_image->getUrl('x1200'), ['alt' => $product['name'], 'class' => 'product-img'])?>
                                    <div class="name"><?=$product['name'] ?></div>
                                    <div class="price">
                                        <?php if ($product['discount_price'] && $product['type'] == 2): ?>
                                            <div class="price-before"><s><?=$product['price'] ?>р.</s></div>
	                                            <?=$product['discount_price'] ?>р. за
                                            <?php else: ?>
	                                            <?=$product['price'] ?>р. за
                                        <?php endif; ?>
                                        <?php if ($product['weight']): ?>
                                           <?= $product['weight'].' '.($product['special'] ? 'шт.' : 'гр.') ?> <br>
                                        <?php endif; ?>

                                        <?php if (!$product['special'] && $product['price_per_kg']): ?>
                                            (<?=$product['price_per_kg'] ?>р. за кг)
                                        <?php endif; ?>
                                    </div>
                                    <?php if ($product['country'] != ''): ?>
                                        <div class="country"><?= $product['country'] ?></div>
                                    <?php endif; ?>
                                    <div class="presence <?= ($product['presence'] ? 'in-stock': 'out-stock') ?>"><?= ($product['presence'] ? 'в наличии': 'нет в наличии') ?></div>
                                    <div class="fade">
                                        <div class="buttons">
                                            <div class="add-to-cart"><a href="<?= \yii\helpers\Url::to(['/cart/add', 'id' => $product['id']])?>" data-id="<?= $product['id'] ?>"><img src="/img/icons/s-cart.svg" alt=""></a></div>
                                            <div class="show-product"><a href="<?= \yii\helpers\Url::to(['/products/product-card', 'slug' => $product['slug']])?>"><img src="/img/icons/search.svg" alt=""></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <div class="row"></div>
                        <div class="my-pagination">
                            <?= LinkPager::widget([
                                'pagination' => $pages,
                                'maxButtonCount' => 5,
                                'options' => [
                                    'class' => false,
                                ],
                                'prevPageLabel'=>false,
                                'nextPageLabel'=>'<img src="/img/icons/arrow.svg">',
                            ]); ?>
                        </div>
	                    <?php \yii\widgets\Pjax::end(); ?>
                    <?php else: ?>
                        <div class="text-center">
                            <h3>Товаров в данной категории нет.</h3>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
