<?php
namespace frontend\controllers;

use common\models\Articles;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ArticlesController extends Controller
{
	public function actionArticles(){

		$query = Articles::find();

		$countQuery = clone $query;

		$pages = new Pagination(['totalCount' => $countQuery->count(),'pageSize' => Articles::ARTICLES_PER_PAGE]);

		$models = $query->offset($pages->offset)
		                ->limit($pages->limit)
		                ->all();

		return $this->render('articles',[
			'model' => $models,
			'pages' => $pages,
		]);
	}

	public function actionArticlesItem($slug) {

		$item = Articles::find()->where(['slug' => $slug])->one();

		$last = Articles::find()->where(['!=','id',$item->id])->orderBy('id DESC')->limit(Articles::ARTICLES_PER_PAGE)->all();

		return $this->render('articles-item',[
			'item' => $item,
			'last' => $last,
		]);
	}
}
