<?php
namespace frontend\controllers;

use common\models\Order;
use frontend\models\Profile;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use yii\base\Model;
use yii\web\NotFoundHttpException;


class ProfileController extends Controller
{

	public function actionProfile()
	{
		$id = Yii::$app->user->id;
		$user = User::find()
		            ->select(['juristic_person.*', 'user.*'])
		            ->leftJoin('juristic_person','juristic_person.id_user = user.id')
		            ->where(['=','user.id', $id ])
					->asArray()
		            ->one();

		$page = Yii::$app->request->get('page');
		$client_type = Yii::$app->request->get('client_type');

		$model_user = new User();
		$model_profile = new Profile();

		$orders = Order::find()
					->select('*')
					->where(['id_user' => $user['id']])
					->asArray()
					->all();

		$discount_sums_labels = Profile::getDiscountSumsArray();
		$discount_percents_labels = Profile::getDiscountPercentsArray();

		return $this->render('profile',[
			'user' => $user,
			'page' => $page,
			'client_type' => $client_type,
			'model_user' => $model_user,
			'model_profile' => $model_profile,
			'orders' => $orders,
			'discount_sums_labels' => $discount_sums_labels,
			'discount_percents_labels' => $discount_percents_labels,
		]);
	}

	public function actionSave($id)
	{
		$attributes = Yii::$app->request->post();
		$user = User::findOne($id);
		if (!$user) {
			throw new NotFoundHttpException("Юзер не найден.");
		}

		$profile = Profile::find()->where(['=','id_user', $id ])->one();

		if (!$profile) {
			$profile = new Profile();
		}

		if ($profile->load(Yii::$app->request->post()) && $user->load(Yii::$app->request->post())) {
			$isValid = $user->validate();
			$isValid = $profile->validate() && $isValid;
			if ($isValid) {
				if ($attributes['User']['name'] != '') {
					$name = explode(' ', $attributes['User']['name']);
					$user->last_name = $name[0];
					$user->first_name = $name[1];
				}
				$user->client_type = $attributes['User']['client_type'];
				$user->email = $attributes['User']['email'];
				$user->telephone = $attributes['User']['telephone'];
				$user->address = $attributes['User']['address'];
				$user->birthday = strtotime($attributes['birth_year'].'-'.$attributes['birth_month'].'-'.$attributes['birth_day']);

				$user->save();
				if (($attributes['User']['client_type'] == 0)) {
					$profile->delete();
				}
				else {
					$profile->id_user = $user->id;
					$profile->save();
				}
				Yii::$app->session->setFlash('success', 'Данные успешно сохранены.');

				return $this->redirect(['profile/profile', 'page' => 2]);
			}
		}
		Yii::$app->session->setFlash('error', 'Ошибка сохранения данных.');

		return $this->redirect(['profile/profile', 'page' => 2]);
	}

}
