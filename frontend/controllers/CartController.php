<?php
namespace frontend\controllers;

use backend\models\Promo;
use common\models\PaymentsType;
use common\models\User;
use frontend\models\Cart;
use frontend\models\Scratboxes;
use Yii;
use yii\db\Exception;
use yii\web\Controller;
use common\models\Products;
use common\models\Order;
use common\models\OrderItems;
use yii\web\NotFoundHttpException;

class CartController extends Controller
{

 	public function actionCart()
    {
		$session = Yii::$app->session;
	    $session->open();

	    //$this->layout = false;
	    return $this->render('cart', compact('session'));
    }

    public function actionAdd()
    {
	    $session = Yii::$app->session;
	    $session->open();

	    $cart = new Cart();

		$is_scratbox = Yii::$app->request->get('scratbox');

		if ($is_scratbox) {
			$name = Yii::$app->request->get('name');
			$count = Yii::$app->request->get('count');

			if (empty($_SESSION['current-scrat-box']['id'])) {
				return false;
			}

			$scratbox = Scratboxes::findOne($_SESSION['current-scrat-box']['id']);

			$scratbox->name = $name;
			$scratbox->save(false);

			$cart->addScratBoxToCart($scratbox, $count);

			$session->remove('current-scrat-box');

		}
		else {
			$id = Yii::$app->request->get('id');
			$weight = Yii::$app->request->get('weight');

			$weight = !$weight ? Products::getMinProductWeight($id) : $weight;

			//$product = Products::findOne($id);
			$product = Products::find()
			                   ->select(['products.*'])
			                   ->where(['=','products.id', $id])
			                   ->asArray()
			                   ->one();
			//print_r($product);
			if (empty($product)) return false;
			if (!$product['presence']) return false;

			$model = Products::findOne($id);
			$model->hits++;
			$model->save(false);

			$cart->addToCart($product, $weight);
		}


		if (!Yii::$app->request->isAjax) {
			return $this->redirect(Yii::$app->request->referrer);
		}

		$this->layout = false;
		return $this->render('cart-modal', compact('session'));
    }

    public function actionClear()
    {
    	$session = Yii::$app->session;
    	$session->open();
    	$session->remove('cart');
    	$session->remove('cart.sum');
	    $session->remove('promo');
	    $session->remove('promo.used');
	    $session->remove('cart.points');
	    $session->remove('cart.max-points');

	    $this->layout = false;
	    return $this->render('cart-modal', compact('session'));
    }

    public function actionDelProduct()
    {
	    $id = Yii::$app->request->get('id');
	    $is_scratbox = Yii::$app->request->get('scratbox') ? Yii::$app->request->get('scratbox') : false;

	    $session = Yii::$app->session;
	    $session->open();
	    $cart = new Cart();
	    $cart->deleteProduct($id, $is_scratbox);

	    $this->layout = false;
	    return $this->render('cart-modal', compact('session'));
    }

	public function actionDecreaseProductWeight()
	{
		$id = Yii::$app->request->get('id');
		$is_scratbox = Yii::$app->request->get('scratbox') ? Yii::$app->request->get('scratbox') : false;

		$session = Yii::$app->session;
		$session->open();
		$cart = new Cart();
		$cart->decreaseProductWeight($id, $is_scratbox);

		$this->layout = false;
		return $this->render('cart-modal', compact('session'));
	}

	public function actionIncreaseProductWeight()
	{
		$id = Yii::$app->request->get('id');
		$is_scratbox = Yii::$app->request->get('scratbox') ? Yii::$app->request->get('scratbox') : false;

		$session = Yii::$app->session;
		$session->open();
		$cart = new Cart();
		$cart->increaseProductWeight($id, $is_scratbox);

		$this->layout = false;
		return $this->render('cart-modal', compact('session'));
	}

/***********************************************************************************************************************/

	public function actionClearCartPage()
	{
		$session = Yii::$app->session;
		$session->open();
		$session->remove('cart');
		$session->remove('cart.sum');
		$session->remove('promo');
		$session->remove('promo.used');
		$session->remove('cart.points');
		$session->remove('cart.max-points');

		$this->layout = false;
		return $this->render('cart', compact('session'));
	}

	public function actionDelProductCartPage()
	{
		$id = Yii::$app->request->get('id');
		$is_scratbox = Yii::$app->request->get('scratbox') ? Yii::$app->request->get('scratbox') : false;

		$session = Yii::$app->session;
		$session->open();
		$cart = new Cart();
		$cart->deleteProduct($id, $is_scratbox);

		$this->layout = false;
		return $this->render('cart', compact('session'));
	}

	public function actionDecreaseProductWeightCartPage()
	{
		$id = Yii::$app->request->get('id');
		$is_scratbox = Yii::$app->request->get('scratbox') ? Yii::$app->request->get('scratbox') : false;

		$session = Yii::$app->session;
		$session->open();
		$cart = new Cart();
		$cart->decreaseProductWeight($id, $is_scratbox);

		$this->layout = false;
		return $this->render('cart', compact('session'));
	}

	public function actionIncreaseProductWeightCartPage()
	{
		$id = Yii::$app->request->get('id');
		$is_scratbox = Yii::$app->request->get('scratbox') ? Yii::$app->request->get('scratbox') : false;

		$session = Yii::$app->session;
		$session->open();
		$cart = new Cart();
		$cart->increaseProductWeight($id, $is_scratbox);

		$this->layout = false;
		return $this->render('cart', compact('session'));
	}

/***********************************************************************************************************************/

	public function actionClearCheckout()
	{
		$session = Yii::$app->session;
		$session->open();
		$session->remove('cart');
		$session->remove('cart.sum');
		$session->remove('promo');
		$session->remove('promo.used');
		$session->remove('cart.points');
		$session->remove('cart.max-points');

		$this->layout = false;
		return $this->render('cart-checkout', compact('session'));
	}

	public function actionDelProductCheckout($delivery_type)
	{
		$id = Yii::$app->request->get('id');
		$is_scratbox = Yii::$app->request->get('scratbox') ? Yii::$app->request->get('scratbox') : false;

		$session = Yii::$app->session;
		$session->open();
		$cart = new Cart();
		$response = $cart->deleteProduct($id, $is_scratbox);

		if ($response === 'MIN_SUM_FOR_PROMO_ERROR') {
			return $response;
		}

		$user = User::findOne(Yii::$app->user->id);

		if ($user) {
			$session['cart.max-points'] = Cart::getMaxSumForUsersPoints(CartController::maxSumForBonusPoint($session['cart']), $user->users_points);
			if ($session['cart.points'] > $session['cart.max-points']) {
				$session['cart.points'] = $session['cart.max-points'];
			}
		}

		$session['cart.delivery-cost'] = ($session['cart.sum'] >= Order::getMinSumForAdv()) ? 0 : Order::getDeliveryTypeArray()[$delivery_type]['cost'];

		$this->layout = false;
		return $this->render('cart-checkout', compact('session'));
	}

	public function actionDecreaseProductWeightCheckout($delivery_type)
	{
		$id = Yii::$app->request->get('id');
		$is_scratbox = Yii::$app->request->get('scratbox') ? Yii::$app->request->get('scratbox') : false;

		$session = Yii::$app->session;
		$session->open();
		$cart = new Cart();
		$response = $cart->decreaseProductWeight($id, $is_scratbox);

		if ($response === 'MIN_SUM_FOR_PROMO_ERROR') {
			return $response;
		}

		$user = User::findOne(Yii::$app->user->id);

		if ($user) {
			$session['cart.max-points'] = Cart::getMaxSumForUsersPoints(CartController::maxSumForBonusPoint($session['cart']), $user->users_points);
			if ($session['cart.points'] > $session['cart.max-points']) {
				$session['cart.points'] = $session['cart.max-points'];
			}
		}

		$session['cart.delivery-cost'] = ($session['cart.sum'] >= Order::getMinSumForAdv()) ? 0 : Order::getDeliveryTypeArray()[$delivery_type]['cost'];

		$this->layout = false;
		return $this->render('cart-checkout', compact('session'));
	}

	public function actionIncreaseProductWeightCheckout($delivery_type)
	{
		$id = Yii::$app->request->get('id');
		$is_scratbox = Yii::$app->request->get('scratbox') ? Yii::$app->request->get('scratbox') : false;

		$session = Yii::$app->session;
		$session->open();
		$cart = new Cart();
		$cart->increaseProductWeight($id, $is_scratbox);

		$user = User::findOne(Yii::$app->user->id);

		if ($user) {
			$session['cart.max-points'] = Cart::getMaxSumForUsersPoints(CartController::maxSumForBonusPoint($session['cart']), $user->users_points);
			if ($session['cart.points'] > $session['cart.max-points']) {
				$session['cart.points'] = $session['cart.max-points'];
			}
		}

		$session['cart.delivery-cost'] = ($session['cart.sum'] >= Order::getMinSumForAdv()) ? 0 : Order::getDeliveryTypeArray()[$delivery_type]['cost'];

		$this->layout = false;
		return $this->render('cart-checkout', compact('session'));
	}

	public function actionDecreaseBonusPointsCheckout()
	{
		$session = Yii::$app->session;
		$session->open();
		$cart = new Cart();
		if ($cart->decreaseBonusPoints()) {
			$user = User::findOne(Yii::$app->user->id);

			if ($user) {
				$session['cart.max-points'] = Cart::getMaxSumForUsersPoints(CartController::maxSumForBonusPoint($session['cart']), $user->users_points);
				if ($session['cart.points'] <= 0) {
					$session['cart.points'] = 0;
				}
			}

			$this->layout = false;
			return $this->render('cart-checkout', compact('session'));
		}
		else {
			return false;
		}
	}

	public function actionIncreaseBonusPointsCheckout()
	{
		$session = Yii::$app->session;
		$session->open();
		$cart = new Cart();
		if ($cart->increaseBonusPoints()) {
			$user = User::findOne(Yii::$app->user->id);

			if ($user) {
				$session['cart.max-points'] = Cart::getMaxSumForUsersPoints(CartController::maxSumForBonusPoint($session['cart']), $user->users_points);
				if ($session['cart.points'] > $session['cart.max-points']) {
					$session['cart.points'] = $session['cart.max-points'];
				}
			}

			$this->layout = false;
			return $this->render('cart-checkout', compact('session'));
		}
		else {
			return false;
		}
	}

	public function actionChangeBonusPointsCheckout($points)
	{
		$session = Yii::$app->session;
		$session->open();

		$user = User::findOne(Yii::$app->user->id);

		if ($user) {
			$session['cart.max-points'] = Cart::getMaxSumForUsersPoints(CartController::maxSumForBonusPoint($session['cart']), $user->users_points);

			if ($points > $session['cart.max-points']) {
				$session['cart.points'] = $session['cart.max-points'];
			}
			else {
				$session['cart.points'] = $points;
			}
		}

		$this->layout = false;
		return $this->render('cart-checkout', compact('session'));
	}

	public function actionChangeDeliveryType($delivery_type)
	{
		$session = Yii::$app->session;
		$session->open();
		$session['cart.delivery-cost'] = ($session['cart.sum'] >= Order::getMinSumForAdv()) ? 0 : Order::getDeliveryTypeArray()[$delivery_type]['cost'];

		if (($session['cart.sum'] < Order::getMinSumForPromo()) && $session['promo']) {
			return 'MIN_SUM_FOR_PROMO_ERROR';
		}

		$user = User::findOne(Yii::$app->user->id);

		if ($user) {
			$session['cart.max-points'] = Cart::getMaxSumForUsersPoints(CartController::maxSumForBonusPoint($session['cart']), $user->users_points);
			if ($session['cart.points'] > $session['cart.max-points']) {
				$session['cart.points'] = $session['cart.max-points'];
			}
		}

		$this->layout = false;
		return $this->render('cart-checkout', compact('session'));
	}

	public function actionApplyPromo($promo_name)
	{
		$promo = Promo::find()
					->select('*')
					->where(['name' => $promo_name])
					->andWhere(['<=', 'date_from', date('U')])
					->andWhere(['>=', 'date_to', date('U')])
					->andWhere(['=', 'used', 0])
					->one();

		if ($promo) {
			$session = Yii::$app->session;
			$session->open();
			$scrat_box_sum = Cart::findScratBoxesSum();
			$session['cart.points'] = 0;

			if (($session['cart.sum'] - $scrat_box_sum) < Order::getMinSumForPromo()) {
				return 'MIN_SUM_FOR_PROMO_ERROR';
			}

			$session['promo'] = $promo->id;
			$session['promo.used'] = 1;

			$this->layout = false;
			return $this->render('cart-checkout', compact('session'));
		}
		else {
			return 'WRONG_PROMO_ERROR';
		}
	}

	public function actionRollbackPromo()
	{
		$session = Yii::$app->session;
		$session->open();
		$session->remove('promo');
		$session->remove('promo.used');

		$this->layout = false;
		return $this->render('cart-checkout', compact('session'));
	}

	public function actionGetMinSumForPromo()
	{
		return Order::getMinSumForPromo();
	}

	public function actionCheckoutFirst()
	{
		$session = Yii::$app->session;
		$session->open();

		$session['step'] = 1;               //what's the step, for jur_persons

		$order = new Order();

		if ($order->load(Yii::$app->request->post())) {
			$order_delivery_date = Yii::$app->request->post('Order')['delivery_date'];
			$order_delivery_date = date('U', strtotime($order_delivery_date));
			
			if (empty($session['cart'])) {
				Yii::$app->session->setFlash('error', 'Ваша корзина пуста!');
			}
			else {
				$order->delivery_cost = $session['cart.delivery-cost'] ? $session['cart.delivery-cost'] : 0;
				$order->bonus_points = $session['cart.points'] ? $session['cart.points'] : 0;
				$order->delivery_date = $order_delivery_date;

				$promo = Promo::findOne($session['promo']);
				if ($promo) {
					$order->id_promo = $promo->id;
					$order->sum =
						($promo ?
							($promo->discount_type ?
								(
									($session['cart.sum'] > $promo->discount) ?
										($session['cart.sum'] - $promo->discount)
										: 1
								)
								: ($session['cart.sum'] - round(($session['cart.sum'] - Cart::findScratBoxesSum()) / 100 * $promo->discount, 2))
							)
							: $session['cart.sum']
						)  + $session['cart.delivery-cost'] - round($session['cart.points'] / Order::getOnePointCost(), 2);

					if (!$promo->person_type) {
						$promo->used = 1;
						$promo->save( false );
					}
					else {
						if ($promo->max_times_used - $promo->times_used > 0) {
							if ($promo->max_times_used - $promo->times_used == 1) {
								$promo->used = 1;
							}
							$promo->times_used += 1;
							$promo->save( false );
						}

					}
				}
				else {
					$order->id_promo = null;
					$order->sum = $session['cart.sum'] + $session['cart.delivery-cost'] - round($session['cart.points'] / Order::getOnePointCost(), 2);
				}

				$order->id_user = Yii::$app->user->isGuest ? 0 : Yii::$app->user->id;
//				$order->unique_token = Yii::$app->security->generateRandomString(10);
				$order->unique_token = random_int(1000000000, 9999999999);

				if ($order->delivery_date === "NaN"){
				    $order->delivery_date = null;
                }

				if ($order->save()) {

                    $this->saveOrderItems($session['cart'], $order->id);

					$user = User::findOne(Yii::$app->user->id);

					if ($user && $session['cart.points'] > 0) {
						if (!Order::subClientsPoints($order->bonus_points, $user->id, $order->id)) {
							Yii::$app->session->setFlash('error', "Ошибка. Невозможно списать баллы.");
						}
					}

					$session->remove('cart');
					$session->remove('cart.weight');
					$session->remove('cart.sum');
					$session->remove('cart.points');
					$session->remove('cart.max-points');
					$session->remove('step');
					$session->remove('promo');
					$session->remove('promo.used');

					if ($order->id_pay_type == 1) {
						return $this->render('online-payment-erip-redirect', ['order' => $order]);
					}

					if ($order->id_pay_type == 2){
						return $this->render('online-payment-redirect', ['order' => $order]);
					}

					$this->sendOrderEmail($order, $session['cart.points']);
					Yii::$app->session->setFlash('success', 'Ваш заказ под № '.$order->id.' принят.  Мы свяжемся с вами в кратчайшие сроки, чтобы уточнить все детали.');

					return $this->goHome();
				}
				else {
					Yii::$app->session->setFlash('error', 'Произошла ошибка оформления заказа!');
				}
			}
		}

		$pay_types = PaymentsType::getPayTypes();

		$user = User::findOne(Yii::$app->user->id);

		if ($user) {
			$session['cart.max-points'] = Cart::getMaxSumForUsersPoints(CartController::maxSumForBonusPoint($session['cart']), $user['users_points']);
			$session['cart.points'] = 0;
		}
		else {
			$user = [];
		}

		return $this->render('checkout-first', compact('session', 'order', 'pay_types', 'user'));
	}


	public function actionCheckoutSecond()
	{
		$session = Yii::$app->session;
		$session->open();
		$order = new Order();

		if ($session['step'] == 1) {
			$order->load(Yii::$app->request->post());
			$session['order'] = Yii::$app->request->post('Order');
		}

		if ($order->load(Yii::$app->request->post()) && ($session['step'] == 2)) {
			if (empty($session['cart'])) {
				Yii::$app->session->setFlash('error', 'Ваша корзина пуста!');
			}
			else {
				$order_delivery_date = $session['order']['delivery_date'];
				$order_delivery_date = date('U', strtotime($order_delivery_date));

				$order->name = $session['order']['name'];
				$order->email = $session['order']['email'];
				$order->telephone = $session['order']['telephone'];
				$order->address = $session['order']['address'];
				$order->comment = $session['order']['comment'];
				$order->delivery_type = $session['order']['delivery_type'];
				$order->delivery_time = $session['order']['delivery_time'];
				$order->id_pay_type = $session['order']['id_pay_type'];
				$order->is_jur_person = 1;

				$order->delivery_cost = $session['cart.delivery-cost'] ? $session['cart.delivery-cost'] : 0;
				$order->bonus_points = $session['cart.points'] ? $session['cart.points'] : 0;
				$order->delivery_date = $order_delivery_date;

				$promo = Promo::findOne($session['promo']);
				if ($promo) {
					$order->id_promo = $promo->id;
					$order->sum =
						($promo ?
							($promo->discount_type ?
								(
								($session['cart.sum'] > $promo->discount) ?
									($session['cart.sum'] - $promo->discount)
									: 1
								)
								: ($session['cart.sum'] - round(($session['cart.sum'] - Cart::findScratBoxesSum()) / 100 * $promo->discount, 2))
							)
							: $session['cart.sum']
						)  + $session['cart.delivery-cost'] - round($session['cart.points'] / Order::getOnePointCost(), 2);

					if (!$promo->person_type) {
						$promo->used = 1;
						$promo->save( false );
					}
					else {
						if ($promo->max_times_used - $promo->times_used > 0) {
							if ($promo->max_times_used - $promo->times_used == 1) {
								$promo->used = 1;
							}
							$promo->times_used += 1;
							$promo->save( false );
						}

					}
				}
				else {
					$order->id_promo = null;
					$order->sum = $session['cart.sum'] + $session['cart.delivery-cost'] - round($session['cart.points'] / Order::getOnePointCost(), 2);
				}

				$order->id_user = Yii::$app->user->isGuest ? 0 : Yii::$app->user->id;
//				$order->unique_token = Yii::$app->security->generateRandomString(10);
				$order->unique_token = random_int(1000000000, 9999999999);

				if ($order->delivery_date === "NaN"){
					$order->delivery_date = null;
				}

				if ($order->save()) {

					$this->saveOrderItems($session['cart'], $order->id);

					$user = User::findOne(Yii::$app->user->id);

					if ($user && $session['cart.points'] > 0) {
						if (!Order::subClientsPoints($order->bonus_points, $user->id, $order->id)) {
							Yii::$app->session->setFlash('error', "Ошибка. Невозможно списать баллы.");
						}
					}

					$session->remove('cart');
					$session->remove('cart.weight');
					$session->remove('cart.sum');
					$session->remove('cart.points');
					$session->remove('cart.max-points');
					$session->remove('step');
					$session->remove('promo');
					$session->remove('promo.used');

					if ($order->id_pay_type == 1) {
						return $this->render('online-payment-erip-redirect', ['order' => $order]);
					}

					if ($order->id_pay_type == 2) {
						return $this->render('online-payment-redirect', ['order' => $order]);
					}

					$this->sendOrderEmail($order, $session['cart.points']);
					Yii::$app->session->setFlash('success', 'Ваш заказ под № '.$order->id.' принят.  Мы свяжемся с вами в кратчайшие сроки, чтобы уточнить все детали.');


					return $this->goHome();
				}
				else {
					$error_str = '';
					foreach ($order->getErrors() as $key => $value) {
						$error_str .= ' '.$value[0];
					}
					Yii::$app->session->setFlash('error', 'Произошла ошибка оформления заказа: '.$error_str);
				}
			}
		}
		$session['step'] = 2;

		$id = Yii::$app->user->id;
		$user = User::find()
		            ->select(['juristic_person.*','user.*'])
		            ->leftJoin('juristic_person','juristic_person.id_user = user.id')
		            ->where(['=','user.id', $id ])
		            ->asArray()
		            ->one();

		if ($user) {
			$session['cart.max-points'] = Cart::getMaxSumForUsersPoints(CartController::maxSumForBonusPoint($session['cart']), $user['users_points']);
		}

		return $this->render('checkout-second', compact('session', 'order', 'pay_types', 'user'));
	}

	private static function maxSumForBonusPoint($items) {
		$temp_sum_for_points = 0;

		if (!empty($items)) {
			foreach ($items as $id => $item) {
				// if not scrat-box
				if ($id != 'scrat-box') {
					$product = Products::findOne($id);
					$order_item_sum = $item['price'] * ($item['weight'] / Products::getMinProductWeight($id));

					if ($product->type != 2) {  // if product is not on sale
						$temp_sum_for_points += $order_item_sum;
					}
				}
			}
		}

		return $temp_sum_for_points;
	}

	protected function saveOrderItems($items, $id_order)
	{
		$temp_sum_for_points = 0;
		$order = Order::findOne($id_order);

		foreach ($items as $id => $item) {
			// if scrat-box
			if ($id == 'scrat-box') {
				foreach ($item as $key => $value) {
					$order_item = new OrderItems();
					$order_item->id_order = $id_order;
					$order_item->is_scratbox = 1;
					$order_item->id_scratbox = $key;
					$order_item->name = $value['name'];
					$order_item->weight = $value['weight'];
					$order_item->special = 0;
					$order_item->price = $value['price'];
					$order_item->sum = $value['price'] * $value['weight'];
					$order_item->save();
				}
			}
			// if regular product
			else {
				$product = Products::findOne($id);
				$order_item_sum = $item['price'] * ($item['weight'] / Products::getMinProductWeight($id));

				$order_item = new OrderItems();
				$order_item->id_order = $id_order;
				$order_item->id_product = $id;
				$order_item->name = $item['name'];
				$order_item->weight = $item['weight'];
				$order_item->special = $product->special;
				$order_item->price = $item['price'];
				$order_item->sum = $order_item_sum;
				$order_item->save();

				if ($order->id_promo == null && $product->type != 2) {  // if promo didn't use and product is not on sale
					$temp_sum_for_points += $order_item_sum;
				}
			}
		}
		if ($order->bonus_points != null) {
			$temp_sum_for_points -= $order->bonus_points * Order::getOnePointCost();
		}
		if ($temp_sum_for_points > 0 && $order->id_user) {
			$order->bonus_points_to_get = round($temp_sum_for_points * (Order::getOrderAdditionalPercentForPoints($temp_sum_for_points) + Order::getOrderPercentForPoints($order->id_user)) / 100, 2);
			$order->overall_sum_to_get = $temp_sum_for_points;
		}
		$order->save(false);
	}

	public function actionCompeteOrder() {
        $params = $_GET;

        if (isset($params['wsb_order_num']) && $params['wsb_order_num']){

            /** @var Order $order */
            $order = Order::find()->where(['unique_token' => $params['wsb_order_num']])->one();

            if (!$order){
                throw new NotFoundHttpException('Страница не найдена',404);
            }

            $order->status = 1;

//            $order->unique_token = Yii::$app->security->generateRandomString(20);
	        $order->unique_token = random_int(10000000000000000000, 99999999999999999999);

            if ($order->save(false)){
                $this->sendOrderEmail($order);
                return $this->render('online-payment-complete');
            }
            else{
                Yii::error($order->getErrors());
                throw new NotFoundHttpException('Возникла проблема, пожалуйста обратитесь в тех поддержку.',500);
            }
        }
        else{
            throw new NotFoundHttpException('Страница не найдена',404);
        }
    }

	protected function sendOrderEmail($order, $points = 0)
	{
		// to user
		Yii::$app->mailer
					->compose('order', ['order' => $order, 'points' => $points])
					->setTo($order->email)
					->setFrom(Yii::$app->params['adminEmail'])
					->setSubject('Вы оформили заказ на сайте Scrathouse.by')
					->send();
//		// to admin
//		Yii::$app->mailer
//					->compose('order', ['order' => $order])
//					->setTo('ScratHouse@mail.ru')
//					->setFrom(Yii::$app->params['adminEmail'])
//					->setSubject('Поступил новый заказ')
//					->send();

	}
}
