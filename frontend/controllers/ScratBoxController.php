<?php
namespace frontend\controllers;

use frontend\models\Scratboxes;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Products;
use yii\data\Pagination;
use common\models\Category;
use yii\web\NotFoundHttpException;
use common\models\User;


class ScratBoxController extends Controller
{
	public function actionIndex($sort_type = 'hits')
	{
		$query = Products::find()
		                 ->select(['products.*', 'category.slug as slug_cat'])
		                 ->leftJoin('category','category.id = products.id_cat')
		                 ->where(['and', ['=', 'products.special', 0], ['=', 'products.presence', 1]])
		                 ->andWhere(['=', 'products.can_be_in_scratbox', 1])
						 ->andWhere(['in', 'products.id_cat', Scratboxes::getScratboxCategories()])
		                 ->orderBy([(($sort_type == 'hits') ? 'hits' : 'price') => (($sort_type == 'hits') ? SORT_DESC : SORT_ASC)])
		                 ->asArray();

		$pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 12]);
		$pages->pageSizeParam = false;
		$products = $query->offset($pages->offset)
		                  ->limit($pages->limit)
		                  ->all();
		$session = Yii::$app->session;
		$session->open();

		return $this->render('scrat-box-products',[
			'products' => $products,
			'pages' => $pages,
			'session' => $session,
		]);
	}

 	public function actionScratBox()
    {
	    $query = Products::find()
	                     ->select(['products.*', 'category.slug as slug_cat'])
		                 ->leftJoin('category','category.id = products.id_cat')
		                 ->where(['=', 'products.presence', '1'])
		                 ->andWhere(['=', 'products.can_be_in_scratbox', 1])
		                 ->andWhere(['in', 'products.id_cat', Scratboxes::getScratboxCategories()])
	                     ->orderBy(['hits'=> SORT_DESC])
	                     ->asArray();
	    $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 9]);

	    $pages->pageSizeParam = false;
	    $products = $query->offset($pages->offset)
	                      ->limit($pages->limit)
	                      ->all();

	    return $this->render('scrat-box',[
		    'products' => $products,
		    'pages' => $pages,
	    ]);

    }

	public function actionScratBoxPopularProducts()
	{
		$query = Products::find()
		                 ->select(['products.*', 'category.slug as slug_cat'])
		                 ->leftJoin('category','category.id = products.id_cat')
						 ->where(['=', 'products.presence', '1'])
						 ->andWhere(['=', 'products.can_be_in_scratbox', 1])
						 ->andWhere(['in', 'products.id_cat', Scratboxes::getScratboxCategories()])
		                 ->orderBy(['hits'=> SORT_DESC])
		                 ->asArray();
		$pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 9, 'route' => 'scrat-box/scrat-box']);

		$pages->pageSizeParam = false;
		$products = $query->offset($pages->offset)
		                          ->limit($pages->limit)
		                          ->all();

		if (!Yii::$app->request->isAjax) {
			return $this->redirect(Yii::$app->request->referrer);
		}

		$this->layout = false;
		return $this->render('scrat-box-popular-products',[
			'products' => $products,
			'pages' => $pages,
		]);
	}

	public function actionScratBoxProducts($slug, $sort_type = 'hits')
	{
		//$slug = Category::getCorrectSlug($slug);
		$category = Category::findOne(['slug' => $slug]);
		if (empty($category)) throw new NotFoundHttpException('Страница не найдена',404);

		$id = $category->id;

		// выводим категорию и все её дочерние
		$search_cats = Category::getTree($id);

		$query = Products::find()
		                 ->select(['products.*', 'category.slug as slug_cat'])
		                 ->leftJoin('category','category.id = products.id_cat')
						 ->where(['and', ['in', 'id_cat', $search_cats], ['=', 'products.special', 0], ['=', 'products.presence', '1']])
						 ->andWhere(['=', 'products.can_be_in_scratbox', 1])
						 ->andWhere(['in', 'products.id_cat', Scratboxes::getScratboxCategories()])
		                 ->orderBy([(($sort_type == 'hits') ? 'hits' : 'price') => (($sort_type == 'hits') ? SORT_DESC : SORT_ASC)])
		                 ->asArray();

		$pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 12]);
		// приводим параметры в ссылке к ЧПУ
		$pages->pageSizeParam = false;
		$products = $query->offset($pages->offset)
		                  ->limit($pages->limit)
		                  ->all();
		$session = Yii::$app->session;
		$session->open();

		return $this->render('scrat-box-products',[
			'products' => $products,
			'pages' => $pages,
			'session' => $session,
		]);
	}

	public function actionScratBoxProductPopup($slug)
	{
		$product = Products::find()
		                   ->select(['products.*', 'category.slug as slug_cat'])
		                   ->leftJoin('category','category.id = products.id_cat')
		                   ->where(['=','products.slug', $slug])
		                   ->asArray()
		                   ->one();
		$id = Yii::$app->user->id;
		$user = User::find()
		            ->select(['juristic_person.*', 'user.*'])
		            ->leftJoin('juristic_person','juristic_person.id_user = user.id')
		            ->where(['=','user.id', $id ])
		            ->asArray()
		            ->one();

		if (empty($product)) throw new \yii\web\NotFoundHttpException('Страница не найдена',404);

		$model = Products::find()->where(['=', 'slug', $slug])->one();
		$model->hits++;
		$model->save(false);

		$this->layout = false;
		return $this->render('scrat-box-product-popup',[
			'product' => $product,
			'user' => $user,
		]);
	}

	public function actionAddToScratBox()
	{
		$id = Yii::$app->request->get('id');

		$product = Products::find()
		                   ->select(['products.*'])
		                   ->where(['=','products.id', $id])
						   ->andWhere(['and', ['=', 'products.special', 0], ['=', 'products.presence', 1]])
		                   ->andWhere(['=', 'products.can_be_in_scratbox', 1])
		                   ->asArray()
		                   ->one();
		if (empty($product)) return false;
		$session = Yii::$app->session;
		$session->open();

		if (empty($_SESSION['current-scrat-box'])) {
			$scratbox = new Scratboxes();
			$scratbox->name = 'Бокс';
			$scratbox->save(false);
			$_SESSION['current-scrat-box']['id'] = $scratbox->id;
		}
		else {
			$scratbox = Scratboxes::findOne($_SESSION['current-scrat-box']['id']);
		}

		if (!Yii::$app->request->isAjax) {
			return $this->redirect(['/scrat-box/scrat-box-products', 'slug' => \common\models\Category::getStartCategory()], 301);
		}

		$try_to_add = $scratbox->addToScratbox($product);

		if ($try_to_add !== 'SUCCESS') {
			return $try_to_add;
		}

		$this->layout = false;
		return $this->render('scrat-box-client', compact('session'));
	}

	public function actionClearScratBox()
	{
		if (!empty($_SESSION['current-scrat-box']['id'])) {
			$scratbox = Scratboxes::findOne($_SESSION['current-scrat-box']['id']);
			$scratbox->delete();
		}

		$session = Yii::$app->session;
		$session->open();
		$session->remove('current-scrat-box');

		$this->layout = false;
		return $this->render('scrat-box-client', compact('session'));
	}

	public function actionDeleteProductFromScratBox()
	{
		$id = Yii::$app->request->get('id');
		$session = Yii::$app->session;
		$session->open();

		if (empty($_SESSION['current-scrat-box']['id'])) {
			return false;
		}

		$scratbox = Scratboxes::findOne($_SESSION['current-scrat-box']['id']);
		$scratbox->deleteProductFromScratBox($id);

		$this->layout = false;
		return $this->render('scrat-box-client', compact('session'));
	}

	public function actionRefreshScratBoxClient()
	{
		$session = Yii::$app->session;
		$session->open();
		$this->layout = false;
		return $this->render('scrat-box-client', compact('session'));
	}
 
}
