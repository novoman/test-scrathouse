<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use common\models\ContactForm;
use backend\models\Shops;
use common\models\ContactPhoneForm;
use common\services\auth\SignupService;
use common\models\User;
use frontend\models\Auth;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\db\Exception;

/* @var $auth Auth */
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
	            'class' => 'yii\authclient\AuthAction',
	            'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
	    if (!Yii::$app->user->isGuest) {
		    return $this->goHome();
	    }

	    $form = new LoginForm();

	    if (Yii::$app->request->isAjax && $form->load(Yii::$app->request->post())) {
		    Yii::$app->response->format = Response::FORMAT_JSON;
		    return ActiveForm::validate($form);
	    }

	    if ($form->load(Yii::$app->request->post())) {
		    try {
			    if ($form->login()) {
				    return $this->goHome();
			    }
			    else {
				    Yii::$app->session->setFlash('error', 'Не верно введены логин или пароль!');
				    return $this->goHome();
			    }
		    }
		    catch (\DomainException $e) {
			    Yii::$app->session->setFlash('error', $e->getMessage());
			    return $this->goHome();
		    }
	    }
	    return $this->goHome();
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


	public function onAuthSuccess($client)
	{
		$attributes = $client->getUserAttributes();
		//echo $attributes['name']['familyName'];
//		echo '<pre>';
//		var_dump($attributes);
//		die;
		$auth = Auth::find()->where([
			'source' => $client->getId(),
			'source_id' => $attributes['id'],
		])->one();

		if (empty($attributes['email']) && empty($attributes['emails'])) {
			Yii::$app->getSession()->setFlash('error', 'Ошибка авторизации! Пустой email.');
			return false;
		}
		if (Yii::$app->user->isGuest) {
			if ($auth) { // авторизация
				$user = $auth->user;
				if ($this->updateUserInfo($user)) {
					Yii::$app->user->login($user, Yii::$app->params['user.rememberMeDuration']);
					Yii::$app->getSession()->setFlash('success', 'Вы удачно авторизировались!');
				}
				else {
					Yii::$app->getSession()->setFlash('error', 'Ошибка авторизации!');
				}
			}
			else { // регистрация
				switch ($client->name) {
					case 'google':
						$existingUser = User::findOne(['email' => $attributes['emails'][0]['value']]);
						break;
					default:
						$existingUser = User::findOne(['email' => $attributes['email']]);
						break;
				}
				if ($existingUser) {
					$auth = new Auth( [
						'user_id'   => $existingUser->id,
						'source'    => $client->getId(),
						'source_id' => (string) $attributes['id'],
					] );
					if ( $this->updateUserInfo( $existingUser ) && $auth->save() ) {
						Yii::$app->user->login( $existingUser, Yii::$app->params['user.rememberMeDuration'] );
						Yii::$app->getSession()->setFlash('success', 'Вы удачно авторизировались!');
					}
					else {
						Yii::$app->getSession()->setFlash( 'error', $auth->getErrors(), [ 'client' => $client->getTitle() ] );
					}
				}
				else {
					$password = Yii::$app->security->generateRandomString( 16 );
					switch ($client->name) {
						case 'google':
							$user = new User( [
								'last_name' => $attributes['name']['familyName'],
								'first_name'  => $attributes['name']['givenName'],
								'email'      => $attributes['emails'][0]['value'],
								'password'   => $password,
								'status'     => User::STATUS_ACTIVE,
							] );
							break;
						default:
							$name = explode( ' ', $attributes['name'] );
							$user = new User( [
								'first_name' => $name[0],
								'last_name'  => $name[1],
								'email'      => $attributes['email'],
								'password'   => $password,
								'status'     => User::STATUS_ACTIVE,
							] );
							break;
					}

					$user->generateAuthKey();
					$user->generatePasswordResetToken();
					$transaction = $user->getDb()->beginTransaction();
					if ( $user->save() ) {
						$auth = new Auth( [
							'user_id'   => $user->id,
							'source'    => $client->getId(),
							'source_id' => (string) $attributes['id'],
						] );
						if ( $auth->save() ) {
							$transaction->commit();
							Yii::$app->user->login( $user, Yii::$app->params['user.rememberMeDuration'] );
							Yii::$app->getSession()->setFlash('success', 'Вы удачно авторизировались!');
						}
						else {
							$transaction->rollBack();
							Yii::$app->getSession()->setFlash( 'error', $auth->getErrors(), [ 'client' => $client->getTitle() ] );
						}
					}
					else {
						$transaction->rollBack();
						Yii::$app->getSession()->setFlash( 'error', 'Ошибка авторизации', [ 'client' => $client->getTitle() ] );
					}
				}
			}
		}
		else { // Пользователь уже зарегистрирован
			if (!$auth) { // добавляем внешний сервис аутентификации
				$auth = new Auth([
					'user_id' => Yii::$app->user->id,
					'source' => $client->getId(),
					'source_id' => $attributes['id'],
				]);
				$auth->save();
			}
		}
		return true;
	}

	public function updateUserInfo($user)
	{
		if ($user->status === User::STATUS_WAIT) {
			$password = Yii::$app->security->generateRandomString(16);
			$user->status = User::STATUS_ACTIVE;
			$user->password = $password;
			return $user->save();
		}
		return $user->status !== User::STATUS_BLOCKED;
	}

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
	    $form = new SignupForm();

	    if (Yii::$app->request->isAjax && $form->load(Yii::$app->request->post())) {
		    Yii::$app->response->format = Response::FORMAT_JSON;
		    return ActiveForm::validate($form);
	    }

	    if ($form->load(Yii::$app->request->post()) && $form->validate()) {
		    $signupService = new SignupService();

		    try{
			    $user = $signupService->signup($form);
			    Yii::$app->session->setFlash('success', 'Для окончания регистрации подтвердите Ваш email.');
			    $signupService->sentEmailConfirm($user);
			    return $this->goHome();
		    }
		    catch (\RuntimeException $e){
			    Yii::$app->errorHandler->logException($e);
			    Yii::$app->session->setFlash('error', $e->getMessage());
		    }
	    }
	    else {
		    Yii::$app->session->setFlash('error', 'Oops... Произошла ошибка при регистрации. Пожалуйста, проверьте вводимые Вами данные.');
	    }
	    return $this->goHome();
    }


	public function actionSignupConfirm($token)
	{
		$signupService = new SignupService();

		try{
			$signupService->confirmation($token);
			Yii::$app->session->setFlash('success', 'Вы успешно зарегистрировались.');
		}
		catch (\Exception $e){
			Yii::$app->errorHandler->logException($e);
			Yii::$app->session->setFlash('error', $e->getMessage());
		}

		return $this->goHome();
	}

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Проверьте Ваш email для дальнейших инструкций.');

                return $this->goHome();
            }
            else {
                Yii::$app->session->setFlash('error', 'Извините, мы не можем изменить пароль для введённого email.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        }
        catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Новый пароль сохранён.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionWholesale()
    {
	    return $this->render('wholesale', []);
    }

	public function actionContact()
	{
		$shopList = Shops::find()->asArray()->all();

		return $this->render('contact', [
			'shopList' => $shopList
		]);
	}

	/**
	 * Displays contact-email page.
	 *
	 * @return mixed
	 */
	public function actionContactEmail()
	{
		$model = new ContactForm();

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			if ($model->sendEmail(Yii::$app->params['adminEmail']) && $model->save(false)) {
				Yii::$app->session->setFlash('success', 'Спасибо за связь с нами. Мы постараемся ответить Вам так скоро, как это возможно.');
				return $this->goHome();
			}
			else {
				Yii::$app->session->setFlash('error', 'Ошибка отправки сообщения.');
			}

			return $this->refresh();
		}
		else {
			$this->createAction('captcha')->getVerifyCode(true);

			return $this->render('contact-email', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Displays contact-phone page.
	 *
	 * @return mixed
	 */
	public function actionContactPhone()
	{
		$model = new ContactPhoneForm();

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			if ($model->sendEmail(Yii::$app->params['adminEmail']) && $model->save(false)) {
				Yii::$app->session->setFlash('success', 'Спасибо за связь с нами. Мы постараемся ответить Вам так скоро, как это возможно.');
				return $this->goHome();
			}
			else {
				Yii::$app->session->setFlash('error', 'Ошибка отправки сообщения.');
			}
			return $this->refresh();
		}
		else {
			$this->createAction('captcha')->getVerifyCode(true);

			return $this->render('contact-phone', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Displays about page.
	 *
	 * @return mixed
	 */
	public function actionAbout()
	{
		return $this->render('about');
	}

	public function actionContactOffer()
	{
		return $this->render('contact-offer');
	}

}
