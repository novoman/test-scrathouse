<?php
namespace frontend\controllers;

use common\models\Category;
use Yii;
use yii\web\Controller;
use common\models\Products;
use yii\data\Pagination;
use yii\helpers\Json;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use common\models\User;

class ProductsController extends Controller
{

	public function actionIndex($sort_type = 'hits')
	{
		$query = Products::find()
		                 ->select(['products.*'])
						 ->where(['<>', 'products.type', 3])
						 ->orderBy(['presence' => SORT_DESC])
						 ->addOrderBy([(($sort_type == 'hits') ? 'hits' : 'price') => (($sort_type == 'hits') ? SORT_DESC : SORT_ASC)])
		                 ->asArray();

		$pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 9]);
		// приводим параметры в ссылке к ЧПУ
		$pages->pageSizeParam = false;
		$products = $query->offset($pages->offset)
		                  ->limit($pages->limit)
		                  ->all();

		return $this->render('products', [
			'products' => $products,
			'pages' => $pages,
		]);
	}

 	public function actionProducts($slug, $sort_type = 'hits')
    {
	    //$slug = Category::getCorrectSlug($slug);
	    $category = Category::findOne(['slug' => $slug]);
	    if (empty($category)) throw new NotFoundHttpException('Страница не найдена',404);

	    $id = $category->id;

	    // выводим категорию и все её дочерние
	    $search_cats = Category::getTree($id);

	    $query = Products::find()
		                   ->select(['products.*'])
	                       ->where(['in', 'id_cat', $search_cats])
	                       ->andWhere(['<>', 'products.type', 3])
	                       ->orderBy(['presence' => SORT_DESC])
	                       ->addOrderBy([(($sort_type == 'hits') ? 'hits' : 'price') => (($sort_type == 'hits') ? SORT_DESC : SORT_ASC)])
	                       ->asArray();

	    $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 9]);
	    // приводим параметры в ссылке к ЧПУ
	    $pages->pageSizeParam = false;
	    $products = $query->offset($pages->offset)
	                    ->limit($pages->limit)
	                    ->all();

        return $this->render('products', [
	        'products' => $products,
	        'pages' => $pages,
        ]);
    }

	public function actionProductCard($slug)
	{
		$product = Products::find()
		                   ->select(['products.*', 'category.slug as slug_cat'])
		                   ->leftJoin('category','category.id = products.id_cat')
		                   ->where(['=','products.slug', $slug])
		                   ->andWhere(['<>', 'products.type', 3])
		                   ->asArray()
		                   ->one();
		//print_r($product);
		$id = Yii::$app->user->id;
		$user = User::find()
		            ->select(['juristic_person.*', 'user.*'])
		            ->leftJoin('juristic_person','juristic_person.id_user = user.id')
		            ->where(['=','user.id', $id ])
		            ->asArray()
		            ->one();

		if (empty($product)) throw new NotFoundHttpException('Страница не найдена',404);

		$model = Products::find()->where(['=', 'slug', $slug])->one();
		$model->hits++;
		$model->save(false);

		return $this->render('product-card', [
			'product' => $product,
			'model' => $model,
			'user' => $user,
		]);
	}

	public function actionSearch()
	{
		$q = trim(Yii::$app->request->get('q'));
		$query = Products::find()
		                 ->select(['products.*'])
		                 ->andWhere(['like', 'products.name', '%'.$q.'%', false])
						 ->orWhere(['like', 'products.country', '%'.$q.'%', false]);
		$words = explode(' ', $q);

		foreach ($words as $word) {
			$query = $query
						->orWhere(['like', 'products.name', '%'.$word.'%', false])
						->orWhere(['like', 'products.country', '%'.$word.'%', false]);
		}

		$query = $query->having(['<>', 'products.type', 3])->asArray();

		$pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 9]);

		$pages->pageSizeParam = false;
		$products = $query->offset($pages->offset)
		                  ->limit($pages->limit)
		                  ->all();

		return $this->render('search', compact('products', 'pages', 'q'));
	}

	public function actionSearchByName()
	{
		if (Yii::$app->request->isAjax) {
			if ($data = Yii::$app->request->post('data')) {

				$data = trim(Json::decode($data)['data']);

				$query = Products::find()
								 ->select(['slug', 'name', 'country', 'type'])
				                 ->andWhere(['like', 'products.name', '%'.$data.'%', false])
				                 ->orWhere(['like', 'products.country', '%'.$data.'%', false]);
				$words = explode(' ', $data);

				foreach ($words as $word) {
					if ($word == '') continue;
					$query = $query
								->orWhere(['like', 'products.name', '%'.$word.'%', false])
								->orWhere(['like', 'products.country', '%'.$word.'%', false]);
				}

				$products = $query->having(['<>', 'products.type', 3])->asArray()->all();

				Yii::$app->response->format = Response::FORMAT_JSON;

				if (!$products) {
					return '';
				}

				return $products;
			}
		}
		return '';
	}

}
