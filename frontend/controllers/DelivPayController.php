<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Shops;

class DelivPayController extends Controller
{

 	public function actionDelivPay()
    {
	    $shopList = Shops::find()->asArray()->all();
        return $this->render('deliv-pay',[
	        'shopList' => $shopList
        ]);
    }

}
