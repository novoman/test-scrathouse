<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Shops;

class ShopsController extends Controller
{

 	public function actionShops()
    {
	    $shopList = Shops::find()->asArray()->all();
        return $this->render('shops',[
	        'shopList' => $shopList
        ]);
    }

}
