<?php
/**
 * Created by PhpStorm.
 * User: Евгений Майоров
 * Date: 25.02.2019
 * Time: 17:50
 */

namespace frontend\components\webpay;

use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\httpclient\Client;

class WebPay
{
    const COMPLETED = 'Completed';
    const Declined = 'Declined';
    const PENDING = 'Pending';
    const AUTHORIZED = 'Authorized';
    const REFUNDED = 'Refunded';
    const SYSTEM = 'System';
    const VOIDED = 'Voided';
    const FAILED = 'Failed';
    const PARTIAL_VOIDED = 'Partial Voided';
    const RECURRENT = 'Recurrent';

    public static $completed = [
        self::COMPLETED,
        self::AUTHORIZED,
    ];

    public static $canceled = [
        self::REFUNDED,
        self::VOIDED,
        self::PARTIAL_VOIDED,
    ];

    public static $system = [
        self::SYSTEM,
        self::FAILED,
    ];

    public function sendTestData(){

        $wsb_invoice_item_name = [
            'Орехи',
            'Приправы'
        ];

        $wsb_invoice_item_quantity = [
            2,
            1
        ];

        $wsb_invoice_item_price = [
            30,
            15
        ];

        try {
            $wsb_storeid = \Yii::$app->params['webPayStoreId'];
            $wsb_order_num = 'wafw423rdw235';
            $wsb_test = \Yii::$app->params['webPayTestMode'];
            $wsb_currency_id = 'BYN';
            $wsb_seed = \Yii::$app->security->generateRandomString(10);
            $wsb_total = null;
            $secretKey = \Yii::$app->params['webPaySecretKey'];

            if (count($wsb_invoice_item_quantity) === count($wsb_invoice_item_price)){
                $iterator = count($wsb_invoice_item_quantity);

                for($i = 0; $i < $iterator; $i++){
                    $wsb_total += $wsb_invoice_item_quantity[$i] * $wsb_invoice_item_price[$i];
                }
            } else {
                \Yii::error('Не соответствие wsb_invoice_item_quantity и wsb_invoice_item_price');
                return false;
            }

        } catch (Exception $e) {
            \Yii::error($e->getMessage());
            return false;
        }

        $client = new Client();
        try {
            $response = $client->createRequest()
                ->setMethod('POST')
                ->setUrl(\Yii::$app->params['webPayRequestURL'])
                ->setData([
                    '*scart' => '',
                    'wsb_storeid' => $wsb_storeid,
                    'wsb_order_num' => $wsb_order_num,
                    'wsb_currency_id' => $wsb_currency_id,
                    'wsb_version' => '2',
                    'wsb_seed' => $wsb_seed,
                    'wsb_test' => $wsb_test,
                    'wsb_customer_name' => 'Майоров Евгений Борисович',
                    'wsb_signature' => sha1($wsb_seed . $wsb_storeid. $wsb_order_num . $wsb_test. $wsb_currency_id . $wsb_total .
                        $secretKey),
                    'wsb_invoice_item_name' => $wsb_invoice_item_name,
                    'wsb_invoice_item_quantity' => $wsb_invoice_item_quantity,
                    'wsb_invoice_item_price' => $wsb_invoice_item_price,
                    'wsb_total' => $wsb_total
                ])
                ->send();

            return $response;
        } catch (InvalidConfigException $e) {
            \Yii::error($e->getMessage());
            return false;
        }
    }
}