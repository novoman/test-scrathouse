<?php

namespace frontend\models;

use common\models\Order;
use Yii;
use common\models\User;
use common\models\Products;
use backend\models\AdminNumbers;

class Cart extends \yii\db\ActiveRecord
{
	public function behaviors()
	{
		return [
			'image' => [
				'class' => 'rico\yii2images\behaviors\ImageBehave',
			]
		];
	}

    public function addToCart ($product, $weight)
    {
	    $price = $product['type'] != 2 ? $product['price'] : $product['discount_price'];

    	if (!$weight) {
    		$weight = Products::getMinProductWeight($product['id']);
	    }

		if (isset($_SESSION['cart'][$product['id']])) {
			$_SESSION['cart'][$product['id']]['weight'] += $weight;
		}
		else {
			$_SESSION['cart'][$product['id']] = [
				'name' => $product['name'],
				'weight' => $weight,
				'price' => $price,
				'price_before' => $product['type'] == 2 ? $product['price'] : null,
				'slug' => $product['slug'],
			];
		}
	    $_SESSION['cart.sum'] = isset($_SESSION['cart.sum']) ? ($_SESSION['cart.sum'] + ($weight / Products::getMinProductWeight($product['id'])) * $price) : (($weight / Products::getMinProductWeight($product['id'])) * $price);
    }

	public function addScratBoxToCart ($scratbox, $count)
	{
		$count = $count ? $count : 1;

		if (isset($_SESSION['cart']['scrat-box'][$scratbox->id])) {
			$_SESSION['cart']['scrat-box'][$scratbox->id]['weight'] += $count;
		}
		else {
			$_SESSION['cart']['scrat-box'][$scratbox->id] = [
				'name' => $scratbox->name,
				'weight' => $count,
				'price' => Scratboxes::getScratboxPrice(),
			];
		}
		$_SESSION['cart.sum'] = isset($_SESSION['cart.sum']) ? ($_SESSION['cart.sum'] + $count * Scratboxes::getScratboxPrice()) : ($count * Scratboxes::getScratboxPrice());
	}

    public function deleteProduct($id, $is_scratbox = false)
    {
    	if ($is_scratbox) {
		    if (!isset($_SESSION['cart']['scrat-box'][$id])) return false;
		    $sumMinus = $_SESSION['cart']['scrat-box'][$id]['weight'] * Scratboxes::getScratboxPrice();

		    if ((($_SESSION['cart.sum'] - $sumMinus) < Order::getMinSumForPromo()) && isset($_SESSION['promo'])) {
			    return 'MIN_SUM_FOR_PROMO_ERROR';
		    }

		    $_SESSION['cart.sum'] -= $sumMinus;
		    unset($_SESSION['cart']['scrat-box'][$id]);
		    if (empty($_SESSION['cart']['scrat-box'])) {
			    unset($_SESSION['cart']['scrat-box']);
		    }
	    }
	    else {
		    if (!isset($_SESSION['cart'][$id])) return false;
		    $sumMinus = ($_SESSION['cart'][$id]['weight'] / Products::getMinProductWeight($id)) * $_SESSION['cart'][$id]['price'];

		    if ((($_SESSION['cart.sum'] - $sumMinus) < Order::getMinSumForPromo()) && isset($_SESSION['promo'])) {
			    return 'MIN_SUM_FOR_PROMO_ERROR';
		    }

		    $_SESSION['cart.sum'] -= $sumMinus;
		    unset($_SESSION['cart'][$id]);
	    }
	    return true;
    }

	public function decreaseProductWeight($id, $is_scratbox = false)
	{
		if ($is_scratbox) {
			if (!isset($_SESSION['cart']['scrat-box'][$id])) return false;
			if ($_SESSION['cart']['scrat-box'][$id]['weight'] > 1) {
				if ((($_SESSION['cart.sum'] - Scratboxes::getScratboxPrice()) < Order::getMinSumForPromo()) && isset($_SESSION['promo'])) {
					return 'MIN_SUM_FOR_PROMO_ERROR';
				}

				$_SESSION['cart']['scrat-box'][$id]['weight'] -= 1;
				$_SESSION['cart.sum'] -= Scratboxes::getScratboxPrice();
			}
			else {
				return false;
			}
		}
		else {
			if (!isset($_SESSION['cart'][$id])) return false;
			if ($_SESSION['cart'][$id]['weight'] > Products::getMinProductWeight($id)) {
				$sumMinus = $_SESSION['cart'][$id]['price'];

				if ((($_SESSION['cart.sum'] - $sumMinus) < Order::getMinSumForPromo()) && isset($_SESSION['promo'])) {
					return 'MIN_SUM_FOR_PROMO_ERROR';
				}

				$_SESSION['cart'][$id]['weight'] -= Products::getMinProductWeight($id);
				$_SESSION['cart.sum'] -= $sumMinus;
			}
			else {
				return false;
			}
		}
		return true;
	}

	public function increaseProductWeight($id, $is_scratbox = false)
	{
		if ($is_scratbox) {
			if (!isset($_SESSION['cart']['scrat-box'][$id])) return false;
				$_SESSION['cart']['scrat-box'][$id]['weight'] += 1;
				$_SESSION['cart.sum'] += Scratboxes::getScratboxPrice();
		}
		else {
			if (!isset($_SESSION['cart'][$id])) return false;
			$sumPlus = $_SESSION['cart'][$id]['price'];
			$_SESSION['cart'][$id]['weight'] += Products::getMinProductWeight($id);
			$_SESSION['cart.sum'] += $sumPlus;
		}
		return true;
	}

	public function decreaseBonusPoints()
	{
		$_SESSION['cart.points']--;

		return true;
	}

	public function increaseBonusPoints()
	{
		$_SESSION['cart.points']++;

		return true;
	}

	public static function getMaxSumForUsersPoints($sum, $users_points)
	{
		$max_percent = AdminNumbers::findOne(['name' => 'max-percent-for-users-points']) ? AdminNumbers::findOne(['name' => 'max-percent-for-users-points'])->value : 0;

		if (!$max_percent) return 0;

		$max_points = floor($sum / 100 * $max_percent) * Order::getOnePointCost();

		return $max_points > $users_points ? $users_points : $max_points;
	}

	public static function findScratBoxesSum()
	{
		$count = 0;
		if (isset($_SESSION['cart']['scrat-box'])) {
			foreach($_SESSION['cart']['scrat-box'] as $value) {
				$count += $value['weight'];
			}
			return $count * Scratboxes::getScratboxPrice();
		}

		return 0;
	}

}
