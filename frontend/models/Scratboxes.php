<?php

namespace frontend\models;

use backend\models\AdminNumbers;
use common\models\Category;
use common\models\Products;
use Yii;

/**
 * This is the model class for table "scratboxes".
 *
 * @property int $id
 * @property string $name
 *
 * @property Cart $cart
 * @property ScratboxesProducts[] $scratboxesProducts
 */
class Scratboxes extends \yii\db\ActiveRecord
{
	const NUMBER_OF_PRODUCTS_IN_SCRATBOX = 4;

	public static function getScratboxPrice()
	{
		return AdminNumbers::findOne(['name' => 'scrat-box-price']) ? AdminNumbers::findOne(['name' => 'scrat-box-price'])->value : 50;
	}

	public static function getScratboxCategories()
	{
		$first_category_id = AdminNumbers::findOne(['name' => 'scratbox-first-category-id'])->value;
		$second_category_id = AdminNumbers::findOne(['name' => 'scratbox-second-category-id'])->value;
		$third_category_id = AdminNumbers::findOne(['name' => 'scratbox-third-category-id'])->value;

		return array_values(array_merge(Category::getTree($first_category_id), Category::getTree($second_category_id), Category::getTree($third_category_id)));
	}

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scratboxes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_cart'], 'integer'],
            [['name'], 'string', 'max' => 30],
            [['id_cart'], 'exist', 'skipOnError' => true, 'targetClass' => Cart::className(), 'targetAttribute' => ['id_cart' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'id_cart' => 'Id Cart',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCart()
    {
        return $this->hasOne(Cart::className(), ['id' => 'id_cart']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScratboxesProducts()
    {
        return $this->hasMany(ScratboxesProducts::className(), ['id_box' => 'id']);
    }

	private function checkProductCategory($category_id)
	{
		if (!in_array($category_id, Scratboxes::getScratboxCategories())) return false;

		$first_category_id = (int)AdminNumbers::findOne(['name' => 'scratbox-first-category-id'])->value;
		$second_category_id = (int)AdminNumbers::findOne(['name' => 'scratbox-second-category-id'])->value;
		$third_category_id = (int)AdminNumbers::findOne(['name' => 'scratbox-third-category-id'])->value;

		$category_count_array = [];
		$category_count_array[$first_category_id] = 0;
		$category_count_array[$second_category_id] = 0;

		foreach ($_SESSION['current-scrat-box']['products'] as $value) {
			$current_category_id = Products::findOne($value['id'])->id_cat;

			if (in_array($first_category_id, Category::getParentsIds($current_category_id))) {
				$category_count_array[$first_category_id] += 1;
			}
			else if (in_array($second_category_id, Category::getParentsIds($current_category_id))) {
				$category_count_array[$second_category_id] += 1;
			}
			// 1 и 3 категория вместе
			else if (in_array($third_category_id, Category::getParentsIds($current_category_id))) {
				$category_count_array[$first_category_id] += 1;
			}
		}

		if ((in_array($first_category_id, Category::getParentsIds($category_id)) && ($category_count_array[$first_category_id] >= 2)) ||
		    (in_array($second_category_id, Category::getParentsIds($category_id)) && ($category_count_array[$second_category_id] >= 2)) ||
		    (in_array($third_category_id, Category::getParentsIds($category_id)) && ($category_count_array[$first_category_id] >= 2))) {
			return false;
		}

		return true;
	}

	public function addToScratBox($product)
	{
		if (!$product['can_be_in_scratbox']) return false;
		if (!empty($_SESSION['current-scrat-box']['products'])) {
			if (count($_SESSION['current-scrat-box']['products']) >= Scratboxes::NUMBER_OF_PRODUCTS_IN_SCRATBOX) {
				return 'MAX_COUNT_ERROR';
			}

			foreach ($_SESSION['current-scrat-box']['products'] as $value) {
				if ($value['id'] == $product['id']) {
					return 'SIMILAR_PRODUCT_ERROR';
				}
			}

			if (!$this->checkProductCategory($product['id_cat'])) {
				return 'WRONG_COUNT_FOR_ONE_CATEGORY';
			}
		}

		$product2scratbox = new ScratboxesProducts();

		$product2scratbox->id_box = $this->id;
		$product2scratbox->id_product = $product['id'];
		$product2scratbox->weight = Yii::$app->params['min_scrat_box_weight'];

		$product2scratbox->save();

		$_SESSION['current-scrat-box']['products'][$product2scratbox->id] = ['id' => $product['id']];

		return 'SUCCESS';
	}

	public function deleteProductFromScratBox($id)
	{
		$product2scratbox = ScratboxesProducts::findOne($id);

		if (!isset($_SESSION['current-scrat-box']['products'][$product2scratbox->id])) return false;
		unset($_SESSION['current-scrat-box']['products'][$product2scratbox->id]);

		$product2scratbox->delete();

		return true;
	}

	public function clearScratBox()
	{
		$this->delete();

		$session = Yii::$app->session;
		$session->open();
		$session->remove('current-scrat-box');
	}

}
