<?php

namespace frontend\models;

use common\models\BonusPercents;
use common\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "juristic_person".
 *
 * @property int $id
 * @property string $jur_name
 * @property string $jur_address
 * @property string $bank
 * @property string $checking_account
 * @property string $bank_code
 * @property int $unp
 * @property string $director
 * @property string $based
 * @property integer $id_user
 *
 * @property User[] $users
 */
class Profile extends \yii\db\ActiveRecord
{

	const CELLS_COUNT = 6;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'juristic_person';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['unp'], 'integer'],
            [['jur_name', 'bank', 'director', 'based'], 'string', 'max' => 100],
            [['jur_address'], 'string', 'max' => 200],
            [['checking_account', 'bank_code'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jur_name' => 'Юридическое имя',
            'jur_address' => 'Юридический адрес',
            'bank' => 'Банк',
            'checking_account' => 'Checking Account',
            'bank_code' => 'Код банка',
            'unp' => 'УНП',
            'director' => 'Руководитель',
            'based' => 'Основано на',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id_jur_person' => 'id']);
    }

	public function getDaysList() {
		$day_from = 1;
		$day_to = 31;
		$days_range = range($day_from, $day_to);

		return array_combine($days_range, $days_range);
	}

	public function getMonthsList() {
    	$month_array = [];

		for ($i = 1; $i <= 12; $i++) {
			if ($i < 10) {
				$val = '0'.$i;
			}
			else {
				$val = $i;
			}
			array_push($month_array, $val);
		}

		return array_combine($month_array, $month_array);
	}

	public function getYearsList() {
		$currentYear = date('Y');
		$yearFrom = 1900;
		$yearsRange = range($yearFrom, $currentYear);

		return array_combine($yearsRange, $yearsRange);
	}

	public static function getDiscountPercentsArray() {
		return BonusPercents::find()->select(['percent'])->where(['=', 'type', 0])->asArray()->all();
	}

	public static function getDiscountSumsArray() {
		$discount_sums_labels = BonusPercents::find()->select(['sum'])->where(['=', 'type', 0])->asArray()->all();
		array_pop($discount_sums_labels);
		array_push($discount_sums_labels, ['sum' => '>'.end($discount_sums_labels)['sum']]);

		return $discount_sums_labels;
	}
}
