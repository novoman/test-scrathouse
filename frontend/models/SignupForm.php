<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * SignupWidget form
 */
class SignupForm extends Model
{
	public $first_name;
	public $last_name;
	public $email;
	public $password;
	public $password_confirm;

	public function attributeLabels()
	{
		return [
			'first_name' => 'Имя',
			'last_name' => 'Фамилия',
			'email' => 'Email',
			'password' => 'Пароль',
			'password_confirm' => 'Повтор пароля',
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			['first_name', 'trim'],
			['first_name', 'required'],

			['last_name', 'trim'],
			['last_name', 'required'],

			['email', 'trim'],
			['email', 'required'],
			['email', 'email'],
			['email', 'string', 'max' => 255],

			[
				'email', 'unique',
				'targetClass' => '\common\models\User',
				'message' => 'Пользователь с таким email уже зарегистрирован.'
			],

			['password', 'required'],
			['password', 'string', 'min' => 6],

			['password_confirm', 'required'],
			['password_confirm', 'compare', 'compareAttribute'=>'password', 'message'=>"Пароли не совпадают!" ],
		];
	}

	public function validateEmail($attribute, $params) {
		$users = new User();
		if ($users->emailExist($this->email)) {
			$this->addError('email', 'Пользователь с таким email уже зарегистрирован.');
		}
	}
}
