<?php

namespace frontend\models;

use common\models\Products;
use Yii;

/**
 * This is the model class for table "scratboxes_products".
 *
 * @property int $id
 * @property int $id_product
 * @property int $id_box
 * @property string $weight
 *
 * @property Products $product
 * @property Scratboxes $box
 */
class ScratboxesProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scratboxes_products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_product', 'id_box', 'id'], 'integer'],
            [['weight'], 'number'],
            [['id_product'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['id_product' => 'id']],
            [['id_box'], 'exist', 'skipOnError' => true, 'targetClass' => Scratboxes::className(), 'targetAttribute' => ['id_box' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_product' => 'Id Product',
            'id_box' => 'Id Box',
            'weight' => 'Weight',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'id_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBox()
    {
        return $this->hasOne(Scratboxes::className(), ['id' => 'id_box']);
    }
}
