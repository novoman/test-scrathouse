<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class CheckoutSecondAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'css/checkout-first.css',
		'css/checkout-second.css',
	];
	public $js = [
		'js/checkout-first.js',
	];

}
