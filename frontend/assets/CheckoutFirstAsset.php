<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class CheckoutFirstAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'libs/jquery-timepicker/jquery.timepicker.css',
		'css/shops-popup.css',
		'css/checkout-first.css',
	];
	public $js = [
		'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
		'js/shops.js',
		'js/checkout-first.js',
	];

}
