<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ProductCardAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'libs/image-magnifier/image-magnifier.css',
		'css/product-card.css',
	];
	public $js = [
		'js/product-card.js',
	];

}
