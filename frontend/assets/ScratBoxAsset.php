<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ScratBoxAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'css/scrat-box.css',
		'css/product-popup.css',
	];
	public $js = [
		'js/scrat-box.js',
	];

}
