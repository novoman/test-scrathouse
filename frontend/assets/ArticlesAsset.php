<?php

namespace frontend\assets;

use yii\web\AssetBundle;


class ArticlesAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
        'css/articles.css'
	];
	public $js = [
		'js/articles.js',
	];
}
