<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'libs/pushy/pushy.css',
		'libs/magnific-popup/magnific-popup.css',
		'css/cart-popup.css',
		'css/site.css',
	];
	public $js = [
		'libs/pushy/pushy.js',
		'libs/magnific-popup/jquery.magnific-popup.js',
		'libs/cart-toast/jToast.min.js',
		'js/site.js',
	];

	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
	];

}
