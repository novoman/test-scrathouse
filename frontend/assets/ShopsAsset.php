<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ShopsAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'css/shops-page.css',
	];
	public $js = [
		'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
		'js/shops.js',
	];

}
