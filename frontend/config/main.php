<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
	    'gii' => [
		    'class' => 'yii\gii\Module',
	    ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => ''
        ],
        'assetManager' => [
	        'appendTimestamp' => true,
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
            'timeout'=> 24 * 60 * 60,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => '/',
            'rules' => [
            	'' => 'main-page/index',
            	'shops' => 'shops/shops',
            	'articles' => 'articles/articles',
            	'article/<slug:[\w-]+>' => 'articles/articles-item',
	            'products' => 'products/index',
	            'products/<slug:[\w-]+>' => 'products/products',
	            'product/<slug:[\w-]+>' => 'products/product-card',
	            'search-by-name' => 'products/search-by-name',
	            'search' => 'products/search',
	            'scrat-box' => 'scrat-box/scrat-box',
	            'scrat-box-products' => 'scrat-box/index',
	            'scrat-box-products/<slug:[\w-]+>' => 'scrat-box/scrat-box-products',
	            'delivery-pay' => 'deliv-pay/deliv-pay',
	            'contact' => 'site/contact',
	            'contact-phone' => 'site/contact-phone',
	            'contact-email' => 'site/contact-email',
	            'wholesale' => 'site/wholesale',
	            'contact-offer' => 'site/contact-offer',
	            'callback' => 'callback/callback',
	            'cart' => 'cart/cart',
	            'checkout-first' => 'cart/checkout-first',
	            'compete-order' => 'cart/compete-order',
	            'checkout-second' => 'cart/checkout-second',
	            'profile/<page[\d-]+>' => 'profile/profile',
	            '<id:([0-9])+>/images/image-by-item-and-alias' => 'yii2images/images/image-by-item-and-alias',
            ],
        ],

        'authClientCollection' => [
	        'class'   => \yii\authclient\Collection::className(),
	        'clients' => [
		        // here is the list of clients you want to use
		        'twitter' => [
			        'class' => 'yii\authclient\clients\Twitter',
			        'attributeParams' => [
				        'include_email' => 'true'
			        ],
			        'consumerKey' => 'bqL0GkJqSSTmL2zvhv6Nys1dt',
			        'consumerSecret' => 'bXGFeAIYgx1t4wtxid3GZf9MYTHSoBJqkTi6QYBXp3AVXSdz4o',
		        ],

		        'google' => [
			        'class'        => 'yii\authclient\clients\Google',
			        'clientId'     => '786451411781-v561ubc45eq5k5mahfkcs06e27mho283.apps.googleusercontent.com',
			        'clientSecret' => 'J5SM3O2ycAoGfUqtNc-rQGuy',
			        'returnUrl' => 'https://scrathouse.by/site/auth?authclient=google',

		        ],

		        'facebook' => [
			        'class'        => 'yii\authclient\clients\Facebook',
			        'clientId'     => '170588016960842',
			        'clientSecret' => '83af637978bf6b2922250b2cfb030db3',
		        ],

	        ],
        ],
        
    ],
    'params' => $params,
];
