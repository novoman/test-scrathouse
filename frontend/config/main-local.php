<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'yApszDEC_yVCVs5AV8kgt246zOdltLbk',
        ],
    ],
];

return $config;
