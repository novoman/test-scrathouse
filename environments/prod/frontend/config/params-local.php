<?php
return [
    'webPayRequestURL' => 'https://securesandbox.webpay.by/',
    'webPayStoreId' => '',
    'webPaySecretKey' => '',
    'webPayTestMode' => 1,
];
