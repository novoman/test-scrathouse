<?php
namespace console\controllers;

use yii\console\Controller;
use common\models\Products;
use common\models\Category;
use yii\helpers\Inflector;

class AddtodbController extends Controller
{
	public $message;

	public function options($actionID)
	{
		return ['message'];
	}

	public function optionAliases()
	{
		return ['m' => 'message'];
	}

	public function actionIndex()
	{
		echo $this->message . "\n";
	}

	public function actionAddProductsSlug()
	{
		$products = Products::find()->all();
		foreach($products as $product){
			$product->slug = Inflector::slug($product->name);
			$product->save();
		}
		echo 'Slugs for products added' . "\n";
	}

	public function actionAddCategorySlug()
	{
		$cats = Category::find()->all();
		foreach($cats as $cat){
			$cat->slug = Inflector::slug($cat->name);
			$cat->save();
		}
		echo 'Slugs for categories added' . "\n";
	}
}