<?php

use yii\db\Migration;

/**
 * Class m181211_123828_contact_info_insert_values
 */
class m181211_123828_contact_info_insert_values extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		Yii::$app->db->createCommand()->batchInsert('contact_info',
			['location', 'field_name', 'value'], [
				['product', 'viber', '+375251839418'],
				['product', 'whatsapp', '+375251839418'],
				['product', 'skype', ''],
				['product', 'tg', ''],
			])->execute();
	}

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181211_123828_contact_info_insert_values cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181211_123828_contact_info_insert_values cannot be reverted.\n";

        return false;
    }
    */
}
