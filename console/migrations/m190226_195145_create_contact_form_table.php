<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contact_form`.
 */
class m190226_195145_create_contact_form_table extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->createTable('contact_form', [
			'id' => $this->primaryKey(),
			'name' => $this->string(255),
			'email' => $this->string(100),
			'subject' => $this->string(255),
			'body' => $this->text(),
			'verifyCode' => $this->string(255),
			'status' => $this->smallInteger(1)->notNull()->defaultValue(0),
			'created_at' => $this->integer(),
			'updated_at' => $this->integer(),
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('contact_form');
	}
}
