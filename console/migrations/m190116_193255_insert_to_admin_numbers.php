<?php

use yii\db\Migration;

/**
 * Class m190116_193255_insert_to_admin_numbers
 */
class m190116_193255_insert_to_admin_numbers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    Yii::$app->db->createCommand()->batchInsert('admin_numbers',
		    [
			    'name',
			    'value',
			    'description'
		    ],
		    [
			    [
				    'min-sum-for-adv',
				    25,
				    'минимальная сумма для бесплатной доставки/применения промокода'
			    ]
		    ]
	    )->execute();

	    Yii::$app->db->createCommand()->batchInsert('admin_numbers',
		    [
			    'name',
			    'value',
			    'description'
		    ],
		    [
			    [
				    'courier',
				    3,
				    'доставка курьером'
			    ]
		    ]
	    )->execute();

	    Yii::$app->db->createCommand()->batchInsert('admin_numbers',
		    [
			    'name',
			    'value',
			    'description'
		    ],
		    [
			    [
				    'post',
				    5,
				    'доставка почтой'
			    ]
		    ]
	    )->execute();

	    Yii::$app->db->createCommand()->batchInsert('admin_numbers',
		    [
			    'name',
			    'value',
			    'description'
		    ],
		    [
			    [
				    'pickup',
				    0,
				    'самовывоз'
			    ]
		    ]
	    )->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190116_193255_insert_to_admin_numbers cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190116_193255_insert_to_admin_numbers cannot be reverted.\n";

        return false;
    }
    */
}
