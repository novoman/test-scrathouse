<?php

use yii\db\Migration;

/**
 * Class m180706_185443_box_products_alter_column
 */
class m180706_185443_box_products_alter_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->alterColumn('scratboxes_products', 'weight', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180706_185443_box_products_alter_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180706_185443_box_products_alter_column cannot be reverted.\n";

        return false;
    }
    */
}
