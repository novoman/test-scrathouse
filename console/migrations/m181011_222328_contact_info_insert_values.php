<?php

use yii\db\Migration;

/**
 * Class m181011_222328_contact_info_insert_values
 */
class m181011_222328_contact_info_insert_values extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    Yii::$app->db->createCommand()->batchInsert('contact_info',
		    ['location', 'field_name', 'value'], [
		    ['popup', 'tel1', '+375 (25) 183-94-18'],
		    ['popup', 'tel2', '+375 (25) 183-94-18'],
		    ['popup', 'tel3', '+375 (25) 183-94-18'],
		    ['footer', 'name', '© 2017 Scrat House'],
		    ['footer', 'domain', 'Домен внесен в торговый реестр<br>28.12.2015'],
		    ['footer', 'reg_number', 'Регистрационный номер<br>299621'],
		    ['footer', 'email', 'Emal: info@scrathouse.by'],
		    ['footer', 'tel', '+375 (25) 183-94-18'],
		    ['footer', 'skype', 'Skype: info@lime-express.by'],
		    ['footer', 'fb', 'https://www.facebook.com/ScratHouse.by/'],
		    ['footer', 'tw', ''],
		    ['footer', 'vk', 'https://vk.com/scrathouse'],
		    ['footer', 'in', ''],
	    ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    Yii::$app->db->createCommand()->delete('contact_info');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181011_222328_contact_info_insert_values cannot be reverted.\n";

        return false;
    }
    */
}
