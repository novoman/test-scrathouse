<?php

use yii\db\Migration;

/**
 * Class m181228_080610_insert_to_admin_numbers_and_text_by_admin
 */
class m181228_080610_insert_to_admin_numbers_and_text_by_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    Yii::$app->db->createCommand()->batchInsert('admin_numbers',
		    [
		    	'name',
			    'value',
			    'description'
		    ],
		    [
			    [
				    'scrat-box-price',
				    20,
				    'цена за один скрат-бокс'
			    ]
		    ]
	    )->execute();

	    Yii::$app->db->createCommand()->batchInsert('admin_numbers',
		    [
			    'name',
			    'value',
			    'description'
		    ],
		    [
			    [
			    	'one-point-cost',
				    1,
				    'столько рублей равен 1 балл скидочной системы в личном кабинете'

			    ]
		    ]
	    )->execute();

	    Yii::$app->db->createCommand()->batchInsert('text_by_admin',
		    [
			    'type',
			    'text',
			    'page'
		    ],
		    [
			    [
				    '1',
				    '<p><span style=\"background-color:rgb(209, 193, 179); color:rgb(255, 255, 255); font-family:roboto,sans-serif; font-size:38px\">Собери уникальный бокс</span></p>\r\n',
				    'scrat-box'
			    ]
		    ]
	    )->execute();

	    Yii::$app->db->createCommand()->batchInsert('text_by_admin',
		    [
			    'type',
			    'text',
			    'page'
		    ],
		    [
			    [
				    '2',
				    '<p><span style=\"background-color:rgb(209, 193, 179); color:rgb(255, 255, 255); font-family:roboto,sans-serif; font-size:20px\">Промокод:&nbsp;</span><span style=\"background-color:rgb(209, 193, 179); color:rgb(255, 255, 255); font-family:roboto,sans-serif; font-size:20px\">magicbox</span></p>\r\n',
				    'scrat-box'
			    ]
		    ]
	    )->execute();

	    Yii::$app->db->createCommand()->batchInsert('text_by_admin',
		    [
			    'type',
			    'text',
			    'page'
		    ],
		    [
			    [
				    '3',
				    '<div class="box-name" style="box-sizing: border-box; color: rgb(93, 91, 90); line-height: 20px; margin-bottom: 15px; font-size: 25px; font-family: Roboto, sans-serif;">Скрат-бокс &mdash; это просто!</div>
 						<div class="box-description" style="box-sizing: border-box; color: rgb(93, 91, 90); font-size: 15px; margin-bottom: 35px; font-family: Roboto, sans-serif;">Скрат-боксы от Scrathouse.by это очень удобная и практичная штука! Как это работает: выбираешь на нашем сайте что-нибудь из нашего ассортимента. Например, орешки, специи, сухофрукты или что-то ещё, но общим весом не больше 200 грамм. Затем кладёшь это всё в одну из четырёх ячеек (тех самых скрат-боксов) и оформляешь свой заказ. Смотри: на твой скрат-бокс всегда будет фиксированная цена, независимо от его наполнения - главное &quot;вместить&quot; всё в общий вес в 200 граммов. Столько всего вкусного и полезного &mdash; и в &quot;одном флаконе&quot;!</div>',
				    'scrat-box'
			    ]
		    ]
	    )->execute();

	    Yii::$app->db->createCommand()->batchInsert('text_by_admin',
		    [
			    'type',
			    'text',
			    'page'
		    ],
		    [
			    [
				    '1',
					'<p><span style="background-color:rgb(209, 193, 179); color:rgb(255, 255, 255); font-family:roboto,sans-serif; font-size:38px">Скидки на орехи премиум качества</span></p>',
				    'main-page'
			    ]
		    ]
	    )->execute();

	    Yii::$app->db->createCommand()->batchInsert('text_by_admin',
		    [
			    'type',
			    'text',
			    'page'
		    ],
		    [
			    [
				    '2',
					'<p><span style="background-color:rgb(209, 193, 179); color:rgb(255, 255, 255); font-family:roboto,sans-serif; font-size:20px">Промокод:&nbsp;</span><span style="background-color:rgb(209, 193, 179); color:rgb(255, 255, 255); font-family:roboto,sans-serif; font-size:20px">welcomefall</span></p>',
				    'main-page'
			    ]
		    ]
	    )->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181228_080610_insert_to_admin_numbers_and_text_by_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181228_080610_insert_to_admin_numbers_and_text_by_admin cannot be reverted.\n";

        return false;
    }
    */
}
