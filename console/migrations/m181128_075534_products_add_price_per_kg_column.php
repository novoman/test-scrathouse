<?php

use yii\db\Migration;

/**
 * Class m181128_075534_products_add_price_per_kg_column
 */
class m181128_075534_products_add_price_per_kg_column extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->addColumn('products', 'price_per_kg', $this->double());
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropColumn('products', 'price_per_kg');
	}

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181128_075534_products_add_price_per_kg_column cannot be reverted.\n";

        return false;
    }
    */
}
