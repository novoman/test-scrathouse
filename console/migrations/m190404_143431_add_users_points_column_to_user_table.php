<?php

use yii\db\Migration;

/**
 * Handles adding users_points to table `user`.
 */
class m190404_143431_add_users_points_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->addColumn('user', 'users_points', $this->Integer()->defaultValue(0));
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropColumn('user', 'users_points');
	}
}
