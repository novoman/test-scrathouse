<?php

use yii\db\Migration;

/**
 * Class m180706_024703_order_add_id_user_column
 */
class m180706_024703_order_add_id_user_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn('order', 'id_user', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropColumn('order', 'id_user');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180706_024703_order_add_id_user_column cannot be reverted.\n";

        return false;
    }
    */
}
