<?php

use yii\db\Migration;

/**
 * Class m181010_051652_products_add_country_and_presence_columns
 */
class m181010_051652_products_add_country_and_presence_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn('products', 'country', $this->string(100)->defaultValue(''));
	    $this->addColumn('products', 'presence', $this->integer(1)->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropColumn('products', 'country');
	    $this->dropColumn('products', 'presence');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181010_051652_products_add_country_and_presence_columns cannot be reverted.\n";

        return false;
    }
    */
}
