<?php

use yii\db\Migration;

/**
 * Class m190305_204600_add_product_is_special_column
 */
class m190305_204600_add_product_is_special_column extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->addColumn('order_items', 'special', $this->smallInteger(1)->notNull()->defaultValue(0));
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropColumn('order_items', 'special');
	}

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190305_204600_add_product_is_special_column cannot be reverted.\n";

        return false;
    }
    */
}
