<?php

use yii\db\Migration;

/**
 * Class m191222_211240_create_points_histories
 */
class m191222_211240_create_points_histories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable('points_histories', [
		    'id'         => $this->primaryKey(),
		    'type'       => $this->smallInteger(1)->notNull(),
		    'comment'    => $this->text(),
		    'created_at' => $this->integer(),
		    'updated_at' => $this->integer(),
		    'points_id'  => $this->integer(),
	    ]);

	    $this->addForeignKey('points_histories_points', 'points_histories', 'points_id','points', 'id');
	    $this->createIndex('indx_points_histories_type','points_histories', 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('points_histories');
    }
}
