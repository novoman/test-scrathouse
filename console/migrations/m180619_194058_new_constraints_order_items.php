<?php

use yii\db\Migration;

/**
 * Class m180619_194058_new_constraints_order_items
 */
class m180619_194058_new_constraints_order_items extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$this->dropForeignKey('order_items_order', 'order_items');
    	$this->dropForeignKey('order_items_products', 'order_items');
    	$this->dropForeignKey('order_items_scratboxes', 'order_items');

	    $this->addForeignKey('order_items_order', 'order_items', 'id_order',
		    'order', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('order_items_products', 'order_items', 'id_product',
		    'products', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('order_items_scratboxes', 'order_items', 'id_scratbox',
		    'scratboxes', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180619_194058_new_constraints_order_items cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180619_194058_new_constraints_order_items cannot be reverted.\n";

        return false;
    }
    */
}
