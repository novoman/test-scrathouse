<?php

use yii\db\Migration;

/**
 * Class m190313_120955_add_deliv_pay_item_to_text_by_admin
 */
class m190313_120955_add_deliv_pay_item_to_text_by_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    Yii::$app->db->createCommand()->batchInsert('text_by_admin',
		    [
			    'type',
			    'text',
			    'page'
		    ],
		    [
			    [
				    '4',
					'<p>Порядок покупки товаров:</p>
					 
					 <span>Покупать товары на веб-сайте Scrathouse.by очень просто.</span>
					 
					 <ol>
					    <li>Выберите товар, из представленных продавцом.</li>
					    <li>Нажмите &laquo;Добавить в корзину&raquo;.</li>
					    <li>Выберите количество товара.</li>
					    <li>Нажмите &laquo;Оформить заказ&raquo;.</li>
					    <li>Введите свои персональные данные и адрес доставки.</li>
					    <li>Проверьте введенные данные и подтвердите Ваш заказ.</li>
					    <li>Введите данные Вашей банковской карты: номер карты, срок действия, код CVV (данные банковских карт защищены шифрованием и обрабатываются исключительно биллинговой системой банка).</li>
					 </ol>
					 
					 <span>После этого мы в кратчайшие сроки вышлем Вам выбранный товар.</span>
					 ',
				    'delivery-pay'
			    ]
		    ]
	    )->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190313_120955_add_deliv_pay_item_to_text_by_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190313_120955_add_deliv_pay_item_to_text_by_admin cannot be reverted.\n";

        return false;
    }
    */
}
