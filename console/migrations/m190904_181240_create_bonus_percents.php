<?php

use yii\db\Migration;

/**
 * Class m190904_201240_create_bonus_percents
 */
class m190904_181240_create_bonus_percents extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable('bonus_percents', [
		    'id'         => $this->primaryKey(),
		    'sum'        => $this->double(2)->notNull(),
		    'percent'    => $this->double(2)->notNull(),
		    'type'       => $this->smallInteger(1)->notNull()->defaultValue(0),
		    'created_at' => $this->integer(),
		    'updated_at' => $this->integer(),
	    ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('bonus_percents');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190904_181240_create_bonus_percents cannot be reverted.\n";

        return false;
    }
    */
}
