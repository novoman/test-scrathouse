<?php

use yii\db\Migration;

/**
 * Handles adding weight to table `category`.
 */
class m180626_041824_add_weight_column_to_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('category', 'weight', $this->integer()->notNull()->defaultValue(100));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('category', 'weight');
    }
}
