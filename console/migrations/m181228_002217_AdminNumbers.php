<?php

use yii\db\Migration;

/**
 * Class m181228_002217_AdminNumbers
 */
class m181228_002217_AdminNumbers extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->createTable('admin_numbers', [
			'id'          => $this->primaryKey(),
			'name'        => $this->string(),
			'value'       => $this->double(),
			'description' => $this->text(),
			'created_at'  => $this->integer()->notNull(),
			'updated_at'  => $this->integer()->notNull(),
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('admin_numbers');
	}

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181228_002217_AdminNumbers cannot be reverted.\n";

        return false;
    }
    */
}
