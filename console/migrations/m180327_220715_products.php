<?php

use yii\db\Migration;

/**
 * Class m180327_220714_products
 */
class m180327_220715_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
 		$this->createTable(
            	'products',
            	[
                	'id' => 'pk',
                	'name' => 'varchar(100)',
                	'price' => 'double',
                	'description' => 'text',
               		'articule' => 'varchar(50)',
                	'hits' => 'int',
                	'sort' => 'int',
                	'id_cat' => 'int',
           		],
            	'ENGINE=InnoDB'
        );
	    $this->addForeignKey('products_cats', 'products', 'id_cat','category', 'id');
	    $this->createIndex('fk_products_name_indx','products', 'name');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	     $this->dropTable('products');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180327_220714_products cannot be reverted.\n";

        return false;
    }
    */
}
