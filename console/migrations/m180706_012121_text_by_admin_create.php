<?php

use yii\db\Migration;

/**
 * Class m180706_012121_text_by_admin_create
 */
class m180706_012121_text_by_admin_create extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable('text_by_admin', [
		    'id'         => $this->primaryKey(),
		    'type'       => $this->integer(),
		    'text'       => $this->text(),
		    'created_at' => $this->integer()->notNull(),
		    'updated_at' => $this->integer()->notNull(),
	    ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('text_by_admin');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180706_012121_text_by_admin_create cannot be reverted.\n";

        return false;
    }
    */
}
