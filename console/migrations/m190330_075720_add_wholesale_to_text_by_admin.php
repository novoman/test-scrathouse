<?php

use yii\db\Migration;

/**
 * Class m190330_075720_add_wholesale_to_text_by_admin
 */
class m190330_075720_add_wholesale_to_text_by_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    Yii::$app->db->createCommand()->batchInsert('text_by_admin',
		    [
			    'type',
			    'text',
			    'page'
		    ],
		    [
			    [
				    '',
					'<p style="text-align:center"><span style="font-size:18px"><strong>По всем вопросам сотрудничества присылайте Ваше предложение на электронную почту opt@scrathouse.by.<br />
 Телефон для связи: + 375 29 363 06 05</strong></span></p>',
				    'wholesale'
			    ]
		    ]
	    )->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190330_075720_add_wholesale_to_text_by_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190330_075720_add_wholesale_to_text_by_admin cannot be reverted.\n";

        return false;
    }
    */
}
