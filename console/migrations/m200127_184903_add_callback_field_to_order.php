<?php

use yii\db\Migration;

/**
 * Class m200127_184903_add_callback_field_to_order
 */
class m200127_184903_add_callback_field_to_order extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->addColumn('order', 'callback', $this->tinyInteger()->defaultValue(0));
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropColumn('order', 'callback');
	}
}
