<?php

use yii\db\Migration;

/**
 * Class m190721_100148_insert_seo_to_contact_info
 */
class m190721_100148_insert_seo_to_contact_info extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    Yii::$app->db->createCommand()->batchInsert('contact_info',
		    ['location', 'field_name', 'value'], [
			    ['footer', 'seo', 'ScratHouse.by — интернет-магазин по продаже в Минске и минском районе орехов, сухофруктов и специй. Купить орехи себе домой или в офис, заказать доставку в Минске — все это и многое другое делает ScratHouse.by! Все права защищены.'],
		    ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190721_100148_insert_seo_to_contact_info cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190721_100148_insert_seo_to_contact_info cannot be reverted.\n";

        return false;
    }
    */
}
