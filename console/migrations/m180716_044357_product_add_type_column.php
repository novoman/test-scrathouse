<?php

use yii\db\Migration;

/**
 * Class m180716_044357_product_add_type_column
 */
class m180716_044357_product_add_type_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn('products', 'type', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropColumn('products', 'type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180716_044357_product_add_type_column cannot be reverted.\n";

        return false;
    }
    */
}
