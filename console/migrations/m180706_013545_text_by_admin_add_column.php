<?php

use yii\db\Migration;

/**
 * Class m180706_013545_text_by_admin_add_column
 */
class m180706_013545_text_by_admin_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn('text_by_admin', 'page', $this->string()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropColumn('text_by_admin', 'page');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180706_013545_text_by_admin_add_column cannot be reverted.\n";

        return false;
    }
    */
}
