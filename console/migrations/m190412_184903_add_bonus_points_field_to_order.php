<?php

use yii\db\Migration;

/**
 * Class m190412_184903_add_bonus_points_field_to_order
 */
class m190412_184903_add_bonus_points_field_to_order extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->addColumn('order', 'bonus_points', $this->integer()->defaultValue(0));
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropColumn('order', 'bonus_points');
	}

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190412_184903_add_bonus_points_field_to_order cannot be reverted.\n";

        return false;
    }
    */
}
