<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contact_phone_form`.
 */
class m190226_204033_create_contact_phone_form_table extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->createTable('contact_phone_form', [
			'id' => $this->primaryKey(),
			'name' => $this->string(255),
			'phone' => $this->string(255),
			'verifyCode' => $this->string(255),
			'status' => $this->smallInteger(1)->notNull()->defaultValue(0),
			'created_at' => $this->integer(),
			'updated_at' => $this->integer(),
		]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('contact_phone_form');
	}
}
