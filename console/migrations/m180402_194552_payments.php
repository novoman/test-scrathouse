<?php

use yii\db\Migration;

/**
 * Class m180402_194552_payments
 */
class m180402_194552_payments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable(
		    'payments',
		    [
			    'id' => 'pk',
			    'success' => 'tinyint(1)',
			    'date' => 'int',
		    ],
		    'ENGINE=InnoDB'
	    );
	    $this->createIndex('fk_payments_date_indx', 'payments', 'date');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('payments');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180402_194552_payments cannot be reverted.\n";

        return false;
    }
    */
}
