<?php

use yii\db\Migration;

/**
 * Class m181228_141405_swap_columns_in_category_and_product
 */
class m181228_141405_swap_columns_in_category_and_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->dropColumn('category', 'special');
	    $this->addColumn('products', 'special', $this->smallInteger(1)->notNull()->defaultValue(0));

	    $this->dropColumn('category', 'weight');
	    $this->addColumn('products', 'weight', $this->integer()->notNull()->defaultValue(100));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181228_141405_swap_columns_in_category_and_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181228_141405_swap_columns_in_category_and_product cannot be reverted.\n";

        return false;
    }
    */
}
