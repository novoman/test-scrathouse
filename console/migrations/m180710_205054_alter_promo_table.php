<?php

use yii\db\Migration;

/**
 * Class m180710_205054_alter_promo_table
 */
class m180710_205054_alter_promo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->dropColumn('promo', 'product_type');
	    $this->addColumn('promo', 'person_type', $this->smallInteger(1)->defaultValue(0));
	    $this->addColumn('promo', 'discount_type', $this->smallInteger(1)->defaultValue(0));
	    $this->addColumn('promo', 'used', $this->smallInteger(1)->defaultValue(0));
	    $this->renameColumn('promo', 'percent', 'discount');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropColumn('promo', 'person_type');
	    $this->dropColumn('promo', 'discount_type');
	    $this->dropColumn('promo', 'used');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180710_205054_alter_promo_table cannot be reverted.\n";

        return false;
    }
    */
}
