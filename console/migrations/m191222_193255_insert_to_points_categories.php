<?php

use yii\db\Migration;

/**
 * Class m191222_193255_insert_to_points_categories
 */
class m191222_193255_insert_to_points_categories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$inserted_values = [
		    ['Баллы за заказ', 'order'],
			['Новый год', 'new_year'],
			['День рождения клиента', 'clients_birthday'],
			['День рождения Скрэт-хауз', 'shops_birthday'],
			['Другой праздник', 'others']
	    ];

    	foreach ($inserted_values as $value) {
		    Yii::$app->db->createCommand()->batchInsert('points_categories',
			    ['name', 'lookup_code'],
			    [$value]
		    )->execute();
	    }


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191222_193255_insert_to_points_categories cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190116_193255_insert_to_admin_numbers cannot be reverted.\n";

        return false;
    }
    */
}
