<?php

use yii\db\Migration;

/**
 * Class m181210_170651_unique_index_for_text_by_admin
 */
class m181210_170651_unique_index_for_text_by_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->createIndex('UK_page_type', 'text_by_admin', ['page', 'type'], $unique = true );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropIndex('UK_page_type', 'text_by_admin');
	}

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181210_170651_unique_index_for_text_by_admin cannot be reverted.\n";

        return false;
    }
    */
}
