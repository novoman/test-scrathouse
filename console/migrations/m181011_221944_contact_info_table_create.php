<?php

use yii\db\Migration;

/**
 * Class m181011_221944_contact_info_table_create
 */
class m181011_221944_contact_info_table_create extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable('contact_info', [
		    'id'         => $this->primaryKey(),
		    'location'   => $this->string(),
		    'field_name' => $this->string(),
		    'value'      => $this->string(),
		    'created_at' => $this->integer()->notNull(),
		    'updated_at' => $this->integer()->notNull(),
	    ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('contact_info');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181011_221944_contact_info_table_create cannot be reverted.\n";

        return false;
    }
    */
}
