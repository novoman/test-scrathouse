<?php

use yii\db\Migration;

/**
 * Class m200215_184903_add_overall_sum_to_get_field_to_order
 */
class m200215_184903_add_overall_sum_to_get_field_to_order extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->addColumn('order', 'overall_sum_to_get', $this->double(2)->defaultValue(0.00));
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropColumn('order', 'overall_sum_to_get');
	}
}
