<?php

use yii\db\Migration;

/**
 * Class m180402_193636_cart
 */
class m180402_193636_cart extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable(
		    'cart',
		    [
			    'id' => 'pk',
			    'id_user' => 'int',
		    ],
		    'ENGINE=InnoDB'
	    );
	    $this->addForeignKey('cart_user', 'cart', 'id_user',
		    'user', 'id','CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('cart');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180402_193636_cart cannot be reverted.\n";

        return false;
    }
    */
}
