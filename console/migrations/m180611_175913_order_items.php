<?php

use yii\db\Migration;

/**
 * Class m180611_175913_order_items
 */
class m180611_175913_order_items extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->createTable(
			'order_items',
			[
				'id' => $this->primaryKey(),
				'name' => $this->string(255),
				'price' => $this->double(),
				'weight' => $this->integer(),
				'sum' => $this->double(),
				'id_order' => $this->integer(),
				'id_product' => $this->integer(),
				'is_scratbox' => $this->smallInteger()->notNull()->defaultValue(0),
				'id_scratbox' => $this->integer()->defaultValue(null),
			],
			'ENGINE=InnoDB'
		);
		$this->addForeignKey('order_items_order', 'order_items', 'id_order',
			'order', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('order_items_products', 'order_items', 'id_product',
			'products', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('order_items_scratboxes', 'order_items', 'id_scratbox',
			'scratboxes', 'id', 'CASCADE', 'CASCADE');

		$this->createIndex('fk_order_items_name_indx','order_items', 'name');

	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('order_items');
	}

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180611_175910_order_items cannot be reverted.\n";

        return false;
    }
    */
}
