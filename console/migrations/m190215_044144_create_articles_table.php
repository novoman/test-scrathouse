<?php

use yii\db\Migration;

/**
 * Handles the creation of table `articles`.
 */
class m190215_044144_create_articles_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('articles', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'slug' => $this->string(100),
            'keywords' => $this->string(255),
            'description' => $this->text(),
            'preview' => $this->text(),
            'text' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('articles');
    }
}
