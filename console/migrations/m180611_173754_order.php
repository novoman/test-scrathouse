<?php

use yii\db\Migration;

/**
 * Class m180611_173754_order
 */
class m180611_173754_order extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->createTable(
			'order',
			[
				'id' => $this->primaryKey(),
				'created_at' => $this->integer(),
				'updated_at' => $this->integer(),
				'sum' => $this->double(),
				'status' => $this->smallInteger(1)->notNull()->defaultValue(0),
				'name' => $this->string(255),
				'email' => $this->string(255),
				'telephone' => $this->string(255),
				'address' => $this->string(255),
				'comment' => $this->text(),
				'is_jur_person' => $this->smallInteger(1)->notNull()->defaultValue(0),
				'jur_name' => $this->string(255),
				'jur_address' => $this->string(255),
				'bank' => $this->string(255),
				'checking_account' => $this->string(255),
				'bank_code' => $this->string(255),
				'unp' => $this->integer(9),
				'director' => $this->string(255),
				'based' => $this->string(255),
				'id_pay_type' => $this->integer(),
			],
			'ENGINE=InnoDB'
		);

		$this->addForeignKey('order_pay_type', 'order', 'id_pay_type',
			'payments_type', 'id', 'NO ACTION', 'CASCADE');

		$this->createIndex('fk_order_status_indx','order', 'status');
		$this->createIndex('fk_order_created_indx','order', 'created_at');

	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('order');
	}

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180611_173752_order cannot be reverted.\n";

        return false;
    }
    */
}
