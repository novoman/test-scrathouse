<?php

use yii\db\Migration;

/**
 * Class m190116_193946_alter_user_table_add_birthday_column
 */
class m190116_193946_alter_user_table_add_birthday_column extends Migration
{
	public function up()
	{
		$this->addColumn('{{%user}}', 'birthday', $this->integer());
	}

	public function down()
	{
		$this->dropColumn('{{%user}}', 'birthday');
	}

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190116_193946_alter_user_table_add_birthday_column cannot be reverted.\n";

        return false;
    }
    */
}
