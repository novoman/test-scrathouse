<?php

use yii\db\Migration;

/**
 * Class m180710_205054_alter_promo_table
 */
class m190601_205054_alter_promo_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn('promo', 'times_used', $this->integer()->defaultValue(0));
	    $this->addColumn('promo', 'max_times_used', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropColumn('promo', 'times_used');
	    $this->dropColumn('promo', 'max_times_used');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180710_205054_alter_promo_table cannot be reverted.\n";

        return false;
    }
    */
}
