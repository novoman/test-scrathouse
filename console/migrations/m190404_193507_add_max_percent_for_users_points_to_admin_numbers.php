<?php

use yii\db\Migration;

/**
 * Class m190404_193507_add_max_percent_for_users_points_to_admin_numbers
 */
class m190404_193507_add_max_percent_for_users_points_to_admin_numbers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    Yii::$app->db->createCommand()->batchInsert('admin_numbers',
		    [
			    'name',
			    'value',
			    'description'
		    ],
		    [
			    [
				    'max-percent-for-users-points',
				    10,
				    'максимальный % суммы заказа для оплаты личными бонусами клиента'
			    ]
		    ]
	    )->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190404_193507_add_max_percent_for_users_points_to_admin_numbers cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190404_193507_add_max_percent_for_users_points_to_admin_numbers cannot be reverted.\n";

        return false;
    }
    */
}
