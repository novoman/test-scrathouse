<?php

use yii\db\Migration;

/**
 * Class m180402_194713_shops
 */
class m180402_194713_shops extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable(
		    'shops',
		    [
			    'id' => 'pk',
			    'name' => 'varchar(100)',
			    'address' => 'varchar(200)',
			    'city' => 'varchar(50)',
			    'work_time' => 'varchar(20)',
				'telephone' => 'varchar(20)',
			    'coordinante_X' => 'double(5,6)',
			    'coordinante_Y' => 'double(5,6)',
		    ],
		    'ENGINE=InnoDB'
	    );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('shops');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180402_194713_shops cannot be reverted.\n";

        return false;
    }
    */
}
