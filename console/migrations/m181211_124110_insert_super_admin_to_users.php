<?php

use yii\db\Migration;

/**
 * Class m181211_124110_insert_super_admin_to_users
 */
class m181211_124110_insert_super_admin_to_users extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    Yii::$app->db->createCommand()->batchInsert('user',
		    [
		    	'first_name',
			    'last_name',
			    'auth_key',
			    'password_hash',
			    'email',
			    'client_type',
			    'status',
			    'created_at',
			    'updated_at',
			    'is_admin',
			    'is_super_admin'
		    ],
		    [
			    [
				    'Super',
				    'Admin',
				    'QdcUwc61Li4-CvpF',
				    '$2y$13$7uPY5CvDr3yjbyNjEs1yPuys55fxSlmB0EJ6qNZ/wA2.ztifPFE6W',
				    'WhiSto',
				    '0',
				    '10',
				    '1544531603',
				    '1544531624',
				    '1',
				    '1'
			    ]
		    ]
		    )->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181211_124110_insert_super_admin_to_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181211_124110_insert_super_admin_to_users cannot be reverted.\n";

        return false;
    }
    */
}
