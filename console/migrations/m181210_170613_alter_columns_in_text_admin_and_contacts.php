<?php

use yii\db\Migration;

/**
 * Class m181210_170613_alter_columns_in_text_admin_and_contacts
 */
class m181210_170613_alter_columns_in_text_admin_and_contacts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->alterColumn('contact_info', 'location', $this->string(30));
	    $this->alterColumn('contact_info', 'field_name', $this->string(30));

	    $this->alterColumn('text_by_admin', 'page', $this->string(30));
	    $this->alterColumn('text_by_admin', 'type', $this->string(30));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181210_170613_alter_columns_in_text_admin_and_contacts cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181210_170613_alter_columns_in_text_admin_and_contacts cannot be reverted.\n";

        return false;
    }
    */
}
