<?php

use yii\db\Migration;

/**
 * Class m191105_201123_add_scratbox_category_to_admin_numbers
 */
class m191105_201123_add_scratbox_category_to_admin_numbers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $category_id = \common\models\Category::find()->where(['name' => 'Сладости'])->one()->id;

	    Yii::$app->db->createCommand()->batchInsert('admin_numbers',
		    [
			    'name',
			    'value',
			    'description'
		    ],
		    [
			    [
				    'scratbox-third-category-id',
				    $category_id,
				    'ID Третьей категория товаров, которые можно добавить в скрат-бокс'
			    ]
		    ]
	    )->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191105_201123_add_scratbox_category_to_admin_numbers cannot be reverted.\n";

        return false;
    }
}
