<?php

use yii\db\Migration;

/**
 * Class m190305_195433_add_unique_key_column_to_order
 */
class m190305_195433_add_unique_token_column_to_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn('order', 'unique_token', $this->string(20)->notNull());

	    $orders = \common\models\Order::find()->all();

	    foreach ($orders as $order) {
		    $order->unique_token = Yii::$app->security->generateRandomString(20);
		    $order->update(false);
	    }

	    $this->createIndex('indx_unique_token', 'order', 'unique_token', $unique = true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropColumn('order', 'unique_token');

	    $this->dropIndex('indx_unique_token', 'order');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190305_195433_add_unique_key_column_to_order cannot be reverted.\n";

        return false;
    }
    */
}
