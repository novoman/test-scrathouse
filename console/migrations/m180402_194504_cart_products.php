<?php

use yii\db\Migration;

/**
 * Class m180402_194504_cart_products
 */
class m180402_194504_cart_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable(
		    'cart_products',
		    [
			    'id' => 'pk',
			    'id_product' => 'int',
			    'weight' => 'int',
			    'count' => 'int',
			    'id_cart' => 'int',
		    ],
		    'ENGINE=InnoDB'
	    );
	     $this->addForeignKey('cart_product_cart', 'cart_products', 'id_cart',
	     	'cart', 'id', 'CASCADE', 'CASCADE');
         $this->addForeignKey('product_product_cart', 'cart_products', 'id_product',
		    'products', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('cart_product');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180402_194504_cart_products cannot be reverted.\n";

        return false;
    }
    */
}
