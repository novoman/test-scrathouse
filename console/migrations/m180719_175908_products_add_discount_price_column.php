<?php

use yii\db\Migration;

/**
 * Class m180719_175908_products_add_discount_price_column
 */
class m180719_175908_products_add_discount_price_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn('products', 'discount_price', $this->double());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropColumn('products', 'discount_price');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180719_175908_products_add_discount_price_column cannot be reverted.\n";

        return false;
    }
    */
}
