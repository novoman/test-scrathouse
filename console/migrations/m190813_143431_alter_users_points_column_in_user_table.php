<?php

use yii\db\Migration;

/**
 * Handles adding users_points to table `user`.
 */
class m190813_143431_alter_users_points_column_in_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->alterColumn('user', 'users_points', $this->double(2)->defaultValue(0.00));
		$this->addColumn('user', 'overall_sum', $this->double(2)->defaultValue(0.00));
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->alterColumn('user', 'users_points', $this->integer()->defaultValue(0));
		$this->dropColumn('user', 'overall_sum');
	}
}
