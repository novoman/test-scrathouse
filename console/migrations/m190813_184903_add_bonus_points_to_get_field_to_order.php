<?php

use yii\db\Migration;

/**
 * Class m190412_184903_add_bonus_points_field_to_order
 */
class m190813_184903_add_bonus_points_to_get_field_to_order extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->addColumn('order', 'bonus_points_to_get', $this->double(2)->defaultValue(0.00));
		$this->alterColumn('order', 'bonus_points', $this->double(2)->defaultValue(0.00));
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropColumn('order', 'bonus_points_to_get');
		$this->alterColumn('order', 'bonus_points', $this->integer()->defaultValue(0));

	}
}
