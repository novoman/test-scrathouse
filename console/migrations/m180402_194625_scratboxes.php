<?php

use yii\db\Migration;

/**
 * Class m180402_194625_scratboxes
 */
class m180402_194625_scratboxes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable(
		    'scratboxes',
		    [
			    'id' => 'pk',
			    'name' => 'varchar(30)',
			    'id_cart' => 'int',
		    ],
		    'ENGINE=InnoDB'
	    );
	    $this->addForeignKey('scratboxes_cart', 'scratboxes', 'id_cart',
		    'cart', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('scratboxes');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180402_194625_scratboxes cannot be reverted.\n";

        return false;
    }
    */
}
