<?php

use yii\db\Migration;

/**
 * Class m180715_214129_order_add_id_promo_column
 */
class m180715_214129_order_add_id_promo_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn('order', 'id_promo', $this->integer()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropColumn('order', 'id_promo');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180715_214129_order_add_id_promo_column cannot be reverted.\n";

        return false;
    }
    */
}
