<?php

use yii\db\Migration;

/**
 * Class m180527_133908_juristic_person
 */
class m130527_133906_juristic_person extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->createTable(
			'juristic_person',
			[
				'id' => 'pk',
				'jur_name' => 'varchar(100)',
				'jur_address' => 'varchar(200)',
				'bank' => 'varchar(100)',
				'checking_account' => 'varchar(50)',
				'bank_code' => 'varchar(50)',
				'unp' => 'int(9)',
				'director' => 'varchar(100)',
				'based' => 'varchar(100)',
				'id_user' => 'int',
			],
			'ENGINE=InnoDB'
		);

		$this->addForeignKey('juristic_person_user', 'juristic_person', 'id_user',
			'user', 'id', 'CASCADE', 'CASCADE');
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('juristic_person');
	}

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180527_133908_juristic_person cannot be reverted.\n";

        return false;
    }
    */
}
