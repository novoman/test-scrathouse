<?php

use yii\db\Migration;

/**
 * Class m180402_194540_payments_type
 */
class m180402_194540_payments_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable(
		    'payments_type',
		    [
			    'id' => 'pk',
			    'name' => 'varchar(100)',
		    ],
		    'ENGINE=InnoDB'
	    );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('payments_type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180402_194540_payments_type cannot be reverted.\n";

        return false;
    }
    */
}
