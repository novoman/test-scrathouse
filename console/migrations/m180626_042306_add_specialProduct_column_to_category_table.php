<?php

use yii\db\Migration;

/**
 * Handles adding specialProduct to table `category`.
 */
class m180626_042306_add_specialProduct_column_to_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('category', 'special', $this->smallInteger(1)->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('category', 'special');
    }
}
