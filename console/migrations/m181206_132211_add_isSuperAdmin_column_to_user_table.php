<?php

use yii\db\Migration;

/**
 * Handles adding isSuperAdmin to table `user`.
 */
class m181206_132211_add_isSuperAdmin_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn('user', 'is_super_admin', $this->tinyInteger()->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropColumn('user', 'is_super_admin');
    }
}
