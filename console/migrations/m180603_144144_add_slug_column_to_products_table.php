<?php

use yii\db\Migration;

/**
 * Handles adding slug to table `products`.
 */
class m180603_144144_add_slug_column_to_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'slug', 'varchar(100)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('products', 'slug');
    }
}
