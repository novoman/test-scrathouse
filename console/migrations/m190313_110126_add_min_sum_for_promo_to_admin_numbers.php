<?php

use yii\db\Migration;

/**
 * Class m190313_110126_add_min_sum_for_promo_to_admin_numbers
 */
class m190313_110126_add_min_sum_for_promo_to_admin_numbers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    Yii::$app->db->createCommand()->batchInsert('admin_numbers',
		    [
			    'name',
			    'value',
			    'description'
		    ],
		    [
			    [
				    'min-sum-for-promo',
				    25,
				    'минимальная сумма для применения промокода'
			    ]
		    ]
	    )->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190313_110126_add_min_sum_for_promo_to_admin_numbers cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190313_110126_add_min_sum_for_promo_to_admin_numbers cannot be reverted.\n";

        return false;
    }
    */
}
