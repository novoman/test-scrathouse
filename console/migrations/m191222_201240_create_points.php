<?php

use yii\db\Migration;

/**
 * Class m191222_201240_create_points
 */
class m191222_201240_create_points extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable('points', [
		    'id'                 => $this->primaryKey(),
		    'count'              => $this->double(2)->defaultValue(0.00),
		    'date_from'          => $this->integer(),
		    'date_to'            => $this->integer(),
		    'created_at'         => $this->integer(),
		    'updated_at'         => $this->integer(),
		    'points_category_id' => $this->integer(),
		    'user_id'            => $this->integer(),
	    ]);

	    $this->addForeignKey('points_points_categories', 'points', 'points_category_id','points_categories', 'id');
	    $this->addForeignKey('points_users', 'points', 'user_id','user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('points');
    }
}
