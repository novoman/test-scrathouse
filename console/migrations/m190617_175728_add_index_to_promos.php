<?php

use yii\db\Migration;

/**
 * Class m190617_175728_add_index_to_promos
 */
class m190617_175728_add_index_to_promos extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->createIndex('UK_promo_name', 'promo', ['name'], $unique = true );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropIndex('UK_promo_name', 'promo');
	}

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190617_175728_add_index_to_promos cannot be reverted.\n";

        return false;
    }
    */
}
