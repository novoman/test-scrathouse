<?php

use yii\db\Migration;

/**
 * Class m180509_225529_auth
 */
class m180509_225529_auth extends Migration
{
	public function up()
	{
		$this->createTable('auth', [
			'id' => $this->primaryKey(),
			'user_id' => $this->integer()->notNull(),
			'source' => $this->string()->notNull(),
			'source_id' => $this->string()->notNull(),
		]);

		$this->addForeignKey('fk-auth-user_id-user-id', 'auth', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
	}

	public function down()
	{
		$this->dropForeignKey('fk-auth-user_id-user-id', 'auth');
		$this->dropTable('auth');
	}
}
