<?php

use yii\db\Migration;

/**
 * Class m190412_201123_add_order_percent_for_points_to_admin_numbers
 */
class m190412_201123_add_order_percent_for_points_to_admin_numbers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    Yii::$app->db->createCommand()->batchInsert('admin_numbers',
		    [
			    'name',
			    'value',
			    'description'
		    ],
		    [
			    [
				    'order-percent-for-points',
				    10,
				    '% от суммы заказа для начисления скрэтрублей'
			    ]
		    ]
	    )->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190412_201123_add_order_percent_for_points_to_admin_numbers cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190412_201123_add_order_percent_for_points_to_admin_numbers cannot be reverted.\n";

        return false;
    }
    */
}
