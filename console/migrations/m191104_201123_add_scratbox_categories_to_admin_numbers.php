<?php

use yii\db\Migration;

/**
 * Class m191104_201123_add_scratbox_categories_to_admin_numbers
 */
class m191104_201123_add_scratbox_categories_to_admin_numbers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $first_category_id = \common\models\Category::find()->where(['name' => 'Сухофрукты'])->one()->id;
	    $second_category_id = \common\models\Category::find()->where(['name' => 'Орехи'])->one()->id;

	    Yii::$app->db->createCommand()->batchInsert('admin_numbers',
		    [
			    'name',
			    'value',
			    'description'
		    ],
		    [
			    [
				    'scratbox-first-category-id',
				    $first_category_id,
				    'ID Первой категория товаров, которые можно добавить в скрат-бокс'
			    ],
			    [
				    'scratbox-second-category-id',
				    $second_category_id,
				    'ID Второй категория товаров, которые можно добавить в скрат-бокс'
			    ]
		    ]
	    )->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191104_201123_add_scratbox_categories_to_admin_numbers cannot be reverted.\n";

        return false;
    }
}
