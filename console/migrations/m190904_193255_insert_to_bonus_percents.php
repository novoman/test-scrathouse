<?php

use yii\db\Migration;

/**
 * Class m190904_193255_insert_to_bonus_percents
 */
class m190904_193255_insert_to_bonus_percents extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    	$inserted_values = [
    		[5, 300, 0],
    		[6, 600, 0],
    		[7, 1000, 0],
    		[10, 1200, 0],
    		[12, 1500, 0],
    		[15, 9999999, 0],
    		[1, 55, 1],
    		[2, 110, 1],
	    ];

    	foreach ($inserted_values as $value) {
		    Yii::$app->db->createCommand()->batchInsert('bonus_percents',
			    ['percent', 'sum', 'type'],
			    [$value]
		    )->execute();
	    }


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190904_193255_insert_to_bonus_percents cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190116_193255_insert_to_admin_numbers cannot be reverted.\n";

        return false;
    }
    */
}
