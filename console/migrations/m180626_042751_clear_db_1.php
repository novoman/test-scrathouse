<?php

use yii\db\Migration;

/**
 * Class m180626_042751_clear_db_1
 */
class m180626_042751_clear_db_1 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    //$this->dropForeignKey('scratboxes_cart', 'scratboxes');
	    $this->dropForeignKey('cart_product_cart', 'cart_products');
	    $this->dropForeignKey('product_product_cart', 'cart_products');
	    $this->dropForeignKey('cart_user', 'cart');
	    $this->dropForeignKey('products_img', 'product_img');
	    $this->dropIndex('product_img_path_indx','product_img');

	    $this->dropTable('cart_products');
	    $this->dropTable('cart');
	    $this->dropTable('product_img');
	    $this->dropColumn('scratboxes', 'id_cart');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180626_042751_clear_db_1 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180626_042751_clear_db_1 cannot be reverted.\n";

        return false;
    }
    */
}
