<?php

use yii\db\Migration;

/**
 * Class m180708_205533_order_add_delivery_cost_column
 */
class m180708_205533_order_add_delivery_cost_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn('order', 'delivery_cost', $this->double());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropColumn('order', 'delivery_cost');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180708_205533_order_add_delivery_cost_column cannot be reverted.\n";

        return false;
    }
    */
}
