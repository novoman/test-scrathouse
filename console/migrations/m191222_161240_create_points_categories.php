<?php

use yii\db\Migration;

/**
 * Class m191222_161240_create_points_categories
 */
class m191222_161240_create_points_categories extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable('points_categories', [
		    'id'          => $this->primaryKey(),
			'name'        => $this->string()->notNull(),
		    'lookup_code' => $this->string()->notNull(),
		    'created_at'  => $this->integer(),
		    'updated_at'  => $this->integer(),
	    ]);

	    $this->createIndex('points_categories_lookup_code_indx', 'points_categories', 'lookup_code');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('points_categories');
    }
}
