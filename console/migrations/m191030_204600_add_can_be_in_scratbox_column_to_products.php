<?php

use yii\db\Migration;

/**
 * Class m191030_204600_add_can_be_in_scratbox_column_to_products
 */
class m191030_204600_add_can_be_in_scratbox_column_to_products extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->addColumn('products', 'can_be_in_scratbox', $this->smallInteger(1)->notNull()->defaultValue(0));
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropColumn('products', 'can_be_in_scratbox');
	}

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190305_204600_add_product_is_special_column cannot be reverted.\n";

        return false;
    }
    */
}
