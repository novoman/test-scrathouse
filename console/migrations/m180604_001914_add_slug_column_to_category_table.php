<?php

use yii\db\Migration;

/**
 * Handles adding slug to table `category`.
 */
class m180604_001914_add_slug_column_to_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('category', 'slug', 'varchar(100)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('category', 'slug');
    }
}
