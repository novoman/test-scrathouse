<?php

use yii\db\Migration;

/**
 * Class m190404_140639_add_profile_to_text_by_admin
 */
class m190404_140639_add_profile_to_text_by_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    Yii::$app->db->createCommand()->batchInsert('text_by_admin',
		    [
			    'type',
			    'text',
			    'page'
		    ],
		    [
			    [
				    '',
				    '<b>Программа лояльности Scrat House</b> - это бонусная программа, дающая постоянную персональную скидку в размере до 20% на все товары Scrat House. Просто совершайте покупки, подписывайтесь на наши группы в Facebook и ВКонтакте, оставляйте там отзывы — за все это Вы будете получать бонусные баллы. Больше баллов — больше скидка!',
				    'profile'
			    ]
		    ]
	    )->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190404_140639_add_profile_to_text_by_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190404_140639_add_profile_to_text_by_admin cannot be reverted.\n";

        return false;
    }
    */
}
