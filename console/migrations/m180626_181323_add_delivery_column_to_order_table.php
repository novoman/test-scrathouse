<?php

use yii\db\Migration;

/**
 * Handles adding delivery to table `order`.
 */
class m180626_181323_add_delivery_column_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'delivery_date', $this->integer());
        $this->addColumn('order', 'delivery_time', $this->string());
        $this->addColumn('order', 'delivery_type', $this->integer(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('order', 'delivery_date');
        $this->dropColumn('order', 'delivery_time');
        $this->dropColumn('order', 'delivery_type');
    }
}
