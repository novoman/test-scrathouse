<?php

use yii\db\Migration;

/**
 * Class m180613_155927_insert_to_payments_type
 */
class m180613_155927_insert_to_payments_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    Yii::$app->db->createCommand()->batchInsert('payments_type', ['name'], [
		    ['Система Расчет-ЕРИП'],
		    ['Система IPay'],
		    ['Наличными, BYN'],
		    ['Безналичный расчёт'],
	    ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    Yii::$app->db->createCommand()->delete('payments_type', ['in', 'name', ['Система Расчет-ЕРИП', 'Система IPay', 'Наличными, BYN', 'Безналичный расчёт']]
	    )->execute();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180613_155927_insert_to_payments_type cannot be reverted.\n";

        return false;
    }
    */
}
