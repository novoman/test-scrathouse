<?php

use yii\db\Migration;

/**
 * Class m181210_170639_unique_index_for_contact_info
 */
class m181210_170639_unique_index_for_contact_info extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->createIndex('UK_location_field_name', 'contact_info', ['location', 'field_name'], $unique = true );
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropIndex('UK_location_field_name', 'contact_info');
	}

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181210_170639_unique_index_for_contact_info cannot be reverted.\n";

        return false;
    }
    */
}
