<?php

use yii\db\Migration;

/**
 * Class m180527_225442_product_img
 */
class m180527_225442_product_img extends Migration
{
    /**
     * {@inheritdoc}
     */
	public function safeUp()
	{
		$this->createTable(
			'product_img',
			[
				'id' => 'pk',
				'path' => 'varchar(250)',
				'id_product' => 'int',
			],
			'ENGINE=InnoDB'
		);
		$this->addForeignKey('products_img', 'product_img', 'id_product',
			'products', 'id');
		$this->createIndex('product_img_path_indx','product_img', 'path');

	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->dropTable('product_img');
	}

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180527_225442_product_img cannot be reverted.\n";

        return false;
    }
    */
}
