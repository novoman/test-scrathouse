<?php

use yii\db\Migration;

/**
 * Class m180402_194702_scratboxes_products
 */
class m180402_194702_scratboxes_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable(
		    'scratboxes_products',
		    [
			    'id' => 'pk',
			    'id_product' => 'int',
			    'id_box' => 'int',
			    'weight' => 'decimal(24,4)',
		    ],
		    'ENGINE=InnoDB'
	    );
	    $this->addForeignKey('scratboxes_products_scratboxes', 'scratboxes_products', 'id_box',
		    'scratboxes', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('scratboxes_products_products', 'scratboxes_products', 'id_product',
		    'products', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('scratboxes_products');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180402_194702_scratboxes_products cannot be reverted.\n";

        return false;
    }
    */
}
