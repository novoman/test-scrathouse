<?php

use yii\db\Migration;

/**
 * Class m190710_124407_add_deliv_pay_payment_types_to_text_by_admin
 */
class m190710_124407_add_deliv_pay_payment_types_to_text_by_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    Yii::$app->db->createCommand()->batchInsert('text_by_admin',
		    [
			    'type',
			    'text',
			    'page'
		    ],
		    [
			    [
				    '5',
				    '
						<label>Способы оплаты</label>
			            <ul>
			                <li>
			                    <span class="caption">Наличными</span>
			                </li>
			                <li>
			                    <span class="caption">Наложенный платеж</span>
			                </li>
			                <li>
			                    <span class="caption">Безналичный расчет</span>
			                    <span class="desc">Оплата банковской картой в наших магазинах.</span>
			                </li>
			                <li>
			                    <span class="caption">Оплата картой Visa, Mastercard, БЕЛКАРТ - WebPay</span>
			                    <span class="desc">Безопасный сервер WEBPAY устанавливает шифрованное соединение по защищенному протоколу TLS и конфиденциально принимает от клиента данные его платёжной карты (номер карты, имя держателя, дату окончания действия, и контрольный номер банковской карточке CVC/CVC2).
			                        После совершения оплаты с использованием банковской карты необходимо сохранять полученные карт-чеки (подтверждения об оплате, полученные в Интернет-магазине) для
			                        сверки с выпиской из карт-счёта (с целью подтверждения совершённых операций в случае возникновения спорных ситуаций).
			                        В случае, если Вы не получили заказ, Вам необходимо обратиться в службу технической поддержки по телефону +375 (29) 363-06-33 или e-mail: info@scrathouse.by. Менеджеры Вас проконсультируют.
			                        В случае необходимости возврата денежных средств (при отмене заказа, возврате товара и т.п.) при оплате банковской платежной картой возврат денежных средств осуществляется на карточку, с которой была произведена оплата.
			                    </span>
			                </li>
			            </ul>
					 ',
				    'delivery-pay'
			    ]
		    ]
	    )->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190710_124407_add_deliv_pay_payment_types_to_text_by_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190710_124407_add_deliv_pay_payment_types_to_text_by_admin cannot be reverted.\n";

        return false;
    }
    */
}
