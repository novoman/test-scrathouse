<?php

use yii\db\Migration;

/**
 * Class m180709_201240_promo_create
 */
class m180709_201240_promo_create extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable('promo', [
		    'id'           => $this->primaryKey(),
		    'name'         => $this->string()->notNull(),
		    'percent'      => $this->integer()->notNull(),
		    'date_from'    => $this->integer()->notNull(),
		    'date_to'      => $this->integer()->notNull(),
		    'product_type' => $this->string()->notNull(),
	    ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
	    $this->dropTable('promo');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180709_201240_promo_create cannot be reverted.\n";

        return false;
    }
    */
}
