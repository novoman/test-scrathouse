<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'eio3AdgJZCuEOZzhF7EHtdH2Cgy8H7m7',
        ],
    ],
];

return $config;
