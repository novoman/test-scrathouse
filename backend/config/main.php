<?php

const SITE_URL = 'https://scrathouse.by';

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
	        'csrfParam' => '_csrf-backend',
		],
        'assetManager' => [
	        'appendTimestamp' => true,
        ],
		'urlManager' => [
		    'enablePrettyUrl' => true,
		    'showScriptName' => false,
		    'rules' => [
		        '/admin' => 'site/index',
		        '<controller:\w+>/<action:\w+>/' => '<controller>/<action>',
		        '<id:([0-9])+>/images/image-by-item-and-alias' => 'yii2images/images/image-by-item-and-alias',
		    ],
		],

        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
            'timeout'=> 24 * 60 * 60,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

    ],
    'controllerMap' => [
	    'elfinder' => [
		    'class' => 'mihaildev\elfinder\PathController',
		    'root' => [
			    'baseUrl'=> SITE_URL.'/img/files',
			    'basePath'=>'@frontend/web/img/files',
			    'name' => 'Files'
			]
	    ]
    ],
    'params' => $params,
];
