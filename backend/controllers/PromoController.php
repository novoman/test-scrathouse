<?php

namespace backend\controllers;

use Yii;
use backend\models\Promo;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * PromoController implements the CRUD actions for Promo model.
 */
class PromoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Promo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Promo::find()->orderBy(['id' => SORT_DESC]),
            'pagination' => [
	            'pageSize' => 10
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Promo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
    	$model = $this->findModel($id);
	    $model->date_from = date('d-m-Y', $model->date_from);
	    $model->date_to = date('d-m-Y', $model->date_to);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Promo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Promo();
	    if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
		    Yii::$app->response->format = Response::FORMAT_JSON;
		    return ActiveForm::validate($model);
	    }
        if ($model->load(Yii::$app->request->post())) {
	        $model->date_from = (int)strtotime($model->date_from);
	        $model->date_to = (int)strtotime($model->date_to);

	        if ($model->save(false)) {
		        Yii::$app->session->setFlash('success', "Промо-код {$model->name} добавлен.");
		        return $this->redirect(['view', 'id' => $model->id]);
	        }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Promo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
	    if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
		    Yii::$app->response->format = Response::FORMAT_JSON;
		    return ActiveForm::validate($model);
	    }
	    $model->date_from = date('d-m-Y', $model->date_from);
	    $model->date_to = date('d-m-Y', $model->date_to);

        if ($model->load(Yii::$app->request->post())) {

	        $model->date_from = (int)strtotime($model->date_from);
	        $model->date_to = (int)strtotime($model->date_to);

        	if ($model->save()) {
		        Yii::$app->session->setFlash('success', "Промо-код {$model->name} обновлен.");
		        return $this->redirect(['view', 'id' => $model->id]);
	        }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Promo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Promo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Promo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Promo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
