<?php

namespace backend\controllers;

use Yii;
use common\models\Articles;
use common\models\ArticlesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ArticlesController implements the CRUD actions for Articles model.
 */
class ArticlesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Articles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticlesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Articles model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Articles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Articles();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
	        $model->image = UploadedFile::getInstances($model, 'image');
	        if ($model->image) {
		        $model->uploadImage();
	        }
	        Yii::$app->session->setFlash('success', "Статья {$model->title} добавлена.");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Articles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
	        $model->image = UploadedFile::getInstances($model, 'image');
	        if ($model->image) {
		        $model->uploadImage();
	        }
	        Yii::$app->session->setFlash('success', "Статья {$model->title} обновлена.");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

	public function actionDeleteImage($modelId, $imgId)
	{
		$model = $this->findModel($modelId);

		foreach ($model->getImages() as $img) {
			if ($img->id == $imgId) {
				if ($img->isMain) {
					$model->removeImage($img);//will set current image main
					$imgs = $model->getImages();
					if ($imgs[0]->id) {
						$model->setMainImage($imgs[0]);
					}
				}
				else {
					$model->removeImage($img);//will set current image main
				}

			}
		}

		if (Yii::$app->request->isAjax) {
			$this->layout = false;
			return $this->render('images-update', ['model' => $model]);
		}
		else {
			return $this->redirect(['update', 'id' => $model->id]);
		}
	}

	public function actionCheckDownloadImages($countImgs, $modelId)
	{
		$model = $this->findModel($modelId);
		$countExistedImgs = $model->getImage()->id ? count($model->getImages()) : 0;
		if (Yii::$app->request->isAjax) {
			return (($countImgs + $countExistedImgs) <= 1);
		}
		else {
			return $this->redirect(['update', 'id' => $model->id]);
		}
	}

    /**
     * Deletes an existing Articles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Articles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Articles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Articles::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
