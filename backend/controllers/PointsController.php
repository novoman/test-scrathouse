<?php

namespace backend\controllers;

use Yii;
use common\models\Points;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PointsController implements the CRUD actions for Points model.
 */
class PointsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Points models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Points::find()->orderBy(['id' => SORT_DESC]),
            'pagination' => [
	            'pageSize' => 10
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Points model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
	    $model = $this->findModel($id);
	    $model->date_from = date('d-m-Y', $model->date_from);
	    $model->date_to = date('d-m-Y', $model->date_to);

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Points model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Points();

	    if ($model->load(Yii::$app->request->post())) {
		    $model->date_from = (int)strtotime($model->date_from);
		    $model->date_to = (int)strtotime($model->date_to);

		    if ($model->save(false)) {
			    Yii::$app->session->setFlash('success', "Бонусные баллы добавлены.");
			    return $this->redirect(['view', 'id' => $model->id]);
		    }
	    }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Points model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

	    $model->date_from = date('d-m-Y', $model->date_from);
	    $model->date_to = date('d-m-Y', $model->date_to);

	    if ($model->load(Yii::$app->request->post())) {

		    $model->date_from = (int)strtotime($model->date_from);
		    $model->date_to = (int)strtotime($model->date_to);

		    if ($model->save()) {
			    Yii::$app->session->setFlash('success', "Бонусные баллы обновлены.");
			    return $this->redirect(['view', 'id' => $model->id]);
		    }
	    }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Points model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Points model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Points the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Points::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
