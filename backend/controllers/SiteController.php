<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\UploadFile;
use yii\web\UploadedFile;
use backend\models\SendEmailForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@', '?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

	public function actionUploadFile()
	{
		$model = new UploadFile();

		if (Yii::$app->request->isPost) {
			$model->file = UploadedFile::getInstance($model, 'file');
			if ($model->uploadFile()) {
				Yii::$app->session->setFlash('success', "Новый файл загружен.");
			}
			else {
				Yii::$app->session->setFlash('error', "Не удалось загрузить новый файл.");
			}
		}

		return $this->render('upload-file', ['model' => $model]);
	}

	public function actionSendEmail()
	{
		$model = new SendEmailForm();

		if (Yii::$app->request->isPost) {
			$model->recipients = Yii::$app->request->post('SendEmailForm')['recipients'];
			$model->template = Yii::$app->request->post('SendEmailForm')['template'];
			$flag = true;

			foreach ($model->recipients as $user_email) {
				if (!$this->sendEmail($model->template, SendEmailForm::$email_subjects[$model->template], $user_email)) {
					$flag = false;
				}
			}

			if ($flag) {
				Yii::$app->session->setFlash('success', 'Письма отправлены.');
				return $this->goHome();
			}
			else {
				Yii::$app->session->setFlash('error', 'Ошибка отправки писем.');
			}

			return $this->refresh();

		}
		else {
			return $this->render('send-email', compact('model'));
		}
	}

	public function actionDownloadAdminGuideFile()
	{
		return Yii::$app->response->sendFile(Yii::getAlias('@backend') .'/web/uploads/admin_guide_scrathouse.doc');
	}

	protected function sendEmail($template, $subject, $to)
	{
		return Yii::$app->mailer
			->compose($template)
			->setTo($to)
			->setFrom(Yii::$app->params['adminEmail'])
			->setSubject($subject)
			->send();
	}
}
