<?php

namespace backend\controllers;

use common\models\ProductsSearch;
use PHPUnit\Framework\Error\Error;
use Yii;
use common\models\Products;
use yii\base\ErrorException;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
	    $searchModel = new ProductsSearch();
	    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Products();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

	        $model->gallery = UploadedFile::getInstances($model, 'gallery');

	        if ($model->gallery) {
		        $model->uploadGallery();
	        }
	        Yii::$app->session->setFlash('success', "Товар {$model->name} добавлен.");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

	    if ($model->special) {
		    $model->weight = 1;
	    }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

	        $model->gallery = UploadedFile::getInstances($model, 'gallery');
	        if ($model->gallery) {
		        $model->uploadGallery();
	        }
	        Yii::$app->session->setFlash('success', "Товар {$model->name} обновлен.");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model
        ]);
    }


    public function actionSetMainImage($modelId, $imgId)
    {
	    $model = $this->findModel($modelId);

    	foreach ($model->getImages() as $img) {
		    if ($img->id == $imgId) {
			    $model->setMainImage($img); //will set current image main
		    }
	    }

	    if (Yii::$app->request->isAjax)
	    {
		    $this->layout = false;
		    return $this->render('images-update', ['model' => $model]);
	    }
	    else {
		    return $this->redirect(['update', 'id' => $model->id]);
	    }

    }

	public function actionDeleteImage($modelId, $imgId)
	{
		$model = $this->findModel($modelId);

		foreach ($model->getImages() as $img) {
			if ($img->id == $imgId) {
				if ($img->isMain) {
					$model->removeImage($img);//will set current image main
					$imgs = $model->getImages();
					if ($imgs[0]->id) {
						$model->setMainImage($imgs[0]);
					}
				}
				else {
					$model->removeImage($img);//will set current image main
				}

			}
		}

		if (Yii::$app->request->isAjax) {
			$this->layout = false;
			return $this->render('images-update', ['model' => $model]);
		}
		else {
			return $this->redirect(['update', 'id' => $model->id]);
		}
	}

	public function actionCheckDownloadImages($countImgs, $modelId)
	{
		$model = $this->findModel($modelId);
		$countExistedImgs = $model->getImage()->id ? count($model->getImages()) : 0;
		if (Yii::$app->request->isAjax) {
			return (($countImgs + $countExistedImgs) <= Products::MAX_PRODUCTS_IMAGES);
		}
		else {
			return $this->redirect(['update', 'id' => $model->id]);
		}
	}

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Запрашиваемая страница не существует.');
    }
}
