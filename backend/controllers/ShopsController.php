<?php

namespace backend\controllers;

use Yii;
use backend\models\Shops;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ShopsController implements the CRUD actions for Shops model.
 */
class ShopsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Shops models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Shops::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Shops model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Shops model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Shops();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
	        $model->image = UploadedFile::getInstances($model, 'image');
	        if ($model->image) {
		        $model->uploadImage();
	        }
	        Yii::$app->session->setFlash('success', "Магазин {$model->name} добавлен.");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Shops model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

	        $model->image = UploadedFile::getInstances($model, 'image');
	        if ($model->image) {
		        $model->uploadImage();
	        }
	        Yii::$app->session->setFlash('success', "Магазин {$model->name} обновлен.");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

	public function actionDeleteImage($modelId, $imgId)
	{
		$model = $this->findModel($modelId);

		foreach ($model->getImages() as $img) {
			if ($img->id == $imgId) {
				if ($img->isMain) {
					$model->removeImage($img);//will set current image main
					$imgs = $model->getImages();
					if ($imgs[0]->id) {
						$model->setMainImage($imgs[0]);
					}
				}
				else {
					$model->removeImage($img);//will set current image main
				}

			}
		}

		if (Yii::$app->request->isAjax) {
			$this->layout = false;
			return $this->render('images-update', ['model' => $model]);
		}
		else {
			return $this->redirect(['update', 'id' => $model->id]);
		}
	}

	public function actionCheckDownloadImages($countImgs, $modelId)
	{
		$model = $this->findModel($modelId);
		$countExistedImgs = $model->getImage()->id ? count($model->getImages()) : 0;
		if (Yii::$app->request->isAjax) {
			return (($countImgs + $countExistedImgs) <= 1);
		}
		else {
			return $this->redirect(['update', 'id' => $model->id]);
		}
	}

    /**
     * Deletes an existing Shops model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Shops model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Shops the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Shops::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
