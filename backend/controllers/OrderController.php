<?php

namespace backend\controllers;

use Yii;
use common\models\Order;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Order::find()->orderBy(['id' => SORT_DESC]),
	        'pagination' => [
	        	'pageSize' => 10
	        ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
	        Yii::$app->session->setFlash('success', "Заказ №{$model->id} добавлен.");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
	        $model->delivery_date = date('U', strtotime($model->delivery_date));
	        if ($model->save()) {
		        if ($model->status == Order::$status_array['STATUS_DELIVERED']) {
		        	if ($model->id_user && $model->bonus_points_to_get > 0) {
				        if (!Order::addClientsPoints($model->bonus_points_to_get, $model->overall_sum_to_get, $model->id_user, $model->id)) {
					        Yii::$app->session->setFlash('error', "Ошибка. Невозможно начислить баллы клиенту за заказ");
				        }
			        }
		        }
		        // return bonus points to user if they were used and the order was canceled
		        if ($model->status == Order::$status_array['STATUS_CANCELED'] && $model->bonus_points > 0) {
			        if ($model->id_user) {
				        if (!Order::addClientsPoints($model->bonus_points, -$model->overall_sum_to_get, $model->id_user, $model->id)) {
					        Yii::$app->session->setFlash('error', "Ошибка. Невозможно вернуть баллы клиенту");
				        }
			        }
		        }
		        Yii::$app->session->setFlash('success', "Заказ №{$model->id} обновлен.");

		        return $this->redirect(['view', 'id' => $model->id]);
	        }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Страница не найдена.');
    }
}
