$(function() {
	$('.my-modal').magnificPopup({
		closeMarkup: '<button title="Закрыть" type="button" class="mfp-close"><img src="/img/icons/cancel.svg"></button>'
	});

	$('#change-signup').click(function () {
        if ($('#change-signup a')[0].innerText === 'Вход в аккаунт') {
            $('#signup-form').css('display', 'none');
            $('#login-form').css('display', 'block');
            $('#change-signup a')[0].innerText = 'Создать аккаунт';
        }
        else {
            $('#login-form').css('display', 'none');
            $('#signup-form').css('display', 'block');
            $('#change-signup a')[0].innerText = 'Вход в аккаунт';
        }

    });

	$('.navbar-header').addClass('col-lg-3 col-lg-push-0 col-md-3 col-md-push-0 col-sm-6 col-sm-push-6 col-xs-12');

	$('#search_form img').on('click', function () {
        $('#search_form').submit();
    });

    function get_blok_size() {
        if ($(window).width() > 500) {
            blok_size = 170;
        }
        else {
            blok_size = 200;
        }
        //console.log($(window).width() + "  " + blok_size);
        return blok_size;
    }
});
