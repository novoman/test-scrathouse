<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\Points */

$this->title = $model->count;
$this->params['breadcrumbs'][] = ['label' => 'Бонусные баллы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="points-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'count',
	        'date_from:date',
	        'date_to:date',

	        [
		        'attribute' => 'points_category_id',
		        'value' => function ($data) {
			        if ($data->points_category_id) {
				        $points_category = \common\models\PointsCategories::findOne($data->points_category_id);
				        return 'ID: '.$points_category->id.', '.$points_category->name;
			        }
			        return 'Не выбран';
		        },
	        ],
	        [
		        'attribute' => 'user_id',
		        'value' => function ($data) {
			        if ($data->user_id) {
				        $user = \common\models\User::findOne($data->user_id);
				        return 'ID: '.$user->id.', '.$user->first_name.' '.$user->last_name;
			        }
			        return 'Не выбран';
		        },
	        ],
	        'created_at:datetime',
	        'updated_at:datetime',
        ],
    ]) ?>

</div>
