<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Points */

$this->title = 'Бонусные баллы';
$this->params['breadcrumbs'][] = ['label' => 'Бонусные баллы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="points-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
