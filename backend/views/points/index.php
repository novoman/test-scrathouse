<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\assets\AppAsset;

AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Бонусные баллы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="points-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать бонусные баллы', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'count',
            'date_from:date',
            'date_to:date',
	        [
		        'attribute' => 'points_category_id',
		        'value' => function ($data) {
			        if ($data->points_category_id) {
				        return \common\models\PointsCategories::findOne($data->points_category_id)->name;
			        }
			        return 'Не выбран';
		        },
	        ],
	        [
		        'attribute' => 'user_id',
		        'value' => function ($data) {
			        if ($data->user_id) {
				        $user = \common\models\User::findOne($data->user_id);
				        return $user->first_name.' '.$user->last_name;
			        }
			        return 'Не выбран';
		        },
	        ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
