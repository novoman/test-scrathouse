<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\Points */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="points-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'count')->textInput() ?>

    <?php
        echo '<label class="control-label">Дата действия</label>';
        echo \kartik\date\DatePicker::widget([
            'model' => $model,
            'attribute' => 'date_from',
            'attribute2' => 'date_to',
            'language' => 'ru',
            'options' => ['placeholder' => 'Дата начала действия'],
            'options2' => ['placeholder' => 'Дата окончания действия'],
            'type' => \kartik\date\DatePicker::TYPE_RANGE,
            'form' => $form,
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true,
                'autoclose' => true,
            ]
        ]);
	?>

	<?= $form->field($model, 'points_category_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\PointsCategories::getPointsCategories(), 'id', 'name'))?>

    <?= $form->field($model, 'user_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\User::findAllActiveUsers(),'id', 'name')) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
