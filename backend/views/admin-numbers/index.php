<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Цифры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-numbers-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать Цифры', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',
            'value',
            'description:ntext',
            //'created_at',
            //'updated_at',

	        [
		        'class' => 'yii\grid\ActionColumn',
		        'options' => ['width' => '70px']
	        ],
        ],
    ]); ?>
</div>
