<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\assets\AppAsset;

AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $model backend\models\AdminNumbers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-numbers-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'name')->dropDownList(\backend\models\AdminNumbers::$admin_numbers)?>

    <?= $form->field($model, 'value')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
