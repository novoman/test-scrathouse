<?php

use yii\helpers\Html;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model backend\models\AdminNumbers */

$this->title = 'Создать Цифры';
$this->params['breadcrumbs'][] = ['label' => 'Цифры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-numbers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
