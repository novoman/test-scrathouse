<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\bootstrap\Nav;
use common\widgets\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\SignupWidget;
use common\widgets\LoginWidget;
use  yii\web\View;
use common\models\Order;
use common\models\ContactForm;
use common\models\ContactPhoneForm;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<?= $this->render('main/flashes') ?>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<header>

    <?php
	NavBar::begin([
		'brandLabel' => '<img src="/img/Logo.svg" alt="Scrathouse">',
		'brandUrl' => Yii::$app->homeUrl,
		'brandOptions' => [
			'class' => false,
		],
		'options' => [
			'class' => 'nav',
			'id' => false,
		],
		'renderInnerContainer' => true,
		'innerContainerOptions' => [
			'class' => 'container',
		],

	]);
	?>
    <div class="my-help-block col-lg-9 col-md-9 col-sm-12 col-xs-12 text-right">
        <?php if (Yii::$app->user->isGuest): ?>
            <div class="login"><a href="<?= \yii\helpers\Url::to(['/site/login'])?>">Войти</a></div>
        <?php else: ?>
            <div class="login-buttons">
                <a href="#" class="help-button">Профиль</a>
                <div class="wrapper-drop">
                    <div class="help-down">
                        <?= Html::a("Выход", ['site/logout'], [
                            'data' => [
                                'method' => 'post'
                            ],
                        ]);?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
	<?php if (\common\models\User::isAdmin(Yii::$app->user->id)): ?>
    <div class="menu col-lg-12 col-md-12">
	    <?php
            $non_processed_orders_count = Order::getOrdersCountByStatus('STATUS_NEW');
	        $non_processed_contact_forms_count = ContactForm::getContactFormsCountByStatus('STATUS_NEW');
            $non_processed_contact_phone_forms_count = ContactPhoneForm::getContactPhoneFormsCountByStatus('STATUS_NEW');
        ?>
        <div class="text-center">
            <ul>
                <li>
	                <?php if ($non_processed_orders_count): ?>
                        <div class="non-processed"><?= $non_processed_orders_count ?></div>
	                <?php endif; ?>
                    <a href="/backend/web/order">Заказы</a>
                </li>
                <li><a href="/backend/web/user">Пользователи</a></li>
                <li><a href="/backend/web/products">Товары</a></li>
                <li><a href="/backend/web/category">Категории</a></li>
                <li><a href="/backend/web/shops">Магазины</a></li>
                <li><a href="/backend/web/payments-type">Способы оплаты</a></li>
                <li><a href="/backend/web/text-by-admin">Настраиваемый текст</a></li>
                <li><a href="/backend/web/contact-info">Контакты</a></li>
                <li><a href="/backend/web/promo">Промокоды</a></li>
                <li><a href="/backend/web/points-categories">Бонусные категории</a></li>
                <li><a href="/backend/web/points">Бонусные баллы</a></li>
                <li><a href="/backend/web/points-histories">История бонусов</a></li>
                <li><a href="/backend/web/admin-numbers">Цифры</a></li>
                <li><a href="/backend/web/articles">Статьи</a></li>
                <li><a href="/backend/web/site/send-email">Рассылка писем</a></li>
                <li>
	                <?php if ($non_processed_contact_forms_count): ?>
                        <div class="non-processed"><?= $non_processed_contact_forms_count ?></div>
	                <?php endif; ?>
                    <a href="/backend/web/contact-form">Обратная связь</a>
                </li>
                <li>
	                <?php if ($non_processed_contact_phone_forms_count): ?>
                        <div class="non-processed"><?= $non_processed_contact_phone_forms_count ?></div>
	                <?php endif; ?>
                    <a href="/backend/web/contact-phone-form">Обратный звонок</a>
                </li>
            </ul>
        </div>
    </div>
    <?php endif; ?>
	<?php NavBar::end(); ?>
</header>
<?php if ((\common\models\User::isAdmin(Yii::$app->user->id)) ||
          ((Yii::$app->user->getIsGuest()) && (Yii::$app->controller->action->id === 'login'))): ?>
    <div class="container" style="margin-top: 30px;">
		<?= Breadcrumbs::widget([
			'homeLink' => ['label' => 'Главная', 'url' => '/backend/web/'],
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>
    </div>

    <div class="container" style="margin-top: 30px; margin-bottom: 50px;">
	    <?=$content ?>
    </div>

<?php else: ?>
    <div class="container text-center" style="margin-top: 150px; margin-bottom: 200px;">
        <h3>Вам запрещён доступ к этой странице.<br><br>Для просмотра <a href="<?= \yii\helpers\Url::to(['/site/login'])?>">войдите</a> на сайт как администратор.</h3>
    </div>
<?php endif; ?>

<?php $contact_info = \backend\models\ContactInfo::getContactInfoArray();?>

<div class="footer">
    <div class="container">
        <div class="row">
            <div class="content">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 copyright item">
                    <div class="title"><?=$contact_info['footer']['name'] ?></div>
                    <div class="info">
						<?=$contact_info['footer']['domain'] ?>
                    </div>
                    <div class="info">
						<?=$contact_info['footer']['reg_number'] ?>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 contacts item">
                    <div class="title">Контакты</div>
                    <div class="info">
                        Email: <a href="mailto:<?=$contact_info['footer']['email'] ?>"><?=$contact_info['footer']['email'] ?></a>
                    </div>
                    <div class="info">
                        <a href="tel:<?=$contact_info['footer']['tel'] ?>"><?=$contact_info['footer']['tel'] ?></a>
                    </div>

                    <div class="info">
                        <a href="<?=$contact_info['footer']['skype'] ?>"><?=$contact_info['footer']['skype'] ?></a>
                    </div>
                </div>

                <div class="row separator"></div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 menu item">
                    <ul>
                        <li><a href="<?= \yii\helpers\Url::to(['/deliv-pay/deliv-pay'])?>">Доставка и оплата</a></li>
                        <li><a href="<?= \yii\helpers\Url::to(['/shops/shops'])?>">Магазины</a></li>
                        <li><a href="<?= \yii\helpers\Url::to(['/site/wholesale'])?>">Сотрудничество</a></li>
                        <li><a href="<?= \yii\helpers\Url::to(['/scrat-box/scrat-box'])?>">Cкрат боксы</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 copyright item">
                    <div class="title">Социальные сети</div>
                    <div class="socials">
                        <ul>
                            <li><a href="<?=$contact_info['footer']['fb'] ?>" target="_blank">
                                    <img src="/img/icons/socials/fb.svg" alt="" style="width: 10px;">
                                </a></li>
                            <li><a href="<?=$contact_info['footer']['tw'] ?>"  target="_blank">
                                    <img src="/img/icons/socials/twitter.svg" alt="" style="width: 21px;">
                                </a></li>
                            <li><a href="<?=$contact_info['footer']['vk'] ?>" target="_blank">
                                    <img src="/img/icons/socials/vk.svg" alt="" style="width: 25px;">
                                </a></li>
                            <li><a href="<?=$contact_info['footer']['in'] ?>" target="_blank">
                                    <img src="/img/icons/socials/instagram.svg" alt="" style="width: 18px;">
                                </a></li>
                        </ul>
                    </div>
                </div>

                <div class="row separator"></div>
                <div class="col-md-12 item seo-text">
		            <?=$contact_info['footer']['seo'] ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Mobile menu section -->
<?php if (\common\models\User::isAdmin(Yii::$app->user->id)): ?>
<nav class="pushy pushy-left">
    <div class="pushy-content">
        <ul>
            <li class="pushy-link">
                <a href="/backend/web/order">Заказы
	            <?php if ($non_processed_orders_count): ?>
                    (<?= $non_processed_orders_count ?>)
	            <?php endif; ?>
                </a>
            </li>
            <li class="pushy-link"><a href="/backend/web/user">Пользователи</a></li>
            <li class="pushy-link"><a href="/backend/web/products">Товары</a></li>
            <li class="pushy-link"><a href="/backend/web/category">Категории</a></li>
            <li class="pushy-link"><a href="/backend/web/shops">Магазины</a></li>
            <li class="pushy-link"><a href="/backend/web/payments-type">Способы оплаты</a></li>
            <li class="pushy-link"><a href="/backend/web/text-by-admin">Настраиваемый текст</a></li>
            <li class="pushy-link"><a href="/backend/web/contact-info">Контакты</a></li>
            <li class="pushy-link"><a href="/backend/web/promo">Промокоды</a></li>
            <li class="pushy-link"><a href="/backend/web/points-categories">Бонусные категории</a></li>
            <li class="pushy-link"><a href="/backend/web/points">Бонусные баллы</a></li>
            <li class="pushy-link"><a href="/backend/web/points-histories">История бонусов</a></li>
            <li class="pushy-link"><a href="/backend/web/admin-numbers">Цифры</a></li>
            <li class="pushy-link"><a href="/backend/web/articles">Статьи</a></li>
            <li class="pushy-link"><a href="/backend/web/site/send-email">Рассылка писем</a></li>
            <li class="pushy-link">
                <a href="/backend/web/contact-form">Обратная связь
	                <?php if ($non_processed_contact_forms_count): ?>
                        (<?= $non_processed_contact_forms_count ?>)
	                <?php endif; ?>
                </a>
            </li>
            <li class="pushy-link">
                <a href="/backend/web/contact-phone-form">Обратный звонок
	                <?php if ($non_processed_contact_phone_forms_count): ?>
                        (<?= $non_processed_contact_phone_forms_count ?>)
	                <?php endif; ?>
                </a>
            </li>
        </ul>
    </div>
</nav>
<?php endif; ?>

<div class="site-overlay"></div>

<div id="container">
    <button class="menu-btn"><span></span><span></span><span></span></button>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
