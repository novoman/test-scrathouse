<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BonusPercents */

$this->title = 'Update Bonus Percents: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Bonus Percents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bonus-percents-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
