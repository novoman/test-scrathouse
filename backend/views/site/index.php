<?php

/* @var $this yii\web\View */
use backend\assets\AppAsset;

AppAsset::register($this);

$this->title = 'Scrathouse';
?>
<div class="site-index container">

    <div class="jumbotron">
        <h2>Scrathouse</h2>

        <p class="lead">Панель администратора.</p>

	    <?php if (\common\models\User::isSuperAdmin(Yii::$app->user->id)): ?>
            <h5><a href="/site/upload-file">Загрузка файла руководства администратора</a></h5>
        <?php endif; ?>

        <div class="row">
            <ul>
                <li><a href="/backend/web/order">Заказы</a></li>
                <li><a href="/backend/web/user">Пользователи</a></li>
                <li><a href="/backend/web/products">Товары</a></li>
                <li><a href="/backend/web/category">Категории</a></li>
                <li><a href="/backend/web/shops">Магазины</a></li>
                <li><a href="/backend/web/payments-type">Способы оплаты</a></li>
                <li><a href="/backend/web/text-by-admin">Настраиваемый текст</a></li>
                <li><a href="/backend/web/contact-info">Контакты</a></li>
                <li><a href="/backend/web/promo">Промокоды</a></li>
                <li><a href="/backend/web/points-categories">Бонусные категории</a></li>
                <li><a href="/backend/web/points">Бонусные баллы</a></li>
                <li><a href="/backend/web/points-histories">История бонусов</a></li>
                <li><a href="/backend/web/admin-numbers">Цифры</a></li>
                <li><a href="/backend/web/articles">Статьи</a></li>
                <li><a href="/backend/web/contact-form">Обратная связь</a></li>
                <li><a href="/backend/web/contact-phone-form">Обратный звонок</a></li>
                <li><a href="/backend/web/site/send-email">Рассылка писем</a></li>
            </ul>
        </div>
    </div>
</div>
