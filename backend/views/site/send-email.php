<?php

/* @var $this yii\web\View */

use backend\models\SendEmailForm;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use backend\assets\AppAsset;
use dosamigos\multiselect\MultiSelect;

$this->title = 'Рассылка писем';
$this->params['breadcrumbs'][] = $this->title;

AppAsset::register($this);

?>

<div class="send-email-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'recipients')->widget(MultiSelect::className(),[
		"options" => ['multiple'=>"multiple"],
		"data" => \yii\helpers\ArrayHelper::map(SendEmailForm::findUsers(),'email', 'name'),
		"clientOptions" =>
			[
				"includeSelectAllOption" => true,
				'numberDisplayed' => 5
			],
	]) ?>

	<?= $form->field($model, 'template')->dropDownList(SendEmailForm::$templates_list) ?>

    <div class="form-group">
		<?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
    </div>

	<?php ActiveForm::end(); ?>

</div>
