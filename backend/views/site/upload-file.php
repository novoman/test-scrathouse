<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\assets\AppAsset;

AppAsset::register($this);

$this->title = 'Загрузка руководства администратора';

/* @var $this yii\web\View */
/* @var $model backend\models\TextByAdmin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="text-by-admin-form">

    <button style="margin-bottom: 50px;">
		<?= Html::a(Yii::t('app', 'Скачать руководство администратора'), ['site/download-admin-guide-file'],
			['style' => 'text-decoration: none; padding: 30px; color: #000;']) ?>
    </button>

	<?php if (\common\models\User::isSuperAdmin(Yii::$app->user->id)): ?>

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'file')->fileInput() ?>

        <div class="form-group">
            <?= Html::submitButton('Загрузить руководство', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    <?php endif; ?>
</div>
