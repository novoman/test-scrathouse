<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use backend\assets\AppAsset;

$this->title = $name;
AppAsset::register($this);
\frontend\assets\ProductsAsset::register($this);
?>

<div class="site-error container">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
		<?= nl2br(Html::encode($message)) ?>
    </div>

    <br>

    <p>
        В результате обработки Вашего запроса случилась ошибка.
    </p>

    <br>

    <p>
        Пожалуйста, <a href="<?= \yii\helpers\Url::to(['/site/contact'])?>">сообщите нам,</a> если Вы думаете, что это ошибка сервера. Спасибо.
    </p>

    <br>

</div>
