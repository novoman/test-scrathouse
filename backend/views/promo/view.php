<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\assets\AppAsset;

AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $model backend\models\Promo */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Промо-коды', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот промо-код?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'discount',
            'date_from:date',
            'date_to:date',
	        [
		        'attribute' => 'person_type',
		        'value' => function ($data) {
			        return !$data->person_type ? 'Персональная' : 'Для всех';
		        },
	        ],
	        [
		        'attribute' => 'discount_type',
		        'value' => function ($data) {
			        return !$data->discount_type ? '%' : 'руб';
		        },
	        ],
            [
		        'attribute' => 'used',
		        'value' => function ($data) {
			        return !$data->used ? 'Нет' : 'Да';
		        },
	        ],
	        'times_used',
	        'max_times_used',
        ],
    ]) ?>

</div>
