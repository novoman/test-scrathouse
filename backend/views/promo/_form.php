<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model backend\models\Promo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name', ['enableAjaxValidation' => true])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'discount')->textInput() ?>

    <?php
        echo '<label class="control-label">Дата действия</label>';
        echo \kartik\date\DatePicker::widget([
            'model' => $model,
            'attribute' => 'date_from',
            'attribute2' => 'date_to',
            'language' => 'ru',
            'options' => ['placeholder' => 'Дата начала действия'],
            'options2' => ['placeholder' => 'Дата окончания действия'],
            'type' => \kartik\date\DatePicker::TYPE_RANGE,
            'form' => $form,
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
                'todayHighlight' => true,
                'autoclose' => true,
            ]
        ]);
    ?>

	<?= $form->field($model, 'person_type')->dropDownList([0 => 'Персональная', 1 => 'Для всех']) ?>

	<?= $form->field($model, 'discount_type')->dropDownList([0 => '%', 1 => 'руб']) ?>

	<?= $form->field($model, 'used')->dropDownList([0 => 'Нет', 1 => 'Да']) ?>

	<?= $form->field($model, 'times_used')->textInput() ?>

	<?= $form->field($model, 'max_times_used')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
