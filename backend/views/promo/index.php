<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\assets\AppAsset;

AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Промо-коды';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать Промо-код', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'name',
            'discount',
            'date_from:date',
            'date_to:date',
	        [
		        'attribute' => 'person_type',
		        'filter'=>array(0 => 'Персональная', 1 => 'Для всех'),
		        'value' => function ($data) {
			        return !$data->person_type ? 'Персональная' : 'Для всех';
		        },
	        ],
	        [
		        'attribute' => 'discount_type',
		        'filter'=>array(0 => '%', 1 => 'руб'),
		        'value' => function ($data) {
			        return !$data->discount_type ? '%' : 'руб';
		        },
	        ],
            [
		        'attribute' => 'used',
		        'filter'=>array(0 => 'Нет', 1 => 'Да'),
		        'value' => function ($data) {
			        return !$data->used ? 'Нет' : 'Да';
		        },
	        ],
	        'times_used',
	        'max_times_used',
	        [
		        'class' => 'yii\grid\ActionColumn',
		        'options' => ['width' => '70px']
	        ],
        ],
    ]); ?>
</div>
