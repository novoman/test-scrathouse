<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ContactInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contact-info-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
        $location_array = array();
        $field_name_array = array();

        foreach (\backend\models\ContactInfo::$contact_info_fileds as $location => $field_names) {
            $location_array[$location] = $location;
            foreach ($field_names as $field_name) {
	            $field_name_array[$field_name] = $field_name;
            }
        }
    ?>
	<?= $form->field($model, 'location')->dropDownList($location_array)?>

	<?= $form->field($model, 'field_name')->dropDownList($field_name_array)?>

    <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
