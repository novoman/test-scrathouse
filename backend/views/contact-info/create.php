<?php

use yii\helpers\Html;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model backend\models\ContactInfo */

$this->title = 'Создать пункт Контактной информации';
$this->params['breadcrumbs'][] = ['label' => 'Контактная информация', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-info-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
