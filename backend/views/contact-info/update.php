<?php

use yii\helpers\Html;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model backend\models\ContactInfo */

$this->title = 'Обновить Контактную информацию: ' . $model->field_name;
$this->params['breadcrumbs'][] = ['label' => 'Контактная информация', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->field_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="contact-info-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
