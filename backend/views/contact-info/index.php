<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\assets\AppAsset;

AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Контактная информация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-info-index">

    <button style="margin-bottom: 30px; margin-top: 20px;">
		<?= Html::a(Yii::t('app', 'Скачать руководство администратора'), ['site/download-admin-guide-file'],
			['style' => 'text-decoration: none; padding: 30px; color: #000;']) ?>
    </button>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать пункт Контактной информации', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'location',
            'field_name',
            'value',
	        'created_at:datetime',
	        'updated_at:datetime',

	        [
		        'class' => 'yii\grid\ActionColumn',
		        'options' => ['width' => '70px']
	        ],
        ],
    ]); ?>
</div>
