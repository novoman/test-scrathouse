<?php

use yii\helpers\Html;
use backend\assets\AppAsset;

AppAsset::register($this);


/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Создать Пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
