<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'birthday')->textInput(['maxlength' => true, 'value' => date('Y-m-d', $model->birthday)]) ?>

	<?= $form->field($model, 'client_type')->dropDownList([0 => 'Физическое лицо', 1 => 'Юридическое лицо']) ?>

	<?= $form->field($model, 'status')->dropDownList([User::STATUS_ACTIVE => 'Активен',
	                                                             User::STATUS_WAIT => 'Ожидает подтверждения',
	                                                             User::STATUS_DELETED => 'Удален',
	                                                             User::STATUS_BLOCKED => 'Заблокирован']) ?>

	<?= $form->field($model, 'users_points')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'overall_sum')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'is_admin')->dropDownList([0 => 'Нет', 1 => 'Да']) ?>

    <?php if ($model->is_super_admin): ?>
	    <?= $form->field($model, 'is_super_admin')->dropDownList([0 => 'Нет', 1 => 'Да']) ?>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
