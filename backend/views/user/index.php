<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use backend\assets\AppAsset;
use yii\web\View;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs("
    $('#add_points').click(function(e){
        e.preventDefault();
        var points = prompt('Введите количество скрэтублей:', 0);
        $.ajax({
            url: '/user/add-points-to-users',
            type: 'GET',
            data: { points: points },
            success: function(){}
        });
    });
", View::POS_LOAD);

?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать Пользователя', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Добавить бонусные баллы', [null], [
                'class' => 'btn btn-primary',
                'id' => 'add_points',
        ]) ?>
        <?= Html::a('Обнулить общую сумму заказов', ['set-overall-sums-to-null'], [
	        'class' => 'btn btn-danger',
	        'data' => [
		        'confirm' => 'Вы уверены?',
		        'method' => 'post',
	        ],
        ]) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'first_name',
            'last_name',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            'email:email',
            //'email_confirm_token:email',
            'telephone',
            'address',
	        'users_points',
	        'overall_sum',

	        [
		        'attribute' => 'birthday',
		        'value' => function ($data) {
			        return $data->birthday ? date("d.m.Y", $data->birthday) : '(не задано)';
		        },
		        'options' => ['width' => '130px']
            ],
	        [
		        'attribute' => 'client_type',
		        'filter'=>array(1 => 'Юр. лицо', 0 => 'Физ. лицо'),
		        'value' => function ($data) {
			        return !$data->client_type ? 'Физ. лицо' : 'Юр. лицо';
		        },
		        'options' => ['width' => '110px']
	        ],

	        [
		        'attribute' => 'status',
		        'filter' => array(User::STATUS_ACTIVE => 'Активен',
		                          User::STATUS_WAIT => 'Ожидает подтверждения',
                                  User::STATUS_DELETED => 'Удален',
                                  User::STATUS_BLOCKED => 'Заблокирован'),
		        'value' => function ($data) {
                    switch ($data->status) {
                        case User::STATUS_ACTIVE:
                            return '<span class="text-success">Активен</span>';
                        case User::STATUS_WAIT:
                            return '<span class="text-warning">Ожидает подтверждения</span>';
                        case User::STATUS_DELETED:
                            return '<span class="text-danger">Удален</span>';
                        case User::STATUS_BLOCKED:
                            return '<span class="text-secondary">Заблокирован</span>';
                        default:
                            return'Не назначено';
                    }
		        },
		        'format' => 'html',
	        ],

            //'created_at',
            //'updated_at',
	        [
		        'attribute' => 'is_admin',
		        'filter'=> array(1 => 'Да', 0 => 'Нет'),
		        'value' => function ($data) {
			        return !$data->is_admin ? '<span class="text-danger">Нет</span>' : '<span class="text-success">Да</span>';
		        },
		        'format' => 'html',
	        ],

	        [
		        'class' => 'yii\grid\ActionColumn',
		        'options' => ['width' => '90px']
	        ],
        ],
    ]); ?>
</div>
