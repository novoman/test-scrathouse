<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->first_name.' '.$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этого пользователя?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
            'auth_key',
            'password_hash',
            'password_reset_token',
            'email:email',
            'email_confirm_token:email',
            'telephone',
            'address',
	        'users_points',
	        'overall_sum',
            'birthday:date',
	        [
		        'attribute' => 'client_type',
		        'filter'=>array(1 => 'Юридическое лицо', 0 => 'Физическое лицо'),
		        'value' => function ($data) {
			        return !$data->client_type ? 'Физическое лицо' : 'Юридическое лицо';
		        },
		        'options' => ['width' => '110px']
	        ],
	        [
		        'attribute' => 'status',
		        'filter' => array(User::STATUS_ACTIVE => 'Активен',
		                          User::STATUS_WAIT => 'Ожидает подтверждения',
		                          User::STATUS_DELETED => 'Удален',
		                          User::STATUS_BLOCKED => 'Заблокирован'),
		        'value' => function ($data) {
			        switch ($data->status) {
				        case User::STATUS_ACTIVE:
					        return '<span class="text-success">Активен</span>';
				        case User::STATUS_WAIT:
					        return '<span class="text-warning">Ожидает подтверждения</span>';
				        case User::STATUS_DELETED:
					        return '<span class="text-danger">Удален</span>';
				        case User::STATUS_BLOCKED:
					        return '<span class="text-secondary">Заблокирован</span>';
				        default:
					        return'Не назначено';
			        }
		        },
		        'format' => 'html',
	        ],
            'created_at:datetime',
            'updated_at:datetime',
	        [
		        'attribute' => 'is_admin',
		        'filter'=>array(1 => 'Да', 0 => 'Нет'),
		        'value' => function ($data) {
			        return !$data->is_admin ? '<span class="text-danger">Нет</span>' : '<span class="text-success">Да</span>';
		        },
		        'format' => 'html',
	        ],
	        [
		        'attribute' => 'is_super_admin',
		        'filter'=>array(1 => 'Да', 0 => 'Нет'),
		        'value' => function ($data) {
			        return !$data->is_super_admin ? '<span class="text-danger">Нет</span>' : '<span class="text-success">Да</span>';
		        },
		        'format' => 'html',
	        ],
        ],
    ]) ?>

</div>
