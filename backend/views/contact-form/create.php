<?php

use yii\helpers\Html;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\ContactForm */

$this->title = 'Создать Контактную форму';
$this->params['breadcrumbs'][] = ['label' => 'Контактные формы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-form-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
