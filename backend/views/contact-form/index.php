<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\ContactForm;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Контактные формы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-form-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать Контактную форму', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',
            'email:email',
            'subject',
            'body:ntext',
            //'verifyCode',
	        [
		        'attribute' => 'status',
		        'value' => function ($data) {
			        switch ($data->status) {
				        case ContactForm::$status_array['STATUS_NEW']:
					        return '<span class="text-danger">Новый</span>';
				        case ContactForm::$status_array['STATUS_PROCESSED']:
					        return '<span class="text-success">Обработан</span>';
				        default:
					        return '<span class="text-danger">Новый</span>';
			        }
		        },
		        'format' => 'html',
	        ],
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
