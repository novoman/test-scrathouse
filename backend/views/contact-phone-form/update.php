<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ContactPhoneForm */

$this->title = 'Обновить Форму обратного звонка: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Формы обратного звонка', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="contact-phone-form-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
