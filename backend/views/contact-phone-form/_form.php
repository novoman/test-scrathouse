<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ContactPhoneForm;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\ContactPhoneForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contact-phone-form-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'status')->dropDownList([
		ContactPhoneForm::$status_array['STATUS_NEW'] => 'Новый',
		ContactPhoneForm::$status_array['STATUS_PROCESSED'] => 'Обработан',
	]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
