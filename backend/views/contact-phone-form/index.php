<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\ContactPhoneForm;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Формы обратного звонка';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-phone-form-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать Форму обратного звонка', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',
            'phone',
	        [
		        'attribute' => 'status',
		        'value' => function ($data) {
			        switch ($data->status) {
				        case ContactPhoneForm::$status_array['STATUS_NEW']:
					        return '<span class="text-danger">Новый</span>';
				        case ContactPhoneForm::$status_array['STATUS_PROCESSED']:
					        return '<span class="text-success">Обработан</span>';
				        default:
					        return '<span class="text-danger">Новый</span>';
			        }
		        },
		        'format' => 'html',
	        ],
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
