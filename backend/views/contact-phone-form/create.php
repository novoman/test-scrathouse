<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ContactPhoneForm */

$this->title = 'Создание форму обратного звонка';
$this->params['breadcrumbs'][] = ['label' => 'Формы обратного звонка', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-phone-form-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
