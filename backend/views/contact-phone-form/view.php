<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\ContactPhoneForm;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\ContactPhoneForm */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Формы обратного звонка', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="contact-phone-form-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'phone',
            'verifyCode',
	        [
		        'attribute' => 'status',
		        'value' => function ($data) {
			        switch ($data->status) {
				        case ContactPhoneForm::$status_array['STATUS_NEW']:
					        return '<span class="text-danger">Новый</span>';
				        case ContactPhoneForm::$status_array['STATUS_PROCESSED']:
					        return '<span class="text-success">Обработан</span>';
				        default:
					        return '<span class="text-danger">Новый</span>';
			        }
		        },
		        'format' => 'html',
	        ],
	        'created_at:datetime',
	        'updated_at:datetime',
        ],
    ]) ?>

</div>
