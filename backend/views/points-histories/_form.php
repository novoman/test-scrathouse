<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\PointsHistories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="points-histories-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'type')->dropDownList([0 => 'Списание', 1 => 'Начисление']) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'points_id')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Points::findAllPoints(),'id', 'name')) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
