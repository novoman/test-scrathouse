<?php

use yii\helpers\Html;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\PointsHistories */

$this->title = 'Создать историю бонусов';
$this->params['breadcrumbs'][] = ['label' => 'История бонусов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="points-histories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
