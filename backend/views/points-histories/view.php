<?php

use common\models\Points;
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\PointsHistories */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'История бонусов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="points-histories-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
	        [
		        'attribute' => 'type',
		        'value' => function ($data) {
			        return $data->type == 0 ? 'Списание' : 'Начисление';
		        },
		        'format' => 'html',
	        ],
            'comment:raw',
	        [
		        'attribute' => 'points_id',
		        'format' => 'raw',
		        'value' => function ($data) {
			        return 'Количество: <b>' .(Points::findOne($data->points_id) ? Points::findOne($data->points_id)->count : 'нет данных').'</b><br>Ссылка: <a href="/backend/web/points/view?id=' . $data->points_id . '">ID ' . $data->points_id . '</a>';
		        },
	        ],
	        'created_at:datetime',
	        'updated_at:datetime',
        ],
    ]) ?>

</div>
