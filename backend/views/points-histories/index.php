<?php

use common\models\Points;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\assets\AppAsset;

AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'История бонусов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="points-histories-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать историю бонусов', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
	        [
		        'attribute' => 'type',
		        'value' => function ($data) {
			        return $data->type == 0 ? 'Списание' : 'Начисление';
		        },
		        'format' => 'html',
	        ],
            'comment:raw',
	        [
		        'attribute' => 'points_id',
		        'format' => 'raw',
		        'value' => function ($data) {
			        return 'Количество: <b>' .(Points::findOne($data->points_id) ? Points::findOne($data->points_id)->count : 'нет данных').'</b><br>Ссылка: <a href="/backend/web/points/view?id=' . $data->points_id . '">ID ' . $data->points_id . '</a>';
		        },
	        ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
