<?php

use yii\helpers\Html;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\PointsCategories */

$this->title = 'Создать категории бонусов';
$this->params['breadcrumbs'][] = ['label' => 'Категории бонусов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="points-categories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
