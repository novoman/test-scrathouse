<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model backend\models\Shops */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shops-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'work_time')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'coordinante_X')->textInput() ?>

    <?= $form->field($model, 'coordinante_Y')->textInput() ?>

	<?php
	// Usage with ActiveForm and model
	echo $form->field($model, 'image')->widget(FileInput::classname(), [
		'options' => ['multiple' => false, 'accept' => 'image/*'],
		'pluginOptions' => [
			'previewFileType' => 'image',
			'showCaption' => false,
			'showRemove' => false,
			'showUpload' => false,
			'browseClass' => 'btn btn-primary btn-block',
			'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
			'browseLabel' =>  'Выбрать',
		],
	]);
	?>


    <label style="margin-bottom: 20px; margin-top: 30px;" class="control-label" for="shops-images">Загруженное фото</label>
    <div id="images" style="min-height: 150px; margin-bottom: 20px;">
		<?php echo $this->render('images-update', ['model' => $model]); ?>
    </div>

    <div class="form-group">
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success',
		                                'onclick'=>
			                                "
                            var count =  $(\"input:file\")[0].files.length;
                            var check_imgs = true;
                            $.ajax({
                                 url: '/shops/check-download-images',
                                 data: {countImgs: count, modelId: ".$model->id."},
                                 type:'GET',         
                                 async: false,                   
                                 success  : function(response) {
                                    if (!response) {                                  
                                        alert(\"Максимально допустимое число  фото для магазина - 1!\");
                                        check_imgs = false;
                                    }
                                 },
                                 error: function(){
                                     alert('Ошибка!');
                                 },
                            });
                            return check_imgs;
                        ",

			]
		) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
