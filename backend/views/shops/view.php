<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\assets\AppAsset;

AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $model backend\models\Shops */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Магазины', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shops-view container">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот магазин?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'address',
            'city',
            'work_time',
            'telephone',
            'coordinante_X',
            'coordinante_Y',
	        [
		        'attribute' => 'image',
		        'value' => function ($data) {
			        return "<img src='".($data->getImage()->id ? '' : '/0').$data->getImage()->getUrl('x100')."'>";
		        },
		        'format' => 'html',
	        ],
        ],
    ]) ?>

</div>
