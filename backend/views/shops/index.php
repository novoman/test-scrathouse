<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\assets\AppAsset;

AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Магазины';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shops-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать Магазин', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',
            'address',
            'city',
            'work_time',
	        [
		        'attribute' => 'image',
		        'value' => function ($data) {
			        return "<img src='".($data->getImage()->id ? '' : '/0').$data->getImage()->getUrl('x100')."'>";
		        },
		        'format' => 'html',
	        ],
            //'telephone',
            //'coordinante_X',
            //'coordinante_Y',

	        [
		        'class' => 'yii\grid\ActionColumn',
		        'options' => ['width' => '70px']
	        ],
        ],
    ]); ?>
</div>
