<?php
	if ($model->getImage()->id) {
		foreach ( $model->getImages() as $img ) {
			echo '<div style="float: left;">';
			echo "<img style='margin: 10px;" . ( $img->isMain ? " border: 2px solid #337ab7 " : "" ) . "' src='{$img->getUrl('x100')}'>";
			echo '<div style="text-align: center;">';
			echo \yii\helpers\Html::a( '<span class="glyphicon glyphicon-trash" title="Удалить"></span>', [
				'shops/delete-image',
				'modelId' => $model->id,
				'imgId'   => $img->id
			],
				[
					'onclick' =>
						"if (confirm(\"Хотите удалить это фото?\")) {
	                        $.ajax({
	                             type:'POST',
	                             cache: false,
	                             url: '" . \yii\helpers\Url::to([
																	'shops/delete-image',
																	'modelId' => $model->id,
																	'imgId'   => $img->id
																]) . "',
	                             success  : function(response) {
	                                 $('#images').html(response);
	                             },
                                 error: function() {
                                     alert('Ошибка!');
                                 },
	                        });
			            }			                        
	                    return false;",
				] );
			echo '</div>';
			echo '</div>';
		}
	}
	else {
		echo '<div style="float: left;">';
		echo '<span style="color: red;">Нет фото</span>';
		echo '</div>';
	}
?>
