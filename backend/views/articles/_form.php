<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use backend\assets\AppAsset;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
mihaildev\elfinder\Assets::noConflict($this);

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\Articles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="articles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'preview')->textarea(['rows' => 6]) ?>

	<?php
        echo $form->field($model, 'text')->widget(CKEditor::className(), [
            'editorOptions' => ElFinder::ckeditorOptions(['elfinder'],[]) + [
		        'font_names' => 'Roboto;Arial;Georgia;Tahoma;Verdana;Comic Sans MS/FontComic;Courier New/FontCourier;Times New Roman/FontTimes',
            ],
        ]);
	?>


    <?php
        // Usage with ActiveForm and model
        echo $form->field($model, 'image')->widget(FileInput::classname(), [
            'options' => ['multiple' => false, 'accept' => 'image/*'],
            'pluginOptions' => [
                'previewFileType' => 'image',
                'showCaption' => false,
                'showRemove' => false,
                'showUpload' => false,
                'browseClass' => 'btn btn-primary btn-block',
                'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                'browseLabel' =>  'Выбрать',
            ],
        ]);
	?>


    <label style="margin-bottom: 20px; margin-top: 30px;" class="control-label" for="articles-images">Загруженное фото</label>
    <div id="images" style="min-height: 150px; margin-bottom: 20px;">
		<?php echo $this->render('images-update', ['model' => $model]); ?>
    </div>

    <div class="form-group">
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success',
             'onclick'=>
                 "
                    var count =  $(\"input:file\")[0].files.length;
                    var check_imgs = true;
                    $.ajax({
                         url: '/articles/check-download-images',
                         data: {countImgs: count, modelId: ".$model->id."},
                         type:'GET',         
                         async: false,                   
                         success  : function(response) {
                            if (!response) {                                  
                                alert(\"Максимально допустимое число фото для превью статьи - 1!\");
                                check_imgs = false;
                            }
                         },
                         error: function(){
                             alert('Ошибка!');
                         },
                    });
                    return check_imgs;
                ",
			]
		) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
