<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\assets\AppAsset;

AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\ProductsSearch */

$this->title = Yii::t('app', 'Товары');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать Товар', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<?php Pjax::begin(); ?>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
        'columns' => [
                [
                    'attribute' => 'id',
                    'options' => ['width' => '100px']
                ],
                'name',
                'price',
                [
                    'attribute' => 'presence',
                    'filter'=>array(1 => 'Да', 0 => 'Нет'),
                    'value' => function ($data) {
                        return !$data->presence ? '<span class="text-danger">Нет</span>' : '<span class="text-success">Да</span>';
                    },
                    'format' => 'html',
                ],
                'description:html',
                'articule',
                //'hits',
                //'sort',
                //'id_cat',
                [
                  'attribute' => 'id_cat',
                  'value' => function ($data) {
                        return $data->category->name;
                  },

                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'options' => ['width' => '70px']
                ],
        ],
	]); ?>
<?php Pjax::end(); ?>
</div>

