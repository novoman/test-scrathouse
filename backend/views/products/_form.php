<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Category;
use kartik\file\FileInput;
use backend\assets\AppAsset;
use mihaildev\ckeditor\CKEditor;

AppAsset::register($this);


/* @var $this yii\web\View */
/* @var $model common\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?php if (Yii::$app->controller->action->id == 'update'): ?>
        <?php if (!$model->special): ?>
            <?= $form->field($model, 'price_per_kg')->textInput() ?>
        <?php endif; ?>
    <?php else: ?>
	    <?= $form->field($model, 'price_per_kg')->textInput() ?>
    <?php endif; ?>

	<?= $form->field($model, 'presence')->dropDownList([0 => 'Нет', 1 => 'Да']) ?>

	<?= $form->field($model, 'country')->textInput() ?>

    <?php
        echo $form->field($model, 'description')->widget(CKEditor::className(),[
            'editorOptions' => [
                'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                'inline' => false, //по умолчанию false
                'font_names' => 'Roboto;Arial;Georgia;Tahoma;Verdana;Comic Sans MS/FontComic;Courier New/FontCourier;Times New Roman/FontTimes',
            ],
        ]);
    ?>

    <?= $form->field($model, 'articule')->textInput(['maxlength' => true]) ?>

    <?php
        // Usage with ActiveForm and model
        echo $form->field($model, 'gallery[]')->widget(FileInput::classname(), [
            'options' => ['multiple' => true, 'accept' => 'image/*'],
            'pluginOptions' => [
	            'previewFileType' => 'image',
                'showCaption' => false,
                'showRemove' => false,
                'showUpload' => false,
                'browseClass' => 'btn btn-primary btn-block',
                'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                'browseLabel' =>  'Выбрать',
            ],
        ]);
    ?>


    <label style="margin-bottom: 20px; margin-top: 30px;" class="control-label" for="products-images">Фото</label>
    <div id="images" style="min-height: 150px; margin-bottom: 20px;">
        <?php echo $this->render('images-update', ['model' => $model]); ?>
    </div>

    <?= $form->field($model, 'sort')->textInput() ?>

	<?= $form->field($model, 'type')->dropDownList(\common\models\Products::$products_type) ?>

	<?= $form->field($model, 'discount_price')->textInput(['style' => 'display: none;'])->label('Цена по скидке', ['style' => 'display: none;']) ?>

	<?= $form->field($model, 'weight')->textInput(['type' => 'number', 'style' => 'display: none;', 'prompt' => ''])->label('Вес продукта', ['style' => 'display: none;'])  ?>

	<?= $form->field($model, 'special')->dropDownList(['0' => 'Весовой', '1' => 'Без веса']) ?>

	<?= $form->field($model, 'can_be_in_scratbox')->dropDownList(['0' => 'Нет', '1' => 'Да']) ?>

    <div class='form-group field-attribute-parentId'>
		<?= Html::label('Категория', 'id_cat', ['class' => 'control-label']);?>
		<?= Html::dropdownList(
			'Products[id_cat]',
			$model->id_cat,
			Category::getCatTree($model->id_cat, true),
			['prompt' => 'Нет родительского (корень)', 'class' => 'form-control']
		);?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success',
            'onclick'=>
                "
                    if (($('#products-type').val() == 2) && (! $('#products-discount_price').val())) {
                        alert('Заполните поле \"Цена по скидке\"!');
                        return false;
                    }
                    
                    var count =  $(\"input:file\")[0].files.length;
                    var check_imgs = true;
                    $.ajax({
                         url: '/backend/web/products/check-download-images',
                         data: {countImgs: count, modelId: ".$model->id."},
                         type:'GET',         
                         async: false,                   
                         success: function(response) {
                            if (!response) {                                  
                                alert(\"Максимально допустимое число картинок для товара - ".\common\models\Products::MAX_PRODUCTS_IMAGES."!\");
                                check_imgs = false;
                            }
                         },
                         error: function(){
                             alert('Ошибка!');
                         },
                    });
                    return check_imgs;
                ",
            ]
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs("
    $('#products-type').change(function () {
        if ($(this).val() == 2) {
            $('.field-products-discount_price label').show();
            $('#products-discount_price').show();
        }
        else {
            $('.field-products-discount_price label').hide();
            $('#products-discount_price').hide();
        }
    });
    
    $(document).ready(function(){
        if ($('#products-type').val() == 2) {
            $('.field-products-discount_price label').show();
            $('#products-discount_price').show();
        }
    });
    
    $('#products-special').change(function () {
        if ($(this).val() == 0) {
            $('.field-products-weight label').show();
            $('#products-weight').show();
        }
        else {
            $('.field-products-weight label').hide();
            $('#products-weight').hide();
            $('#products-weight').val(1);
        }
    });
    
    $(document).ready(function(){
        if ($('#products-special').val() == 0) {
            $('.field-products-weight label').show();
            $('#products-weight').show();
        }
    });
", \yii\web\View::POS_READY);
?>
