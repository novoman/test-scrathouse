<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\assets\AppAsset;

AppAsset::register($this);
/* @var $this yii\web\View */
/* @var $model common\models\Products */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-view container">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот товар?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'price',
            'price_per_kg',
	        [
		        'attribute' => 'presence',
		        'filter'=>array(1 => 'Да', 0 => 'Нет'),
		        'value' => function ($data) {
			        return !$data->presence ? '<span class="text-danger">Нет</span>' : '<span class="text-success">Да</span>';
		        },
		        'format' => 'html',
	        ],
            'country',
            'description:html',
            'articule',
            [
	            'attribute' => 'image',
                'value' => "<img src='".($model->getImage()->id ? '' : '/0').$model->getImage()->getUrl('x100')."'>",
                'format' => 'html',
            ],
	        [
		        'attribute' => 'gallery',
		        'value' => function ($data) {
                    $temp = '';
                    if ($data->getImage()->id) {
	                    foreach ($data->getImages() as $img) {
		                    if ($img->isMain) {
			                    continue;
		                    }
		                    $temp .= "<img src='" . ( $img->id ? '' : '/0' ) . $img->getUrl( 'x100' ) . "'>";
	                    }
                    }
			        return $temp;
		        },

		        'format' => 'html',
	        ],
            'hits',
            'sort',
	        [
		        'attribute' => 'id_cat',
		        'value' => function ($data) {
			        return $data->category->name;
		        },
	        ],
	        [
		        'attribute' => 'type',
		        'value' => function ($data) {
			        return \common\models\Products::$products_type[$data->type];
		        },
		        'format' => 'html',
	        ],
            'discount_price',
	        'weight',
	        [
		        'attribute' => 'special',
		        'value' => function ($data) {
			        return $data->special ? 'Без веса' : 'Весовой';
		        },
		        'format' => 'html',
	        ],
	        [
		        'attribute' => 'can_be_in_scratbox',
		        'value' => function ($data) {
			        return $data->can_be_in_scratbox ? 'Да' : 'Нет';
		        },
		        'format' => 'html',
	        ],
            'slug',
        ],
    ]) ?>

</div>
