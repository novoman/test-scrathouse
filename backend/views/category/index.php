<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

use common\models\Category;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel common\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Категории');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать Категорию'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'options' => ['width' => '70px']
            ],
            'name',
            [
                'attribute' => 'tree',
                'label' => 'Корень',
                'filter' => Category::find()->roots()->select('name, id')->indexBy('id')->column(),
                'value' => function ($model) {
                    if ( ! $model->isRoot())
                        return $model->parents()->one()->name;

                    return 'Нет корня';
                }
            ],
            //'parent.name',
            // 'lft',
            // 'rgt',
            // 'depth',
            //'position',
            // 'created_at',
            // 'updated_at',

	        [
		        'class' => 'yii\grid\ActionColumn',
		        'options' => ['width' => '70px']
	        ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
