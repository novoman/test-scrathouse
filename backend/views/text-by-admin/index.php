<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Изменяемые страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="text-by-admin-index">

    <button style="margin-bottom: 30px; margin-top: 20px;">
		<?= Html::a(Yii::t('app', 'Скачать руководство администратора'), ['site/download-admin-guide-file'],
			['style' => 'text-decoration: none; padding: 30px; color: #000;']) ?>
    </button>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать элемент Изменяемой страницы', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'page',
            'type',
            'text:html',
            'created_at:datetime',
            'updated_at:datetime',

	        [
		        'class' => 'yii\grid\ActionColumn',
		        'options' => ['width' => '70px']
	        ],
        ],
    ]); ?>
</div>
