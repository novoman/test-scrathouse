<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model backend\models\TextByAdmin */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="text-by-admin-form">

    <?php $form = ActiveForm::begin(); ?>

	<?php
	$page_array = array();
	$type_array = array();

	foreach (\backend\models\TextByAdmin::$editable_pages as $page => $types) {
		$page_array[$page] = $page;
		foreach ($types as $type) {
			$type_array[$type] = $type;
		}
	}
	?>
	<?= $form->field($model, 'page')->dropDownList($page_array)?>

	<?= $form->field($model, 'type')->dropDownList($type_array)?>

	<?php
	echo $form->field($model, 'text')->widget(CKEditor::className(),[
		'editorOptions' => [
			'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
			'inline' => false, //по умолчанию false
			'font_names' => 'Roboto;Arial;Georgia;Tahoma;Verdana;Comic Sans MS/FontComic;Courier New/FontCourier;Times New Roman/FontTimes',
		],
	]);
	?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
