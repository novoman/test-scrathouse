<?php

use yii\helpers\Html;
use backend\assets\AppAsset;

AppAsset::register($this);


/* @var $this yii\web\View */
/* @var $model backend\models\TextByAdmin */

$this->title = 'Создать элемент Изменяемой страницы';
$this->params['breadcrumbs'][] = ['label' => 'Изменяемые страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="text-by-admin-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
