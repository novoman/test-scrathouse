<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model backend\models\TextByAdmin */

$this->title = $model->page.' '.$model->type;
$this->params['breadcrumbs'][] = ['label' => 'Изменяемые страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="text-by-admin-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'page',
            'type',
            'text:html',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
