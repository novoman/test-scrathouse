<?php

use yii\helpers\Html;
use backend\assets\AppAsset;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model backend\models\TextByAdmin */

$this->title = 'Обновить элемент Изменяемой страницы: ' . $model->page.' '.$model->type;
$this->params['breadcrumbs'][] = ['label' => 'Изменяемые страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->page.' '.$model->type, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="text-by-admin-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
