<?php

use yii\helpers\Html;
use backend\assets\AppAsset;

AppAsset::register($this);


/* @var $this yii\web\View */
/* @var $model common\models\PaymentsType */

$this->title = 'Создать Способ оплаты';
$this->params['breadcrumbs'][] = ['label' => 'Способы оплаты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
