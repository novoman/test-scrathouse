<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\assets\AppAsset;
use common\models\Products;
use common\models\Order;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот заказ?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_at:datetime',
            'updated_at:datetime',
            'name',
            'email:email',
            'telephone',
            'address',
            'comment:ntext',
	        'sum',
	        'delivery_date:date',
	        'delivery_time',

	        [
		        'attribute' => 'delivery_type',
		        'value' => function ($data) {
			        return \common\models\Order::getDeliveryTypeArray()[$data->delivery_type]['name'];
		        },
		        'format' => 'html',
	        ],
            'delivery_cost',
            'bonus_points',
            'bonus_points_to_get',
            'overall_sum_to_get',
	        [
		        'attribute' => 'status',
		        'value' => function ($data) {
                    switch ($data->status) {
                        case Order::$status_array['STATUS_NEW']:
                            return 'Новый';
                        case Order::$status_array['STATUS_PAID']:
                            return 'Оплачен';
                        case Order::$status_array['STATUS_SENT']:
                            return 'Отправлен';
                        case Order::$status_array['STATUS_DELIVERED']:
                            return 'Доставлен';
	                    case Order::$status_array['STATUS_CANCELED']:
		                    return 'Отменен';
                        default:
                            return 'Новый';
                    }
		        },
		        'format' => 'html',
	        ],
	        [
		        'attribute' => 'is_jur_person',
		        'value' => function ($data) {
			        return !$data->is_jur_person ? 'Физическое лицо' : 'Юридическое лицо';
		        },
		        'format' => 'html',
	        ],
            'jur_name',
            'jur_address',
            'bank',
            'checking_account',
            'bank_code',
            'unp',
            'director',
            'based',
	        [
		        'attribute' => 'id_pay_type',
		        'value' => function ($data) {
			        if ($data->id_pay_type) {
				        return \common\models\PaymentsType::findOne(['id' => $data->id_pay_type])->name;
			        }
			        return 'Не выбран';
		        },
	        ],
	        [
		        'attribute' => 'id_user',
		        'value' => function ($data) {
			        if ($data->id_user) {
			            $user = \common\models\User::findOne(['id' => $data->id_user]);
				        return 'ID: '.$user->id.', '.$user->first_name.' '.$user->last_name;
			        }
			        return 'Не выбран';
		        },
	        ],
	        [
		        'attribute' => 'id_promo',
		        'value' => function ($data) {
			        if ($data->id_promo) {
				        return \backend\models\Promo::findOne(['id' => $data->id_promo])->name;
			        }
			        return 'Не выбран';
		        },
	        ],
        ],
    ]) ?>

    <hr>

    <?php $items = $model->orderItems; ?>
    <table style="width: 100%; border: 1px solid #ddd; border-collapse: collapse;">
        <thead>
        <tr style="background: #f9f9f9">
            <th style="padding: 8px; border: 1px solid #ddd;">Наименование</th>
            <th style="padding: 8px; border: 1px solid #ddd;">Вес</th>
            <th style="padding: 8px; border: 1px solid #ddd;">Цена</th>
            <th style="padding: 8px; border: 1px solid #ddd;">Сумма</th>
        </tr>
		<?php foreach ($items as $product): ?>
			<?php if ($product['is_scratbox'] == 1): ?>
                <tr>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $product['name'] ?></td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $product['weight'] * \frontend\models\Scratboxes::NUMBER_OF_PRODUCTS_IN_SCRATBOX * Yii::$app->params['min_scrat_box_weight'] ?> rp.</td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $product['price'] ?>р.</td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $product['sum'] ?>р.</td>
                </tr>
                <?php
                $options = \frontend\models\ScratboxesProducts::find()
                                                              ->select(['scratboxes_products.*', 'products.name as name', 'products.country as country'])
                                                              ->leftJoin('products','products.id = scratboxes_products.id_product')
                                                              ->where(['=','scratboxes_products.id_box', $product['id_scratbox']])
                                                              ->asArray()
                                                              ->all();
                $option_count = 1;
                ?>

                <?php foreach ($options as $option): ?>
                    <tr>
                        <td style="padding: 8px; border: 1px solid #ddd; text-align: right;">Отсек <?= $option_count ?></td>
                        <td style="padding: 8px; border: 1px solid #ddd;"><?= $option['country'] != '' ? $option['name'].' ('.$option['country'].')' : $option['name'] ?></td>
                        <td style="padding: 8px; border: 1px solid #ddd;"><?= $product['weight'] * $option['weight'] ?> rp.</td>
                        <td style="padding: 8px; border: 1px solid #ddd;"><?= $product['weight'] * \frontend\models\Scratboxes::getScratboxPrice() / \frontend\models\Scratboxes::NUMBER_OF_PRODUCTS_IN_SCRATBOX ?>р.</td>
                    </tr>
                    <?php $option_count++; ?>
                <?php endforeach; ?>
			<?php else: ?>
                <?php $country = Products::findOne($product['id_product'])->country;?>
                <tr>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $country != '' ? $product['name'].' ('.$country.')' : $product['name'] ?></td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $product['weight'] ?> <?= \common\models\Products::findOne($product['id_product'])->special ? '' : 'rp.'?></td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?=$product['price'] ?> р.</td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $product['sum'] ?>p.</td>
                </tr>
			<?php endif; ?>
		<?php endforeach;?>
        <tr>
            <td style="padding: 8px; border: 1px solid #ddd;" colspan="3">Итого:</td>
            <td style="padding: 8px; border: 1px solid #ddd;"><?=$model['sum'] ?>р.</td>
        </tr>
        </thead>
    </table>
</div>
