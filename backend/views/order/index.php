<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\assets\AppAsset;
use common\models\Order;

AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать Заказ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'rowOptions'=>function ($data){
            switch ($data->status) {
                case Order::$status_array['STATUS_NEW']:
                    return ['class'=> 'danger'];
                case Order::$status_array['STATUS_PAID']:
	                return ['class'=> 'warning'];
                case Order::$status_array['STATUS_SENT']:
	                return ['class'=> 'warning'];
                case Order::$status_array['STATUS_DELIVERED']:
	                return ['class'=> 'success'];
	            case Order::$status_array['STATUS_CANCELED']:
		            return ['class'=> 'success'];
                default:
	                return ['class'=> 'danger'];
            }
        },
        'columns' => [
	        [
		        'attribute' => 'id',
		        'options' => ['width' => '100px']
	        ],
	        [
		        'attribute' => 'status',
		        'value' => function ($data) {
			        switch ($data->status) {
				        case Order::$status_array['STATUS_NEW']:
					        return '<span class="text-danger">Новый</span>';
				        case Order::$status_array['STATUS_PAID']:
					        return '<span class="text-warning">Оплачен</span>';
				        case Order::$status_array['STATUS_SENT']:
					        return '<span class="text-warning">Отправлен</span>';
				        case Order::$status_array['STATUS_DELIVERED']:
					        return '<span class="text-success">Доставлен</span>';
				        case Order::$status_array['STATUS_CANCELED']:
					        return '<span class="text-success">Отменен</span>';
				        default:
					        return '<span class="text-danger">Новый</span>';
			        }
		        },
		        'format' => 'html',
	        ],
            //'created_at',
            //'updated_at',
            'name',
            'email:email',
            'telephone',
            'address',
            'comment:ntext',
            'delivery_date:date',
            'delivery_time',
	        [
		        'attribute' => 'id_pay_type',
		        'value' => function ($data) {
			        if ($data->id_pay_type) {
				        return \common\models\PaymentsType::findOne(['id' => $data->id_pay_type])->name;
			        }
			        return 'Не выбран';
		        },
	        ],
	        'sum',

            //'is_jur_person',
            //'jur_name',
            //'jur_address',
            //'bank',
            //'checking_account',
            //'bank_code',
            //'unp',
            //'director',
            //'based',
            //'id_pay_type',

	        [
		        'class' => 'yii\grid\ActionColumn',
		        'options' => ['width' => '70px']
	        ],
        ],
    ]); ?>
</div>
