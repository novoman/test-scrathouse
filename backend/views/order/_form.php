<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Order;

/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'status')->dropDownList([
		Order::$status_array['STATUS_NEW'] => 'Новый',
		Order::$status_array['STATUS_PAID'] => 'Оплачен',
		Order::$status_array['STATUS_SENT'] => 'Отправлен',
		Order::$status_array['STATUS_DELIVERED'] => 'Доставлен',
		Order::$status_array['STATUS_CANCELED'] => 'Отменен',
	]) ?>

    <?= $form->field($model, 'sum')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'delivery_date')->textInput(['maxlength' => true, 'value' => date('Y-m-d', $model->delivery_date)]) ?>

	<?= $form->field($model, 'delivery_time')->textInput(['maxlength' => true]) ?>

    <?php
        $delivery_type_array = array();
        foreach (\common\models\Order::getDeliveryTypeArray() as $key => $value) {
	        $delivery_type_array[$key] = $value['name'];
        }
    ?>

	<?= $form->field($model, 'delivery_type')->dropDownList($delivery_type_array) ?>

	<?= $form->field($model, 'delivery_cost')->textInput() ?>

	<?= $form->field($model, 'bonus_points')->textInput() ?>

	<?= $form->field($model, 'bonus_points_to_get')->textInput() ?>

	<?= $form->field($model, 'overall_sum_to_get')->textInput() ?>

    <?= $form->field($model, 'is_jur_person')->dropDownList([0 => 'Физическое лицо', 1 => 'Юридическое лицо']) ?>

    <?= $form->field($model, 'jur_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jur_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bank')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'checking_account')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bank_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unp')->textInput() ?>

    <?= $form->field($model, 'director')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'based')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'id_pay_type')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\PaymentsType::getPayTypes(), 'id', 'name'))?>

	<?= $form->field($model, 'id_user')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\User::findAllActiveUsers(),'id', 'name')) ?>

	<?= $form->field($model, 'id_promo')->dropDownList(\yii\helpers\ArrayHelper::map(\backend\models\Promo::findAllPromos(),'id', 'name')) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
