<?php

namespace backend\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadFile extends Model
{
	/**
	 * @var UploadedFile
	 */
	public $file;

	public function rules()
	{
		return [
			[['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'doc'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'file' => 'Руководство администратора',
		];
	}

	public function uploadFile()
	{
		if ($this->validate()) {
			$this->file->saveAs(\Yii::getAlias('@backend') .'/web/uploads/admin_guide_scrathouse.doc');
			return true;
		} else {
			return false;
		}
	}
}
