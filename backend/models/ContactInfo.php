<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "contact_info".
 *
 * @property int $id
 * @property string $location
 * @property string $field_name
 * @property string $value
 * @property int $created_at
 * @property int $updated_at
 */
class ContactInfo extends \yii\db\ActiveRecord
{
	public static $contact_info_fileds = [
		'popup' => [
			'tel1',
			'tel2',
			'tel3',
		],
		'footer' => [
			'name',
			'domain',
			'reg_number',
			'email',
			'tel',
			'skype',
			'fb',
			'tw',
			'vk',
			'in',
			'seo',
		],
		'product' => [
			'viber',
			'whatsapp',
			'skype',
			'tg',
		],
	];

	/**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact_info';
    }

	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
				'value' => function() { return date('U'); },
			],
		];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'integer'],
            [['location', 'field_name', 'value'], 'string', 'max' => 255],
	        [['location', 'field_name'], 'unique', 'targetAttribute' => ['location', 'field_name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'location' => 'Расположение',
            'field_name' => 'Название поля',
            'value' => 'Значение',
            'created_at' => 'Создано',
            'updated_at' => 'Изменено',
        ];
    }

    public static function getContactInfoArray()
    {
	    $query_array = ContactInfo::find()->select(['location', 'field_name', 'value'])->asArray()->all();
	    $contact_info = array();
	    foreach ($query_array as $item) {
		    $contact_info[$item['location']][$item['field_name']] = $item['value'];
	    }
	    return $contact_info;
    }
}
