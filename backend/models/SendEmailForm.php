<?php
namespace backend\models;

use yii\base\Model;
use common\models\User;

class SendEmailForm extends Model
{
	public $recipients;
	public $template;

	public static $templates_list = [
		'bonus-system-refresh' => 'Обновление бонусной системы с баллами 24.02.2020',
		'bonus-system-refresh-no-points' => 'Обновление бонусной системы без баллов 24.02.2020',
	];

	public static $email_subjects = [
		'bonus-system-refresh' => 'Кэшбэк до 15% с каждого заказа',
		'bonus-system-refresh-no-points' => 'Кэшбэк до 15% с каждого заказа',
	];

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'recipients' => 'Получатели',
			'template' => 'Шаблон',
		];
	}

	public static function findUsers()
	{
		$users = User::find()
		             ->select(['id', 'email', "CONCAT('ID: ',id, ',  ',email, ', ', first_name, ' ', last_name) name"])
		             ->where(['status' => User::STATUS_ACTIVE])
		             ->asArray()
		             ->all();

		return $users;
	}
}
