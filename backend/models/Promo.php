<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "promo".
 *
 * @property int $id
 * @property string $name
 * @property int $discount
 * @property int $date_from
 * @property int $date_to
 * @property string $person_type
 * @property string $discount_type
 * @property int $used
 * @property int $times_used
 * @property int $max_times_used
 */
class Promo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'promo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
	        [
		        'name', 'unique',
		        'message' => 'Такой промокод уже есть'
	        ],
            [['name', 'discount', 'date_from', 'date_to', 'person_type', 'discount_type', 'max_times_used'], 'required'],
            [['discount', 'person_type', 'discount_type', 'used', 'times_used', 'max_times_used'], 'integer'],
            [['name'], 'string', 'max' => 255],
	        [['times_used', 'max_times_used'], 'default', 'value' => 0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Промо-код',
            'discount' => 'Размер скидки',
            'date_from' => 'Дата начала действия',
            'date_to' => 'Дата окончания действия',
            'person_type' => 'Тип скидки',                  // 0 - персональная, 1 - для всех
            'discount_type' => 'Измерения скидки',          // 0 - %, 1 - руб
            'used' => 'Использован (не действителен)',      // 0 - нет, 1 - да
            'times_used' => 'Использована раз',
            'max_times_used' => 'Максимальное число использования',
        ];
    }

	public static function findAllPromos()
	{
		$promos = Promo::find()
		               ->select(['id', 'name'])
		               ->asArray()
		               ->all();
		array_unshift($promos, ['0' => '']);

		return $promos;
	}
}
