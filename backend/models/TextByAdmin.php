<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "text_by_admin".
 *
 * @property int $id
 * @property string $page
 * @property int $type
 * @property string $text
 * @property int $created_at
 * @property int $updated_at
 */
class TextByAdmin extends \yii\db\ActiveRecord
{
	public static $editable_pages = [
		'delivery-pay' => [
			'1',
			'2',
			'3',
			'4',
			'5',
		],
		'callback' => [
			'',
		],
		'wholesale' => [
			'',
		],
		'contact-offer' => [
			'',
		],
		'profile' => [
			'',
		],
		'scrat-box' => [
			'1',
			'2',
			'3',
		],
		'main-page' => [
			'1',
			'2',
		],
	];

	public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'text_by_admin';
    }

	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
				'value' => function() { return date('U'); },
			],
		];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['text', 'page'], 'string'],
	        [['page', 'type'], 'unique', 'targetAttribute' => ['page', 'type']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page' => 'Страница',
            'type' => 'Месторасположение текста (см. в руководстве админа)',
            'text' => 'Текст',
            'created_at' => 'Создано',
            'updated_at' => 'Изменено',
        ];
    }
}
