<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "admin_numbers".
 *
 * @property int $id
 * @property string $name
 * @property double $value
 * @property string $description
 * @property int $created_at
 * @property int $updated_at
 */
class AdminNumbers extends \yii\db\ActiveRecord
{
	public static $admin_numbers = [
		'scrat-box-price' => 'scrat-box-price',
		'one-point-cost' => 'one-point-cost',
		'min-sum-for-adv' => 'min-sum-for-adv',
		'courier' => 'courier',
		'post' => 'post',
		'pickup' => 'pickup',
		'max-percent-for-users-points' => 'max-percent-for-users-points',
		'order-percent-for-points' => 'order-percent-for-points',
		'scratbox-first-category-id' => 'scratbox-first-category-id',
		'scratbox-second-category-id' => 'scratbox-second-category-id',
	];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin_numbers';
    }

	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
				'value' => function() { return date('U'); },
			],
		];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value'], 'number'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название параметра',
            'value' => 'Значение',
            'description' => 'Описание',
            'created_at' => 'Создано',
            'updated_at' => 'Изменено',
        ];
    }
}
