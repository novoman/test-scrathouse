<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "shops".
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string $city
 * @property string $work_time
 * @property string $telephone
 * @property double $coordinante_X
 * @property double $coordinante_Y
 */
class Shops extends \yii\db\ActiveRecord
{
	// shops's image
	public $image;

	public function behaviors() {
		return [
			'image' => [
				'class' => 'rico\yii2images\behaviors\ImageBehave',
			]
		];
	}

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shops';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['coordinante_X', 'coordinante_Y'], 'number'],
            [['name'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 200],
            [['city'], 'string', 'max' => 50],
            [['work_time', 'telephone'], 'string', 'max' => 20],
	        [['image'], 'file', 'extensions' => 'png, jpg', 'maxFiles' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№ Магазина',
            'name' => 'Название магазина',
            'address' => 'Адрес',
            'city' => 'Город',
            'work_time' => 'Время работы',
            'telephone' => 'Телефон',
            'coordinante_X' => 'Координата  X',
            'coordinante_Y' => 'Координата  Y',
            'image' => 'Фото',
        ];
    }

	public function uploadImage() {
		if ($this->validate()) {
			foreach ($this->image as $file) {
				$path = Yii::getAlias('@frontend') .'/web/img/store/'.$file->baseName. '.'.$file->extension;
				$file->saveAs($path);
				$this->attachImage($path);
				@unlink($path);
			}
		}
		else {
			return false;
		}
	}
}
