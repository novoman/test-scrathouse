<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'language' => 'ru-RU',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
    'modules' => [
	    'yii2images' => [
		    'class' => 'rico\yii2images\Module',
		    //be sure, that permissions ok
		    //if you cant avoid permission errors you have to create "images" folder in web root manually and set 777 permissions
		    'imagesStorePath' => '@frontend/web/img/store', //path to origin images
		    'imagesCachePath' => '@frontend/web/img/cache', //path to resized copies
		    'graphicsLibrary' => 'GD', //but really its better to use 'Imagick'
		    'placeHolderPath' => '@frontend/web/img/store/no-img.jpg', // if you want to get placeholder when image not exists, string will be processed by Yii::getAlias
		    'imageCompressionQuality' => 100, // Optional. Default value is 85.
	    ],
    ],
];
