<?php
return [
	'adminEmail' => 'scrathouse@yandex.by',
	'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'min_product_weight' => 100,
    'min_scrat_box_weight' => 250,
];
