<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $telephone
 * @property integer $address
 * @property integer $client_type
 * @property integer $is_admin
 * @property integer $is_super_admin
 * @property integer $birthday
 * @property double $users_points
 * @property double $overall_sum
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
	const STATUS_WAIT = 5;
	const STATUS_BLOCKED = -1;

	public static $client_type = [
		'0' => 'Физическое лицо',
		'1' => 'Юридическое лицо',
	];

	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			TimestampBehavior::className(),
		];
	}

	/**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

	public function attributeLabels()
	{
		return [
			'id' => '№ Пользователя',
			'first_name' => 'Имя',
			'last_name' => 'Фамилия',
			'email' => 'Email',
			'telephone' => 'Телефон',
			'address' => 'Адрес',
			'client_type' => 'Тип клиента',
			'status' => 'Статус пользователя',
			'created_at' => 'Создан',
			'updated_at' => 'Изменен',
			'is_admin' => 'Администратор',
			'is_super_admin' => 'Супер Администратор',
			'birthday' => 'День рождения',
			'users_points' => 'Количество бонуснов',
			'overall_sum' => 'Общая сумма заказов'
		];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
	    return [
		    [['first_name', 'last_name', 'status'], 'required'],
		    [['first_name', 'last_name', 'telephone', 'address'], 'string', 'max' => 255],
		    [['users_points', 'overall_sum'], 'number'],
		    [['email'], 'email'],
		    ['status', 'in', 'range' => [self::STATUS_DELETED, self::STATUS_WAIT, self::STATUS_ACTIVE, self::STATUS_BLOCKED]],
	    ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString(16);
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public static function isAdmin($id)
    {
	    if (empty($id)) {
		    return false;
	    }
    	return User::findOne($id)->is_admin;
    }

	public static function isSuperAdmin($id)
	{
		if (empty($id)) {
			return false;
		}
		return User::findOne($id)->is_super_admin;
	}


	public function emailExist($email) {

    	if (empty($email)) {
			return true;
		}

		return User::find()->where(['=', 'email', $email])->exists();
	}

	public static function findAllActiveUsers()
	{
		$users = User::find()
		             ->select(['id', 'CONCAT(\'ID: \',id, \',  \', first_name, \' \', last_name) name'])
		             ->where(['status' => self::STATUS_ACTIVE])
		             ->asArray()
		             ->all();
		array_unshift($users, ['0' => '']);
		return $users;
	}

	public static function getClientsPoints($id_user)
	{
		return User::findOne($id_user)->users_points;
	}

	public static function getClientsDiscountPercent($id_user)
	{
		$sum = User::findOne($id_user)->overall_sum;
		$percent = BonusPercents::find()
								->where(['and', ['=', 'type', 0], ['>=', 'sum', $sum]])
		                        ->min('percent');

		return $percent ? $percent : 5;
	}

}
