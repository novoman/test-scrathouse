<?php

namespace common\models;

use common\models\PointsCategories;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "points".
 *
 * @property int $id
 * @property int $count
 * @property int $date_from
 * @property int $date_to
 * @property int $created_at
 * @property int $updated_at
 * @property int $points_category_id
 * @property int $user_id
 *
 * @property PointsCategories $pointsCategory
 * @property User $user
 * @property PointsHistories[] $pointsHistories
 */
class Points extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'points';
    }

	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at']
				],
				'value' => function() { return date('U'); },
			],
		];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['count', 'created_at', 'updated_at', 'points_category_id', 'user_id'], 'integer'],
	        [['date_from', 'date_to', 'count'], 'required'],
	        [['points_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => PointsCategories::className(), 'targetAttribute' => ['points_category_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'count' => 'Количество',
            'date_from' => 'Дата начала действия',
            'date_to' => 'Дата окончания действия',
            'created_at' => 'Добавлено',
            'updated_at' => 'Обновлено',
            'points_category_id' => 'Категория бонусов',
            'user_id' => 'Пользователь',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPointsCategory()
    {
        return $this->hasOne(PointsCategories::className(), ['id' => 'points_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPointsHistories()
    {
        return $this->hasMany(PointsHistories::className(), ['points_id' => 'id']);
    }

	public static function findAllPoints()
	{
		$points = Points::find()
		             ->select(['id', 'CONCAT(\'ID: \',id) name'])
		             ->asArray()
		             ->all();
		array_unshift($points, ['0' => '']);
		return $points;
	}

	public static function addPointsToUser($count, $user_id, $type = 'order', $order_id = 0)
	{
		$points = new Points();

		$points->count = $count;
		$points->points_category_id = PointsCategories::findOne(['lookup_code' => $type])->id;
		$points->user_id = $user_id;
		$points->date_from = date('U');
		$points->date_to = date('U', strtotime('+1 year'));

		if ($points->save(false)) {
			$points_histories = new PointsHistories();
			$points_histories->type = 1;    //начисление
			$points_histories->comment = 'Начислено за заказ № <a href="/backend/web/order/view?id='.$order_id.'">'.$order_id.'</a>';
			$points_histories->points_id = $points->id;
			if ($points_histories->save(false)) {
				return true;
			}
		}

		return false;
	}

	public static function subPointsFromUser($count, $user_id, $type = 'order', $order_id = 0)
	{
		$points = new Points();

		$points->count = $count;
		$points->points_category_id = PointsCategories::findOne(['lookup_code' => $type])->id;
		$points->user_id = $user_id;
		$points->date_from = date('U');
		$points->date_to = date('U', strtotime('+1 year'));

		if ($points->save(false)) {
			$points_histories = new PointsHistories();
			$points_histories->type = 0;    //списание
			$points_histories->comment = 'Списано за заказ № <a href="/backend/web/order/view?id='.$order_id.'">'.$order_id.'</a>';
			$points_histories->points_id = $points->id;
			if ($points_histories->save(false)) {
				return true;
			}
		}

		return false;
	}
}
