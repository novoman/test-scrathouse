<?php

namespace common\models;

use common\models\Points;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "points_categories".
 *
 * @property int $id
 * @property string $name
 * @property string $lookup_code
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Points[] $points
 */
class PointsCategories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'points_categories';
    }

	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at']
				],
				'value' => function() { return date('U'); },
			],
		];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'lookup_code'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name', 'lookup_code'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'lookup_code' => 'Код',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoints()
    {
        return $this->hasMany(Points::className(), ['points_category_id' => 'id']);
    }

	public static function getPointsCategories()
	{
		$points_categories = PointsCategories::find()
		                         ->select(['id', 'name'])
		                         ->asArray()
		                         ->all();
		if (empty($points_categories)) return array();

		return $points_categories;
	}
}
