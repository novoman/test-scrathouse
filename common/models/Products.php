<?php

namespace common\models;

use Yii;
use skeeks\yii2\slug\SlugBehavior;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string $name
 * @property double $price
 * @property string $description
 * @property string $articule
 * @property int $hits
 * @property int $sort
 * @property int $id_cat
 * @property string $slug
 * @property string $type
 * @property string $discount_price
 * @property string $price_per_kg
 * @property string $country
 * @property string $presence
 * @property string $weight
 * @property string $special
 * @property string $can_be_in_scratbox
 *
 * @property Category $category
 *
 */
class Products extends \yii\db\ActiveRecord
{
	public static $products_type = [
		0 => '-',
		1 => 'Новинка',
		2 => 'Скидка',
		3 => 'В архиве',
	];

	// product's images
	public $gallery;

	const MAX_PRODUCTS_IMAGES = 3;

	public static function getMinProductWeight($product_id)
	{
		if (!$product_id) {
			return Yii::$app->params['min_product_weight'];
		}

		$product = Products::findOne($product_id);
		return $product ? $product->weight : Yii::$app->params['min_product_weight'];
	}

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    public function behaviors() {
	    return [
		    'slug' => [
			    'class' => 'skeeks\yii2\slug\SlugBehavior',
			    'slugAttribute' => 'slug',                      //The attribute to be generated
			    'attribute' => 'name',                          //The attribute from which will be generated
			    // optional params
			    'maxLength' => 64,                              //Maximum length of attribute slug
			    'minLength' => 3,                               //Min length of attribute slug
			    'ensureUnique' => true,
			    'slugifyOptions' => [
				    'lowercase' => true,
				    'separator' => '-',
				    'trim' => true,
				    //'regexp' => '/([^A-Za-z0-9]|-)+/',
				    'rulesets' => ['russian'],
				    //@see all options https://github.com/cocur/slugify
			    ]
		    ],
		    'image' => [
			    'class' => 'rico\yii2images\behaviors\ImageBehave',
		    ]
	    ];
    }

	/**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'discount_price', 'price_per_kg'], 'number'],
            [['description', 'country'], 'string'],
            [['hits', 'sort', 'id_cat', 'type', 'presence', 'weight', 'special', 'can_be_in_scratbox'], 'integer'],
            [['name'], 'string', 'max' => 100],
	        [['name', 'price', 'description','articule', 'id_cat', 'weight', 'special'], 'required'],
            [['articule'], 'string', 'max' => 400],
	        [['gallery'], 'file', 'extensions' => 'png, jpg', 'maxFiles' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№ Продукта',
            'name' => 'Название',
            'price' => 'Цена',
            'description' => 'Описание',
            'articule' => 'Артикул',
	        'image' => 'Главное фото',
	        'gallery' => 'Галерея',
            'hits' => 'Количество просмотров',
            'sort' => 'Сортировка',
            'id_cat' => 'Категория',
            'type' => 'Тип',
            'discount_price' => 'Цена по скидке',
            'price_per_kg' => 'Цена за кг',
            'country' => 'Страна производства',
            'presence' => 'Наличие',
            'weight' => 'Вес продукта',
            'special' => 'Без веса',
            'can_be_in_scratbox' => 'Может добавляться в скрат-бокс',
        ];
    }

    public function getCategory()
    {
    	return $this->hasOne(Category::className(), ['id' => 'id_cat']);
    }

	public function uploadGallery() {
		if ($this->validate()) {
			foreach ($this->gallery as $file) {
				$path = Yii::getAlias('@frontend') .'/web/img/store/'.$file->baseName. '.'.$file->extension;
				$file->saveAs($path);
				$this->attachImage($path);
				@unlink($path);
			}
			return true;
		}
		else {
			return false;
		}
	}
}
