<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "points_histories".
 *
 * @property int $id
 * @property int $type
 * @property string $comment
 * @property int $created_at
 * @property int $updated_at
 * @property int $points_id
 *
 * @property Points $points
 */
class PointsHistories extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'points_histories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type', 'created_at', 'updated_at', 'points_id'], 'integer'],
            [['comment'], 'string'],
            [['points_id'], 'exist', 'skipOnError' => true, 'targetClass' => Points::className(), 'targetAttribute' => ['points_id' => 'id']],
        ];
    }

	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at']
				],
				'value' => function() { return date('U'); },
			],
		];
	}

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',    // 0 - списание, 1 - начисление
            'comment' => 'Комментарий',
            'created_at' => 'Добавлено',
            'updated_at' => 'Обновлено',
            'points_id' => 'Бонусные баллы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoints()
    {
        return $this->hasOne(Points::className(), ['id' => 'points_id']);
    }
}
