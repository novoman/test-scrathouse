<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "contact_phone_form".
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $verifyCode
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class ContactPhoneForm extends \yii\db\ActiveRecord
{
	public static $status_array = [
		'STATUS_NEW' => 0,
		'STATUS_PROCESSED' => 1,
	];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact_phone_form';
    }

	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
				'value' => function() { return date('U'); },
			],
		];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'phone', 'verifyCode'], 'string', 'max' => 255],
	        // name, phone are required
	        [['name', 'phone'], 'required'],
	        // verifyCode needs to be entered correctly
	        ['verifyCode', 'captcha'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone'=> 'Телефон',
            'verifyCode' => 'Подтверждающий код',
            'status' => 'Статус',
            'created_at' => 'Добавлен',
            'updated_at' => 'Обновлен',
        ];
    }

	/**
	 * Sends an email to the specified email address using the information collected by this model.
	 *
	 * @param string $email the target email address
	 * @return bool whether the email was sent
	 */
	public function sendEmail($email)
	{
		return Yii::$app->mailer
			->compose()
			->setTo($email)
			->setFrom([$email => $this->name])
			->setSubject('Обратный звонок')
			->setTextBody('Здравствуйте. Меня зовут '.$this->name.' Пожалуйста, перезвоните мне на номер:'.$this->phone)
			->send();
	}

	public static function getContactPhoneFormsCountByStatus($status)
	{
		return ContactPhoneForm::find()->where(['status' => ContactPhoneForm::$status_array[$status]])->count();
	}
}
