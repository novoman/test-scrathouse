<?php

namespace common\models;

use backend\models\AdminNumbers;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property double $sum
 * @property int $status
 * @property string $name
 * @property string $email
 * @property string $telephone
 * @property string $address
 * @property string $comment
 * @property int $is_jur_person
 * @property string $jur_name
 * @property string $jur_address
 * @property string $bank
 * @property string $checking_account
 * @property string $bank_code
 * @property int $unp
 * @property string $director
 * @property string $based
 * @property int $id_pay_type
 * @property int $delivery_date
 * @property int $delivery_time
 * @property int $delivery_type
 * @property int $delivery_cost
 * @property int $id_user
 * @property int $id_promo
 * @property int $unique_token
 * @property double $bonus_points
 * @property double $bonus_points_to_get
 * @property double $overall_sum_to_get
 *
 * @property PaymentsType $payType
 * @property OrderItems[] $orderItems
 */
class Order extends \yii\db\ActiveRecord
{
	public static $status_array = [
		'STATUS_NEW' => 0,
		'STATUS_PAID' => 1,
		'STATUS_SENT' => 2,
		'STATUS_DELIVERED' => 3,
		'STATUS_CANCELED' => 4,
	];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
				'value' => function() { return date('U'); },
			],
		];
	}

	/**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
	        [['name', 'email', 'telephone', 'address', 'delivery_type'], 'required'],
            [['is_jur_person', 'unp', 'id_pay_type', 'status', 'id_promo'], 'integer'],
            [['sum', 'delivery_cost', 'bonus_points', 'bonus_points_to_get', 'overall_sum_to_get'], 'number'],
            [['comment', 'delivery_time'], 'string'],
            [['email'], 'email'],
            [['name', 'email', 'telephone', 'address', 'jur_name', 'jur_address', 'bank', 'checking_account', 'bank_code', 'director', 'based', 'delivery_date'], 'string', 'max' => 255],
            [['id_pay_type'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentsType::className(), 'targetAttribute' => ['id_pay_type' => 'id']],
//            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
	        'id' => '№ Заказа',
	        'created_at' => 'Создан',
	        'updated_at' => 'Изменён',
	        'sum' => 'Сумма, руб',
	        'status' => 'Статус',
	        'name' => 'Имя',
	        'email' => 'Email',
	        'telephone' => 'Телефон',
	        'address' => 'Адрес',
	        'comment' => 'Комментарий',
            'is_jur_person' => 'Тип клиента',
            'jur_name' => 'Юридическое название',
            'jur_address' => 'Юридический адрес',
            'bank' => 'Банк',
            'checking_account' => 'Расчётный счёт',
            'bank_code' => 'Код банка',
            'unp' => 'УНП',
            'director' => 'Директор (Сотрудник)',
            'based' => 'На основании',
            'id_pay_type' => 'Способ оплаты',
	        'delivery_date' => 'Дата доставки',
	        'delivery_time' => 'Время доставки',
	        'delivery_type' => 'Способ доставки',
	        'delivery_cost' => 'Стоимость доставки, руб',
	        'id_user' => 'Пользователь',
	        'id_promo' => 'Промокод',
	        'bonus_points' => 'Оплачено бонусами клиента, рублей',
	        'bonus_points_to_get' => 'Начислено бонусов за заказ',
	        'overall_sum_to_get' => 'Сумма заказа для начисления в общую сумму клиента',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayType()
    {
        return $this->hasOne(PaymentsType::className(), ['id' => 'id_pay_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItems::className(), ['id_order' => 'id']);
    }

	// минимальная сумма для бесплатной доставки
	public static function getMinSumForAdv()
	{
		return AdminNumbers::findOne(['name' => 'min-sum-for-adv']) ? AdminNumbers::findOne(['name' => 'min-sum-for-adv'])->value : 0;
	}

	// минимальная сумма для применения промокода
	public static function getMinSumForPromo()
	{
		return AdminNumbers::findOne(['name' => 'min-sum-for-promo']) ? AdminNumbers::findOne(['name' => 'min-sum-for-promo'])->value : 0;
	}

	public static function getDeliveryTypeArray()
	{
		return [
			1 =>
				[
					'name' => 'Курьер',
					'cost' => AdminNumbers::findOne(['name' => 'courier']) ? AdminNumbers::findOne(['name' => 'courier'])->value : 0,
				],
			2 =>
				[
					'name' => 'Почта',
					'cost' => AdminNumbers::findOne(['name' => 'post']) ? AdminNumbers::findOne(['name' => 'post'])->value : 0,
				],
			3 =>
				[
					'name' => 'Самовывоз',
					'cost' => AdminNumbers::findOne(['name' => 'pickup']) ? AdminNumbers::findOne(['name' => 'pickup'])->value : 0,
				],
		];
	}

	public static function getOnePointCost()
	{
		return AdminNumbers::findOne(['name' => 'one-point-cost']) ? AdminNumbers::findOne(['name' => 'one-point-cost'])->value : 1;
	}

	public static function getOrderPercentForPoints($user_id)
	{
		return User::getClientsDiscountPercent($user_id);
	}

	public static function getOrderAdditionalPercentForPoints($sum)
	{
		$percent = BonusPercents::find()
		                        ->where(['and', ['=', 'type', 1], ['<=', 'sum', $sum]])
		                        ->max('percent');

		return $percent ? $percent : 0;
	}

	public static function getOrdersCountByStatus($status)
	{
		return Order::find()->where(['status' => Order::$status_array[$status]])->count();
	}

	public static function addClientsPoints($points, $sum, $user_id, $order_id = 0)
	{
		$user = User::findOne($user_id);

		if (!$user) return false;

		if (!Points::addPointsToUser($points, $user_id, 'order', $order_id)) {
			return false;
		}

		$user->users_points += $points;
		$user->overall_sum += $sum;

		return $user->save() ? true : false;
	}

	public static function subClientsPoints($points, $user_id, $order_id = 0)
	{
		$user = User::findOne($user_id);

		if (!$user) return false;

		if (!Points::subPointsFromUser($points, $user_id, 'order', $order_id)) {
			return false;
		}

		$user->users_points -= $points;

		return $user->save() ? true : false;
	}
}
