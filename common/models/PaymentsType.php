<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "payments_type".
 *
 * @property int $id
 * @property string $name
 *
 * @property Order[] $orders
 */
class PaymentsType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№ Способа оплаты',
            'name' => 'Способ оплаты',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['id_pay_type' => 'id']);
    }

    public static function getPayTypes()
    {
	    $pay_types = PaymentsType::find()
	                       ->select(['id', 'name'])
	                       ->asArray()
	                       ->all();
	    if (empty($pay_types)) return array();

	    return $pay_types;
    }
}
