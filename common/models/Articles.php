<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "articles".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $keywords
 * @property string $description
 * @property string $preview
 * @property string $text
 * @property int $created_at
 * @property int $updated_at
 */
class Articles extends \yii\db\ActiveRecord
{
	const ARTICLES_PER_PAGE = 5;

	public $image;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'articles';
    }

	public function behaviors() {
		return [
			'slug' => [
				'class' => 'skeeks\yii2\slug\SlugBehavior',
				'slugAttribute' => 'slug',                      //The attribute to be generated
				'attribute' => 'title',                          //The attribute from which will be generated
				// optional params
				'maxLength' => 64,                              //Maximum length of attribute slug
				'minLength' => 3,                               //Min length of attribute slug
				'ensureUnique' => true,
				'slugifyOptions' => [
					'lowercase' => true,
					'separator' => '-',
					'trim' => true,
					//'regexp' => '/([^A-Za-z0-9]|-)+/',
					'rulesets' => ['russian'],
					//@see all options https://github.com/cocur/slugify
				]
			],
			'image' => [
				'class' => 'rico\yii2images\behaviors\ImageBehave',
			],
			[
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
				'value' => function() { return date('U'); },
			],
		];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
	        [['title'], 'required', 'message' => 'Необходимо заполнить'],
	        [['title'], 'filter','filter'=>'\yii\helpers\HtmlPurifier::process'],
	        [['keywords', 'description', 'preview', 'text','slug'], 'string'],
	        [['created_at', 'updated_at'], 'integer'],
	        [['title', 'slug'], 'string', 'max' => 200, 'message' => 'Превышен лимит символов (200 символов)'],
	        [['slug'], 'unique'],
	        [['image'], 'file', 'extensions' => 'png, jpg', 'maxFiles' => 2],
        ];
    }

	/**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
	        'id' => 'ID',
	        'title' => 'Заголовок',
	        'slug' => 'Slug',
	        'keywords' => 'Ключевые слова',
	        'description' => 'Описание',
	        'preview' => 'Короткий текст',
	        'text' => 'Текст',
	        'image' => 'Фото',
	        'created_at' => 'Создана',
            'updated_at' => 'Обновлена',
        ];
    }

	public function uploadImage() {
		if ($this->validate()) {
			foreach ($this->image as $file) {
				$path = Yii::getAlias('@frontend') .'/web/img/store/'.$file->baseName. '.'.$file->extension;
				$file->saveAs($path);
				$this->attachImage($path);
				@unlink($path);
			}
			return true;
		}
		else {
			return false;
		}
	}

}
