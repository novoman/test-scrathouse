<?php

namespace common\models;

use backend\models\AdminNumbers;
use Yii;
use yii\behaviors\TimestampBehavior;
use skeeks\yii2\slug\SlugBehavior;
use creocoder\nestedsets\NestedSetsBehavior;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $tree
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $slug
 */
class Category extends \yii\db\ActiveRecord
{

	public static function getStartCategory()
	{
		return self::find()
	              ->orderBy(['id' => SORT_ASC])
	              ->limit(1)
	              ->one()->slug;
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'category';
	}

	public function behaviors() {
		return [
			TimeStampBehavior::className(),
			'tree' => [
				'class' => NestedSetsBehavior::className(),
				'treeAttribute' => 'tree',
				// 'leftAttribute' => 'lft',
				// 'rightAttribute' => 'rgt',
				// 'depthAttribute' => 'depth',
			],
			'slug' => [
				'class' => 'skeeks\yii2\slug\SlugBehavior',
				'slugAttribute' => 'slug',                      //The attribute to be generated
				'attribute' => 'name',                          //The attribute from which will be generated
				// optional params
				'maxLength' => 64,                              //Maximum length of attribute slug
				'minLength' => 3,                               //Min length of attribute slug
				'ensureUnique' => true,
				'slugifyOptions' => [
					'lowercase' => true,
					'separator' => '-',
					'trim' => true,
					//'regexp' => '/([^A-Za-z0-9]|-)+/',
					'rulesets' => ['russian'],
					//@see all options https://github.com/cocur/slugify
				]
			],
		];
	}

	public function transactions()
	{
		return [
			self::SCENARIO_DEFAULT => self::OP_ALL,
		];
	}

	public static function find()
	{
		return new CategoryQuery(get_called_class());
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name'], 'required'],
			[['position'], 'default', 'value' => 0],
			[['tree', 'lft', 'rgt', 'depth', 'position', 'created_at', 'updated_at'], 'integer'],
			[['name'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'         => Yii::t('app', 'ID'),
			'name'       => Yii::t('app', 'Категория'),
			'tree'       => Yii::t('app', 'ID корня'),
			'lft'        => Yii::t('app', 'Левая'),
			'rgt'        => Yii::t('app', 'Правая'),
			'depth'      => Yii::t('app', 'Уровень'),
			'position'   => Yii::t('app', 'Позиция'),
			'created_at' => Yii::t('app', 'Создана'),
			'updated_at' => Yii::t('app', 'Изменена'),
		];
	}

	// return slug if it exists & start category slug if doesn't
	public static function getCorrectSlug($slug)
	{
		$category = Category::findOne(['slug' => $slug]);

		return empty($category) ? Category::getStartCategory() : $category->slug ;
	}

	public static function getCategoryNameBySlug($slug)
	{
		$slug = Category::getCorrectSlug($slug);
		$category = Category::findOne(['slug' => $slug]);
		return $category ? $category->name : 'Товары';
	}

	/**
	 * Get parent's ID
	 * @return \yii\db\ActiveQuery
	 */
	public function getParentId()
	{
		$parent = $this->parent;
		return $parent ? $parent->id : $this->id;
	}

	public function getParentSlug()
	{
		$parent = $this->parent;
		return $parent ? $parent->slug : $this->slug;
	}

	/**
	 * Get parent's node
	 * @return \yii\db\ActiveQuery
	 */
	public function getParent()
	{
		return $this->parents(1)->one();
	}

	/**
	 * Get the node and its children
	 * @param  integer $node_id node's ID
	 * @param  boolean $navArrFlag whether return array for navbar
	 * @return array array of node
	 */
	public static function getTree($node_id = 1, $navArrFlag = false)
	{
		// don't include children and the node
		$children = [];

		if (!empty($node_id))
			$children = array_merge(
				self::findOne($node_id)->children()->column(),
				[$node_id]
			);

		$rows = self::find()->
		select('id, name, depth, slug')->
		where(['in', 'id', $children])->
		orderBy('tree, lft, position')->
		all();

		$return = [];
		foreach ($rows as $row)
			if ($navArrFlag) {
				$return[] = array('id' => $row->id, 'depth' => $row->depth, 'name' => $row->name, 'slug' => $row->slug);
			}
			else {
				$return[] = $row->id;
			}

		return $return;
	}

	// Get all nodes except the $node_id, $flag_all - to get all the categories
	public static function getCatTree($node_id = 1, $flag_all = false)
	{
		// don't include children and the node
		$children = [];

		if (!empty($node_id))
			$children = array_merge(
				self::findOne($node_id)->children()->column(),
				[$node_id]
			);

		if ($flag_all) {
			$rows = self::find()
			            ->select('id, name, depth, slug')
			            ->orderBy('tree, lft, position')
			            ->all();
		}
		else {
			$rows = self::find()
			            ->select('id, name, depth, slug')
			            ->where(['not in', 'id', $children])
			            ->orderBy('tree, lft, position')
			            ->all();
		}

		$return = [];
		foreach ($rows as $row)
			$return[$row->id] = str_repeat('-', $row->depth) . ' ' . $row->name;

		return $return;
	}


	/**
	 * Get the node and its parents
	 * @param  string $node_slug node's slug
	 * @return array array of node
	 */
	public static function getTreeOfParents($node_slug)
	{
		$return = [];
		if ($node_slug == '') return $return;

		$node_slug = Category::getCorrectSlug($node_slug);

		$category = Category::findOne(['slug' => $node_slug]);
		$parents = $category->parents()->all();

		foreach ($parents as $parent) {
			$return[$parent->slug] = $parent->name;
		}
		$return[$category->slug] = $category->name;
		return $return;
	}

	public static function getParentsIds($child_id)
	{
		$result_array = [];
		$category = Category::findOne($child_id);
		$parents = $category->parents()->all();
		foreach ($parents as $parent) array_push($result_array, $parent->id);
		array_push($result_array, $category->id);

		return $result_array;
	}
}
