<?php

namespace common\models;

use Yii;
use frontend\models\Scratboxes;

/**
 * This is the model class for table "order_items".
 *
 * @property int $id
 * @property string $name
 * @property double $price
 * @property int $weight
 * @property double $sum
 * @property int $id_order
 * @property int $id_product
 * @property int $is_scratbox
 * @property int $id_scratbox
 * @property int $special
 *
 * @property Order $order
 * @property Products $product
 * @property Scratboxes $scratbox
 */
class OrderItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'sum'], 'number'],
            [['weight', 'id_order', 'id_product', 'is_scratbox', 'id_scratbox', 'special'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['id_order'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['id_order' => 'id']],
            [['id_product'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['id_product' => 'id']],
            [['id_scratbox'], 'exist', 'skipOnError' => true, 'targetClass' => Scratboxes::className(), 'targetAttribute' => ['id_scratbox' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№ Продукта заказа',
            'name' => 'Название',
            'price' => 'Цена',
            'weight' => 'Вес',
            'sum' => 'Сумма',
            'id_order' => '№ Заказа',
            'id_product' => '№ Продукта',
            'is_scratbox' => 'Является ли скрат-боксом',
            'id_scratbox' => '№ Скрат-бокса',
	        'special' => 'Без веса'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'id_order']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'id_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScratbox()
    {
        return $this->hasOne(Scratboxes::className(), ['id' => 'id_scratbox']);
    }
}
