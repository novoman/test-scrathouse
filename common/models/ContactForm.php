<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "contact_form".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $subject
 * @property string $body
 * @property string $verifyCode
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class ContactForm extends \yii\db\ActiveRecord
{
	public static $status_array = [
		'STATUS_NEW' => 0,
		'STATUS_PROCESSED' => 1,
	];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact_form';
    }

	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
				'value' => function() { return date('U'); },
			],
		];
	}

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at'], 'integer'],
	        // name, email, subject and body are required
	        [['name', 'email', 'subject', 'body'], 'required'],
	        // email has to be a valid email address
	        ['email', 'email'],
	        // verifyCode needs to be entered correctly
	        ['verifyCode', 'captcha'],
        ];
    }

    /**
     * {@inheritdoc}
     */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Имя',
			'email' => 'Email',
			'subject' => 'Тема',
			'body' => 'Вопрос/предложение',
			'verifyCode' => 'Подтверждающий код',
			'status' => 'Статус',
			'created_at' => 'Добавлен',
			'updated_at' => 'Обновлен',
		];
	}

	/**
	 * Sends an email to the specified email address using the information collected by this model.
	 *
	 * @param string $email the target email address
	 * @return bool whether the email was sent
	 */
	public function sendEmail($email)
	{
		return Yii::$app->mailer
			->compose()
			->setTo($email)
			->setFrom([$email => $this->name])
			->setSubject($this->subject)
			->setTextBody("Здравстуйте. У меня есть вопрос/предложение: \n\n".$this->body."\n\nМой email для обратной связи: ".$this->email)
			->send();
	}

	public static function getContactFormsCountByStatus($status)
	{
		return ContactForm::find()->where(['status' => ContactForm::$status_array[$status]])->count();
	}
}
