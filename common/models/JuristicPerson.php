<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "juristic_person".
 *
 * @property int $id
 * @property string $jur_name
 * @property string $jur_address
 * @property string $bank
 * @property string $checking_account
 * @property string $bank_code
 * @property int $unp
 * @property string $director
 * @property string $based
 * @property int $id_user
 *
 * @property User $user
 */
class JuristicPerson extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'juristic_person';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user'], 'integer'],
	        [['unp'], 'integer'],
	        [['jur_name', 'jur_address', 'bank', 'director', 'based', 'checking_account', 'bank_code', 'unp'], 'required'],
            [['jur_name', 'bank', 'director', 'based'], 'string', 'max' => 100],
            [['jur_address'], 'string', 'max' => 200],
            [['checking_account', 'bank_code'], 'string', 'max' => 50],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№ Юр. лица',
            'jur_name' => 'Юридическое название',
            'jur_address' => 'Юридический адрес',
            'bank' => 'Банк',
            'checking_account' => 'Расчётный счёт',
            'bank_code' => 'Код банка',
            'unp' => 'УНП',
            'director' => 'Директор (Сотрудник)',
            'based' => 'На основании',
            'id_user' => '№ пользователя',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }
}
