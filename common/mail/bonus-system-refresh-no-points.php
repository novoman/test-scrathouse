<div style="max-width:600px;margin:0 auto;font-size:16px;line-height:24px">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                <tr>
                                    <td style="padding-top:30px;background-color: #d1c1b3;padding-bottom:30px">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color: #5d5b5a;">
                                            <tbody>
                                            <tr>
                                                <td align="center" style="padding-top:0;padding-bottom:20px">
                                                    <a href="https://scrathouse.by/" target="_blank">
                                                        <img src="https://scrathouse.by/img/Logo.png" alt="Scrathouse" class="CToWUd" width="200" height="40" style="display:block;vertical-align:middle">
                                                    </a></td>
                                            </tr>
                                            <tr>
                                                <td style="font-family:Helvetica,Arial,sans-serif!important;font-size:16px;line-height:24px;word-break:break-all;padding-left:20px;background-color: #d1c1b3;padding-right:20px;padding-top:20px;padding-bottom:20px">
                                                    <h3 style="margin-top:0;margin-bottom:0;font-family:'Montserrat',Helvetica,Arial,sans-serif!important;font-weight:700;font-size:20px;background-color: #d1c1b3;line-height:30px;color:#222">Ура!<br>
                                                        Программа лояльности Scrat House снова в строю!</h3></td>
                                            </tr>
                                            <tr>
                                                <td style="font-family:Helvetica,Arial,sans-serif!important;font-size:16px;line-height:24px;word-break:break-all;padding-left:20px;background-color: #d1c1b3;padding-right:20px;padding-top:20px">
                                                    Теперь с каждого заказа на ваш счёт приходит кэшбэк в виде бонусных баллов - скрэтублей.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-family:Helvetica,Arial,sans-serif!important;font-size:16px;line-height:24px;word-break:break-all;padding-left:20px;background-color: #d1c1b3;padding-right:20px;padding-top:20px">
                                                    Совершайте покупки - накапливайте скрэтубли - оплачивайте следующие покупки кэшбэком!
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-family:Helvetica,Arial,sans-serif!important;font-size:16px;line-height:24px;word-break:break-all;padding-left:20px;background-color: #d1c1b3;padding-right:20px;padding-top:20px">
                                                    Пора позволить себе больше за меньшие деньги!
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-family:Helvetica,Arial,sans-serif!important;font-size:16px;line-height:24px;word-break:break-all;padding-left:20px;background-color: #d1c1b3;padding-right:20px;padding-top:10px">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tbody>
                                                        <tr>
                                                            <td style="font-size:0;line-height:0">&nbsp;</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-family:Helvetica,Arial,sans-serif!important;font-size:16px;line-height:24px;word-break:break-all;padding-left:20px;background-color: #d1c1b3;padding-right:20px;padding-top:30px">
                                                    <table style="text-align:center" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <div style="text-align:center;margin:0 auto">
                                                                    <a style="margin-left: 30%;width:200px;background-color:#f2f2f2;border-radius:5px;color:#5d5b5a;box-shadow: 2px 2px 7px 0 rgba(0,0,0,0.3);white-space:nowrap;font-weight:bold;display:block;font-family:Helvetica,Arial,sans-serif;font-size:16px;line-height:36px;text-align:center;text-decoration:none" href="https://scrathouse.by/products" target="_blank">Хочу!</a></div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="color: #000;font-family:Helvetica,Arial,sans-serif!important;font-size:16px;line-height:24px;word-break:break-all;padding-left:20px;background-color: #d1c1b3;padding-right:20px;padding-top:30px">
                                                    <div style="padding-top:10px">Команда Scrathouse.</div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
