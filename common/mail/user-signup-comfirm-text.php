<?php

/* @var $user \common\models\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['site/signup-confirm', 'token' => $user->email_confirm_token]);
?>
	Здравствуйте, <?= $user->first_name ?> <?= $user->last_name ?>,

	Перейдите по ссылке ниже, чтобы подтвердить Ваш email:

	<?= $confirmLink ?>