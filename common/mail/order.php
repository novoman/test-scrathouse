<?php use frontend\models\Cart;

$items = $order->orderItems; ?>

<table style="width: 100%; border: 1px solid #ddd; border-collapse: collapse; text-align: center;">
    <thead>
        <tr style="background: #f9f9f9">
            <th style="padding: 8px; border: 1px solid #ddd;">Наименование</th>
            <th style="padding: 8px; border: 1px solid #ddd;">Вес</th>
            <th style="padding: 8px; border: 1px solid #ddd;">Цена</th>
            <th style="padding: 8px; border: 1px solid #ddd;">Сумма</th>
        </tr>

        <?php foreach ($items as $product): ?>
	        <?php if ($product['is_scratbox']): ?>
                <tr>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $product['name'] ?></td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $product['weight'] * \frontend\models\Scratboxes::NUMBER_OF_PRODUCTS_IN_SCRATBOX * Yii::$app->params['min_scrat_box_weight'] ?> rp.</td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $product['price'] ?>р.</td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $product['sum'] ?>р.</td>
                </tr>
                <?php
                    $options = \frontend\models\ScratboxesProducts::find()
                                                                  ->select(['scratboxes_products.*', 'products.name as name', 'products.country as country'])
                                                                  ->leftJoin('products','products.id = scratboxes_products.id_product')
                                                                  ->where(['=','scratboxes_products.id_box', $product['id_scratbox']])
                                                                  ->asArray()
                                                                  ->all();
                    $option_count = 1;
                ?>

                <?php foreach ($options as $option): ?>
                    <tr>
                        <td style="padding: 8px; border: 1px solid #ddd; text-align: right;">Отсек <?= $option_count ?></td>
                        <td style="padding: 8px; border: 1px solid #ddd;"><?= $option['country'] != '' ? $option['name'].' ('.$option['country'].')' : $option['name'] ?></td>
                        <td style="padding: 8px; border: 1px solid #ddd;"><?= $product['weight'] * $option['weight'] ?> rp.</td>
                        <td style="padding: 8px; border: 1px solid #ddd;"><?= $product['weight'] * \frontend\models\Scratboxes::getScratboxPrice() / \frontend\models\Scratboxes::NUMBER_OF_PRODUCTS_IN_SCRATBOX ?>р.</td>
                    </tr>
                    <?php $option_count++; ?>
                <?php endforeach; ?>
            <?php else: ?>
		        <?php $country = \common\models\Products::findOne($product['id_product'])->country;?>
                <tr>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?= $country != '' ? $product['name'].' ('.$country.')' : $product['name'] ?></td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?=$product['weight'] ?> <?= $product['special'] ? '' : 'rp.'?></td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?=$product['price'] ?> p.</td>
                    <td style="padding: 8px; border: 1px solid #ddd;"><?=$product['price'] * ($product['weight'] / \common\models\Products::getMinProductWeight($product['id_product'])) ?>р.</td>
                </tr>
	        <?php endif; ?>
        <?php endforeach; ?>
        <?php $promo = \backend\models\Promo::findOne($order->id_promo); ?>
        <tr>
            <td style="padding: 8px; border: 1px solid #ddd; text-align: left;" colspan="3">Итого<?= $promo ? ' (со скидкой по промокоду)' : '' ?>:</td>
            <td style="padding: 8px; border: 1px solid #ddd;"><?= $order->sum ?> р.</td>
        </tr>
    </thead>
</table>
