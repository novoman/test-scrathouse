<?php
use yii\helpers\Html;

/* @var $user \common\models\User */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['site/signup-confirm', 'token' => $user->email_confirm_token]);
?>
<div class="password-reset">
	<p>Здравствуйте, <?= $user->first_name ?> <?= $user->last_name ?>,</p>

	<p>Перейдите по ссылке ниже, чтобы подтвердить Ваш email:</p>

	<p><?= Html::a(Html::encode($confirmLink), $confirmLink) ?></p>
</div>