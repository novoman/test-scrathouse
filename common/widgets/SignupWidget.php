<?php
/**
 * Created by PhpStorm.
 * User: kirya
 * Date: 08.05.2018
 * Time: 6:19
 */

namespace common\widgets;

use frontend\models\SignupForm;
use yii\base\Widget;
use yii\helpers\Html;

class SignupWidget extends Widget
{
	public function init()
	{

	}

	public function run()
	{
		$model = new SignupForm();
		return $this->render('signup', [
			'model' => $model,
		]);
	}

}