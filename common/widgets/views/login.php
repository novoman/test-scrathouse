<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\authclient\widgets\AuthChoice;
?>



	<?php $form = ActiveForm::begin(['id' => 'login-form',
                                     'enableClientScript' => true,
                                     'enableAjaxValidation' => true,
                                     'action' => ['/site/login']
    ]); ?>
    <div class="title">Вход</div>
    <div class="soc">
        <ul>
	        <?php $authAuthChoice = AuthChoice::begin(['baseAuthUrl' => ['site/auth'], 'popupMode' => true, 'autoRender' => false]); ?>

		        <?php foreach ($authAuthChoice->getClients() as $client): ?>
                    <li><?= Html::a( '',
                            ['site/auth', 'authclient'=> $client->getName(), ],
                            ['class' => $client->getName()])
                    ?></li>
		        <?php endforeach; ?>
	        <?php AuthChoice::end(); ?>
        </ul>
    </div>
    <div class="or">или</div>

	<?= $form->field($model, 'email', ['enableAjaxValidation' => true])->textInput(array('placeholder' => 'Email', 'class' => 'form-input'))->label(false); ?>

    <?= $form->field($model, 'password', ['enableAjaxValidation' => true])->passwordInput(array('placeholder' => 'Пароль', 'class' => 'form-input'))->label(false); ?>

    <div class="help-password-tip">
        <p>Забыли пароль?<br><?= Html::a('Восстановить', ['site/request-password-reset']) ?></p>
    </div>


	<?= Html::submitButton('Войти', ['class'=>'my-send']) ?>

	<?php ActiveForm::end(); ?>