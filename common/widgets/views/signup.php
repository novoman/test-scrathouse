<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
    use yii\authclient\widgets\AuthChoice;
?>

	<?php $form = ActiveForm::begin(['id' => 'signup-form',
                                     'enableClientScript' => true,
                                     'enableAjaxValidation' => true,
                                     'action' => ['/site/signup']
        ]); ?>
        <div class="title">Регистрация</div>
        <div class="soc">
            <ul>
                <?php $authAuthChoice = AuthChoice::begin(['baseAuthUrl' => ['site/auth'], 'popupMode' => true, 'autoRender' => false]); ?>

                <?php foreach ($authAuthChoice->getClients() as $client): ?>
                    <li><?= Html::a( '',
                            ['site/auth', 'authclient'=> $client->getName(), ],
                            ['class' => $client->getName()])
                        ?></li>
                <?php endforeach; ?>
                <?php AuthChoice::end(); ?>
            </ul>
        </div>
        <div class="or">или</div>

        <?= $form->field($model, 'first_name')->textInput(array('placeholder' => 'Имя', 'class' => 'form-input'))->label(false); ?>
        <?= $form->field($model, 'last_name')->textInput(array('placeholder' => 'Фамилия', 'class' => 'form-input'))->label(false); ?>
        <?= $form->field($model, 'email', ['enableAjaxValidation' => true])->textInput(array('placeholder' => 'Email', 'class' => 'form-input'))->label(false); ?>
        <?= $form->field($model, 'password')->passwordInput(array('placeholder' => 'Пароль', 'class' => 'form-input'))->label(false) ?>
        <?= $form->field($model, 'password_confirm')->passwordInput(array('placeholder' => 'Повторите пароль', 'class' => 'form-input'))->label(false) ?>
        <?= Html::submitButton('Регистрация', ['class'=>'my-send']) ?>

	<?php ActiveForm::end(); ?>
