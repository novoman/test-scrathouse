<?php
/**
 * Created by PhpStorm.
 * User: kirya
 * Date: 08.05.2018
 * Time: 6:19
 */

namespace common\widgets;

use common\models\LoginForm;
use yii\base\Widget;
use yii\helpers\Html;

class LoginWidget extends Widget
{
	public function init()
	{

	}

	public function run()
	{
		$model = new LoginForm();
		return $this->render('login', [
			'model' => $model,
		]);
	}

}